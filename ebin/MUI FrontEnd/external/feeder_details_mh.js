var MEASURE_VAL = 0;
var REFILL_VAL = 0;
var MAX_CAPACITY = 0;
function handle_feeder_info(Content){
    AtrDiv = document.getElementById("atr_div");
    AtrDiv = remove_all_children(AtrDiv);
    if (document.getElementById("bio_div")!=undefined){
        document.getElementById("bio_div").remove();
    }
    var Attributes = Content.atr;
    var StaticDiv = document.createElement("div");
    StaticDiv.setAttribute("id","static_feeder_atrs");
    StaticDiv.setAttribute("class","three-col");
    StaticDiv = add_element_to_element(StaticDiv,"label",{},"House id:");
    StaticDiv = add_element_to_element(StaticDiv,"label",{},Attributes["House Id"].value);
    StaticDiv = add_element_to_element(StaticDiv,"button",{"onclick":"schedule_feeder_relocation()"},"Schedule feeder relocation");

    StaticDiv = add_element_to_element(StaticDiv,"label",{},"Current amount");
    StaticDiv = add_element_to_element(StaticDiv,"label",{},Attributes["Current amount"].value + " (g)");
    StaticDiv = add_element_to_element(StaticDiv,"div",{},"");

    StaticDiv = add_element_to_element(StaticDiv,"label",{},"Current product id");
    StaticDiv = add_element_to_element(StaticDiv,"label",{},Attributes["Current product id"].value);
    StaticDiv = add_element_to_element(StaticDiv,"div",{},"");

    StaticDiv = add_element_to_element(StaticDiv,"label",{},"Maximum capacity");
    StaticDiv = add_element_to_element(StaticDiv,"label",{},Attributes["Maximum capacity"].value + " (g)");
    StaticDiv = add_element_to_element(StaticDiv,"button",{"onclick":"change_feeder_max()"},"Edit");
    MAX_CAPACITY = Attributes["Maximum capacity"].value;

    StaticDiv = add_element_to_element(StaticDiv,"label",{},"Sensor id");
    StaticDiv = add_element_to_element(StaticDiv,"label",{},Attributes["Sensor id"].value);
    StaticDiv = add_element_to_element(StaticDiv,"div",{},"");

    AtrDiv.appendChild(StaticDiv);
    ManagementHeader = document.createElement("h3");
    ManagementHeader.innerHTML = "Management attributes";
    AtrDiv.appendChild(ManagementHeader);
    
    var ManagementDiv = document.createElement("div");
    ManagementDiv.setAttribute("id","management_feeder_atrs");
    ManagementDiv.setAttribute("class","four-col");
    //MEASURE FREQUENCY
    ManagementDiv = add_element_to_element(ManagementDiv,"label",{},"Measure frequency");
    var PerTypeDiv = document.createElement("div");
    PerTypeDiv = add_element_to_element(PerTypeDiv,"label",{},"Period type: ");
    var PerSelect = document.createElement("select");
    PerSelect.setAttribute("onchange","period_type_changed(this)");
    PerSelect.setAttribute("id","measure_frequency_per_type");
    var PerList = ["days","hours","minutes","times"];
    var MeasureType = Attributes["Measure frequency"].context;
    PerSelect = add_element_to_element(PerSelect,"option",{"value":MeasureType},MeasureType);
    for (e in PerList){
        if (PerList[e]!=MeasureType){
            PerSelect = add_element_to_element(PerSelect,"option",{"value":PerList[e]},PerList[e]);
        }
        
    }
    PerTypeDiv.appendChild(PerSelect);
    var PerValueDiv = document.createElement("div");
    
    MEASURE_VAL = Attributes["Measure frequency"].value;

    if (MeasureType=="times"){
        PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Times of day: ");
        PerValueDiv = add_element_to_element(PerValueDiv,"p",{"type":"text","id":"measure_time_p"},MEASURE_VAL.toString());
        PerValueDiv = add_element_to_element(PerValueDiv,"input",{"type":"time","id":"new_measure_time","step":"1"},"00:00:00");
        PerValueDiv = add_element_to_element(PerValueDiv,"button",{"onclick":"add_measure_time()"},"Add time");
    }else{
        PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Number of "+MeasureType+": ");
        PerValueDiv = add_element_to_element(PerValueDiv,"input",{"id":"measure_val","type":"text","value":(MEASURE_VAL).toString()},null);
    }
    PerValueDiv.setAttribute("id","measure_value_div");
    ManagementDiv.appendChild(PerTypeDiv);
    ManagementDiv.appendChild(PerValueDiv);
    ManagementDiv = add_element_to_element(ManagementDiv,"button",{"onclick":"commit_feeder_attribute('Measure frequency','Management','measure_frequency_per_type',MEASURE_VAL)"},"Commit changes");
    //REFILL INDICATOR
    ManagementDiv = add_element_to_element(ManagementDiv,"label",{},"Refill indicator");
    var PerTypeDiv = document.createElement("div");
    PerTypeDiv = add_element_to_element(PerTypeDiv,"label",{},"Indicator type: ");
    var PerSelect = document.createElement("select");
    PerSelect.setAttribute("onchange","indicator_type_changed(this)");
    PerSelect.setAttribute("id","indicator_type");
    var PerList = ["minimum amount","times"];
    var MeasureType = Attributes["Refill indicator"].context;
    PerSelect = add_element_to_element(PerSelect,"option",{"value":MeasureType},MeasureType);
    for (e in PerList){
        if (PerList[e]!=MeasureType){
            PerSelect = add_element_to_element(PerSelect,"option",{"value":PerList[e]},PerList[e]);
        }
        
    }
    PerTypeDiv.appendChild(PerSelect);
    var PerValueDiv = document.createElement("div");
    
    REFILL_VAL = Attributes["Refill indicator"].value;

    if (MeasureType=="times"){
        PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Times of day: ");
        PerValueDiv = add_element_to_element(PerValueDiv,"p",{"type":"text","id":"refill_time_p"},REFILL_VAL.toString());
        PerValueDiv = add_element_to_element(PerValueDiv,"input",{"type":"time","id":"new_refill_time","step":"1"},"00:00:00");
        PerValueDiv = add_element_to_element(PerValueDiv,"button",{"onclick":"add_refill_time()"},"Add time");
    }else{
        PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Minimum amount (g)");
        PerValueDiv = add_element_to_element(PerValueDiv,"input",{"id":"refill_val","type":"text","value":(REFILL_VAL).toString()},null);
    }
    PerValueDiv.setAttribute("id","refill_value_div");
    ManagementDiv.appendChild(PerTypeDiv);
    ManagementDiv.appendChild(PerValueDiv);
    ManagementDiv = add_element_to_element(ManagementDiv,"button",{"onclick":"commit_feeder_attribute('Refill indicator','Management','indicator_type',REFILL_VAL)"},"Commit changes");
    //FILL UP SPECS
    ManagementDiv = add_element_to_element(ManagementDiv,"label",{},"Fill up specs");
    var PerTypeDiv = document.createElement("div");
    PerTypeDiv = add_element_to_element(PerTypeDiv,"label",{},"Specification type: ");
    var PerSelect = document.createElement("select");
    PerSelect.setAttribute("id","fill_up_type");
    var PerList = ["amount (g)","up to (%)"];
    var MeasureType = Attributes["Fill up specs"].context;
    PerSelect = add_element_to_element(PerSelect,"option",{"value":MeasureType},MeasureType);
    for (e in PerList){
        if (PerList[e]!=MeasureType){
            PerSelect = add_element_to_element(PerSelect,"option",{"value":PerList[e]},PerList[e]);
        }
    }
    PerTypeDiv.appendChild(PerSelect);
    ManagementDiv.appendChild(PerTypeDiv);
    ManagementDiv = add_element_to_element(ManagementDiv,"input",{"id":"fill_up_value","type":"text","value":Attributes["Fill up specs"].value},null);
    ManagementDiv = add_element_to_element(ManagementDiv,"button",{"onclick":"commit_feeder_attribute('Fill up specs','Management','fill_up_type','fill_up_value')"},"Commit changes");
    //Throw away period
    ManagementDiv = add_element_to_element(ManagementDiv,"label",{},"Throw away period");
    var PerTypeDiv = document.createElement("div");
    PerTypeDiv = add_element_to_element(PerTypeDiv,"label",{},"Period type: ");
    var PerSelect = document.createElement("select");
    PerSelect.setAttribute("id","throw_away_type");
    var PerList = ["days","hours"];
    var MeasureType = Attributes["Throw away period"].context;
    PerSelect = add_element_to_element(PerSelect,"option",{"value":MeasureType},MeasureType);
    for (e in PerList){
        if (PerList[e]!=MeasureType){
            PerSelect = add_element_to_element(PerSelect,"option",{"value":PerList[e]},PerList[e]);
        }
        
    }
    PerTypeDiv.appendChild(PerSelect);
    ManagementDiv.appendChild(PerTypeDiv);
    ManagementDiv = add_element_to_element(ManagementDiv,"input",{"id":"throw_away_value","type":"text","value":Attributes["Throw away period"].value},null);
    ManagementDiv = add_element_to_element(ManagementDiv,"button",{"onclick":"commit_feeder_attribute('Throw away period','Management','throw_away_type','throw_away_value')"},"Commit changes");
    AtrDiv.appendChild(ManagementDiv);    

}