function change_feeder_max(){
    var NewFeederMax = prompt("Enter the new maximum capacity in grams","");
    if (NewFeederMax!=null){
        var Max = Number(NewFeederMax);
        if (isNaN(Max)){
            alert("ERROR: Only numbers are allowed");
        }else{
            var Details = JSON.stringify({"id":"Maximum capacity","type":"Feeders","context":"g","value":Max});
            var Content = JSON.stringify({"address":RESOURCE_ADDRESS,"action":"add","component":"attributes","details":Details});
            var Msg = JSON.stringify({"Request":"edit_resource","Content":Content});
            sendMsg(Msg);
        }
    }
}

function period_type_changed(PerSelect){
    var PerValueDiv = document.getElementById("measure_value_div");
    while (PerValueDiv.firstChild) {
        PerValueDiv.removeChild(PerValueDiv.lastChild);
      }
    var MeasureType = PerSelect.value;
    if (MeasureType == "times"){
          PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Times of day: ");
          PerValueDiv = add_element_to_element(PerValueDiv,"p",{"type":"text","id":"measure_time_p"},"");
          MEASURE_VAL = [];
          PerValueDiv = add_element_to_element(PerValueDiv,"input",{"type":"time","id":"new_measure_time","step":"1"},"00:00:00");
          PerValueDiv = add_element_to_element(PerValueDiv,"button",{"onclick":"add_measure_time()"},"Add time");
    }else{
        MEASURE_VAL = 1;
        PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Number of "+MeasureType+": ");
        PerValueDiv = add_element_to_element(PerValueDiv,"input",{"id":"measure_val","type":"text","value":"1"},null);
    }
}

function indicator_type_changed(PerSelect){
    var PerValueDiv = document.getElementById("refill_value_div");
    while (PerValueDiv.firstChild) {
        PerValueDiv.removeChild(PerValueDiv.lastChild);
      }
    var MeasureType = PerSelect.value;
    if (MeasureType == "times"){
        REFILL_VAL = [];
        PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Times of day: ");
        PerValueDiv = add_element_to_element(PerValueDiv,"p",{"type":"text","id":"refill_time_p"},REFILL_VAL.toString());
        PerValueDiv = add_element_to_element(PerValueDiv,"input",{"type":"time","id":"new_refill_time","step":"1"},"00:00:00");
        PerValueDiv = add_element_to_element(PerValueDiv,"button",{"onclick":"add_refill_time()"},"Add time");
    }else{
        REFILL_VAL = 0;
        PerValueDiv = add_element_to_element(PerValueDiv,"label",{},"Minimum amount (g)");
        PerValueDiv = add_element_to_element(PerValueDiv,"input",{"id":"refill_val","type":"text","value":(REFILL_VAL).toString()},null);
    }
}

function add_refill_time(){
    var NewTime = document.getElementById("new_refill_time").value;
    if (NewTime!=""){
        if (REFILL_VAL.includes(NewTime)){
            alert("ERROR: This time is already added");
        }else{ 
            REFILL_VAL.push(NewTime);
            document.getElementById("refill_time_p").innerHTML = REFILL_VAL.toString();
        }
       
    }else{
        alert("ERROR: No time selected");
    }
}

function add_measure_time(){
    var NewTime = document.getElementById("new_measure_time").value;
    if (NewTime!=""){
        if (MEASURE_VAL.includes(NewTime)){
            alert("ERROR: This time is already added");
        }else{ 
            MEASURE_VAL.push(NewTime);
            document.getElementById("measure_time_p").innerHTML = MEASURE_VAL.toString();
        }
    }else{
        alert("ERROR: No time selected");
    }
}

function commit_feeder_attribute(AtrId,AtrType,ContextId,AtrValue){
    var Context = document.getElementById(ContextId).value;
    if (Context!="times"){
        if (AtrId == "Measure frequency"){
            var Value = Number(document.getElementById("measure_val").value);
        }else if (AtrId =="Refill indicator"){
            var Value = Number(document.getElementById("refill_val").value);
        }else{
            var Value = Number(document.getElementById(AtrValue).value);
        }
        
    }else{
        //console.log("times");
        
        var Value = AtrValue;
        //console.log(Value);
    }
    if (isNaN(Value)&&(Context!="times")){
        alert("ERROR: Only numbers are excepted in fill up specs and throw away period input boxes");
    }else{
        
        if (AtrId=='Fill up specs'){
            if (Context == ("up to (%)")){
                if (Value>100){
                    Value = 100;
                }
                if (Value<0){
                    Value = 0;
                }
            }
        }
        var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Value});
        var Content = JSON.stringify({"address":RESOURCE_ADDRESS,"action":"add","component":"attributes","details":Details});
        var Msg = JSON.stringify({"Request":"edit_resource","Content":Content});
        sendMsg(Msg);
    }
    
    
}