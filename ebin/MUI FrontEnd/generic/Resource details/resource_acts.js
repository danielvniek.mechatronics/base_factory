var S1DATA = undefined;
function new_resource_acts(ResourceType){
    S1DATA = undefined;
    try {
        var List = [""];
        var RList = window["Sheep_get_RAs"]();
        List = List.concat(RList);
    } catch (error) {
        console.log("No RAs for "+ResourceType+ ". Caught: ");
        console.log(error);
        var List = [];
    }
    
    if (List!=[]){
        var TypeSelect= document.createElement("select");
        TypeSelect.setAttribute("onchange","act_type_changed("+ResourceType+"_update_s1_div"+",this)");
        TypeSelect.setAttribute("id","sched_type");
        for (e in List){
            TypeSelect = add_element_to_element(TypeSelect,"option",{"value":List[e]},List[e]);   
        }
        var NewActDiv = document.getElementById("new_act_div");
        NewActDiv = remove_all_children(NewActDiv);
        NewActDiv.setAttribute("class","five-col");
        var NewActHeaders = ["Activity type","Date","Time","Data",""];
        for (e in NewActHeaders){
            NewActDiv = add_element_to_element(NewActDiv,"h5",{},NewActHeaders[e]);
        }
        NewActDiv.appendChild(TypeSelect);
        var date = new Date();
        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        var hh = String(date.getHours()).padStart(2, '0');
        var min = String(date.getMinutes()).padStart(2, '0'); 
        var ss = String(date.getSeconds()).padStart(2, '0');

        var currentDate = dd + '/' + mm + '/' + yyyy;
        var currentTime = hh+':'+min+':'+ss;
        NewActDiv = add_element_to_element(NewActDiv,"input",{"id":"sched_date_input","type":"date","value":currentDate},null);
        NewActDiv = add_element_to_element(NewActDiv,"input",{"id":"sched_time_input","type":"time","step":"1","value":currentTime},null);
        NewActDiv = add_element_to_element(NewActDiv,"div",{"id":"s1_div"},null);
        NewActDiv = add_element_to_element(NewActDiv,"button",{"onclick":"add_to_schedule()"},"Add");
    }else{
        alert("ERROR: No activity options to choose from for this resource");
    }
    
}

function act_type_changed(UpdateS1DivCall,TypeSelect){
    S1DATA = undefined;
    var Type = TypeSelect.value;
    UpdateS1DivCall(Type);
    
}

function add_to_schedule(){
    if (S1DATA!=undefined){
        if (S1DATA instanceof Function){
            S1DATA = S1DATA();
        }
        if (S1DATA!=null){//if function returns null it is an indication not to update schedule
            var Type = document.getElementById("sched_type").value;
            var SchedDate = document.getElementById("sched_date_input").value;
            var SchedTime = document.getElementById("sched_time_input").value;
            if ((SchedDate=="")||(SchedTime=="")){
                alert("Please select a valid date and time");
            }else{
                var Details = JSON.stringify({"type":Type,"date":SchedDate,"time":SchedTime,"data":S1DATA});
                update_schedule(Details);
            }    
        }      
    }else{
        alert("ERROR: activity data is undefined");
    }
}

function update_schedule(Details){
    var Content = JSON.stringify({"address":RESOURCE_ADDRESS,"action":"add","component":"schedule","details":Details});
    var Msg = JSON.stringify({"Request":"edit_resource","Content":Content});
    sendMsg(Msg);
}

function update_adr_schedule(ResourceAdr,Details){
    var Content = JSON.stringify({"address":ResourceAdr,"action":"add","component":"schedule","details":Details});
    var Msg = JSON.stringify({"Request":"edit_resource","Content":Content});
    sendMsg(Msg);
}