var RESOURCE_ADDRESS = "";
var PHOTOS = [];
function handle_resource_info(Content){
    RESOURCE_ADDRESS = Content.address;
    ResourceType = Content.type;
    RESOURCE_TYPE = ResourceType;
    var DetNav = document.getElementById("details_nav");
    DetNav = remove_all_children(DetNav);
    DetNav = add_element_to_element(DetNav,"button",{"onclick":"refresh_details()"},"Refresh");
    DetNav = add_element_to_element(DetNav,"button",{"onclick":"remove_resource()"},"Remove");
   //STANDARD
    if (Content.atr != undefined){
      add_shortcuts(Content);
      photos_and_atr_table(Content); 
      create_sbb_table(Content);
    }
    if(Content.sched!=undefined){ 
        var SchedDiv = document.getElementById("sched_tab_div");
        SchedDiv = remove_all_children(SchedDiv);
        SchedDiv = create_act_table(Content.sched,SchedDiv,"ra_tab","Resource activities (RA)",["ID","Type","Scheduled start time","Act data","remove"],["id","type","tsched","s1data"],"remove_sched_act",null,"(RA)");
        
        SchedDiv = create_act_table(Content.sched,SchedDiv,"spa_tab","Service provision activities (SPA)",["ID","Type","Scheduled start time","Act data"],["id","type","tsched","s1data"],null,null,"(SPA)");
        SchedDiv = add_element_to_element(SchedDiv,"button",{"onclick":"new_resource_acts('"+ResourceType+"')"},"Schedule new acts");
        SchedDiv = add_element_to_element(SchedDiv,"div",{"id":"new_act_div"},null);
   }
    if (Content.exe!=undefined){
        var ExeDiv = document.getElementById("exe_div");
        create_act_table(Content.exe,ExeDiv,"exe_tab",null,["ID","Type","Scheduled start time","Act data","Actual start time","Execution data"],["id","type","tsched","s1data","tstart","s2data"],null,null,null);
    }
    if (Content.bio!=undefined){
        var BioDiv = document.getElementById("bio_tab_div");
        BioDiv = remove_all_children(BioDiv);
        if (GET_BIO_OR_GET_RD=="bio"){
          create_act_table(Content.bio,BioDiv,"bio_tab",null,["ID","Type","Scheduled start time","Act data","Actual start time","Execution data","Actual end time","Post-execution data"],["id","type","tsched","s1data","tstart","s2data","tend","s3data"],null,["(RD)"],BIO_INCLUDE);
        }else{
          create_rd_graph_and_table(Content.bio)
        }         
    }     
        
}

function photos_and_atr_table(Content){
  var ResourceType = Content.type;
  
      var Attributes = Content.atr;
      if (Attributes["Photos"]!=undefined){
        var Photos = Attributes["Photos"].value;
        if (Photos.length>0){
          PHOTOS = Photos;
          add_to_photo_div(Photos);
        }else{
          PHOTOS = [];
        }
        delete Attributes["Photos"];
      }
      delete Attributes["BC"];
     
     
      var AtrDiv = document.getElementById("atr_div");
      if (AtrDiv!=undefined){
        AtrDiv = remove_all_children(AtrDiv);
      }
      NewTables = [];
      var TableTypes = ["Personal","Relational","Management","State"];
      var TypeList = [];
      for (t in TableTypes){
        //console.log(0);
        var CurType = TableTypes[t];
        var NewTable = document.createElement("table");
        NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
        NewTable.setAttribute("id",CurType+"_atr_tab");
        NewTable.setAttribute("class","tab");
        
        var Head = document.createElement("tr");
        var SubH = document.createElement("tr");
        var ResourceRow = document.createElement("tr");
        var CheckRow = document.createElement("tr");
        
        for (atr in Attributes){
          //console.log(1);
            var Atr = Attributes[atr];
            var Type = Atr.type;
            
            if ((!(TypeList.includes(Type)))&&(Type!="State variables")&&(Type!="Calculation variables")&&((Type==CurType)||(CurType=="State"))){
                var HeadSpan = 0;
                TypeList.push(Type);
                for (a in Attributes){
                  //console.log(2);
                    var OtherAtr = Attributes[a];
                    if (OtherAtr.type ==Type){
                        HeadSpan +=1;
                        
                        SubH = add_head(SubH,a,1); 
                        var Value = OtherAtr.value;
                        //console.log(toString(Value));
                        if (toString(Value).length>50){
                          var ViewBtn = document.createElement("button");
                          var AlertText = toString(Value);
                          ViewBtn.setAttribute("onclick","my_alert(this,"+AlertText+")");
                          ViewBtn.innerHTML = "View";
                          ResourceRow = add_cell(ResourceRow,ViewBtn,1);
                        }else{
                            ResourceRow = add_cell(ResourceRow,Value,1);
                        }                    
              
                        if ((Content.basic_bio)==undefined){//if base resource
                          if ((Type=="Personal")||(Type=="Management")){
                            var EditBtn = document.createElement("button");
                            EditBtn.innerHTML = "Edit";
                            if (typeof(Value)=="string"){
                              EditBtn.setAttribute("onclick","edit_"+ResourceType.replace(/\s/g, '_')+"_attribute('"+a+"','"+OtherAtr.type+"','"+OtherAtr.context+"','"+Value.toString()+"')");
                            }else if(typeof(Value)=="object"){
                                EditBtn.setAttribute("onclick","edit_"+ResourceType.replace(/\s/g, '_')+"_attribute('"+a+"','"+OtherAtr.type+"','"+OtherAtr.context+"',"+toString(Value)+")");
                              }  
                            else if((typeof(Value)=="boolean")||(typeof(Value)=="number")){
                              EditBtn.setAttribute("onclick","edit_"+ResourceType.replace(/\s/g, '_')+"_attribute('"+a+"','"+OtherAtr.type+"','"+OtherAtr.context+"',"+Value.toString()+")");
                            } 
                            
                            var LastElement = EditBtn;
                          
                          }else if((Type!="Relational")&&(Type!="BC")&&(Type!="State variables")){
                            var BioIncludeRadio = document.createElement("input");
                            BioIncludeRadio.setAttribute("type","checkbox");
                            var BioTypes = OtherAtr.context;
                            BioIncludeRadio.setAttribute("onclick","add_or_remove_checked_atrs_act_type("+JSON.stringify(BioTypes)+",this)");
                            var LastElement = BioIncludeRadio;
                          }else{
                            var LastElement = document.createElement("p");
                          }
                          CheckRow = add_cell(CheckRow,LastElement,1); 
                        }
                        delete Attributes[a];
                    }
                }  
                Head = add_head(Head,Type,HeadSpan); 
            } 
        }
        NewTable.appendChild(Head);
        NewTable.appendChild(SubH);
        NewTable.appendChild(ResourceRow);
        NewTable.appendChild(CheckRow);
        if (AtrDiv!=undefined){
          AtrDiv = add_element_to_element(AtrDiv,"h3",{},CurType);
          AtrDiv.appendChild(NewTable);
        }
        
        NewTables.push(NewTable);
      }
              
  return NewTables;
}

function create_sbb_table(Content){
  var NewTable = document.createElement("table");
  NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
  NewTable.setAttribute("id","sbb_tab");
  NewTable.setAttribute("class","tab");
  var Attributes = Content.atr;  
      delete Attributes["Photos"];
      var SubH = document.createElement("tr");
      var ResourceRow = document.createElement("tr");
      var CheckRow = document.createElement("tr");

              for (a in Attributes){
                  var OtherAtr = Attributes[a];
                  if ((OtherAtr.type =="State variables")){
                      SubH = add_head(SubH,a,1); 
                      var Value = OtherAtr.value;
                      //console.log(OtherAtr);
                      var Val = Value["Value"];
                      var ViewBtn = document.createElement("button");
                      var AlertText = toString(Value);
                      ViewBtn.setAttribute("onclick","my_alert(this,"+AlertText+")");
                      ViewBtn.innerHTML = "View";
                      var ValDiv = document.createElement("div");
                      ValDiv.setAttribute("class","two-col");
                      ValDiv = add_element_to_element(ValDiv,"p",{},Val);
                      ValDiv.appendChild(ViewBtn);
                        
                        ResourceRow = add_cell(ResourceRow,ValDiv,1);
                                          
            
                      if ((Content.basic_bio)==undefined){//if base resource
                          var BioIncludeRadio = document.createElement("input");
                          BioIncludeRadio.setAttribute("type","checkbox");
                          var BioTypes = OtherAtr.context;
                          BioIncludeRadio.setAttribute("onclick","add_or_remove_checked_atrs_act_type("+JSON.stringify(BioTypes)+",this)");
                          var LastElement = BioIncludeRadio;
                          CheckRow = add_cell(CheckRow,LastElement,1); 
                      }
                  }
                
          
      }
      NewTable.appendChild(SubH);
      NewTable.appendChild(ResourceRow);
      NewTable.appendChild(CheckRow);
  if (document.getElementById("sbb_tab")!=undefined){
    (document.getElementById("sbb_tab")).remove();
  }
  if (document.getElementById("sbb_div")!=undefined){
    document.getElementById("sbb_div").appendChild(NewTable);
  }
  return NewTable;
}

function create_act_table(Acts,Div,TableId,TableTitle,HeadList,KeyList,RemoveFunction,TypeExclude,TypeInclude){
        var NewTable = document.createElement("table");
        NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
        NewTable.setAttribute("id",TableId);
        NewTable.setAttribute("class","tab");
        var Head = document.createElement("tr");
        for (e in HeadList){
          Head = add_head(Head,HeadList[e],1,"this.innerHtml = 'bla';sortTable('"+TableId+"',"+e.toString()+",1)");
        }
        NewTable.appendChild(Head);
        for (key in Acts){
          var Act = Acts[key];
          //console.log(Act);
          if((((Act.type).includes(TypeInclude))||(TypeInclude==null)) && (!((Act.type).includes(TypeExclude)))){
            var ActRow = document.createElement("tr");
            for (k in KeyList){
              var Field = KeyList[k];
              var Val = Act[Field];
              if ((Field=="s2data")&&(typeof(Val)=="object")){
                  if (Val["Result"]!=undefined){
                    if (Val["Result"]=="Success"){
                        var Info = "'Result':'Success'";
                        var S2Div = createS2Div(Info,Act.id,toString(Val),"rgb(140, 231, 110)");
                        ActRow = add_cell(ActRow,S2Div,1);
                    }else if(Val["Result"]=="Failed"){
                        var Info = "'Result':'Failed' <br> 'Reason':"+Val["Reason"].toString();
                        var S2Div = createS2Div(Info,Act.id,toString(Val),"rgb(236, 113, 113)");
                        ActRow = add_cell(ActRow,S2Div,1);
                    }else if (Val["Result"]=="Cancelled"){
                        var Info = "'Result':'Cancelled' <br> 'Reason':"+Val["Reason"].toString();
                        var S2Div = createS2Div(Info,Act.id,toString(Val),"rgb(240, 164, 93)");
                        ActRow = add_cell(ActRow,S2Div,1);
                    }
                    else{
                      ActRow = add_s1_or_s2_cell(ActRow,Val);
                    }
                  }else{
                    add_s1_or_s2_cell(ActRow,Val);
                  }
              }else if(((Field=="s1data")||(Field=="s3data"))&&(typeof(Val)=="object")){
                add_s1_or_s2_cell(ActRow,Val);
              }else{
                ActRow = add_cell(ActRow,Val,1);
              }
            }
            if (RemoveFunction!=null){
              var RemBtn = document.createElement("button");
              RemBtn.setAttribute("onclick",RemoveFunction+"('"+Act.id+"')");
              RemBtn.innerHTML = "Remove";
              ActRow = add_cell(ActRow,RemBtn,1);
            } 
            NewTable.appendChild(ActRow);
          }
        }
        if (TableTitle!=null){
          Div = add_element_to_element(Div,"h4",{},TableTitle);
          Div.appendChild(NewTable);
        }else{
          Div.appendChild(NewTable);
        }
        return Div
}

function add_s1_or_s2_cell(ActRow,Val){
    if (toString(Val).length>50){
      var ViewBtn = document.createElement("button");
      var AlertText =toString(Val);  
      AlertText = JSON.stringify(Val);
      ViewBtn.setAttribute("onclick","my_alert(this,"+AlertText+")");
      ViewBtn.innerHTML = "View";
      ActRow = add_cell(ActRow,ViewBtn,1);
    }else{
      ActRow = add_cell(ActRow,Val,1);
    }
    
    return ActRow;
}
function my_alert(ThisBtn,AlertText){
  if (typeof(AlertText)=="string"){
    var T = AlertText;
  }else{
    T = JSON.stringify(AlertText)
  }
  if (ThisBtn.innerHTML=="View"){
    ThisBtn.innerHTML = "<strong>Click on this button to minimize content:</strong> <br>"+T;
  }else{
    ThisBtn.innerHTML = "View";
  }
  
}

                      

function createS2Div(Info,ActId,Val,Col){
  var Div = document.createElement("div");
  Div.setAttribute("class","two-col");
  Div = add_element_to_element(Div,"p",{},Info);
  Div = add_element_to_element(Div,"button",{"onclick":"my_alert(this,"+Val+")"},"View");
  Div.style.backgroundColor = Col;
  Div.style.width = "95%";
  return Div;
}



function create_rd_graph_and_table(Biography){
    var RDGraph = document.getElementById("rd_graph");
    var RDTableDiv = document.getElementById("rd_table_div")
    RDGraph = remove_all_children(RDGraph);
    RDTableDiv = remove_all_children(RDTableDiv);
    var NewDiv = document.createElement("div");
    NewDiv.setAttribute("id","rd_graph");
    document.getElementById("rd_div").appendChild(NewDiv);
    var xData = [];
    var yData = [];
    var Units = [];
    var labels = [];
    var colors = [];
    var lineTh = [];
    var TypeList = [];
    var qualityDatas = {};
    var AllText = [];
    for (bio in Biography){
        var Bio = Biography[bio];
        var Type = Bio.type;
        
        if ((!(TypeList.includes(Type)))&&(Type.includes("(RD)"))){
            //console.log("VALID RD TYPE")
            var x = [];
            var y = [];
            var text = [];
           
            var S1Data = Bio.s1data;
            var Unit = S1Data.Unit;
            var Color = getRandomColor();
            for (b in Biography){
                var OtherBio = Biography[b];
                if (OtherBio.type ==Type){
                    var S2Data = OtherBio.s2data;
                    var Val = S2Data["Value"];
                    
                    var Time = OtherBio.tstart;
                    TypeList.push(OtherBio.type);
                    if (typeof(Val)=='number'){
                      //console.log("Val is number");
                      x.push(Time);
                      y.push(Val);
                      var Accuracy = S1Data["Accuracy"];
                      var SensorDescr = S1Data["Sensor Description"];
                      var SensorId = S1Data["Sensor ID"];
                      var HoverText = "Accuracy: "+Accuracy.toString()+ " % <br>Sensor Description: "+SensorDescr+"<br>Sensor ID: "+SensorId+"<br><br>Post-execution data:<br>";
                      var s3data = OtherBio.s3data;
                      if (typeof(s3data) == 'object'){
                        HoverText = HoverText + object_keys_on_new_lines(s3data)
                      }
                      text.push(HoverText);
                    }else{
                      //console.log("Val is not number");
                      //console.log(Val);
                      //console.log(typeof(Val))
                    }
                    
                        var s3data = OtherBio.s3data;
                        if (typeof(s3data) == 'object'){
                          for (s3id in s3data){
                            var qdata = s3data[s3id];
                            var qval = qdata.Value;
                            var qunit = qdata.Unit;
                            if ((qval!=undefined)&&(typeof(qval)=="number")&&(qunit!=undefined)&&(typeof(qunit)=="string")){//s3data automatically plotted if has structure id:{"Value":val,"Unit":unit,...anything else}
                              var fullId = s3id + " ("+qunit+")";
                              if (qualityDatas[fullId] == undefined){
                                var NewQObj = {"x":[Time],"y":[qval],"color":Color,"act_type":Type,"text":JSON.stringify({s3id:qval})};
                                qualityDatas[fullId] = NewQObj;
                              }else{
                                
                                var OldObj = qualityDatas[fullId];
                                if (OldObj.act_type == Type){
                                  var OldX = OldObj.x;
                                  var OldY = OldObj.y;
                                  OldX.push(Time);
                                  OldY.push(qval);
                                  OldObj.x = OldX;
                                  OldObj.y = OldY;
                                  qualityDatas[fullId] = OldObj;
                                }
                                
                              }
                            }
                          }
                    }
                }
            }  
            if (x.length>0){
              xData.push(x);
              yData.push(y);
              Units.push(Unit);
              labels.push(Type);
              AllText.push(text);
              lineTh.push(4);
              colors.push(Color);
            }
        }
        
    }
    for (id in qualityDatas){
      //console.log(id);
      var Obj = qualityDatas[id];
      //console.log("Object plotted:");
      //console.log(Obj);
      //console.log(Obj);
      xData.push(Obj.x);
      yData.push(Obj.y);
      Units.push(Obj.act_type);
      AllText.push(Obj.text);
      labels.push(id);
      lineTh.push(1);
      colors.push(Obj.color);
    }
    var data = [];

    for ( var i = 0 ; i < xData.length ; i++ ) {
      var result = {
        x: xData[i],
        y: yData[i],
        type: 'scatter',
        mode: 'lines',
        text:AllText[i],
        name: labels[i] + ' ('+Units[i]+')',
        line: {
          color: colors[i],
          width: lineTh[i]
        }
      };
      data.push(result);
    }
    var RDGTDiv = document.getElementById("rd_graph_and_table");
    RDGTDiv = remove_all_children(RDGTDiv);
    RDGTDiv = add_element_to_element(RDGTDiv,"div",{"id":"rd_graph"},null);
    RDGTDiv = add_element_to_element(RDGTDiv,"div",{"id":"rd_table_div"},null);
    var layout = get_std_layout("Numeric resource data"); 
    var checkExist = setInterval(function() {
      if (document.getElementById("rd_table_div")!=null) {
        if (data.length>0){
          Plotly.newPlot('rd_graph', data, layout);
        //TABLE
        }
        TableDiv = document.getElementById("rd_table_div");
        create_act_table(Biography,TableDiv,"rd_table","All resource data acts (including numeric)",["ID","Type","Scheduled start time","Act data","Actual start time","Execution data","Actual end time","Post-execution data"],["id","type","tsched","s1data","tstart","s2data","tend","s3data"],null,null,"(RD)");
        
    
          clearInterval(checkExist);
      }
   }, 100);
  } 

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }



