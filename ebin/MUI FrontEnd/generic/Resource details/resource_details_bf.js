var RESOURCE_ID = "";
var RESOURCE_TYPE = "";
function get_resource_details(ResourceID){
    CheckedBios = [];
    change_page('resource_details.html',ResourceID+" ("+PAGE+")");
    RESOURCE_ID = ResourceID;
    var Components = JSON.stringify({"atr":"all","exe":"all"});
    var checkExist = setInterval(function() {
        if (document.getElementById("details_nav")!=null) {
            var EDiv = document.getElementById("details_nav");
            add_busy_to_div(EDiv);
            send_get_resource_details_msg(Components);
            clearInterval(checkExist);
        }
     }, 100);
    
}

var CheckedBios = [];
function add_or_remove_checked_atrs_act_type(BioTypes,CheckBox){
    if (CheckBox.checked){
        if (typeof(BioTypes)=="string"){
            CheckedBios.push(BioTypes);
        }else{
            CheckedBios = CheckedBios.concat(BioTypes)
        }
            
    }else{
        for (var i = 0; i<CheckedBios.length;i++){
            if (typeof(BioTypes)=="string"){
                if (CheckedBios[i]==BioTypes){
                    CheckedBios.splice(i,1);
                    break;
                }
            }else{
                if (BioTypes.includes(CheckedBios[i])){
                    CheckedBios.splice(i,1);
                }
            }
            
        }
    }
}

function get_atrs_rd(){
    GET_BIO_OR_GET_RD = "rd";
    if (CheckedBios.length>0){
        var DateSelected = document.getElementById("rd_date_input").value;
        if (DateSelected==""){
            var Bio = JSON.stringify({"types":CheckedBios});
        }else{
            var date1 = new Date(DateSelected);
            var today = new Date();
            var difference = today.getTime() - date1.getTime();
            var Days = Math.ceil(difference / (1000 * 3600 * 24));
            if (Days>0){
                var Bio = JSON.stringify({"types":CheckedBios,"days":Days});
            }
            else{
                var Bio = JSON.stringify({"types":CheckedBios,"days":0});
            }
        }
        var Components = JSON.stringify({"bio":Bio});
        var EDiv = document.getElementById("rd_graph_and_table");
        add_busy_to_div(EDiv);
        send_get_resource_details_msg(Components);
    }else{
        RDGraph = document.getElementById("rd_graph");
        RDTableDiv = document.getElementById("rd_table_div");
        RDGraph = remove_all_children(RDGraph);
        RDTableDiv = remove_all_children(RDTableDiv);
        alert("No attributes checked");
    } 
}
function get_all_rds(){
    GET_BIO_OR_GET_RD = "rd";
    var DateSelected = document.getElementById("rd_date_input").value;
    if (DateSelected==""){
        var Bio = "all";
    }else{
        var date1 = new Date(DateSelected);
        var today = new Date();
        var difference = today.getTime() - date1.getTime();
        var Days = Math.ceil(difference / (1000 * 3600 * 24));
        if (Days>0){
            var Bio = JSON.stringify({"days":Days});
        }
        else{
            var Bio = JSON.stringify({"days":0});
        }
    }
    var Components = JSON.stringify({"bio":Bio});
    var EDiv = document.getElementById("rd_graph_and_table");
    add_busy_to_div(EDiv);
    send_get_resource_details_msg(Components); 
}

function get_specific_rds(){
    GET_BIO_OR_GET_RD = "rd";
    var ActTypes = [];
    var ActType = null;
    var ActTypesStr = "";
    while(ActType!=""){
        var PromptMessage = "Enter the activity types of the resource data you want to find by clicking on ok after every type and then clicking on ok with the entry field empty when you are done. So far you are searching for these types: \n"+ActTypesStr;
        ActType = prompt(PromptMessage,"");
        if (ActType!=null){
            if (ActType==""){
                if (ActTypes.length>0){
                    var DateSelected = document.getElementById("rd_date_input").value;
                    if (DateSelected==""){
                        var Bio = JSON.stringify({"types":ActTypes});
                    }else{
                        var date1 = new Date(DateSelected);
                        var today = new Date();
                        var difference = today.getTime() - date1.getTime();
                        var Days = Math.ceil(difference / (1000 * 3600 * 24));
                        if (Days>0){
                            var Bio = JSON.stringify({"types":ActTypes,"days":Days});
                        }
                        else{
                            var Bio = JSON.stringify({"types":ActTypes,"days":0});
                        }
                    }
                    
                    var Components = JSON.stringify({"bio":Bio});
                    var EDiv = document.getElementById("rd_graph_and_table");
                    add_busy_to_div(EDiv);
                    send_get_resource_details_msg(Components); 
                }
            }else{
                if (!(ActType.includes("(RD)"))){
                    ActType = ActType + " (RD)";
                }
                ActTypes.push(ActType);
                ActTypesStr += ActType+"\n"; 
            }
        }else{
            break;
        }
    }
}
    


function refresh_details(){
    CheckedBios = [];
    change_page('resource_details.html',PAGE);
    var Components = JSON.stringify({"atr":"all","exe":"all"});
   send_get_resource_details_msg(Components);
}

function get_schedule(){
    var Days = prompt("How many days ahead do you want to see the schedule? Enter 'all' to see the entire schedule.");
    if (Days!=null){
        if(Days!=""){
            if (Days=="all"){
                var Components = JSON.stringify({"sched":"all"});
                var EDiv = document.getElementById("sched_tab_div");
                add_busy_to_div(EDiv);
                send_get_resource_details_msg(Components);
            }else{
                Days = parseInt(Days);
                if(isNaN(Days)){
                    alert("ERROR: Only whole numbers or 'all' is allowed");
                }else{
                    var Components = JSON.stringify({"sched":{"days":Days}});
                    var EDiv = document.getElementById("sched_tab_div");
                    add_busy_to_div(EDiv);
                    send_get_resource_details_msg(Components);
                }
            }
        }
    }    
}
var GET_BIO_OR_GET_RD = "none";
BIO_INCLUDE = null;
function get_entire_biography(){
    BIO_INCLUDE=null;
    get_bio();
}
function get_bio_that_includes(Includes){
    BIO_INCLUDE=Includes;
    get_bio();
}
function get_sched_refl_and_anal_errors(){
    BIO_INCLUDE=null;
    GET_BIO_OR_GET_RD = "bio";
    var Days = prompt("How many days back do you want to see the biography? Enter 'all' to see the entire biography.");
    if (Days!=null){
        if(Days!=""){
            if (Days=="all"){
                var Specs = JSON.stringify({"types":["Scheduling error","Reflection error","Analysis error"]});
                var Components = JSON.stringify({"bio":Specs});
                send_get_resource_details_msg(Components);
            }else{
                Days = parseInt(Days);
                if(isNaN(Days)){
                    alert("ERROR: Only whole numbers or 'all' is allowed");
                }else{
                    var Specs = JSON.stringify({"days":Days,"types":["Scheduling error","Reflection error","Analysis error"]});
                    var Components = JSON.stringify({"bio":Specs});
                    send_get_resource_details_msg(Components);
                }
            }
        }
    }    
}
function get_bio(){
    GET_BIO_OR_GET_RD = "bio";
    var Days = prompt("How many days back do you want to see the biography? Enter 'all' to see the entire biography.");
    if (Days!=null){
        if(Days!=""){
            if (Days=="all"){
                var Components = JSON.stringify({"bio":"all"});
                var EDiv = document.getElementById("bio_tab_div");
                add_busy_to_div(EDiv);
                send_get_resource_details_msg(Components);
            }else{
                Days = parseInt(Days);
                if(isNaN(Days)){
                    alert("ERROR: Only whole numbers or 'all' is allowed");
                }else{
                    var Components = JSON.stringify({"bio":{"days":Days}});
                    var EDiv = document.getElementById("bio_tab_div");
                    add_busy_to_div(EDiv);
                    send_get_resource_details_msg(Components);
                }
            }
        }
    }    
}

function send_get_resource_details_msg(Components){
    var Content = JSON.stringify({"id":RESOURCE_ID,"components":Components});
    var Msg = JSON.stringify({"Request":"get_resource_info","Content":Content});
    sendMsg(Msg);
}

function remove_resource(){
    if (confirm("Are you sure you want to remove this resource. Note that the resource's data will not be removed with it and you can access all removed resources' data from Resources->Removed resources")==true){
        var Msg = JSON.stringify({"Request":"remove_resource","Content":RESOURCE_ID});
        sendMsg(Msg);
    }
        
}

function edit_individual_plugins(){
    alert("ERROR: You do not have permission to edit plugins");
}