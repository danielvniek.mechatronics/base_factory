var SC_CASE_STUDIES = {};
function add_shortcuts(Content){
    if (RESOURCE_TYPE=="Groups"){
        var ShortCutDiv = document.getElementById("shortcut_div");
        ShortCutDiv = remove_all_children(ShortCutDiv);
        var Div = document.createElement("div");
        Div.setAttribute("class","two-col");
        Div = add_element_to_element(Div,"label",{},"Case studies");
        Div = add_element_to_element(Div,"label",{},"");
        var Selector = document.createElement("select");
        Selector.setAttribute("id","case_study_selector_cs");
        var Atr = Content.atr;
        SC_CASE_STUDIES = (Atr["Case studies"]).value;

        for (c in SC_CASE_STUDIES){
            Selector = add_element_to_element(Selector,"option",{"value":c},c);  
        }
        Div.appendChild(Selector);
        Div = add_element_to_element(Div,"button",{"onclick":"download_group_case_study_data()"},"Download case study data (excel)");
        ShortCutDiv.appendChild(Div);
    }
}

function download_group_case_study_data(){
    var CSName = (document.getElementById("case_study_selector_cs")).value;
    var CSDets = SC_CASE_STUDIES[CSName];
    var CSType = CSDets["Case study type"];
    if (CSType=="FCR"){
        REQUEST_REPLY_FUNCTION = fcr_data;
        var Details = {"cs_name":CSName};
        var Content = JSON.stringify({"id":RESOURCE_ID,"type":"Case study data","details":Details});
        var Msg = JSON.stringify({"Request":"request","Content":Content});
        sendMsg(Msg);
        //request case study data from group
    }else{
        alert("Excel data cannot be download for this type of case study")
    }
}

function fcr_data(Content){
    var CSName = Content["Case study name"];
    delete Content["Case study name"];
    var wb = XLSX.utils.book_new();
    var ResId = Content.id;
    delete Content["id"];
    var WSName = CSName+" "+ResId;
    wb.Props = {
            Title: WSName,
            Subject: "FCR Case studies",
            Author: "Daniel van Niekerk",
            CreatedDate: new Date()
    };
    
    ////console.log(Content);
    var AllEatenSinceW = {};
    var AllDailyIntakeSinceW= {};
    var AllDuration= {};
    var AllFreq= {};
    var AllTotalEaten= {};
    var AllOverallIntake= {};
    var AllGainSinceW= {};
    var AllTotalGain= {};
    var AllDailyGain= {};

    var AllFCRs = {}; //{Date:[{Sheep1:FCRVal,Sheep2:FCRVal...}]}
    var AllOFCRs = {};
    wb.SheetNames.push("Group Average");
    for (Sheep in Content){
        wb.SheetNames.push(Sheep);
        var ws_data = [[' ' , 'Date','Time','Eating (g)','Eating duration (s)','Weight (kg)',' ','Last 24h eaten (kg)','Eaten since last weighed (kg)','Average daily intake since last weighed (kg/day)','Average eating duration since last weighed (s)','Average eating frequency since last weighed (meals/day)','Total eaten (kg)','Overall average daily intake (kg/day)',' ','Gain since last weighed (kg)','Total gain (kg)','Daily gain since last weighed (kg/day)','FCR since last weighed (kg_eaten/kg_gained)','Overall FCR (kg_eaten/kg_gained)']];
        var TotalEaten = 0;
        var EatenSinceLastW = 0;
        var LastWBaseT = 0;
        var LastWeight = "pending";
        var Last24h = {};
        Dets = Content[Sheep];
        var CSStartT = Dets["CS start time"];
        var [StartDate,StartTime] = base_time_to_date_and_time(CSStartT);
        var CSEndT = Dets["CS end time"];
        var [EndDate,EndTime] = base_time_to_date_and_time(CSEndT);
        var StartW = Dets["Starting weight"];
        var StartWT = Dets["Starting weight time"];
        var FirstEatT = 0;
        var FirstMeal = 0;
        var NrMealsSinceW = 0;
        var DurationSinceLastW =0;
        if (Array.isArray(StartWT)){
            var WDate = "-";
            var WTime = "-";
            
        }else{
            var [WDate,WTime] = base_time_to_date_and_time(StartWT);
            LastWeight = StartW;
        }
        
        var NewRow = ['Starting weight',WDate,WTime,' ',' ',StartW,' ',' '];
        ws_data.push(NewRow);
        NewRow = ['Case study start',StartDate,StartTime,' ',' ',' ',' '];
        ws_data.push(NewRow);
        var Data = Dets["Data"];
        var Times = Object.keys(Data);
        Times = Times.map(Number);
        Times.sort(function(a, b) {
            return a - b;
        }); 
        var Intake24 = 0;
        for (t in Times){
            var T = Times[t];
            var key = toString(T);
            var [EntryDate,EntryTime] = base_time_to_date_and_time(T);
            var Entry = Data[key];
            for (Field in Entry){
                var Value = Entry[Field];
                
                var Weight = "";
                var FCR = "";
                var OFCR="";
                var Gain="";
                var TotalGain="";
                var DailyGain="";
                if (FirstEatT == 0){
                    DailyIntake = "";
                    ODailyIntake = "";
                    var Freq = "";
                }else{
                    ODailyIntake = ((TotalEaten-FirstMeal)/(T-FirstEatT))*3600*24;
                    if (LastWBaseT==0){
                        DailyIntake = ODailyIntake;
                        var Freq = ((NrMealsSinceW-1)/(T-FirstEatT))*3600*24;
                    }else{
                        var Freq = (NrMealsSinceW/(T-LastWBaseT))*3600*24,
                        DailyIntake = (EatenSinceLastW/(T-LastWBaseT))*3600*24;
                    }  
                }
                if (Field=="eating (g)"){//EATING
                    NrMealsSinceW++;
                    Eaten = Value["eating (g)"];
                    Duration = Value["duration"];
                    DurationSinceLastW=DurationSinceLastW+Duration;
                    var AvgDurSinceW=DurationSinceLastW/NrMealsSinceW;
                    TotalEaten = TotalEaten+Eaten/1000;
                    
                    EatenSinceLastW = EatenSinceLastW+Eaten/1000;
                    if (FirstEatT == 0){
                        FirstEatT = T;
                        FirstMeal = Eaten/1000;
                    }
                    Last24h[key] = Eaten;
                    Intake24 = 0;
                    for (k in Last24h){
                        var t24 = Number(k);
                        if ((T-t24)>=(3600*24)){
                            delete Last24h[k];
                        }else{
                            Intake24 = Last24h[k]/1000+Intake24;
                        }
                    }
                }else{//WEIGHT
                    Weight = Value;
                    Eaten="";
                    Duration = "";
                    
                    Intake24="";
                    if (Array.isArray(StartW)){
                        StartW = Value;

                    }else{
                        var Gain = Value-LastWeight;
                        if (Gain!=0){
                            var FCR = EatenSinceLastW/Gain;
                        }else{
                            FCR = "infinity";
                        }
                        var DailyGain = (Gain/(T-LastWBaseT))*3600*24;                      
                        var TotalGain = Value-StartW;
                        if (TotalGain!=0){
                            var OFCR = TotalEaten/TotalGain;       
                        }else{
                            OFCR = "infinity";
                        }
                    }    
                    LastWeight=Value;
                    LastWBaseT=T;
                    //Group averages on weigh days
                   
                    AllEatenSinceW = calc_all_fcrs(AllEatenSinceW,Sheep,EntryDate,EatenSinceLastW);
                    
                    AllDailyIntakeSinceW = calc_all_fcrs(AllDailyIntakeSinceW,Sheep,EntryDate,DailyIntake);
                    AllDuration = calc_all_fcrs(AllDuration,Sheep,EntryDate,AvgDurSinceW);
                    AllFreq = calc_all_fcrs(AllFreq,Sheep,EntryDate,Freq);
                    AllTotalEaten = calc_all_fcrs(AllTotalEaten,Sheep,EntryDate,TotalEaten);
                    AllOverallIntake = calc_all_fcrs(AllOverallIntake,Sheep,EntryDate,ODailyIntake);
                    AllGainSinceW = calc_all_fcrs(AllGainSinceW,Sheep,EntryDate,Gain);
                    AllTotalGain = calc_all_fcrs(AllTotalGain,Sheep,EntryDate,TotalGain);
                    AllDailyGain = calc_all_fcrs(AllDailyGain,Sheep,EntryDate,DailyGain);
                    AllFCRs = calc_all_fcrs(AllFCRs,Sheep,EntryDate,FCR);
                    AllOFCRs = calc_all_fcrs(AllOFCRs,Sheep,EntryDate,OFCR);
                }
                
                var NewRow = [' ',EntryDate,EntryTime,Eaten,Duration,Weight," ",Intake24,EatenSinceLastW,DailyIntake,AvgDurSinceW,Freq,TotalEaten,ODailyIntake," ",Gain,TotalGain,DailyGain,FCR,OFCR];
                if (!(Field=="eating (g)")){
                    EatenSinceLastW=0; 
                    NrMealsSinceW=0;
                    DurationSinceLastW=0;
                }
                ws_data.push(NewRow);

                
            }
            
            ////console.log(ws_data);
        }
        NewRow = ['Case study end',EndDate,EndTime,' ',' ',' ',' '];
        ws_data.push(NewRow);
        var ws = XLSX.utils.aoa_to_sheet(ws_data);
        var wscols = [
            {wch:15},
            {wch:12},
            {wch:9},
            {wch:9},
            {wch:18},
            {wch:11},
            {wch:6},
            {wch:17},
            {wch:26},
            {wch:43},
            {wch:50},
            {wch:51},
            {wch:14},
            {wch:33},
            {wch:6},
            {wch:25},
            {wch:13},
            {wch:40},
            {wch:38},
            {wch:31}
        ];
        
        ws['!cols'] = wscols;
        wb.Sheets[Sheep] = ws;
    }
    AllEatenSinceW = get_average(AllEatenSinceW);
    AllDailyIntakeSinceW = get_average(AllDailyIntakeSinceW);
    AllDuration = get_average(AllDuration);
    AllFreq = get_average(AllFreq);
    AllTotalEaten = get_average(AllTotalEaten);
    AllOverallIntake = get_average(AllOverallIntake);
    AllGainSinceW = get_average(AllGainSinceW);
    AllTotalGain = get_average(AllTotalGain);
    AllDailyGain = get_average(AllDailyGain);
    AllFCRs = get_average(AllFCRs);
    AllOFCRs = get_average(AllOFCRs);
    var ws_data = [['Date','Eaten since last weighed (kg)','Average daily intake since last weighed (kg/day)','Average eating duration since last weighed (s)','Average eating frequency since last weighed (meals/day)','Total eaten (kg)','Overall average daily intake (kg/day)',' ','Gain since last weighed (kg)','Total gain (kg)','Daily gain since last weighed (kg/day)','FCR since last weighed (kg_eaten/kg_gained)','Overall FCR (kg_eaten/kg_gained)']];
    for (a in AllFCRs){
        var NewRow = [a,AllEatenSinceW[a],AllDailyIntakeSinceW[a],AllDuration[a],AllFreq[a],AllTotalEaten[a],AllOverallIntake[a],'',AllGainSinceW[a],AllTotalGain[a],AllDailyGain[a],AllFCRs[a],AllOFCRs[a]];
        ws_data.push(NewRow);
    }
    var GroupSheet = XLSX.utils.aoa_to_sheet(ws_data);
    var wscols = [
        {wch:20},
        {wch:26},
        {wch:43},
        {wch:50},
        {wch:51},
        {wch:14},
        {wch:33},
        {wch:6},
        {wch:25},
        {wch:13},
        {wch:40},
        {wch:38},
        {wch:31}
    ];
    
    GroupSheet['!cols'] = wscols;
    wb.Sheets["Group Average"] = GroupSheet;
    try {
        var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
        function s2ab(s) {

                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
                
        }
        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), WSName+'.xlsx');
        
      }
      catch(err) {
        alert(ResId+" has no data yet for "+CSName);
      }
}

function calc_all_fcrs(All,Sheep,EntryDate,SheepVal){
    if (typeof(SheepVal)=="number"){
        var E = All[EntryDate];
        if (E==undefined){
            
            var E = {};
            E[Sheep] = SheepVal;
            All[EntryDate] = E;
        }else{
            E[Sheep] = SheepVal;
            All[EntryDate] = E;
        }
        
    }
    return All;
}

function get_average(AllFCRs){
    for (e in AllFCRs){
        var Entries = AllFCRs[e];
        var Total = 0;
        var n = 0
        for (i in Entries){
            var FCR = Entries[i];
            if (FCR!="infinity"){
                Total = Total + FCR;
                n = n+1;
            }
           
        }
        if (n>0){
            AllFCRs[e] = Total/n;
        }else{
            AllFCRs[e] = "infinity";
        }
       
    }
    return AllFCRs;
}