function edit_Sensors_attribute(AtrId,AtrType,Context,Value){
    alert("This attribute cannot be edited");
}
function edit_Sheep_attribute(AtrId,AtrType,Context,Value){
    var EditDiv = document.getElementById("atr_edit_div"),
    EditDiv = remove_all_children(EditDiv);
    //console.log(AtrId);
    if (AtrId == 'Date of birth'){
        EditDiv.setAttribute("class","two-col"),
        EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"input",{"id":"dob_input","type":"date"},null);
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit('"+AtrId+"','"+AtrType+"','"+Context+"','dob_input')"},"Commit")
        scroll_and_blink(EditDiv);
    }else if(AtrId=="Offspring"){
        EditDiv.setAttribute("class","three-col");
        EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"p",{"id":"desired_content_p"},"");
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"add_children()"},"Add");
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit('"+AtrId+"','"+AtrType+"','"+Context+"',ATR_CONTENT)"},"Commit");
        scroll_and_blink(EditDiv);
    }else if(AtrId == 'Desired food content'){
        EditDiv.setAttribute("class","three-col");
        EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"p",{"id":"desired_content_p"},"");
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"add_desired_content()"},"Add");
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit('"+AtrId+"','"+AtrType+"','"+Context+"',ATR_CONTENT)"},"Commit");
        scroll_and_blink(EditDiv);
    }else if(AtrId=="EID"){
        prompt_and_update(AtrId,AtrType,Context,"Enter the new EID:");
    }else if(AtrId == "Gender"){
        var Val = prompt("Enter the sheep's gender('Male' or 'Female'):","");
        if (Val!=null){
            if (Val!=""){
                if ((Val=="Male")||(Val=="Female")){
                    var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Val});
                    update_attribute(Details);
                }else{
                    alert("ERROR: Only 'Male' or 'Female' are excepted inputs");
                }
                
            }else{
                alert("ERROR: Nothing was entered");
            }
        }
    }else if((AtrId=="Sire")||(AtrId=="Dam")){
        EditDiv.setAttribute("class","three-col");
        EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        var DBDiv = create_label_input_div("Database (Enter 'Base ID' if part of this system):","db_input",Context);
        EditDiv.appendChild(DBDiv);
        var IDDiv = create_label_input_div("ID (in respective database):","id_input",Value);
        EditDiv.appendChild(IDDiv);
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit('"+AtrId+"','"+AtrType+"','db_input','id_input')"},"Commit");
        scroll_and_blink(EditDiv);
    }else if((AtrId=="In labor")||(AtrId=="On heat")||(AtrId=="Pregnant")){
        yes_or_no_prompt(AtrId,AtrType,Context,"Is this sheep "+AtrId.toLowerCase()+" ('yes'/'no')?");
    }else if(AtrId=="Healthy"){
        var Val = prompt("Is this sheep ill ('yes'/'no')?","");
        if (Val!=null){
            if (Val!=""){
                if ((Val=="yes")||(Val=="no")){
                    if (Val=="yes"){
                        prompt_and_update(AtrId,AtrType,Context,"Enter the illness:")
                    }else{
                        var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":true});
                    update_attribute(Details);
                    }
                    
                }else{
                    alert("ERROR: Only 'Yes' or 'No' are excepted inputs");
                }
                
            }else{
                alert("ERROR: Nothing was entered");
            }
        }
     
    }
    else if(AtrId=="Case studies"){
        update_cs_table(Value);
    }else if(AtrId=="Breed"){
        prompt_and_update(AtrId,AtrType,Context,"Enter the breed:");
    }else if (AtrId == 'Max hours between eating activities'){
        prompt_for_number(AtrId,AtrType,Context,"Enter the number of hours that the sheep has not eaten, after which all human workers should receive an sms that this sheep has not eaten for this amount of hours:")
    }  
    else{
        alert("No editing available for this attribute");
    }
}


function edit_Groups_attribute(AtrId,AtrType,Context,Value){
    var EditDiv = document.getElementById("atr_edit_div");
    EditDiv = remove_all_children(EditDiv);

    if ((AtrId == 'Age boundaries')||(AtrId =='Avg food intake boundaries')||(AtrId=='Weight boundaries')){
        EditDiv.setAttribute("class","three-col");
        EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
        EditDiv = add_element_to_element(EditDiv,"h5",{}," Context: "+Context);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        var MinDiv = create_label_input_div("Minimum:","min_input",Value[0].toString());
        EditDiv.appendChild(MinDiv);
        var MaxDiv = create_label_input_div("Maximum:","max_input",Value[1].toString());
        EditDiv.appendChild(MaxDiv);
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit_interval('"+AtrId+"','"+AtrType+"','"+Context+"')"},"Commit");
        scroll_and_blink(EditDiv);
    }else if(AtrId=="Genders allowed"){
        var Val = prompt("Which genders are allowed in this group (Male,Female,all)","");
        if (Val!=null){
            if (Val!=""){
                if ((Val=="Male")||(Val=="Female")||(Val=="all")){
                    var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Val});
                    update_attribute(Details);
                }else{
                    alert("ERROR: Only 'Male', 'Female' and 'all' are allowed");
                }
                
            }else{
                alert("ERROR: Nothing was entered");
            }
        }
    }else if(AtrId =="Set food content"){
        EditDiv.setAttribute("class","three-col");
        EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"p",{"id":"desired_content_p"},"");
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"add_desired_content()"},"Add");
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit('"+AtrId+"','"+AtrType+"','"+Context+"',ATR_CONTENT)"},"Commit");
        scroll_and_blink(EditDiv);
    }else if(AtrId=="States allowed"){
        var Val = prompt("Are all states allowed in this group ('yes'/'no')?","");
        if (Val!=null){
            if (Val!=""){
                if (Val=="yes"){
                    var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":"all"});
                    update_attribute(Details);
                }else{
                    EditDiv.setAttribute("class","three-col");
                    EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
                    EditDiv = add_element_to_element(EditDiv,"div",{},null);
                    EditDiv = add_element_to_element(EditDiv,"div",{},null);
                    EditDiv = add_element_to_element(EditDiv,"p",{"id":"desired_content_p"},"");
                    EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"add_states_allowed()"},"Add");
                    EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit('"+AtrId+"','"+AtrType+"','"+Context+"',ATR_CONTENT)"},"Commit");
                    scroll_and_blink(EditDiv);
                }
                
            }else{
                alert("ERROR: Nothing was entered");
            }
        }
    }
    else if(AtrId=="Case studies"){
        update_cs_table(Value);
    }
    else{
        alert("No editing available for this attribute");
    }
}

function edit_Houses_attribute(AtrId,AtrType,Context,Value){
    var EditDiv = document.getElementById("atr_edit_div"),
    EditDiv = remove_all_children(EditDiv);
    //console.log(AtrId);
    if (AtrId == 'Size'){
        var L = prompt("Enter the length of the house in meters","");
        if (L!=null){
            if (L!=""){
                var Ln = Number(L);
                if (isNaN(Ln)){
                    alert("ERROR: Only numbers are excepted");
                }else{
                    var W = prompt("Enter the width of the house in meters","");
                    if (W!=null){
                        if (W!=""){
                            var Wn = Number(W);
                            if (isNaN(Wn)){
                                alert("ERROR: Only numbers are excepted");
                            }else{
                                var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":L+"x"+W+"m"});
                                update_attribute(Details);
                            }
                        }else{
                            alert("ERROR: Nothing was entered")
                        }
                    }
                }
            }else{
                alert("ERROR: Nothing was entered");
            }
        }
    }else if(AtrId=="EID"){
        prompt_and_update(AtrId,AtrType,Context,"Enter the new EID:");
    }else if(AtrId=="Floor material"){
        prompt_and_update(AtrId,AtrType,Context,"Enter the new floor material description:");
    }else if(AtrId=="Roof"){
        yes_or_no_prompt(AtrId,AtrType,Context,"Does this house have a roof?");
    }else if (AtrId=="Solid walls"){
        yes_or_no_prompt(AtrId,AtrType,Context,"Does this house have solid walls (wind resistant)?");
    }else if(AtrId=="Sheep limit"){
        var Nr = prompt("Enter the maximum number of sheep allowed in this house","");
        if (Nr!=null){
            if (Nr!=""){
                Nr = Number(Nr);
                if (isNaN(Nr)){
                    alert("ERROR: Only numbers are excepted");
                }else{
                    var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Nr});
                    update_attribute(Details);
                }
            }else{
                alert("ERROR: Nothing was entered")
            }
        }
    }
    else{
        alert("No editing available for this attribute");
    }
}

function edit_Human_workers_attribute(AtrId,AtrType,Context,Value){
    var EditDiv = document.getElementById("atr_edit_div"),
    EditDiv = remove_all_children(EditDiv);
    //console.log(AtrId);
    if (AtrId == 'Cell phone number'){
        
        prompt_and_update(AtrId,AtrType,Context,"Enter the cell phone number of the worker (27*******):");
    }else if(AtrId == 'Date of birth'){
        EditDiv.setAttribute("class","two-col"),
        EditDiv = add_element_to_element(EditDiv,"h4",{},"Editing "+AtrId);
        EditDiv = add_element_to_element(EditDiv,"div",{},null);
        EditDiv = add_element_to_element(EditDiv,"input",{"id":"dob_input","type":"date"},null);
        EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"commit('"+AtrId+"','"+AtrType+"','"+Context+"','dob_input')"},"Commit")
        scroll_and_blink(EditDiv);
    }else if(AtrId =='Gender'){
        var Val = prompt("Enter the human's gender('Male' or 'Female'):","");
        if (Val!=null){
            if (Val!=""){
                if ((Val=="Male")||(Val=="Female")){
                    var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Val});
                    update_attribute(Details);
                }else{
                    alert("ERROR: Only 'Male' or 'Female' are excepted inputs");
                }
                
            }else{
                alert("ERROR: Nothing was entered");
            }
        }
    }else if(AtrId =="Height (cm)"){
        prompt_for_number(AtrId,AtrType,Context,"Enter the height (cm) for this worker:");
    }else if(AtrId=="Weight (kg)"){
        prompt_for_number(AtrId,AtrType,Context,"Enter the weight (kg) for this worker:")
    }
    else{
        alert("No editing available for this attribute");
    }
}

function edit_Feeders_attribute(AtrId,AtrType,Context,Value){
    
    //console.log(AtrId);
    if (AtrId == 'Current food product id'){
        var IncludeEmpty =true;
        var Content = JSON.stringify({"wh_id":"Sheep Food WH","request":"get_del_or_pop_all_products","get_del_or_pop":"get","include_empty":IncludeEmpty});
        var Msg = JSON.stringify({"Request":"warehouse_functions","Content":Content});
        sendMsg(Msg);
    }    
}

function edit_Sensors_attribute(AtrId,AtrType,Context,Value){
    
    //console.log(AtrId);
    if (AtrId == 'Max minutes with no activity'){
        prompt_for_number(AtrId,AtrType,Context,"Enter the number of minutes of no activity, after which all human workers should receive an sms that this sensor had no activity for this amount of minutes:")
    }    
}

function feeder_current_product_edit(Content){
    var EditDiv = document.getElementById("atr_edit_div"),
    EditDiv = remove_all_children(EditDiv);
    var Selecter = document.createElement("select");
    Selecter.setAttribute("id","feeder_product_selecter");
    for (key in Content){
        Selecter = add_element_to_element(Selecter,"option",{"value":key},key);
    }
    EditDiv.setAttribute("class","two-col");
    EditDiv.appendChild(Selecter);
    EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"select_feeder_product()"},"Select");
    scroll_and_blink(EditDiv);
}

function select_feeder_product(){
    var Selecter = document.getElementById("feeder_product_selecter");
    var ProdId = Selecter.value;
    var Details = JSON.stringify({"id":'Current food product id',"type":"Management","context":'Product in sheep_food_warehouse',"value":ProdId});
    update_attribute(Details);
}

ATR_CONTENT={};
function add_children(){
    var DB = prompt("Enter the database name where this child is located ('Base ID' if part of this system ):","");
    if (DB!=null){
        if (DB!=""){
            var ID = prompt("Enter the id of the child in the database chosen","");
            if (ID!=null){
                if (ID!=""){
                    ATR_CONTENT[DB] = ID;
                    document.getElementById("desired_content_p").innerHTML = JSON.stringify(ATR_CONTENT);
                }else{
                    alert("ERROR: No id was entered");          
                }
            }
            
        }else{
            alert("ERROR: Nothing was entered");
        }
    }
}
function add_desired_content(){
    var Nutrient = prompt("Enter the content identifier (like protein/energy):","");
    if (Nutrient!=null){
        if (Nutrient!=""){
            var Perc = prompt("Enter the percentage of "+Nutrient+" desired","");
            if (Perc!=null){
                Perc = Number(Perc);
                if (isNaN(Perc)){
                    alert("ERROR: Only numbers between 0 and 100 are allowed");
                }else{
                    if ((Perc<0)||(Perc>100)){
                        alert("ERROR: Only numbers between 0 and 100 are allowed");
                    }else{
                            ATR_CONTENT[Nutrient] = Perc;
                            document.getElementById("desired_content_p").innerHTML = JSON.stringify(ATR_CONTENT);
                    }
                }
            }
            
        }else{
            alert("ERROR: Nothing was entered");
        }
    }
}
function add_states_allowed(){
    var State = prompt("Enter the state ('Healthy','On heat', 'Pregnant', 'In labor') you want to add requirement for: ","");
    if (State!=null){
        if (State!=""){
            if ((State=="Healthy")||(State=="On heat")||(State=="Pregnant")||(State=="In labor")){
                var TrueOrFalse = prompt("Enter 'true' or 'false' for state requirement of being "+State.toLowerCase(),"");
                if (TrueOrFalse=="true"){
                    ATR_CONTENT[State]=true;  
                    document.getElementById("desired_content_p").innerHTML = JSON.stringify(ATR_CONTENT);
                }else if(TrueOrFalse=="false"){
                    ATR_CONTENT[State]=false;  
                    document.getElementById("desired_content_p").innerHTML = JSON.stringify(ATR_CONTENT);
                }else{
                    alert("ERROR: Only true and false allowed");
                }
                
            }else{
                alert("ERROR: Only 'Healthy','On heat', 'Pregnant' and 'In labor' are permitted states")
            }
            
        }else{
            alert("ERROR: Nothing was entered");
        }
    }
}
function prompt_and_update(AtrId,AtrType,Context,PromptMsg){
    var Val = prompt(PromptMsg,"");
    if (Val!=null){
        if (Val!=""){
            var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Val});
            update_attribute(Details);
        }else{
            alert("ERROR: Nothing was entered");
        }
    }
}
function prompt_for_number(AtrId,AtrType,Context,PromptText){
    var Val = prompt(PromptText,"");
    if (Val!=null){
        if (Val!=""){
            Val = Number(Val);
            if (isNaN(Val)){
                alert("ERROR: Only numbers allowed. Use '.' for decimals, not ','.");
            }else{
                var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Val});
                update_attribute(Details);
            }
            
        }else{
            alert("ERROR: Nothing was entered");
        }
    }
}
function yes_or_no_prompt(AtrId,AtrType,Context,PromptMsg){
    var Val = prompt(PromptMsg,"");
    if (Val!=null){
        if (Val!=""){
            if ((Val=="yes")||(Val=="no")){
                if (Val=="yes"){
                    Val = true;
                }else{
                    Val = false;
                }
                var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Val});
                update_attribute(Details);
            }else{
                alert("ERROR: Only 'Yes' or 'No' are excepted inputs");
            }
            
        }else{
            alert("ERROR: Nothing was entered");
        }
    }
}

function commit(AtrId,AtrType,Context,ValueElId){
    if (typeof(ValueElId)=="string"){
        if (document.getElementById(ValueElId)!=undefined){
            var Value = (document.getElementById(ValueElId)).value;
        }else{
            var Value = ValueElId;
        }
    }else{
        var Value = ValueElId;
    }  
    
    if (document.getElementById(Context)!=undefined){
        Context = (document.getElementById(Context)).value;
    }
    var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":Value});
    update_attribute(Details);
    ATR_CONTENT = {};
}

function commit_interval(AtrId,AtrType,Context){
    var Min = document.getElementById("min_input").value;
    var Max = document.getElementById("max_input").value;
    if (Min!="none"){
        Min = Number(Min);
    }
    if (Max!="none"){
        Max = Number(Max);
    }
    if ((Min!="none")&&(isNaN(Min))||((Max!="none")&&(isNaN(Max)))){
        alert("ERROR: min and max can only be numbers or 'none'")
    }else{
        var Details = JSON.stringify({"id":AtrId,"type":AtrType,"context":Context,"value":[Min,Max]});
        update_attribute(Details);
    }
    
}

function update_attribute(Details){
    var Content = JSON.stringify({"address":RESOURCE_ADDRESS,"action":"add","component":"attributes","details":Details});
    var Msg = JSON.stringify({"Request":"edit_resource","Content":Content});
    sendMsg(Msg);
}

function update_adr_attribute(ResourceAdr,Details){
    var Content = JSON.stringify({"address":ResourceAdr,"action":"add","component":"attributes","details":Details});
    var Msg = JSON.stringify({"Request":"edit_resource","Content":Content});
    sendMsg(Msg);
}
