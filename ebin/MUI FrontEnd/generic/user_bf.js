var USERNAME = "";
var PASSWORD = "";

function get_users(){
    var Msg = JSON.stringify({"Request":"get_users","Content":""});
    sendMsg(Msg);
}
function validate_user(){
    USERNAME = document.getElementById("username").value;
    PASSWORD = document.getElementById("password").value;
    var ContentString = JSON.stringify({"user_name":USERNAME,"password":PASSWORD});
    var Msg = JSON.stringify({"Request":"validate_user","Content":ContentString});
    sendMsg(Msg);
}

function change_pswd(){
    var OldPswd = document.getElementById("old_pswd").value;
    var NewPswd = document.getElementById("new_pswd").value;
    var ContentString = JSON.stringify({"user_name":USERNAME,"old_pswd":OldPswd,"new_pswd":NewPswd});
    var Msg = JSON.stringify({"Request":"change_password","Content":ContentString});
    sendMsg(Msg);

}

function add_user(){
    var NewUser = document.getElementById("new_user_name").value;
    var NewPswd = document.getElementById("new_user_pswd").value;
    var NewPermLevel = document.getElementById("new_user_perm").value;
    if ((NewPermLevel == "1") || (NewPermLevel =="2") || (NewPermLevel =="3")){
        var ContentString = JSON.stringify({"new_user":NewUser,"new_pswd":NewPswd,"permission_level":NewPermLevel});
        var Msg = JSON.stringify({"Request":"add_user","Content":ContentString});
        sendMsg(Msg);
    }else{
        alert("Permission level must be a number between 1 and 3")
    }
}


function handle_users(Users){
    //console.log(Users);
    var ExistingTable = document.getElementById("users_tab");
    if (ExistingTable!=undefined){
        ExistingTable.remove();
    }
    var NewTable = document.createElement("table");
    NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
    NewTable.setAttribute("id","users_tab");
    NewTable.setAttribute("class","tab");
    var Head = document.createElement("tr");
    Head = add_head(Head,"User names",1);
    Head = add_head(Head,"",1);
    NewTable.appendChild(Head);
    for (e in Users){
        var UserN = Users[e];
        var Row = document.createElement("tr");
        Row = add_cell(Row,UserN,1);
        var RemBtn = document.createElement("button");
        RemBtn.setAttribute("onclick","remove_user('"+UserN+"')");
        RemBtn.innerHTML = "Remove";
        Row = add_cell(Row,RemBtn,1);
        NewTable.appendChild(Row);
    }
    document.getElementById("users_div").appendChild(NewTable);
}


function remove_user(UserNameRem){
    //console.log("rem");
    var ContentString = JSON.stringify({"user_rem":UserNameRem});
    var Msg = JSON.stringify({"Request":"remove_user","Content":ContentString});
    var r = confirm("Are you sure you want to remove this user?");
    if (r == true) {
        sendMsg(Msg);
    }        
}

