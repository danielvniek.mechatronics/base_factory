function get_std_layout(GraphTitle){
    var layout = {
        hovermode:'closest',
        showlegend: true,
        legend: {
          x: 1,
          xanchor: 'right',
          y: 1
        },
        height: 600,
        width: 1400,
        xaxis: {
          showgrid: true,
          zeroline: false,
          showline: false,
          showticklabels: true
            
        /*  showline: true,
          showgrid: false,
          showticklabels: false,
          linecolor: 'rgb(204,204,204)',
          linewidth: 2,
          autotick: false,
          ticks: 'outside',
          tickcolor: 'rgb(204,204,204)',
          tickwidth: 2,
          ticklen: 5,
          tickfont: {
            family: 'Arial',
            size: 12,
            color: 'rgb(82, 82, 82)'
          }*/
        },
        yaxis: {
          showgrid: true,
          zeroline: false,
          showline: false,
          showticklabels: true
          
        },
        autosize: false,
        margin: {
          autoexpand: false,
          l: 100,
          r: 20,
          t: 100
        },
        annotations: [
          {
            xref: 'paper',
            yref: 'paper',
            x: 0.0,
            y: 1.05,
            xanchor: 'left',
            yanchor: 'bottom',
            text: GraphTitle,
            font:{
              family: 'Arial',
              size: 30,
              color: 'rgb(37,37,37)'
            },
            showarrow: false
          }
        ]
      };
    return layout;
}