var wsHost = "ws://" + window.location.host + "/websocket";
var websocket = new WebSocket(wsHost);
websocket.onopen = function(evt) { onOpen(evt) }; 
websocket.onclose = function(evt) { onClose(evt) }; 
websocket.onmessage = function(evt) { onMessage(evt) }; 
websocket.onerror = function(evt) { onError(evt) }; 


/////////////////////////////////////////////////////////
// WEBSOCKET STUFF

function connect()
      {
          websocket.onopen = function(evt) { onOpen(evt) }; 
          websocket.onclose = function(evt) { onClose(evt) }; 
          websocket.onmessage = function(evt) { onMessage(evt) }; 
          websocket.onerror = function(evt) { onError(evt) }; 
      };  
      
      function disconnect() {
          websocket.close();
      }; 

      function toggle_connection(){
          if(websocket.readyState == websocket.OPEN){
              disconnect();
          } else {
              connect();
          };
      };

      function sendMsg(MSG) {
          if(websocket.readyState == websocket.OPEN){
              console.log("Websocket sending message");
              websocket.send(MSG);
          } else {
              console.log("ERROr: Could not send message");
          };
      };

      function onOpen(evt) { 
    };  

    function onClose(evt) { 
        websocket = new WebSocket(wsHost);
        websocket.onopen = function(evt) { onOpen(evt) }; 
        websocket.onclose = function(evt) { onClose(evt) }; 
        websocket.onmessage = function(evt) { onMessage(evt) }; 
        websocket.onerror = function(evt) { onError(evt) }; 
    };  
CUSTOM_EXISTING_RESOURCES_FUNCTION = null;
BF_CONFIRM_PROMPTS = true;
REQUEST_REPLY_FUNCTION = null;
ACCEPT_MESSAGES = true;

function onMessage(evt) 
    { 
        console.log(evt);
        var Msg = JSON.parse(evt.data);
        if (ACCEPT_MESSAGES==true){
            
            if(Msg.SUBJECT == 'Valid user')
            {
                handle_valid_user(Msg.CONTENT);
            }else if(Msg.SUBJECT=='Basic folder contents'){
                handle_basic_folder_contents(Msg.CONTENT);
            }
            else if(Msg.SUBJECT=='Existing Resources'){
                if (CUSTOM_EXISTING_RESOURCES_FUNCTION!=null){ 
                    CUSTOM_EXISTING_RESOURCES_FUNCTION(Msg.CONTENT);
                    CUSTOM_EXISTING_RESOURCES_FUNCTION = null;
                }else{
                    handle_existing_resources(Msg.CONTENT);
                }
                
            }else if(Msg.SUBJECT=='Resource info'){
                handle_resource_info(Msg.CONTENT);
            }else if(Msg.SUBJECT=='Warehouse products'){
                if (PAGE.includes("(Feeders)")){
                    feeder_current_product_edit(Msg.CONTENT);
                }else{
                    handle_warehouse_products(Msg.CONTENT);
                }
                
            }else if(Msg.SUBJECT=='Resource removed'){
                handle_resource_removed(Msg.CONTENT);
            }else if(Msg.SUBJECT=='users'){
                handle_users(Msg.CONTENT);
            }    
            else if(Msg.SUBJECT =='Error'){
                alert("ERROR!!!:"+Msg.CONTENT);
            }else if (Msg.SUBJECT=="Resource added"){
                var Components = JSON.stringify({"atr":"all"});
                var SearchString = JSON.stringify({"type":PAGE,"SearchBy":"ids_and_type","SearchParameters":[LAST_RESOURCE_ADDED],"components":Components});
                var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
                var EDiv = document.getElementById("existing_resources_div");
                add_busy_to_div(EDiv);
                sleep(2000); 
                sendMsg(Msg);
            }else if(Msg.SUBJECT=='Notify'){
                if (RESOURCE_ID!="RFID Sheep Scale"){
                    if (PAGE!="Home"){
                        alert(Msg.CONTENT);
                    } 
                }
                    

                
                if ((Msg.CONTENT)=="New attribute added to attributes"){
                    if (PAGE!="Home"){
                        refresh_details();
                    }else{
                        home_scs_atr_changed()
                    }
                    
                }
            }else if(Msg.SUBJECT=='Request reply'){
                REQUEST_REPLY_FUNCTION(Msg.CONTENT);
            }else if(Msg.SUBJECT=='Holon interactions'){
                holon_interactions(Msg.CONTENT);
            }
            else
            {
                console.log('Unrecognized subject');
            }
        }else{
            if (ACCEPT_MESSAGES!=false){;
                ACCEPT_MESSAGES(Msg.CONTENT);
            }
        }
        
        
    };  

    function onError(evt) {
        console.log("~n WS ERROR SOMEHOW");
    };
