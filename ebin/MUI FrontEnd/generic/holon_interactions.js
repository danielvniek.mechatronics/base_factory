function holon_interactions_nav(){
    change_page('holon_interactions.html','Holon interactions');
    CUSTOM_EXISTING_RESOURCES_FUNCTION = all_holons;
    var SearchString = JSON.stringify({"SearchBy":"all","SearchParameters":[]});
    var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
    var checkExist = setInterval(function() {
        if (document.getElementById("hi-diagram")!=null) {
            sendMsg(Msg);
            clearInterval(checkExist);
        }else{
            //console.log('asdf')
        }
     }, 100);
    
}
var HolonTypes = {};
var SelectedHolons = [];
function all_holons(Content){
    HolonTypes = {};
    SelectedHolons = [];
    var AllHolons = [];
    for (key in Content){
        var Resource = Content[key];
        AllHolons.push(Resource.id);
        if (HolonTypes[Resource.type]==undefined){
            HolonTypes[Resource.type] = [Resource.id];
        }else{
            var T = HolonTypes[Resource.type];
            T.push(Resource.id);
            HolonTypes[Resource.type] = T;
        }
    }
    var TypesDiv = document.getElementById("hi_types");
    var IdsDiv = document.getElementById("hi_ids");
    for (t in HolonTypes){
        var TypeDiv = label_and_checkbox(t,"hi_type_click(this)");
        TypesDiv.appendChild(TypeDiv);
    }
    for (i in AllHolons){
        var IdDiv = label_and_checkbox(AllHolons[i],"hi_id_click(this)");
        IdsDiv.appendChild(IdDiv);
    }

}

function label_and_checkbox(Label,CheckboxFunc){
    var Div = document.createElement("div");
    Div.setAttribute("class","two-col");
    Div.style.overflow = "hidden";
    Div = add_element_to_element(Div,"label",{},Label);
    Div = add_element_to_element(Div,"input",{"type":"checkbox","onclick":CheckboxFunc,"id":Label},null);
    return Div;
}

function hi_type_click(This){
    var Ids = HolonTypes[This.id];
    if (This.checked)  { 
      for (i in Ids){
          var C = document.getElementById(Ids[i]);
          C.checked = true;
          if (!SelectedHolons.includes(Ids[i])){
            SelectedHolons.push(Ids[i]);
          }
      }
    } else {
        for (i in Ids){
            (document.getElementById(Ids[i])).checked = false;
            SelectedHolons = SelectedHolons.filter(function(value, index, arr){ 
                return value != Ids[i];
            });
        }
    }
}

function hi_id_click(This){
    if (This.checked)  { 
        if (!SelectedHolons.includes(This.id)){
            SelectedHolons.push(This.id);
          }
    } else {
            SelectedHolons = SelectedHolons.filter(function(value, index, arr){ 
                return value != This.id;
            });
    }
}

function get_holon_interactions(){
    var StartDate = get_date_or_time_or_default("start_date","2000/01/01");
    var StartTime = get_date_or_time_or_default("start_time","00:00:00");
    var [NowDate,NowTime] = date_to_date_and_time_string(new Date());
    var EndDate = get_date_or_time_or_default("end_date",NowDate);
    var EndTime = get_date_or_time_or_default("end_time",NowTime);
    var DiagDiv = document.getElementById("hi-diagram");
    DiagDiv = remove_all_children(DiagDiv);
    DiagDiv = add_element_to_element(DiagDiv,"h2",{},"Getting interactions...");
    var Msg = JSON.stringify({"Request":"holon_interactions","Content":{"StartDate":StartDate,"StartTime":StartTime,"EndDate":EndDate,"EndTime":EndTime,"ids":SelectedHolons}});
    sendMsg(Msg);
}

function get_date_or_time_or_default(Id,DefaultVal){
    var Val = (document.getElementById(Id)).value;
    if (Val == ""){
        Val = DefaultVal;
    }
    return Val;
}

function holon_interactions(Content){
    
    var diagram = Diagram.parse(Content);
    var DiagDiv = document.getElementById("hi-diagram");
    DiagDiv = remove_all_children(DiagDiv);
    var options = {theme: 'simple'};
    diagram.drawSVG("hi-diagram",options);
}

function enable_pan_and_zoom(){
    var SVG = document.getElementsByTagName("svg")[0];
    SVG.id = "seq_diag";
    var svgActive = false
    , svgHovered = false

  // Expose to window namespace for testing purposes
  window.panZoom = svgPanZoom('#seq_diag', {
    zoomEnabled: true
  , controlIconsEnabled: true
  , zoomEnabled: false
  , fit: 1
  , center: 1
  , customEventsHandler: {
      init: function(options){
        function updateSvgClassName(){
          options.svgElement.setAttribute('class', '' + (svgActive ? 'active':'') + (svgHovered ? ' hovered':''))
        }

        this.listeners = {
          click: function(){
            if (svgActive) {
              options.instance.disableZoom()
              svgActive = false
            } else {
              options.instance.enableZoom()
              svgActive = true
            }

            updateSvgClassName()
          },
          mouseenter: function(){
            svgHovered = true
            updateSvgClassName()
          },
          mouseleave: function(){
            svgActive = false
            svgHovered = false
            options.instance.disableZoom()

            updateSvgClassName()
          }
        }

        this.listeners.mousemove = this.listeners.mouseenter

        for (var eventName in this.listeners){
          options.svgElement.addEventListener(eventName, this.listeners[eventName])
        }
      }
    , destroy: function(options){
        for (var eventName in this.listeners){
          options.svgElement.removeEventListener(eventName, this.listeners[eventName])
        }
      }
    }
  });
};







