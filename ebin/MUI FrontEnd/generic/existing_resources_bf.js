function get_all(){
    var SearchParameters = [];
    SearchParameters.push(PAGE);
    var Components = JSON.stringify({"atr":"all"});
    var SearchString = JSON.stringify({"SearchBy":"types","SearchParameters":SearchParameters,"components":Components});
    var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
    var EDiv = document.getElementById("existing_resources_div");
    add_busy_to_div(EDiv);
    sendMsg(Msg);
}

function get_specific(){
    var Resources = [];
    var ResourceId = null;
    var ResourceStr = "";
    while(ResourceId!=""){
        var PromptMessage = "Enter the ids of the "+PAGE.toLowerCase()+"you want to find by clicking on ok after every id and then clicking on ok with the entry field empty when you are done. So far you are searching for these ids: \n"+ResourceStr;
        ResourceId = prompt(PromptMessage,"");
        if (ResourceId!=null){
            if (ResourceId==""){
                if (Resources.length>0){
                    var Components = JSON.stringify({"atr":"all"});
                    var SearchString = JSON.stringify({"type":PAGE,"SearchBy":"ids_and_type","SearchParameters":Resources,"components":Components});
                    var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
                    var EDiv = document.getElementById("existing_resources_div");
                    add_busy_to_div(EDiv);
                    sendMsg(Msg);
                }
            }else{
                Resources.push(ResourceId);
                ResourceStr += ResourceId+"\n";
            }
        }else{
            break;
        }
    }
}
var LAST_RESOURCE_ADDED = "_";
function add_new(){
        if (PAGE!="Other"){
            var ResourceId = prompt("Enter ID of new resource ("+PAGE+"):","");
            if (ResourceId!=null){
                if (ResourceId==""){
                    alert("ERROR: No valid id was entered");
                }else{
                    LAST_RESOURCE_ADDED = ResourceId;
                    alert("The resource you are creating will start with default attributes. This can be changed by clicking on 'Get by ids' and entering the new resource's id. The new resource will be displayed on this page and can be edited by clicking on it id, which will open another page with the resource's details")
                    var Content = JSON.stringify({"id":ResourceId,"type":PAGE,"twin_type":"base"});
                    var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                    var EDiv = document.getElementById("existing_resources_div");
                    add_busy_to_div(EDiv);
                    sendMsg(Msg);
                } 
            }                  
        } else{
            alert("ERROR: Cannot add resources from this page");
        }
    
}
