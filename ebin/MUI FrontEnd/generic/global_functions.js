var PAGE = "";


function change_page(HtmlFile,PageName){
    PAGE = PageName;
    $('#root').load(HtmlFile,function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success"){
          document.getElementById("PageName").innerHTML = PageName;
          if (PageName=="Other"){
              (document.getElementById("add_new")).remove();
          }
        }
            
        if(statusTxt == "error")
          alert("Error: This page does not exist" );
      });
}



function add_head(Row,Content,Span,OnclickFunction){
    var HeadCell = document.createElement("th");
    HeadCell.setAttribute("onclick",OnclickFunction);
    if (typeof(Content)=="string"){
        HeadCell.innerHTML = Content;
    }else{
        HeadCell.appendChild(Content);
    }
    
    HeadCell.setAttribute("colspan",Span.toString());
    Row.appendChild(HeadCell);
    return Row;
}
function add_cell(Row,Content,Span){
    var Cell = document.createElement("td");
    if ((typeof(Content)=="string") || (typeof(Content)=="number") || (typeof(Content)=="boolean")){
        Cell.innerHTML = Content;
    }else if(typeof(Content)=="object"){
        if (Content instanceof HTMLElement){
          Cell.appendChild(Content);
        }
        else{
          Cell.innerHTML = JSON.stringify(Content);
        }  
      } 
    Cell.setAttribute("colspan",Span.toString());
    Row.appendChild(Cell);
    return Row;
}


var DIR = "desc";

function sortTable(TableId,n,sortFrom) {
    //console.log("SORT");
    var table, rows, switching, i, x, y, shouldSwitch, switchcount = 0;
    table = document.getElementById(TableId);
    switching = true;
    //Set the sorting direction to ascending:
    if (DIR=="asc"){
        DIR = "desc";
    }else{
        DIR = "asc";
    }
    /*Make a loop that will continue until
    no switching has been done:*/
    rows = table.rows;
    if (rows.length<=500){
      while (switching) {
        //start by saying: no switching is done:
        switching = false;
        
        /*Loop through all table rows (except the
        first two rows, which contains table headers):*/
        for (i = sortFrom; i < (rows.length - 1); i++) {
          //start by saying there should be no switching:
          shouldSwitch = false;
          /*Get the two elements you want to compare,
          one from current row and one from the next:*/
          x = rows[i].getElementsByTagName("TD")[n];
          y = rows[i + 1].getElementsByTagName("TD")[n];
          /*check if the two rows should switch place,
          based on the direction, asc or desc:*/
          
          if (DIR == "asc") {
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
              //if so, mark as a switch and break the loop:
              shouldSwitch= true;
              break;
            }
          } else if (DIR == "desc") {
            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
              //if so, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          }
        }
        if (shouldSwitch) {
          /*If a switch has been marked, make the switch
          and mark that a switch has been done:*/
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
          //Each time a switch is done, increase this count by 1:
          switchcount ++;      
        } else {
          /*If no switching has been done AND the direction is "asc",
          set the direction to "desc" and run the while loop again.*/
  
        }
        //console.log("switch");
      }
    }else{
      alert("Sorry. Cannot sort tables with more than 500 entries. The browser will take too long")
    }
    //console.log("DONE")
  }

  function add_element_to_element(ParentElement,ElementType,Attributes,InnerHtml){
    var NewElem = document.createElement(ElementType);
    for (key in Attributes){
      NewElem.setAttribute(key,Attributes[key]);
    }
    if (InnerHtml!=null){
      NewElem.innerHTML = InnerHtml;
    }
    
    ParentElement.appendChild(NewElem);
    return ParentElement;
  }

  function remove_all_children(Element){
    try {
      while (Element.firstChild) {
        Element.removeChild(Element.lastChild);
      }
      return Element
    } catch (error) {
      return Element
    }
    
  }

  function create_label_input_div(LabelText,InputId,InputText){
    var Div = document.createElement("div");
    Div = add_element_to_element(Div,"label",{},LabelText);
    Div = add_element_to_element(Div,"input",{"id":InputId,"value":InputText});
    return Div;
  }

  function object_keys_on_new_lines(Obj){
    var Text = ""
    for (key in Obj){
      var Val = Obj[key];
      Text = Text + key + " : "
      if ((typeof(Val)=="object") && (Array.isArray(Val)==false)){
        for (subk in Val){
          Text = Text + "<br>   "+subk+" : "+JSON.stringify(Val[subk])
        }
      }else{
        Text = Text +JSON.stringify(Val);
      }
      Text = Text+ "<br>";
    }
    return Text;
  }

function toString(Anything){
  if (typeof(Anything)=="string"){
    var S = Anything;
  }else{
    var S = JSON.stringify(Anything)
  }
  return S
}

function csv_to_object(csv){

  var lines=csv.split("\n");
  //console.log("Lines");
  //console.log(lines);
  var result = [];
  
  if ((lines[0]).includes(",")){
    //console.log(lines[0])
    var headers=lines[0].split(",");
    var splitC = ",";
  }else{
    var headers=lines[0].split(";");
    var splitC = ";";
  }
  
  headers[headers.length-1]=headers[headers.length-1].trim();

  for(var i=1;i<lines.length-1;i++){

      var obj = {};
      var currentline=lines[i].split(splitC);
      currentline[currentline.length-1] = currentline[currentline.length-1].replace(/(\r\n|\n|\r)/gm,"");
      for(var j=0;j<headers.length;j++){
          obj[headers[j]] = currentline[j];
      }
      result.push(obj);

  }

  return result; //JavaScript object
}

function base_time_to_date_and_time(BaseT){
    var d = new Date((BaseT-62167226400)*1000);
    var [DateS,TimeS] = date_to_date_and_time_string(d);
    return [DateS,TimeS];
}

function date_to_date_and_time_string(D){
  var dd = String(D.getDate()).padStart(2, '0');
  var mm = String(D.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = D.getFullYear();
  var hh = String(D.getHours()).padStart(2, '0');
  var min = String(D.getMinutes()).padStart(2, '0'); 
  var ss = String(D.getSeconds()).padStart(2, '0');

  var DateS = yyyy + '-' + mm +'-'+dd;
  var TimeS = hh+':'+min+':'+ss;
  return ([DateS,TimeS]);
}

function scroll_and_blink(f){
    f.scrollIntoView();
    f.style.backgroundColor = "red";
    setTimeout(function() {
       f.style.backgroundColor = "white";
       setTimeout(function() {
        f.style.backgroundColor = "red";
        setTimeout(function() {
          f.style.backgroundColor = "white";
        }, 500);
      }, 500);
      
    }, 500);

 
}

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

function add_busy_to_div(Div){
  Div = remove_all_children(Div);
  Div = add_element_to_element(Div,"h4",{},"Busy getting data...");
}

function id_to_adr(ID){
  ID = ID.toLowerCase();
  ID.replace(/\s/g, '_');
  return ID;
}

function wait_until_element_exist(ElementName){
  var checkExist = setInterval(function() {
    if (document.getElementById(ElementName)!=null) {
      //console.log("EXist");
       clearInterval(checkExist);
    }
 }, 100); 
}

