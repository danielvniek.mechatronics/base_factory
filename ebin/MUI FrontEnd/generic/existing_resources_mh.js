function handle_existing_resources(Content){

        TypesAllowed = ["all"];
        TypesExcluded = ["State variables","Calculation variables"];
        IDsExcluded = ["Photos"]
        IDsAllowed = ["all"];

    //console.log("Handling existing resources");
    var NewTable = document.createElement("table");
    NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
    NewTable.setAttribute("id","existing_resources_tab");
    NewTable.setAttribute("class","tab");
    var LastResourceNr = Object.keys(Content).length;
    var FirstResource = Content[LastResourceNr.toString()];
    if (typeof(FirstResource)=="object"){
        var FirstAttributes = FirstResource.atr;
        var Head = document.createElement("tr");
        var SubH = document.createElement("tr");
        Head = add_head(Head,"",1);
        var IdReorderBtn = document.createElement("button");
        IdReorderBtn.innerHTML = "IDs";
        IdReorderBtn.setAttribute("onclick","sortTable('existing_resources_tab',0,2)");
        var ColNr = 1;
        SubH = add_head(SubH,IdReorderBtn,1);
        var KeyList = [];
        var TypeList = [];
        //Personal
        for (atr in FirstAttributes){
            var Atr = FirstAttributes[atr];
            var Type = Atr.type;
            if (Type=="Personal"){
                if (((TypesAllowed.includes(Type)) ||(TypesAllowed.includes("all")))&&(!(TypesExcluded.includes(Type)))){
                    if (!(TypeList.includes(Type))){
                        var HeadSpan = 0;
                        for (a in FirstAttributes){
                            var OtherAtr = FirstAttributes[a];
                            if (OtherAtr.type ==Type){
                                if (((IDsAllowed.includes(a)) || (IDsAllowed.includes("all")))&&(!(IDsExcluded.includes(a)))){
                                    HeadSpan +=1;
                                    var SubHBtn = document.createElement("button");
                                    SubHBtn.innerHTML = a;
                                    SubHBtn.setAttribute("onclick","sortTable('existing_resources_tab',"+ColNr.toString()+",2)");
                                    ColNr += 1;
                                    KeyList.push(a);
                                    TypeList.push(OtherAtr.type);
                                    SubH = add_head(SubH,SubHBtn,1); 
                                } 
                            }
                        } 
                        if (HeadSpan>0){
                            Head = add_head(Head,Type,HeadSpan); 
                        } 
                       
                    }
                }
            }    
        }
        //Relational
        for (atr in FirstAttributes){
            var Atr = FirstAttributes[atr];
            var Type = Atr.type;
            if (Type=="Relational"){
                if (((TypesAllowed.includes(Type)) ||(TypesAllowed.includes("all")))&&(!(TypesExcluded.includes(Type)))){
                    if (!(TypeList.includes(Type))){
                        var HeadSpan = 0;
                        for (a in FirstAttributes){
                            var OtherAtr = FirstAttributes[a];
                            if (OtherAtr.type ==Type){
                                if (((IDsAllowed.includes(a)) || (IDsAllowed.includes("all")))&&(!(IDsExcluded.includes(a)))){
                                    HeadSpan +=1;
                                    var SubHBtn = document.createElement("button");
                                    SubHBtn.innerHTML = a;
                                    SubHBtn.setAttribute("onclick","sortTable('existing_resources_tab',"+ColNr.toString()+",2)");
                                    ColNr += 1;
                                    KeyList.push(a);
                                    TypeList.push(OtherAtr.type);
                                    SubH = add_head(SubH,SubHBtn,1); 
                                } 
                            }
                        } 
                        if (HeadSpan>0){
                            Head = add_head(Head,Type,HeadSpan); 
                        }    
                    }
                }
            }    
        }
        //Management
        for (atr in FirstAttributes){
            var Atr = FirstAttributes[atr];
            var Type = Atr.type;
            if (Type=="Management"){
                if (((TypesAllowed.includes(Type)) ||(TypesAllowed.includes("all")))&&(!(TypesExcluded.includes(Type)))){
                    if (!(TypeList.includes(Type))){
                        var HeadSpan = 0;
                        for (a in FirstAttributes){
                            var OtherAtr = FirstAttributes[a];
                            if (OtherAtr.type ==Type){
                                if (((IDsAllowed.includes(a)) || (IDsAllowed.includes("all")))&&(!(IDsExcluded.includes(a)))){
                                    HeadSpan +=1;
                                    var SubHBtn = document.createElement("button");
                                    SubHBtn.innerHTML = a;
                                    SubHBtn.setAttribute("onclick","sortTable('existing_resources_tab',"+ColNr.toString()+",2)");
                                    ColNr += 1;
                                    KeyList.push(a);
                                    TypeList.push(OtherAtr.type);
                                    SubH = add_head(SubH,SubHBtn,1); 
                                } 
                            }
                        } 
                        if (HeadSpan>0){
                            Head = add_head(Head,Type,HeadSpan); 
                        }    
                    }
                }
            }    
        }
        //State
        for (atr in FirstAttributes){
            var Atr = FirstAttributes[atr];
            var Type = Atr.type;
            if ((Type!="Management")&&(Type!="Personal")&&(Type!="Relational")){
                if (((TypesAllowed.includes(Type)) ||(TypesAllowed.includes("all")))&&(!(TypesExcluded.includes(Type)))){
                    if (!(TypeList.includes(Type))){
                        var HeadSpan = 0;
                        for (a in FirstAttributes){
                            var OtherAtr = FirstAttributes[a];
                            if (OtherAtr.type ==Type){
                                if (((IDsAllowed.includes(a)) || (IDsAllowed.includes("all")))&&(!(IDsExcluded.includes(a)))){
                                    HeadSpan +=1;
                                    var SubHBtn = document.createElement("button");
                                    SubHBtn.innerHTML = a;
                                    SubHBtn.setAttribute("onclick","sortTable('existing_resources_tab',"+ColNr.toString()+",2)");
                                    ColNr += 1;
                                    KeyList.push(a);
                                    TypeList.push(OtherAtr.type);
                                    SubH = add_head(SubH,SubHBtn,1); 
                                } 
                            }
                        } 
                        if (HeadSpan>0){
                            Head = add_head(Head,Type,HeadSpan); 
                        }    
                    }
                }
            }    
        }
        NewTable.appendChild(Head);
        NewTable.appendChild(SubH);
        for (key in Content){
            var Resource = Content[key];
            var ResourceAtrs = Resource.atr;
            var ResourceRow = document.createElement("tr");
            var IDBtn = document.createElement("button");
            IDBtn.innerHTML = Resource.id;
            IDBtn.setAttribute("onclick","get_resource_details('"+Resource.id+"')");
            ResourceRow = add_cell(ResourceRow,IDBtn,1);
            for (e in KeyList){
                var atr = KeyList[e];
                var Atr = ResourceAtrs[atr];
                if (Atr != undefined){
                    var Value = Atr.value;
                    if (toString(Value).length>50){
                        var ViewBtn = document.createElement("button");
                        var AlertText = toString(Value);
                        ViewBtn.setAttribute("onclick","my_alert(this,"+AlertText+")");
                        ViewBtn.innerHTML = "View";
                        ResourceRow = add_cell(ResourceRow,ViewBtn,1);
                    }else{
                        ResourceRow = add_cell(ResourceRow,Value,1);
                    }
                    
                }else{
                    ResourceRow = add_cell(ResourceRow,"undefined",1);
                }
            }
            NewTable.appendChild(ResourceRow);
        }
        //console.log(NewTable.innerHTML);
    }else{
        NewTable = document.createElement("p");
        NewTable.innerHTML = "No "+ PAGE +" found";
    }
    var EDiv = document.getElementById("existing_resources_div");
    EDiv = remove_all_children(EDiv);
    
    document.getElementById("existing_resources_div").appendChild(NewTable);
}

function handle_resource_removed(Content){
    alert(Content);
    change_page('existing_resources.html',PAGE);
}