var CaseStudies = {};
function update_cs_table(CSAtr){
    CaseStudies = CSAtr;
    var EditDiv = document.getElementById("atr_edit_div");
    EditDiv = remove_all_children(EditDiv);
    EditDiv = add_element_to_element(EditDiv,"h4",{},"Case study editing");
    var NewTable = document.createElement("table");
    NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
    NewTable.setAttribute("id","cs_tab");
    NewTable.setAttribute("class","tab");
    var SubH = document.createElement("tr");
    SubH = add_head(SubH,"Case study name",1);
    SubH = add_head(SubH,"Case study type",1);
    SubH = add_head(SubH,"Case study start date",1);
    SubH = add_head(SubH,"Case study start time",1);
    SubH = add_head(SubH,"Case end date",1);
    SubH = add_head(SubH,"Case study end time",1);
    SubH = add_head(SubH,"Group or individual",1);
    SubH = add_head(SubH," ",1);
    NewTable.appendChild(SubH);
    for (a in CSAtr){
        var CSDets = CSAtr[a];
        var Row = document.createElement("tr");
        Row = add_cell(Row,CSDets["Case study name"],1);
        Row = add_cell(Row,CSDets["Case study type"],1);
        Row = add_cell(Row,CSDets["Case study start date"],1);
        Row = add_cell(Row,CSDets["Case study start time"],1);
        Row = add_cell(Row,CSDets["Case study end date"],1);
        Row = add_cell(Row,CSDets["Case study end time"],1);
        Row = add_cell(Row,CSDets["Group or individual"],1);
        var RemBtn = document.createElement("button");
        RemBtn.innerHTML = "Remove";
        RemBtn.setAttribute("onclick","remove_case_study('"+a+"')");
        Row = add_cell(Row,RemBtn,1);
        NewTable.appendChild(Row); 
    } 
    var Inputs = ["text","select","date","time","date","time"];
    var Ids = ["cs_name","cs_type","cs_start_date","cs_start_time","cs_end_date","cs_end_time"];
    var AddRow = document.createElement("tr");
    for (i in Inputs)   {
        if (Inputs[i]=="select"){
            var I = document.createElement("select");
            I = add_element_to_element(I,"option",{"value":"FCR"},"FCR");   
        }else{
            var I = document.createElement("input");
            I.setAttribute("type",Inputs[i]);
        }
       
        I.setAttribute("id",Ids[i]);
        AddRow = add_cell(AddRow,I,1);
    } 
    if (RESOURCE_TYPE=="Groups"){
        AddRow = add_cell(AddRow,"Group",1);
    }else{
        AddRow = add_cell(AddRow,"Individual",1);
    }
    
    var AddBtn = document.createElement("button");
    AddBtn.innerHTML = "Add";
    AddBtn.setAttribute("onclick","add_case_study()");
    AddRow = add_cell(AddRow,AddBtn,1);
    NewTable.appendChild(AddRow); 
    EditDiv.appendChild(NewTable);
    EditDiv = add_element_to_element(EditDiv,"button",{"onclick":"update_case_studies()"},"Update case studies");    
    scroll_and_blink(EditDiv);                      
}

function remove_case_study(CSName){
    delete CaseStudies[CSName];
    //console.log(CaseStudies);
    update_cs_table(CaseStudies);
}

function add_case_study(){
    var CSName = (document.getElementById("cs_name")).value;
    var CSType = (document.getElementById("cs_type")).value;
    var CSStartDate = (document.getElementById("cs_start_date")).value;
    var CSStartTime = (document.getElementById("cs_start_time")).value;
    var CSEndDate = (document.getElementById("cs_end_date")).value;
    var CSEndTime = (document.getElementById("cs_end_time")).value;
    
    if ((CSName=="") || (CSType=="") || (CSStartDate=="") || (CSStartTime=="")||(CSEndDate=="")||(CSEndTime=="")){
        alert("ERROR: All case study fields must be filled in");
    }else{
        if (RESOURCE_TYPE=="Groups"){
            GroupOrInd = "Group";
        }else{
            GroupOrInd = "Individual";
        }
        CaseStudies[CSName] = {"Case study name":CSName,"Case study type":CSType,"Case study start date":CSStartDate,"Case study start time":CSStartTime,"Case study end date":CSEndDate,"Case study end time":CSEndTime,"Group or individual":GroupOrInd};
        update_cs_table(CaseStudies);
    }
    
}

function update_case_studies(){
    var Details = JSON.stringify({"id":"Case studies","type":"Management","context":"Case studies","value":CaseStudies});
    update_attribute(Details);
}

function remove_case_study_in_included_groups(CSName,IncludedGroups){
    var Confirm = confirm("Are you sure you want to remove this case study");
    if (Confirm==true){
        for (g in IncludedGroups){
            var Group = IncludedGroups[g];
            var CurrentCS = AllGroups[Group];
            delete CurrentCS[CSName];
            HOME_SCS_ATR_CHANGED_FUNC = case_studies_home_sc;
            update_cs(Group,CurrentCS);
        }
    }
   
}
var GIncludedGroups = [];
function add_remove_group_in_cs(){
    var Group = (document.getElementById("group_selector")).value;
    if (GIncludedGroups.includes(Group)){
        GIncludedGroups = GIncludedGroups.filter(function(value, index, arr){ 
            return value != Group;
        });
    }else{
        GIncludedGroups.push(Group);
    }
    (document.getElementById("cs_incl_groups")).innerHTML = JSON.stringify(GIncludedGroups);
}

function add_case_study_to_included_groups(){
    
    var CSName = (document.getElementById("cs_name")).value;
    var CSType = (document.getElementById("cs_type")).value;
    var CSStartDate = (document.getElementById("cs_start_date")).value;
    var CSStartTime = (document.getElementById("cs_start_time")).value;
    var CSEndDate = (document.getElementById("cs_end_date")).value;
    var CSEndTime = (document.getElementById("cs_end_time")).value;
    
    if ((CSName=="") || (CSType=="") || (CSStartDate=="") || (CSStartTime=="")||(CSEndDate=="")||(CSEndTime=="")){
        alert("ERROR: All case study fields must be filled in");
    }else{
        if (GIncludedGroups.length==0){
            alert("At least one group must be included");
        }else{
            for (g in GIncludedGroups){
                var GroupId = GIncludedGroups[g];
                var CaseStudies = AllGroups[GroupId];
                CaseStudies[CSName] = {"Case study name":CSName,"Case study type":CSType,"Case study start date":CSStartDate,"Case study start time":CSStartTime,"Case study end date":CSEndDate,"Case study end time":CSEndTime,"Group or individual":"Group"};
                HOME_SCS_ATR_CHANGED_FUNC = case_studies_home_sc;
                update_cs(GroupId,CaseStudies);
            }
        }
        
        
    }
}

function update_cs(GroupId,CS){
    var Details = JSON.stringify({"id":"Case studies","type":"Management","context":"Case studies","value":CS});
    var Content = JSON.stringify({"id":GroupId,"action":"add","component":"attributes","details":Details});
    var Msg = JSON.stringify({"Request":"edit_resource","Content":Content});
    sendMsg(Msg);
}

function download_case_study_data_in_included_groups(CSName,CSType,GroupsIncluded){
    if (CSType=="FCR"){
        for (g in GroupsIncluded){
            var GroupId = GroupsIncluded[g];
            REQUEST_REPLY_FUNCTION = fcr_data;
            var Details = {"cs_name":CSName};
            var Content = JSON.stringify({"id":GroupId,"type":"Case study data","details":Details});
            var Msg = JSON.stringify({"Request":"request","Content":Content});
            sendMsg(Msg);
        }
       
        //request case study data from group
    }else{
        alert("Excel data cannot be download for this type of case study")
    }
}
var GroupsToView = []
function view_case_study_data_in_included_groups(CSName,CSType,GroupsIncluded){
    GroupsToView = GroupsIncluded;
    if (CSType=="FCR"){
        for (g in GroupsIncluded){
            var GroupId = GroupsIncluded[g];
            REQUEST_REPLY_FUNCTION = view_fcr_data;
            var Details = {"cs_name":CSName};
            var Content = JSON.stringify({"id":GroupId,"type":"Case study data","details":Details});
            var Msg = JSON.stringify({"Request":"request","Content":Content});
            sendMsg(Msg);
        }
    }else{
        alert("Excel data cannot be download for this type of case study")
    }
}
var X_G = [];
var Y_G = [];
var Labels_G = [];
function view_fcr_data(Content){
    var x = [];
    var y = [];
    
    var EditDiv = document.getElementById("home_shortcut_div");
    var GraphDiv = document.getElementById("fcr_graph_div");
    if (GraphDiv!=undefined){
        GraphDiv = remove_all_children(GraphDiv);
    }else{
        GraphDiv = document.createElement("div");
        GraphDiv.id = "fcr_graph_div";
        EditDiv.appendChild(GraphDiv);
    }
    ///////////////////////////////////////////
    var CSName = Content["Case study name"];
    delete Content["Case study name"];
    var ResId = Content.id;
    delete Content["id"];
    
    
    ////console.log(Content);
    var AllEatenSinceW = {};
    var AllDailyIntakeSinceW= {};
    var AllDuration= {};
    var AllFreq= {};
    var AllTotalEaten= {};
    var AllOverallIntake= {};
    var AllGainSinceW= {};
    var AllTotalGain= {};
    var AllDailyGain= {};

    var AllFCRs = {}; //{Date:[{Sheep1:FCRVal,Sheep2:FCRVal...}]}
    var AllOFCRs = {};
    for (Sheep in Content){
        var TotalEaten = 0;
        var EatenSinceLastW = 0;
        var LastWBaseT = 0;
        var LastWeight = "pending";
        var Last24h = {};
        Dets = Content[Sheep];
        var CSStartT = Dets["CS start time"];
        var [StartDate,StartTime] = base_time_to_date_and_time(CSStartT);
        var CSEndT = Dets["CS end time"];
        var [EndDate,EndTime] = base_time_to_date_and_time(CSEndT);
        var StartW = Dets["Starting weight"];
        var StartWT = Dets["Starting weight time"];
        var FirstEatT = 0;
        var FirstMeal = 0;
        var NrMealsSinceW = 0;
        var DurationSinceLastW =0;
        if (Array.isArray(StartWT)){
            var WDate = "-";
            var WTime = "-";
            
        }else{
            var [WDate,WTime] = base_time_to_date_and_time(StartWT);
            LastWeight = StartW;
        }
   
       
        var Data = Dets["Data"];
        var Times = Object.keys(Data);
        Times = Times.map(Number);
        Times.sort(function(a, b) {
            return a - b;
        }); 
        var Intake24 = 0;
        for (t in Times){
            var T = Times[t];
            var key = toString(T);
            var [EntryDate,EntryTime] = base_time_to_date_and_time(T);
            var Entry = Data[key];
            for (Field in Entry){
                var Value = Entry[Field];
                
                var Weight = "";
                var FCR = "";
                var OFCR="";
                var Gain="";
                var TotalGain="";
                var DailyGain="";
                if (FirstEatT == 0){
                    DailyIntake = "";
                    ODailyIntake = "";
                    var Freq = "";
                }else{
                    ODailyIntake = ((TotalEaten-FirstMeal)/(T-FirstEatT))*3600*24;
                    if (LastWBaseT==0){
                        DailyIntake = ODailyIntake;
                        var Freq = ((NrMealsSinceW-1)/(T-FirstEatT))*3600*24;
                    }else{
                        var Freq = (NrMealsSinceW/(T-LastWBaseT))*3600*24,
                        DailyIntake = (EatenSinceLastW/(T-LastWBaseT))*3600*24;
                    }  
                }
                if (Field=="eating (g)"){//EATING
                    NrMealsSinceW++;
                    Eaten = Value["eating (g)"];
                    Duration = Value["duration"];
                    DurationSinceLastW=DurationSinceLastW+Duration;
                    var AvgDurSinceW=DurationSinceLastW/NrMealsSinceW;
                    TotalEaten = TotalEaten+Eaten/1000;
                    
                    EatenSinceLastW = EatenSinceLastW+Eaten/1000;
                    if (FirstEatT == 0){
                        FirstEatT = T;
                        FirstMeal = Eaten/1000;
                    }
                    Last24h[key] = Eaten;
                    Intake24 = 0;
                    for (k in Last24h){
                        var t24 = Number(k);
                        if ((T-t24)>=(3600*24)){
                            delete Last24h[k];
                        }else{
                            Intake24 = Last24h[k]/1000+Intake24;
                        }
                    }
                }else{//WEIGHT
                    Weight = Value;
                    Eaten="";
                    Duration = "";
                    
                    Intake24="";
                    if (Array.isArray(StartW)){
                        StartW = Value;

                    }else{
                        var Gain = Value-LastWeight;
                        if (Gain!=0){
                            var FCR = EatenSinceLastW/Gain;
                        }else{
                            FCR = "infinity";
                        }
                        var DailyGain = (Gain/(T-LastWBaseT))*3600*24;                      
                        var TotalGain = Value-StartW;
                        if (TotalGain!=0){
                            var OFCR = TotalEaten/TotalGain;       
                        }else{
                            OFCR = "infinity";
                        }
                    }    
                    LastWeight=Value;
                    LastWBaseT=T;
                    //Group averages on weigh days
                   
                    AllEatenSinceW = calc_all_fcrs(AllEatenSinceW,Sheep,EntryDate,EatenSinceLastW);
                    
                    AllDailyIntakeSinceW = calc_all_fcrs(AllDailyIntakeSinceW,Sheep,EntryDate,DailyIntake);
                    AllDuration = calc_all_fcrs(AllDuration,Sheep,EntryDate,AvgDurSinceW);
                    AllFreq = calc_all_fcrs(AllFreq,Sheep,EntryDate,Freq);
                    AllTotalEaten = calc_all_fcrs(AllTotalEaten,Sheep,EntryDate,TotalEaten);
                    AllOverallIntake = calc_all_fcrs(AllOverallIntake,Sheep,EntryDate,ODailyIntake);
                    AllGainSinceW = calc_all_fcrs(AllGainSinceW,Sheep,EntryDate,Gain);
                    AllTotalGain = calc_all_fcrs(AllTotalGain,Sheep,EntryDate,TotalGain);
                    AllDailyGain = calc_all_fcrs(AllDailyGain,Sheep,EntryDate,DailyGain);
                    AllFCRs = calc_all_fcrs(AllFCRs,Sheep,EntryDate,FCR);
                    AllOFCRs = calc_all_fcrs(AllOFCRs,Sheep,EntryDate,OFCR);
                }
                
                if (!(Field=="eating (g)")){
                    EatenSinceLastW=0; 
                    NrMealsSinceW=0;
                    DurationSinceLastW=0;
                } 
            }
        }
        
    }
    AllEatenSinceW = get_average(AllEatenSinceW);
    AllDailyIntakeSinceW = get_average(AllDailyIntakeSinceW);
    AllDuration = get_average(AllDuration);
    AllFreq = get_average(AllFreq);
    AllTotalEaten = get_average(AllTotalEaten);
    AllOverallIntake = get_average(AllOverallIntake);
    AllGainSinceW = get_average(AllGainSinceW);
    AllTotalGain = get_average(AllTotalGain);
    AllDailyGain = get_average(AllDailyGain);
    AllFCRs = get_average(AllFCRs);
    AllOFCRs = get_average(AllOFCRs);
    for (a in AllFCRs){
        x.push(a);
        y.push(AllFCRs[a]);
    }
    ////////////////////////////////////
    X_G.push(x);
    Y_G.push(y);
    Labels_G.push(ResId);
    var PlotCont = true;
    for (var gv in GroupsToView){
        if (!(Labels_G.includes(GroupsToView[gv]))){
            PlotCont = false;
            break;
        }
    }
    if (PlotCont==true){
            console.log(X_G);
            console.log(Y_G);
            var data = [];
            for ( var i = 0 ; i < X_G.length ; i++ ) {
              var result = {
                x: X_G[i],
                y: Y_G[i],
                type: 'scatter',
                mode: 'lines',
                name: Labels_G[i],
                line: {
                  color: getRandomColor()
                }
              };
              data.push(result);
            }
            var layout = get_std_layout("FCR since last weighed (kg_eaten/kg_gained) for "+CSName); 
            if (data.length>0){
              Plotly.newPlot('fcr_graph_div', data, layout);
            }
            X_G = [];
            Y_G = [];
            Labels_G = [];
          
    }
   
    

}