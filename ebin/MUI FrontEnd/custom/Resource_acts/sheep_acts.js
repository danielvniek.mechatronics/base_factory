function Sheep_get_RAs(){
    return ["Relocate (RA)","Medicate (RA)"];
}
function Sheep_update_s1_div(Type){
    S1Div = document.getElementById("s1_div");
    S1Div = remove_all_children(S1Div);
    if (Type=="Relocate (RA)"){
        S1Div = document.getElementById("s1_div");
        S1Div.setAttribute("class","two-col");
        S1Div = add_element_to_element(S1Div,"label",{},"Location type:");
        S1Div = add_element_to_element(S1Div,"label",{},"Location id");
        var LocSelect= document.createElement("select");
        LocSelect.setAttribute("onchange","location_type_changed(this)");
        LocSelect.setAttribute("id","loc_type");
        var List = ["Houses","Groups"];
        for (e in List){
            LocSelect = add_element_to_element(LocSelect,"option",{"value":List[e]},List[e]);   
        }
        S1Div.appendChild(LocSelect);
        var IdSelect= document.createElement("select");
        IdSelect.setAttribute("id","location_id");
        S1Div.appendChild(IdSelect);
        CUSTOM_EXISTING_RESOURCES_FUNCTION = sheep_location_resources;
        S1DATA = new_sheep_relocate_act;
        var Components = JSON.stringify({"atr":"all"});
        var SearchString = JSON.stringify({"SearchBy":"types","SearchParameters":["Houses","Groups"],"components":Components});
        var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
        sendMsg(Msg);
    }
}
var HOUSES = [];
var GROUPS = [];
function sheep_location_resources(Content){
    CUSTOM_EXISTING_RESOURCES_FUNCTION = null;
    HOUSES = [];
    GROUPS = [];
    
    for (key in Content){
        var Resource = Content[key];
        var Atrs = Resource.atr;
        if (Resource.type=="Houses"){
            if((Atrs["Group"]).value!="none"){
                HOUSES.push(Resource.id+" (unavailable - occupied by group)");
            }else if ((Atrs["Number of sheep"]).value+(Atrs["Number of incoming sheep"]).value>=(Atrs["Sheep limit"]).value){
                HOUSES.push(Resource.id+" (unavailable - full)")
            }else{
                HOUSES.push(Resource.id);
            }
        }else{
            var HouseId = (Atrs["House"]).value;
            if(HouseId!="none"){
                for (k in Content){
                    var R = Content[k];
                    //console.log(HouseId);
                    //console.log(R.id);
                    if (R.id==HouseId){
                        //console.log("equal");
                        var Ratr = R.atr;
                        if ((Ratr["Number of sheep"]).value+(Ratr["Number of incoming sheep"]).value>=(Ratr["Sheep limit"]).value){
                            GROUPS.push(Resource.id+" (unavailable - group house is full)");
                        }else{
                            GROUPS.push(Resource.id);
                        }
                        break;
                    }
                }
            }else{
                //console.log("pushing group");
                GROUPS.push(Resource.id);
            }
        }
        
    }
    var TypeSelect = document.getElementById("loc_type");
    var IdSelect = document.getElementById("location_id");
    IdSelect = remove_all_children(IdSelect);
    if (TypeSelect.value=="Houses"){
        var List = HOUSES;
    }else{
        var List = GROUPS;
    }
    for (e in List){
        //console.log("adding element");
        IdSelect = add_element_to_element(IdSelect,"option",{"value":List[e]},List[e]);
    }
}
function location_type_changed(TypeSelect){
    var IdSelect = document.getElementById("location_id");
    IdSelect = remove_all_children(IdSelect);
    if (TypeSelect.value=="Houses"){
        var List = HOUSES;
    }else{
        var List = GROUPS;
    }
    for (e in List){
        IdSelect = add_element_to_element(IdSelect,"option",{"value":List[e]},List[e]);
    }
}

function new_sheep_relocate_act(){

    var LocId =  (document.getElementById("location_id")).value;
    if (LocId==""){
        alert("ERROR: No valid location selected");
    }else{
        if (LocId.includes(" (unavailable -")){
            var Cancel = confirm("Are you sure you want to choose this location. It is unavailable and the activity will just fail when the time comes. Click cancel to cancel the activity addition.");
            if (Cancel==true){
                var Index = LocId.indexOf(" (unavailable -");
                LocId = LocId.substring(0,Index);
                return {"Location ID":LocId};
            }
        }else{
            return {"Location ID":LocId};
        }
    }
    
    
}