function Groups_get_RAs(){
    return ["Relocate (RA)","Medicate (RA)"];
}
function Groups_update_s1_div(Type){
    S1Div = document.getElementById("s1_div");
    S1Div = remove_all_children(S1Div);
    if (Type=="Relocate (RA)"){
        S1Div = document.getElementById("s1_div");
        S1Div = add_element_to_element(S1Div,"label",{},"Location id");
        var IdSelect= document.createElement("select");
        IdSelect.setAttribute("id","location_id");
        S1Div.appendChild(IdSelect);
        CUSTOM_EXISTING_RESOURCES_FUNCTION = groups_location_resources;
        S1DATA = new_group_relocate_act;
        var SearchString = JSON.stringify({"SearchBy":"types","SearchParameters":["Houses"]});
        var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
        sendMsg(Msg);
    }
}

function groups_location_resources(Content){
    CUSTOM_EXISTING_RESOURCES_FUNCTION = null;
    Houses = [];
    
    for (key in Content){
        var Resource = Content[key];
        if (Resource.type=="Houses"){
             Houses.push(Resource.id);
        }
        
    }
    var IdSelect = document.getElementById("location_id");
    IdSelect = remove_all_children(IdSelect);
    for (e in Houses){
        IdSelect = add_element_to_element(IdSelect,"option",{"value":Houses[e]},Houses[e]);
    }
}

function new_group_relocate_act(){

    var LocId =  (document.getElementById("location_id")).value;
    if (LocId==""){
        alert("ERROR: No valid location selected");
    }else{
         return {"Location ID":LocId};
    }
    
    
}