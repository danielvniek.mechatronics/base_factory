function Observers_get_RAs(){
    if (RESOURCE_ID=="RFID Sheep Scale"){
        return ["Deliver data (SPA)"];
    }else{
        return [];
    }
}
function Observers_update_s1_div(Type){
    if (RESOURCE_ID=="RFID Sheep Scale"){
        var S1Div = document.getElementById("s1_div");
        var FileSelector = document.createElement("input")
        FileSelector.setAttribute("type","file")
        FileSelector.addEventListener('change',function() {
            var fr=new FileReader();
            fr.onload=function(){
                //console.log(fr.result);
                var data = csv_to_object(fr.result);
                //console.log(data);
                for (i in data){
                    var Entry = data[i];
                    var Identifier = Entry["EID"].replace(" ","");
                    var EDate = Entry["Date"].trim();
                    var ETime = Entry["Time"].trim();
                    var Value = Number(Entry["Weight"]);
                    //console.log(typeof(Value));
                    if ((EDate!=undefined)&&(ETime!=undefined)&&(Value!=undefined)){
                        var DataMap = {"Weight":{"Date":EDate,"Time":ETime,"Value":Value}};
                        var S1Data = {"Identifier":Identifier,"DataMap":DataMap};
                        var today = new Date();
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();
                        var hh = String(today.getHours()).padStart(2, '0');
                        var min = String(today.getMinutes()).padStart(2, '0'); 
                        var ss = String(today.getSeconds()).padStart(2, '0');

                        var CurrentDate = dd + '/' + mm + '/' + yyyy;
                        var CurrentTime = hh+':'+min+':'+ss;
                        var Details = JSON.stringify({"type":"Deliver data (SPA)","date":CurrentDate,"time":CurrentTime,"data":S1Data});
                        
                        update_schedule(Details);
                    }

                }
                alert("Sheep weights shared with each sheep")
            }
            
            fr.readAsText(this.files[0]);
        })
        S1Div.appendChild(FileSelector)
    }
    
}