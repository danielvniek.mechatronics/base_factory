
var USE_INFO = {"wh_id":"","id":"","amount":0,"date":"2021-01-01","time":"00:00:00","handlers":[],"receivers":[],"empty":false}
function get_use_details(ProdId){
    document.getElementById("u_id").innerHTML = ProdId;
    USE_INFO.id = ProdId;
    (document.getElementById("prod_use_div")).style.display = "block";
}
function create_use_div(){
    var UseDiv = document.createElement("div");
    UseDiv.style.display = "none";
    UseDiv.setAttribute("id","prod_use_div");
    
    var Div = document.createElement("div"); 
    Div.setAttribute("class","four-col");

    Div = add_element_to_element(Div,"h3",{},"Enter usage info:");
    Div = add_element_to_element(Div,"div",{},null);
    Div = add_element_to_element(Div,"div",{},null);
    Div = add_element_to_element(Div,"div",{},null);
    
    Div = add_element_to_element(Div,"label",{},"Product ID: ");
    Div = add_element_to_element(Div,"label",{"id":"u_id"},"");
    Div = add_element_to_element(Div,"div",{},null);
    Div = add_element_to_element(Div,"div",{},null);

    Div = add_element_to_element(Div,"label",{},"Date: ");
    Div = add_element_to_element(Div,"input",{"type":"date","id":"u_date"},USE_INFO.date);
    Div = add_element_to_element(Div,"label",{},"Time: ");
    Div = add_element_to_element(Div,"input",{"type":"time","id":"u_time"},USE_INFO.time);

    Div = add_element_to_element(Div,"label",{},"Handled by:");
    var HandledDiv = document.createElement("div");
    var HBtnDiv = document.createElement("div");
    HBtnDiv.setAttribute("class","two-col");
    HBtnDiv = add_element_to_element(HBtnDiv,"button",{"onclick":"add_to_list_in_object(USE_INFO,'handlers','handled_by_par','Enter handler id:')"},"Add");
    HBtnDiv = add_element_to_element(HBtnDiv,"button",{"onclick":"remove_from_list_in_object(USE_INFO,'handlers','handled_by_par','Enter id of handler to remove:')"},"Remove");
    var ParText = list_to_paragraph(USE_INFO.handlers);
    HandledDiv = add_element_to_element(HandledDiv,"p",{"id":"handled_by_par"},ParText);
    HandledDiv.appendChild(HBtnDiv);
    Div.appendChild(HandledDiv);

    Div = add_element_to_element(Div,"label",{},"Received by:");
    var RecDiv = document.createElement("div");
    var RBtnDiv = document.createElement("div");
    RBtnDiv.setAttribute("class","two-col");
    RBtnDiv = add_element_to_element(RBtnDiv,"button",{"onclick":"add_to_list_in_object(USE_INFO,'receivers','received_by_par','Enter receiver id:')"},"Add");
    RBtnDiv = add_element_to_element(RBtnDiv,"button",{"onclick":"remove_from_list_in_object(USE_INFO,'receivers','received_by_par','Enter id of receiver to remove:')"},"Remove");
    var ParText = list_to_paragraph(USE_INFO.receivers);
    RecDiv = add_element_to_element(RecDiv,"p",{"id":"received_by_par"},ParText);
    RecDiv.appendChild(RBtnDiv);
    Div.appendChild(RecDiv);

    Div = add_element_to_element(Div,"label",{},"Amount used:");
    Div = add_element_to_element(Div,"input",{"type":"text","id":"u_amount"},(USE_INFO.amount).toString());
    Div = add_element_to_element(Div,"label",{},"Resource is empty: ");
    Div = add_element_to_element(Div,"input",{"type":"checkbox","id":"u_empty"});

    Div = add_element_to_element(Div,"button",{"onclick":"use_product()"},"Use");
    Div = add_element_to_element(Div,"button",{"onclick":"hide_div('prod_use_div')"},"Cancel");

    UseDiv.appendChild(Div);
    return UseDiv;
}

function use_product(){
    var InputList = ["date","time"];
    var SendMsg = true;
    var Amount = document.getElementById("u_amount").value;
    if (Amount=="all"){
        Amount = "all";
        USE_INFO["empty"] = true;
    }else{
        USE_INFO["empty"] = document.getElementById("u_empty").checked;
        SendMsg = false;
        Amount = Number(Amount);
        if (Amount!=NaN){
            if (Amount>=0){
                SendMsg = true;
            }else{
                alert("ERROR: Amount must be a number >= 0");
            }
        }else{
            alert("ERROR: Amount must be a number >= 0");
        }
    }
    USE_INFO["amount"] = Amount;
    for (e in InputList){
        var ElId = "u_"+InputList[e];
        var El = document.getElementById(ElId);
        var Val = El.value;
        if ((Val == "") || (Val==null)){
            alert("ERROR: Data field "+InputList[e]+" has no valid entry");
            SendMsg = false;
            break;
        }
        USE_INFO[InputList[e]] = Val;
    }
    
    
    
    if (SendMsg==true){
        USE_INFO.wh_id = RESOURCE_ID;
        var ContentObj = {};
        for (key in USE_INFO){
            ContentObj[key] = USE_INFO[key];
        }
        ContentObj["request"] = "use";
        var Content = JSON.stringify(ContentObj);
        var Msg = JSON.stringify({"Request":"warehouse_functions","Content":Content});
        sendMsg(Msg);
        (document.getElementById("prod_use_div")).style.display = "none";
    }
}

function add_to_list_in_object(Obj,Field,ParId,PrompText){
    var NewEntry = prompt(PrompText,"");
    if (NewEntry!=null){
        if (NewEntry==""){
            alert("ERROR: No valid entry");
        }else{
            var List = Obj[Field];
            if (List.includes(NewEntry)){
                alert("Already added");
            }else{
                List.push(NewEntry);
                var ParText = list_to_paragraph(List);
                Obj[Field] = List;
                document.getElementById(ParId).innerHTML = ParText;
            }
        }
    }
}


function remove_from_list_in_object(Obj,Field,ParId,PrompText){
    var NewEntry = prompt(PrompText,"");
    if (NewEntry!=null){
        if (NewEntry==""){
            alert("ERROR: No valid entry");
        }else{
            var List = Obj[Field];
            var filtered = List.filter(function(value, index, arr){ 
                return value != NewEntry;
            });
            List = filtered;
            var ParText = list_to_paragraph(List);
            Obj[Field] = List;
            document.getElementById(ParId).innerHTML = ParText;
        }
    }
}



