function case_studies_home_sc(){
    GIncludedGroups = [];
    var SearchParameters = ["Groups"];
    var Components = JSON.stringify({"atr":{"types":["Case studies"]}});
    var SearchString = JSON.stringify({"SearchBy":"types","SearchParameters":SearchParameters,"components":Components});
    var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
    CUSTOM_EXISTING_RESOURCES_FUNCTION = case_study_groups;
    sendMsg(Msg);
}

var AllGroups = {};
function case_study_groups(Content){
    var GroupCSs = {};
    for (key in Content){
        var Resource = Content[key];
        var Atrs = Resource.atr;
        var CS = (Atrs["Case studies"]).value;
        var ResourceId = Resource.id;
        AllGroups[ResourceId] = CS;
        for (c in CS){
            var CSDets = CS[c];
            var Dets = GroupCSs[c];
            
            if (Dets!=undefined){
                var Included = Dets.included;
                Included.push(ResourceId);
                Dets.included = Included;
            }else{
                Dets = {};
                var Included = [ResourceId];
                Dets.included = Included;
                Dets["Case study start date"] = CSDets["Case study start date"];
                Dets["Case study start time"] = CSDets["Case study start time"];
                Dets["Case study end date"] = CSDets["Case study end date"];
                Dets["Case study end time"] = CSDets["Case study end time"];
                Dets["Case study type"] = CSDets["Case study type"];
            }
            GroupCSs[c] = Dets;
        }
    }
    for (c in GroupCSs){
        var Dets = GroupCSs[c];
        var Included = Dets.included;
        var Excluded = [];
        for (a in AllGroups){
            var Id = a;
            if (!(Included.includes(Id))){
                Excluded.push(Id);
            }
        }
        Dets.excluded = Excluded;
        GroupCSs[c] = Dets;
    }
    var EditDiv = document.getElementById("home_shortcut_div");
    EditDiv = remove_all_children(EditDiv);
    EditDiv = add_element_to_element(EditDiv,"h4",{},"Group case studies");
    var NewTable = document.createElement("table");
    NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
    NewTable.setAttribute("id","cs_tab");
    NewTable.setAttribute("class","tab");
    var SubH = document.createElement("tr");
    SubH = add_head(SubH,"Case study name",1);
    SubH = add_head(SubH,"Case study type",1);
    SubH = add_head(SubH,"Case study start date",1);
    SubH = add_head(SubH,"Case study start time",1);
    SubH = add_head(SubH,"Case study end date",1);
    SubH = add_head(SubH,"Case study end time",1);
    SubH = add_head(SubH,"Groups included",1);
    SubH = add_head(SubH,"Groups excluded",1);
    SubH = add_head(SubH," ",1);
    SubH = add_head(SubH," ",1);
    SubH = add_head(SubH," ",1);
    NewTable.appendChild(SubH);
    for (a in GroupCSs){
        var CSDets = GroupCSs[a];
        var Row = document.createElement("tr");
        Row = add_cell(Row,a,1);
        Row = add_cell(Row,CSDets["Case study type"],1);
        Row = add_cell(Row,CSDets["Case study start date"],1);
        Row = add_cell(Row,CSDets["Case study start time"],1);
        Row = add_cell(Row,CSDets["Case study end date"],1);
        Row = add_cell(Row,CSDets["Case study end time"],1);
        Row = add_cell(Row,CSDets["included"],1);
        Row = add_cell(Row,CSDets["excluded"],1);
        var DBtn = document.createElement("button");
        DBtn.innerHTML = "Download data (excel)";
        DBtn.setAttribute("onclick","download_case_study_data_in_included_groups('"+a+"','"+CSDets["Case study type"]+"',"+JSON.stringify(CSDets["included"])+")");
        Row = add_cell(Row,DBtn,1);
        var VBtn = document.createElement("button");
        VBtn.innerHTML = "View variables of interest";
        VBtn.setAttribute("onclick","view_case_study_data_in_included_groups('"+a+"','"+CSDets["Case study type"]+"',"+JSON.stringify(CSDets["included"])+")");
        Row = add_cell(Row,VBtn,1);
        var RemBtn = document.createElement("button");
        RemBtn.innerHTML = "Remove";
        RemBtn.setAttribute("onclick","remove_case_study_in_included_groups('"+a+"',"+JSON.stringify(CSDets["included"])+")");
        Row = add_cell(Row,RemBtn,1);

        NewTable.appendChild(Row); 
    } 
    var Inputs = ["text","select","date","time","date","time"];
    var Ids = ["cs_name","cs_type","cs_start_date","cs_start_time","cs_end_date","cs_end_time"];
    var AddRow = document.createElement("tr");
    for (i in Inputs)   {
        if (Inputs[i]=="select"){
            var I = document.createElement("select");
            I = add_element_to_element(I,"option",{"value":"FCR"},"FCR");   
        }else{
            var I = document.createElement("input");
            I.setAttribute("type",Inputs[i]);
        }
        I.setAttribute("id",Ids[i]);
        AddRow = add_cell(AddRow,I,1);
    } 
    var InclDiv = document.createElement("div");
    
    
    if (Object.keys(AllGroups).length === 0){
        InclDiv = add_element_to_element(InclDiv,"p",{},"No groups to add");
    }else{
        var GroupSel = document.createElement("select");
        GroupSel.id = "group_selector";
        for (g in AllGroups){
            GroupsSel= add_element_to_element(GroupSel,"option",{"value":g},g);   
        }
        InclDiv.appendChild(GroupSel);
        InclDiv = add_element_to_element(InclDiv,"button",{"onclick":"add_remove_group_in_cs()"},"Add/Remove");
        InclDiv = add_element_to_element(InclDiv,"p",{"id":"cs_incl_groups"},"[]");
    }
    
   
    AddRow = add_cell(AddRow,InclDiv,1);
    var AddBtn = document.createElement("button");
    AddBtn.innerHTML = "Create new case study";
    AddBtn.setAttribute("onclick","add_case_study_to_included_groups()");
    AddRow = add_cell(AddRow,"",1);
    AddRow = add_cell(AddRow,"",1);
    AddRow = add_cell(AddRow,"",1);
    AddRow = add_cell(AddRow,AddBtn,1);
    NewTable.appendChild(AddRow); 
    EditDiv.appendChild(NewTable);
}
var HOME_SCS_ATR_CHANGED_FUNC = null;
function home_scs_atr_changed(){
    if (HOME_SCS_ATR_CHANGED_FUNC!=null){
        HOME_SCS_ATR_CHANGED_FUNC();
        HOME_SCS_ATR_CHANGED_FUNC = null;
    }
}


//quick setup
var QuickSetupData = {};
var QuickSetupGroups = {};
var QuickSetupStep = 0;
var NrMessagesLeft = 0;
function setup_continue(Ignore){
    NrMessagesLeft = NrMessagesLeft-1;
    if (NrMessagesLeft==0){
        QuickSetupStep = QuickSetupStep+1;
        if (QuickSetupStep==1){
            sleep(2000);
            for (i in QuickSetupData){//edit sheep attributes - including house, group and feeders
                var Entry = QuickSetupData[i];
                var Identifier = Entry["EID"].replace(" ","");
                var SheepID = Entry["VID"].trim();
                var Breed = Entry["Breed"].trim();
                var Group = Entry["Group"].trim();  
                var Details = JSON.stringify({"id":"Breed","type":"Personal","context":"Dorper, Merino etc.","value":Breed});
                update_adr_attribute(id_to_adr(SheepID),Details);
                var Details = JSON.stringify({"id":"EID","type":"Personal","context":"Electronic ID of the tag attached to this resource","value":Identifier});
                update_adr_attribute(id_to_adr(SheepID),Details);
                //house, group and feeders
                var Details = JSON.stringify({"id":"House","type":"Relational","context":"Base ID","value":"House "+Group,"value_conv":"binary_to_list"});
                update_adr_attribute(id_to_adr(SheepID),Details);
                var Details = JSON.stringify({"id":"Group","type":"Relational","context":"Base ID","value":"Group "+Group,"value_conv":"binary_to_list"});
                update_adr_attribute(id_to_adr(SheepID),Details);
                var Details = JSON.stringify({"id":"Feeders","type":"Relational","context":"Base ID","value":["Feeder "+Group]});
                update_adr_attribute(id_to_adr(SheepID),Details);
            }
            [SchedDate,SchedTime]=date_to_date_and_time_string(new Date());
            var NrGroups = 0;
            for (g in QuickSetupGroups){//relate houses, groups and feeders - as well as sheep in each
                //Feeder
                NrGroups = NrGroups+1;
                var Details = JSON.stringify({"id":"House","type":"Relational","context":"Base ID","value":"House "+g,"value_conv":"binary_to_list"});
                update_adr_attribute("feeder_"+g,Details);
                //House
                var Details = JSON.stringify({"id":"Feeders","type":"Relational","context":"Base ID","value":["Feeder "+g]});
                update_adr_attribute("house_"+g,Details);
                var Details = JSON.stringify({"id":"Group","type":"Relational","context":"Base ID","value":"Group "+g,"value_conv":"binary_to_list"});
                update_adr_attribute("house_"+g,Details);
                var Details = JSON.stringify({"id":"Sheep","type":"Relational","context":"Base ID","value":QuickSetupGroups[g],"value_conv":"bin_list_to_str_list"});
                update_adr_attribute("house_"+g,Details);
                var Details = JSON.stringify({"id":"Number of sheep","type":"Relational","context":"integer","value":(QuickSetupGroups[g]).length});
                update_adr_attribute("house_"+g,Details);
                //Group
                var Details = JSON.stringify({"id":"Feeders","type":"Relational","context":"Base ID","value":["Feeder "+g]});
                update_adr_attribute("group_"+g,Details);
                var Details = JSON.stringify({"id":"House","type":"Relational","context":"Base ID","value":"House "+g,"value_conv":"binary_to_list"});
                update_adr_attribute("group_"+g,Details);
                var Details = JSON.stringify({"id":"Sheep","type":"Relational","context":"Base ID","value":QuickSetupGroups[g],"value_conv":"bin_list_to_str_list"});
                update_adr_attribute("group_"+g,Details);
                var Details = JSON.stringify({"id":"Number of sheep","type":"Relational","context":"integer","value":(QuickSetupGroups[g]).length});
                update_adr_attribute("group_"+g,Details);
            }
            NrMessagesLeft = QuickSetupData.length*5 + NrGroups*9;
        }else{
            if (QuickSetupStep==2){
                var Content = JSON.stringify({"id":"RFID Sheep Feeder","type":"Sensors","twin_type":"base"});
                var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                sendMsg(Msg);
                var Content = JSON.stringify({"id":"RFID Sheep Scale","type":"Sensors","twin_type":"base"});
                var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                sendMsg(Msg);
                NrMessagesLeft = 2;
            }else{
                ACCEPT_MESSAGES = true;
                var SCDiv = document.getElementById("home_shortcut_div");
                SCDiv = remove_all_children(SCDiv);
                alert("Setup done!");

            }
        }
    }
    
}
function quick_setup(){
    CUSTOM_EXISTING_RESOURCES_FUNCTION = check_if_empty_for_quick_setup;
    var SearchString = JSON.stringify({"SearchBy":"all","SearchParameters":[]});
    var Msg = JSON.stringify({"Request":"search_resources","Content":SearchString});
    sendMsg(Msg);
}
function check_if_empty_for_quick_setup(Content){
    if (Object.keys(Content).length === 0){
        QuickSetupStep = 0;
        Progress = 0;
        var SCDiv = document.getElementById("home_shortcut_div");
        SCDiv = remove_all_children(SCDiv);
        SCDiv = add_element_to_element(SCDiv,"p",{},"Select a csv file from the RFID Sheep Scale and all the sheep in the file will be added to the system in the groups indicated in the file. <br>Groups, houses and a feeder for each house will also automatically be created.<br> The breed and eid of each sheep will also be updated as indicated in the file.<br>The weights in the file will be ignored ")
        var FileSelector = document.createElement("input")
        FileSelector.setAttribute("type","file")
        FileSelector.addEventListener('change',function() {
                
            var fr=new FileReader();
            fr.onload=function(){
                try {
                    var data = csv_to_object(fr.result);
                    //console.log("data");
                    //console.log(data);
                    QuickSetupData = data;
                    //console.log(data);
                    var SetupGroups = {};
                    ACCEPT_MESSAGES = setup_continue;
                
                   
                    for (i in data){//add all sheep
                        var Entry = data[i];
                        var SheepID = Entry["VID"].trim();
                        var Content = JSON.stringify({"id":SheepID,"type":"Sheep","twin_type":"base"});
                        var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                        sendMsg(Msg);
                        var Group = Entry["Group"].trim();
                        if (SetupGroups[Group]==undefined){
                            SetupGroups[Group] = [SheepID];
                        }else{
                            var Existing = SetupGroups[Group];
                            Existing.push(SheepID);
                            SetupGroups[Group] = Existing;
                        }
                    }
                    var NrGroups = 0;
                    for (g in SetupGroups){//create new groups, houses and feeders
                        NrGroups = NrGroups+1;
                        var Content = JSON.stringify({"id":"Group"+" "+g,"type":"Groups","twin_type":"base"});
                        var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                        sendMsg(Msg);
                        var Content = JSON.stringify({"id":"House"+" "+g,"type":"Houses","twin_type":"base"});
                        var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                        sendMsg(Msg);
                        var Content = JSON.stringify({"id":"Feeder"+" "+g,"type":"Feeders","twin_type":"base"});
                        var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                        sendMsg(Msg);
                    }
                     //add food warehouse
                     var CustomArgs = JSON.stringify({"Product type":"Sheep food","Unit":"g"});
                     var Content = JSON.stringify({"id":"Sheep Food WH","type":"Warehouses","twin_type":"basic","folder_name":"warehouses","startup_type":"worker","custom_args":CustomArgs});
                     var Msg = JSON.stringify({"Request":"new_resource","Content":Content});
                     sendMsg(Msg);
                    QuickSetupGroups = SetupGroups;
                    NrMessagesLeft = data.length + NrGroups*3+1;  
                    var SCDiv = document.getElementById("home_shortcut_div");
                    SCDiv = remove_all_children(SCDiv);  
                    SCDiv = add_element_to_element(SCDiv,"h4",{},"Busy setting up Stellenbosch Sheep Farm Resources...");
            
                } catch (error) {
                    alert("ERROR: The file you selected did not have the required data")
                }
            }  
            
            fr.readAsText(this.files[0]);``
        })
        SCDiv.appendChild(FileSelector)
    }else{
        alert("ERROR: This shortcut can only be used when the sheep farm is empty");
    }
    
}

function rfid_scale_sc(){
    var S1Div = document.getElementById("home_shortcut_div");
    var FileSelector = document.createElement("input")
    FileSelector.setAttribute("type","file")
    FileSelector.addEventListener('change',function() {
              
        var fr=new FileReader();
        fr.onload=function(){
            //console.log(fr.result);
            try {
                var data = csv_to_object(fr.result);
                //console.log(data);
                for (i in data){
                    var Entry = data[i];
                    var Identifier = Entry["EID"];
                    var EDate = Entry["Date"].trim();
                    var ETime = Entry["Time"].trim();
                    var Value = Number(Entry["Weight"]);
                    //console.log(typeof(Value));
                    if ((EDate!=undefined)&&(ETime!=undefined)&&(Value!=undefined)){
                        var DataMap = {"Weight":{"Date":EDate,"Time":ETime,"Value":Value}};
                        var S1Data = {"Identifier":Identifier,"DataMap":DataMap};
                        var today = new Date();
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();
                        var hh = String(today.getHours()).padStart(2, '0');
                        var min = String(today.getMinutes()).padStart(2, '0'); 
                        var ss = String(today.getSeconds()).padStart(2, '0');
    
                        var CurrentDate = dd + '/' + mm + '/' + yyyy;
                        var CurrentTime = hh+':'+min+':'+ss;
                        var Details = JSON.stringify({"type":"Deliver data (SPA)","date":CurrentDate,"time":CurrentTime,"data":S1Data});
                        
                        update_adr_schedule("rfid_sheep_scale",Details);
                    }
    
                }
            } catch (error) {
                alert("ERROR: The file you selected did not have the required data")
            }
            var data = csv_to_object(fr.result);
            //console.log(data);
            for (i in data){
                var Entry = data[i];
                var Identifier = Entry["EID"].replace(" ","");
                var EDate = Entry["Date"].trim();
                var ETime = Entry["Time"].trim();
                var Value = Number(Entry["Weight"]);
                //console.log(typeof(Value));
                if ((EDate!=undefined)&&(ETime!=undefined)&&(Value!=undefined)){
                    var DataMap = {"Weight":{"Date":EDate,"Time":ETime,"Value":Value}};
                    var S1Data = {"Identifier":Identifier,"DataMap":DataMap};
                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = today.getFullYear();
                    var hh = String(today.getHours()).padStart(2, '0');
                    var min = String(today.getMinutes()).padStart(2, '0'); 
                    var ss = String(today.getSeconds()).padStart(2, '0');

                    var CurrentDate = dd + '/' + mm + '/' + yyyy;
                    var CurrentTime = hh+':'+min+':'+ss;
                    var Details = JSON.stringify({"type":"Deliver data (SPA)","date":CurrentDate,"time":CurrentTime,"data":S1Data});
                    
                    update_adr_schedule("rfid_sheep_scale",Details);
                }

            }
            alert("Sheep weights shared with each sheep")
        }
          
        fr.readAsText(this.files[0]);
    })
    S1Div = remove_all_children(S1Div);
    S1Div.appendChild(FileSelector)
}

