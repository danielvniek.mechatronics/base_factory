function get_all_products(){
    var IncludeEmpty = document.getElementById("include_empty").checked;
    var Content = JSON.stringify({"wh_id":RESOURCE_ID,"request":"get_del_or_pop_all_products","get_del_or_pop":"get","include_empty":IncludeEmpty});
    var Msg = JSON.stringify({"Request":"warehouse_functions","Content":Content});
    sendMsg(Msg);
}

function get_group(){
    var Group = prompt("Enter the group name you want to see:","");
    if (Group!=null){
        if (Group==""){
            alert("ERROR: No valid group was entered");
        }else{
            var IncludeEmpty = document.getElementById("include_empty").checked;
            var Content = JSON.stringify({"wh_id":RESOURCE_ID,"request":"get_del_or_pop_prods_by_groups","get_del_or_pop":"get","groups":[Group],"include_empty":IncludeEmpty});
            var Msg = JSON.stringify({"Request":"warehouse_functions","Content":Content});
            sendMsg(Msg);
        }
    }
}

var PRODUCT_DATA = {"wh_id":RESOURCE_ID,"id":"","group":"","suppliers":[],"production_date":"2021-01-01","delivery_date":"2021-01-01","expiration_date":"2021-01-01","promised_content":{},"analysed_content":{},"amount_used":0,"amount_left":0,"location_log":{},"temperature_log":{},"empty":false};
function new_product(){
    (document.getElementById("product_input_div")).style.display = "block";
    (document.getElementById("prod_create_or_edit_btn")).innerHTML = "Create";
    NEW_PRODUCT_OR_EDIT = "new_product";
}

function edit_product(ProdDetailsJsonString){
    ProdDetails = JSON.parse(ProdDetailsJsonString);
    //console.log(ProdDetails);
    var InputList = ["id","group","production_date","delivery_date","expiration_date","amount_used","amount_left"];
    for (e in InputList){
        var ElId = "p_"+InputList[e];
        var El = document.getElementById(ElId);
        El.value = (ProdDetails[InputList[e]]).toString();
    }
    var PromContent = ProdDetails.promised_content;
    var ParText = obj_to_paragraph(PromContent);
    document.getElementById("p_promised_content").innerHTML = ParText;
    PRODUCT_DATA.promised_content = PromContent;

    var AnalContent = ProdDetails.analysed_content;
    ParText = obj_to_paragraph(AnalContent);
    document.getElementById("p_analysed_content").innerHTML = ParText;
    PRODUCT_DATA.analysed_content = AnalContent;

    var Suppliers = ProdDetails.suppliers;
    ParText = list_to_paragraph(Suppliers);
    document.getElementById("p_suppliers").innerHTML = ParText;
    PRODUCT_DATA.suppliers = Suppliers;

    (document.getElementById("product_input_div")).style.display = "block";
    (document.getElementById("prod_create_or_edit_btn")).innerHTML = "Edit";
    
    NEW_PRODUCT_OR_EDIT = "edit";
}
var NEW_PRODUCT_OR_EDIT = "new_product";
function create_or_edit_product(){
    var InputList = ["id","group","production_date","delivery_date","expiration_date","amount_used","amount_left"];
    var SendMsg = true;
    for (e in InputList){
        var ElId = "p_"+InputList[e];
        var El = document.getElementById(ElId);
        var Val = El.value;
        if ((Val == "") || (Val==null)){
            alert("ERROR: Data field "+InputList[e]+" has no valid entry");
            SendMsg = false;
            break;
        }
        if ((InputList[e]=="amount_used")||(InputList[e]=="amount_left")){
            SendMsg = false;
            Val = Number(Val);
            if (Val!=NaN){
                if (Val>=0){
                    //console.log(Val);
                    SendMsg = true;
                }else{
                    alert("ERROR: Amount must be a number >= 0");
                    break;
                }
            }else{
                alert("ERROR: Amount must be a number >= 0");
                break;
            } 
        }
        PRODUCT_DATA[InputList[e]] = Val;
    }
    if (SendMsg==true){
        PRODUCT_DATA["wh_id"] = RESOURCE_ID;
        if (PRODUCT_DATA["amount_left"]<=0){
            PRODUCT_DATA["empty"] = true;
        }else{
            PRODUCT_DATA["empty"] = false;
        }
        var ContentObj = {};
        for (key in PRODUCT_DATA){
            ContentObj[key] = PRODUCT_DATA[key];
        }
        ContentObj["request"] = NEW_PRODUCT_OR_EDIT;
        var Content = JSON.stringify(ContentObj);
        //console.log(Content);
        var Msg = JSON.stringify({"Request":"warehouse_functions","Content":Content});
        sendMsg(Msg);
        (document.getElementById("product_input_div")).style.display = "none";
    }
    
}  

function hide_div(DivId){
    (document.getElementById(DivId)).style.display = "none";
}
 
function add_new_content(Field){
    var ContentName = prompt("Enter the name of the content you want to add:","");
    if (ContentName!=null){
        if (ContentName==""){
            alert("ERROR: No valid content name was entered");
        }else{
            var ContentPerc = prompt("Enter the % of "+ContentName+" present in this product:","");
            if (ContentPerc!=null){
                var Nr = Number(ContentPerc);
                if (Nr!=NaN){
                    if ((Nr>=0) && (Nr<=100)){
                        var ParText = "";
                        var Obj = PRODUCT_DATA[Field]
                        Obj[ContentName] = Nr;
                        for (key in Obj){
                            ParText += key;
                            ParText += " : ";
                            ParText += Obj[key].toString();
                            ParText += "<br>";
                        }
                        PRODUCT_DATA[Field] = Obj;
                        var ParId = "p_"+Field;
                        var Elem = document.getElementById(ParId);
                        Elem.innerHTML = ParText;
                    }else{
                        alert("ERROR: Only numbers between (and including) 0 and 100 are allowed. Numbers may have decimals.");
                    }
                }else{
                    alert("ERROR: Only numbers between (and including) 0 and 100 are allowed. Numbers may have decimals.");
                }
            }
        }
    }
}
function remove_prod_content(Field){
    var ContentName = prompt("Enter the name of the content field you want to remove","");
    if (ContentName!=null){
        if (ContentName==""){
            alert("ERROR: No valid content name was entered");
        }else{
            var ParText = "";
            var Obj = PRODUCT_DATA[Field];
            delete Obj[ContentName];
            for (key in Obj){
                ParText += key;
                ParText += " : ";
                ParText += Obj[key].toString();
                ParText += "<br>";
            }
            PRODUCT_DATA[Field] = Obj;
            var ParId = "p_"+Field;
            var Elem = document.getElementById(ParId);
            Elem.innerHTML = ParText;
        }
    }
}
function add_new_supplier(){
    var Supplier = prompt("Enter supplier name:","");
    if (Supplier!=null){
        if (Supplier==""){
            alert("ERROR: No valid supplier name was entered");
        }else{
            var Suppliers = PRODUCT_DATA["suppliers"];
            if (Suppliers.includes(Supplier)){
                alert("This supplier has already been added");
            }else{
                Suppliers.push(Supplier);
                var ParText = "";
                for (key in Suppliers){
                ParText += Suppliers[key];
                ParText += "<br>";
                }
                PRODUCT_DATA["suppliers"] = Suppliers;
                document.getElementById("p_suppliers").innerHTML = ParText;
            }
           
        }
    }
}

function remove_supplier(){
    var Supplier = prompt("Enter name of supplier you want to remove:","");
    if (Supplier!=null){
        if (Supplier==""){
            alert("ERROR: No valid supplier name was entered");
        }else{
            var Suppliers = PRODUCT_DATA["suppliers"];
            var filtered = Suppliers.filter(function(value, index, arr){ 
                return value != Supplier;
            });
            Suppliers = filtered;
            var ParText = "";
            for (key in Suppliers){
            ParText += Suppliers[key];
            ParText += "<br>";
            PRODUCT_DATA["suppliers"] = Suppliers;
            }
            document.getElementById("p_suppliers").innerHTML = ParText;
        }
    }
}

function handle_warehouse_products(Content){
    var NewTable = document.createElement("table");
    NewTable.setAttribute("style","width:100% border: 1px solid black; border-collapse: collapse; overflow-x: scroll;");
    NewTable.setAttribute("id","prod_tab");
    NewTable.setAttribute("class","tab");
    var Head = document.createElement("tr");
    var ColNr = 0;
    for (key in PRODUCT_DATA){
        if ((key!="wh_id")&&(key!="location_log")&&(key!="temperature_log")){
                var CapKey = key.replace("_"," ");
                CapKey = CapKey.charAt(0).toUpperCase() + CapKey.slice(1);
                var HeadLbl = document.createElement("label");
                HeadLbl.innerHTML = CapKey;
                HeadLbl.setAttribute("onclick","sortTable('prod_tab',"+ColNr.toString()+",1)");
            Head = add_head(Head,HeadLbl,1);
            
            ColNr +=1;
        } 
    }
    Head = add_head(Head,"Product history",1);
    Head = add_head(Head,"Use",1);
    Head = add_head(Head,"Edit",1);
    Head = add_head(Head,"Remove",1);
    NewTable.appendChild(Head);
    for (prodId in Content){
        var ResourceRow = document.createElement("tr");
        ResourceRow = add_cell(ResourceRow,prodId,1);
        var ProdDetails = Content[prodId];
        for (key in PRODUCT_DATA){
            if ((key!="wh_id")&&(key!="id")&&(key!="location_log")&&(key!="temperature_log")){
                var FieldDetails = ProdDetails[key];
                if(key.includes("content")){
                    var Cell = document.createElement("p");
                    var ParText = "";
                    for (subkey in FieldDetails){
                        ParText += subkey;
                        ParText += " : ";
                        ParText += FieldDetails[subkey].toString();
                        ParText += "<br>";
                    }
                    Cell.innerHTML = ParText;
                }else if(key=="suppliers"){
                    var Cell = document.createElement("p");
                    var ParText = list_to_paragraph(FieldDetails);
                    Cell.innerHTML = ParText;
                }else{
                    var Cell = FieldDetails;
                }
                //console.log(key);
                //console.log(Cell);
                ResourceRow = add_cell(ResourceRow,Cell,1);
            } 
        }
        var HistoryText = build_history_text(prodId,ProdDetails);
        var HistoryBtn = document.createElement("button");
        HistoryBtn.innerHTML = "View";
        HistoryBtn.setAttribute("onclick","show_prod_history('"+HistoryText+"')");
        var UseBtn = document.createElement("button");
        UseBtn.innerHTML = "Use";
        UseBtn.setAttribute("onclick","get_use_details('"+prodId+"')");
        var RemoveBtn = document.createElement("button");
        RemoveBtn.innerHTML = "Remove";
        RemoveBtn.setAttribute("onclick","remove_product('"+prodId+"')");
        var EditBtn = document.createElement("button");
        EditBtn.innerHTML = "Edit";
        var DetailsWithId = {};
        for (key in ProdDetails){
            DetailsWithId[key] = ProdDetails[key];
        }
        DetailsWithId["id"] = prodId;
        var ProdDetailsJsonString = JSON.stringify(DetailsWithId);
        EditBtn.setAttribute("onclick","edit_product('"+ProdDetailsJsonString+"')");
        ResourceRow = add_cell(ResourceRow,HistoryBtn,1);
        ResourceRow = add_cell(ResourceRow,UseBtn,1);        
        ResourceRow = add_cell(ResourceRow,EditBtn,1);
        ResourceRow = add_cell(ResourceRow,RemoveBtn,1);
        NewTable.appendChild(ResourceRow);
    }
    
    if (document.getElementById("prod_tab")!=null){
        (document.getElementById("prod_tab")).remove();
    }
    document.getElementById("resource_details_container").appendChild(NewTable);
}

function build_history_text(ProdId,ProdDetails){
    var Text = "<strong>"+ProdId + " History </strong>" + "<br> <br>" + "USAGE HISTORY: <br>";
    var Usage = ProdDetails["usage"];
    for (date in Usage) {
        var UsageEntry = Usage[date];
        var Amount = UsageEntry.amount;
        var Handlers = UsageEntry.handlers;
        var Receivers = UsageEntry.receivers;
        Text = Text + "<strong>"+date+"</strong>" + "<br>  Amount used: "+Amount.toString()+"<br>  Handlers: ";
        var FirstH = true;
        for (h in Handlers){
            Handler = Handlers[h];
            if (FirstH == true){
                Text = Text + Handler;
                FirstH = false;
            }else{
                Text = Text + ", "+ Handler
            }            
        }
        Text = Text + "<br>  Receivers: " 
        var FirstH = true;
        for (h in Receivers){
            Handler = Receivers[h];
            if (FirstH == true){
                Text = Text + Handler;
                FirstH = false;
            }else{
                Text = Text + ", "+ Handler
            }            
        }
        Text = Text + "<br>";
    }
    var TempLog = ProdDetails["temperature_log"];
    Text += "<br>TEMPERATURE HISTORY: <br>";
    for (date in TempLog){
        var Temp = TempLog[date];
        Text = Text+date+" : "+ Temp + "C <br>";
    }
    var LocLog = ProdDetails["location_log"];
    Text += "<br>LOCATION HISTORY: <br>";
    for (date in LocLog){
        var Loc = LocLog[date];
        Text = Text+date+" : "+ Loc + " <br>";
    }
    return Text;

}
function show_prod_history(Text){
    document.getElementById("history_par").innerHTML = Text;
    (document.getElementById("prod_history_div")).style.display = "block";
}

function hide_history(){

}


function warehouse_resource_details(Content){

        setTimeout(function() {
            Container = document.getElementById("resource_details_container");
            Container = remove_all_children(Container);
    var AtrDiv = document.createElement("div");
    AtrDiv.setAttribute("id","atr_div");
    var AtrTables = photos_and_atr_table(Content);
    AtrDiv = add_element_to_element(AtrDiv,"h2",{},"Attributes:");
    var TableNames = ["Personal","Relational","Management","State"];
    for (t in AtrTables){
        if (t=="State"){
            var Table = AtrTables[t];
            //console.log(Table);
            AtrDiv = add_element_to_element(AtrDiv,"h3",{},TableNames[t]);
            AtrDiv.appendChild(Table);
        }
        
    }
    Container.appendChild(AtrDiv);
    
    var ButtonDiv = document.createElement("div");
    ButtonDiv.setAttribute("class","four-col");
    var GetAll = document.createElement("button");
    var GetGroup = document.createElement("button");
    var NewProd = document.createElement("button");
    GetAll.innerHTML = "Get all products";
    GetGroup.innerHTML = "Get group";
    NewProd.innerHTML = "Add new product";
    GetAll.setAttribute("onclick","get_all_products()");
    GetGroup.setAttribute("onclick","get_group()");
    NewProd.setAttribute("onclick","new_product()");
    ButtonDiv.appendChild(GetAll);
    ButtonDiv.appendChild(GetGroup);
    ButtonDiv.appendChild(NewProd);
    var IncludeEmptyL = document.createElement("label");
    IncludeEmptyL.innerHTML = "Include old (empty) products in get requests";
    var IncludeEmptyCheck = document.createElement("input");
    IncludeEmptyCheck.setAttribute("type","checkbox");
    IncludeEmptyCheck.setAttribute("id","include_empty");
    var IncludeDiv = document.createElement("div");
    IncludeDiv.setAttribute("class","two-col");
    IncludeDiv.appendChild(IncludeEmptyL);
    IncludeDiv.appendChild(IncludeEmptyCheck);
    ButtonDiv.appendChild(IncludeDiv);
    var Head = document.createElement("h3");
    Head.innerHTML = "Products:";
    Container.appendChild(Head);
    Container.appendChild(ButtonDiv);

    var InputList = ["id","group","suppliers","production_date","delivery_date","expiration_date","promised_content","analysed_content","amount_used","amount_left"];
    
    var InputDiv = document.createElement("div");
    InputDiv.style.display = "none";
    InputDiv.setAttribute("id","product_input_div");
    
    var Div = document.createElement("div"); 
    Div.setAttribute("class","four-col");
    for (e in InputList){
      var Field = InputList[e];
      var Lbl = document.createElement("label");
      Lbl.innerHTML = Field + ":";
      var Input = document.createElement("input");
      Input.setAttribute("id","p_"+Field);
      if (Field.includes("date")){
          Input.setAttribute("type","date");
          Input.value = PRODUCT_DATA[Field];
      }else if(Field.includes("_content")){
        Input = document.createElement("div");
        var Par = document.createElement("p");
        Par.setAttribute("id","p_"+Field);
        Obj = PRODUCT_DATA[Field];
        var ParText = "";
        for (key in Obj){
          ParText += key;
          ParText += " : ";
          ParText += Obj[key].toString();
          ParText += "<br>";
        }
        Par.innerHTML = ParText;
        var AddBtn = document.createElement("button");
        AddBtn.innerHTML = "Add new content";
        AddBtn.setAttribute("onclick","add_new_content('"+Field+"')");
        var RemBtn = document.createElement("button");
        RemBtn.innerHTML = "Remove content";
        RemBtn.setAttribute("onclick","remove_prod_content('"+Field+"')");
        var BtnDiv = document.createElement("div");
        BtnDiv.setAttribute("class","two-col");
        BtnDiv.appendChild(AddBtn);
        BtnDiv.appendChild(RemBtn);
        Input.appendChild(Par);
        Input.appendChild(BtnDiv);
      }else if(Field=="suppliers"){
        Input = document.createElement("div");
        var Par = document.createElement("p");
        Par.setAttribute("id","p_"+Field);
        var ParText = list_to_paragraph(PRODUCT_DATA["suppliers"]);
        
        Par.innerHTML = ParText;
        var AddBtn = document.createElement("button");
        AddBtn.innerHTML = "Add new supplier";
        AddBtn.setAttribute("onclick","add_new_supplier()");
        var RemBtn = document.createElement("button");
        RemBtn.innerHTML = "Remove supplier";
        RemBtn.setAttribute("onclick","remove_supplier()");
        var BtnDiv = document.createElement("div");
        BtnDiv.setAttribute("class","two-col");
        BtnDiv.appendChild(AddBtn);
        BtnDiv.appendChild(RemBtn);
        Input.appendChild(Par);
        Input.appendChild(BtnDiv);

      }else{
        Input.setAttribute("type","text");
        Input.innerHTML = PRODUCT_DATA[Field];
      }
      Div.appendChild(Lbl);
      Div.appendChild(Input);
    }
    
    var CancelBtn = document.createElement("button");
    CancelBtn.innerHTML = "Cancel";
    CancelBtn.setAttribute("onclick","hide_div('product_input_div')");
    Div = add_element_to_element(Div,"button",{"onclick":"create_or_edit_product()","id":"prod_create_or_edit_btn"},"Create");
    
    Div.appendChild(CancelBtn);
    InputDiv.appendChild(Div);
    var HistoryDiv = document.createElement("div");
    HistoryDiv.setAttribute("id","prod_history_div");
    HistoryDiv.style.display = "none";
    HistoryDiv = add_element_to_element(HistoryDiv,"p",{"id":"history_par"},"");
    HistoryDiv = add_element_to_element(HistoryDiv,"button",{"onclick":"hide_div('prod_history_div')"},"Hide History");
    UseDiv = create_use_div();
    
    Container.appendChild(UseDiv);
    Container.appendChild(InputDiv);
    Container.appendChild(HistoryDiv);
    Container.style.border = "solid black 2px";
          }, 500);
    
}

function list_to_paragraph(List){
    var ParText = "";
    for (subkey in List){
        var Element = List[subkey];
        ParText += Element;
        ParText += "<br>";
    }
    return ParText;
}

function remove_product(ProdId){
    var IncludeEmpty = true;
    var Content = JSON.stringify({"wh_id":RESOURCE_ID,"request":"get_del_or_pop_prods_by_ids","get_del_or_pop":"del","ids":[ProdId],"include_empty":IncludeEmpty});
    var Msg = JSON.stringify({"Request":"warehouse_functions","Content":Content});
    sendMsg(Msg);
}

function obj_to_paragraph(Obj){
    var ParText = "";
    for (key in Obj){
        ParText += key;
        ParText += " : ";
        ParText += Obj[key].toString();
        ParText += "<br>";
    }
    return ParText;
}