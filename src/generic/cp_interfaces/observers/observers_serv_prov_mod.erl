%% @author Daniel
%% @doc @todo Add description to sensors_serv_prov_mod.


-module(observers_serv_prov_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_services/0,handle_request/3]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_services()->
	{monitored_by,[CommsRecep]} = process_info(self(),monitored_by),
	{ok,MyBc} = comms_api:get_bc(CommsRecep),
	{ok,ID} = business_cards:get_id(MyBc),
	InterfaceMod = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))++"_interface_mod"),
	{InterfaceDescription,Resources} = InterfaceMod:get_interface_info(ID),
	{ok,ServiceDetails} = cp_interface_creation:create_details(observer,InterfaceDescription, Resources),
	#{"Observe"=>{ServiceDetails,0}}.

handle_request(ReceptionMap,ServType,Contract)->
	case ServType of
		"Observe"->
			SchedRecep = maps:get(sched,ReceptionMap),
			CommsRecep = maps:get(comms,ReceptionMap),
			{ok,MyBc} = comms_api:get_bc(maps:get(comms,ReceptionMap)),
			{ok,MyId} = business_cards:get_id(MyBc),
			InterfaceMod = erlang:list_to_atom(string:lowercase(string:replace(MyId, " ", "_",all))++"_interface_mod"),
			{ok,Services} = business_cards:get_services(MyBc),
			{ok,ServDescr} = business_cards:get_service_description(Services, "Observe"),
			Resources = maps:get(<<"Resources">>,ServDescr),
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientAdr} = business_cards:get_address(ClientBC),
			{ok,RequestArgs} = contracts:get_request_arguments(Contract),

			case maps:is_key(all, Resources) of
				true->
					IdentifierType = maps:get(<<"Identifier type">>,maps:get(all,Resources)),
					case IdentifierType of
						all->
							case valid_topics_if_specified(RequestArgs,maps:get(<<"Topics">>,maps:get(all,Resources))) of
								true->
									{ok,_ActId}=sched_api:new_act(SchedRecep, "Add client", base_time:now(), #{<<"Contract">>=>Contract,<<"Identifier">>=>all}),
									{accept,[],#{},false};
								_->
									{{reject,"This resource does not have data for the topics specified in the request args. Also note that if topics are specified in the request args, it must be a list of strings."},0,0,0}
							end;
						_->
							case IdentifierType of
								id->
									{ok,Identifier} = business_cards:get_id(ClientBC);
								_->
									Identifier = get_identifier(RequestArgs,ClientAdr,CommsRecep,IdentifierType)
							end,
							case InterfaceMod:valid_identifier(IdentifierType,Identifier) of
								true->
									case valid_topics_if_specified(RequestArgs,maps:get(<<"Topics">>,maps:get(all,Resources))) of
										true->
											{ok,_ActId}=sched_api:new_act(SchedRecep, "Add client", base_time:now(), #{<<"Contract">>=>Contract,<<"Identifier">>=>Identifier}),
											{accept,[],#{},false};
										_->
											{{reject,"This resource does not have data for the topics specified in the request args. Also note that if topics are specified in the request args, it must be a list of strings."},0,0,0}
									end;
									
								_->
									{{reject,"Invalid identifier was given for identifier type: "++IdentifierType},0,0,0}
							end
					end;
				_->
					ResourceTypes = maps:keys(Resources),
					{ok,ClientType} = business_cards:get_type(ClientBC),
					case lists:member(ClientType,ResourceTypes) of
						true->
							IdsAndIdentifierType = maps:get(ClientType,Resources),
							IdentifierType = maps:get(<<"Identifier type">>,IdsAndIdentifierType),
							case IdentifierType of
								id->
									{ok,Identifier} = business_cards:get_id(ClientBC);
								_->
									Identifier = get_identifier(RequestArgs,ClientAdr,CommsRecep,IdentifierType)
							end,
							case InterfaceMod:valid_identifier(IdentifierType,Identifier) of
								true->
									AvailableIdentifiers = maps:get(<<"Available identifiers">>,IdsAndIdentifierType),
									case (AvailableIdentifiers==all) orelse (lists:member(Identifier, AvailableIdentifiers)) of
										true->
											case valid_topics_if_specified(RequestArgs,maps:get(<<"Topics">>,maps:get(ClientType,Resources))) of
												true->
													{ok,_ActId}=sched_api:new_act(SchedRecep, "Add client", base_time:now(), #{<<"Contract">>=>Contract,<<"Identifier">>=>Identifier}),
													{accept,[],#{},false};
												_->
													{{reject,"This resource does not have data for the topics specified in the request args. Also note that if topics are specified in the request args, it must be a list of strings."},0,0,0}
											end;
										_->
											{{reject,"Identifier given is not one of the identifiers for which this sensor can provide data"},0,0,0}
									end;
								_->
									{{reject,"Invalid identifier was given for identifier type: "++IdentifierType},0,0,0}
							end;
						_->
							{{reject,"This observer does not have any data for a resource of your type"},0,0,0}
					end
			end
	end.


get_identifier(RequestArgs,ClientAdr,CommsRecep,IdentifierType)->
	case maps:is_key(<<"Identifier">>, RequestArgs) of%normally not the case
		true->
			Identifier = maps:get(<<"Identifier">>,RequestArgs);
		_->
			{ok,AtrMap}=comms_api:send_request_info(CommsRecep, ClientAdr, "Attributes", [IdentifierType]),
			case maps:is_key(IdentifierType, AtrMap) of
				true->
					Identifier = maps:get(IdentifierType,AtrMap);
				_->
					Identifier = none
			end
	end,
	Identifier.

valid_topics_if_specified(RequestArgs,TopicsAllowed)->
	case maps:is_key(<<"Topics">>, RequestArgs) of
		true->
			Topics = maps:get(<<"Topics">>,RequestArgs),
			case custom_erlang_functions:isListOfStrings(Topics) of
				true->
					each_topic_allowed(Topics,TopicsAllowed);
				_->
					false
			end;
		_->
			true
	end.

each_topic_allowed([],_TopicsAllowed)->
	true;
each_topic_allowed([DataField|T],TopicsAllowed)->
	case lists:member(DataField,TopicsAllowed) of
		true->
			each_topic_allowed(T,TopicsAllowed);
		_->
			false
	end.
	
