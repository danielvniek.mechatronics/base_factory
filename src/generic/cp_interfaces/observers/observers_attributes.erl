%% @author Daniel
%% @doc @todo Add description to sensors_attributes.


-module(observers_attributes).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get()->
	StartupDateAndTime = base_time:base_time_to_date_and_time_string(base_time:now()),
	{ok,P1} = attribute_functions:create_attribute("Startup date and time", "Personal", "Date and time", StartupDateAndTime),
	{ok,P2} = attribute_functions:create_attribute("Gateway details", "Personal", "Map with at least the interface type as key and any other details as seperate keys", #{<<"Interface type">>=>"pending"}),%#{InterfaceType=>MQTT, Broker=>,Port=> }
	
	{ok,R1} = attribute_functions:create_attribute("Attached to resource with id", "Relational", "Base id", "none"),
	{ok,R2} = attribute_functions:create_attribute("Clients", "Relational", ["Add client","Remove client"], #{}),%#{Identifier=>Contracts,Identifier2=>Contracts2...}
	
	{ok,S1} = attribute_functions:create_attribute("Interfacing rate (messages per hour)", "Communication", "Every message shared to/from the physical world is regarded as a message", 0),
	{ok,S2} = attribute_functions:create_attribute("Nr of messages delivered (including rejected)", "Communication", "Every message shared to/from the physical world is regarded as a message.", 0),
	{ok,S3} = attribute_functions:create_attribute("Nr of deliveries rejected", "Communication", "Rejected messages are messages that were delivered successfully, but after which a 'rejected' or 'error' reply was received", 0),
	{ok,S4} = attribute_functions:create_attribute("Last delivery rejected", "Communication", "Date and time", #{}), %#{DateAndTime=>{ResourceId=>Rid,Package=>P,Reason=>}}
	
	{ok,S5} = attribute_functions:create_attribute("Device status", "Device state", "Running, down or pending","pending"),%pending, running or down
	{ok,S6} = attribute_functions:create_attribute("Nr of device failures", "Device state", "A failure is recorded when a message is received from the device that it is down or when the device cannot be reached.", 0),
	{ok,S7} = attribute_functions:create_attribute("Last device failure", "Device state", "Date and time", #{}), %#{DateAndTime=>Reason}

	{ok,S8} = attribute_functions:create_attribute("Interface process status", "Interface process state", "Running, down or pending", "pending"),%pending, running or down
	{ok,S9} = attribute_functions:create_attribute("Nr of interface process failures", "Interface process state", "A failure is recorded when the interface process dies because of an error in its code.", 0),
	{ok,S10} = attribute_functions:create_attribute("Last interface process failure", "Interface process state", "Date and time", #{}), %#{DateAndTime=>Reason}
	{ok,M1} = attribute_functions:create_attribute("Max minutes with no activity", "Management", "Can be used by the Interface Process to inform any available humans in the system that there might be a problem with the device", 360),
	{ok,Photos} = attribute_functions:create_attribute("Photos", "Personal", "GD links or actual photos?", []),
	[P1,P2,R1,R2,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,M1,Photos].

