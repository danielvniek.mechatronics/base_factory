%% @author Daniel
%% @doc @todo Add description to sensor_standard_ap_mod.


-module(observers_ap).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,analyse_old_acts/4,analyse_act/3]).

%-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	["Deliver data (SPA)","Device state changed (RD)","Interface processs state changed (RD)"].

analyse_old_acts(ActType,Acts,ReceptionMap,Variables)->%assume already analysed
	AtrRecep = maps:get(atr,ReceptionMap),
	case ActType of
		"Interface state changed (RD)"->
			{ok,InterfaceS} = atr_api:get_attribute_value(AtrRecep, "Interface status"),
			case InterfaceS of 
				"pending"->
					Act = get_newest(Acts,lists:nth(1, Acts)),
					analyse_act(Act,ReceptionMap,Variables);
				_->
					Variables
			end;
		_->
			Variables
	end.
get_newest([],Newest)->
	Newest;
get_newest([Act|T],Newest)->
	case (Act#stage3Activity.execution_info#execution_info.tstart>=Act#stage3Activity.execution_info#execution_info.tstart) of
		true->
			get_newest(T,Act);
		_->
			get_newest(T,Newest)
	end.
analyse_act(Act,ReceptionMap,Variables)->
	ActType = Act#stage3Activity.type,
	AtrRecep = maps:get(atr,ReceptionMap),
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	
	case ActType of
		"Deliver data (SPA)"->
			case is_map(S2Data) of
				true->
					Clients = maps:keys(S2Data);
				_->
					Clients = []
			end,
			NrDataEntries = length(Clients),
			F = fun(Val)->Val+NrDataEntries end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Nr of messages delivered (including rejected)", F),
			{ok,StartupDateAndTime} = atr_api:get_attribute_value(AtrRecep, "Startup date and time"),
			DateTimeList = string:split(StartupDateAndTime," ",all),
			Date = lists:nth(1, DateTimeList),Time = lists:nth(2, DateTimeList),
			T = base_time:date_and_time_strings_to_base_time(Date, Time),
			TimeDif = base_time:now()-T,
			{ok,NrDataEntriesDel} = atr_api:get_attribute_value(AtrRecep, "Nr of messages delivered (including rejected)"),
			case (TimeDif>0) of
				true->
					RatePerS = NrDataEntriesDel/TimeDif;
				_->
					RatePerS = 0
			end,
			RatePerH = RatePerS*3600,
			{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "Interfacing rate (messages per hour)", RatePerH),
			analyse_each_delivery(AtrRecep,Act,Clients,S2Data,0);
			
		"Device state changed (RD)"->
			Result = maps:get(<<"Result">>,S2Data),
			case (Result=="Success") or (Result == <<"Success">>) of
				true->
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Device status", "Running");
				_->
					TimeRejected = Act#stage3Activity.execution_info#execution_info.tstart,
					DateAndTimeRej = base_time:base_time_to_date_and_time_string(TimeRejected),
					Reason = maps:get(<<"Reason">>,S2Data),
					NewLastFailureAtrVal = #{<<"Date and time">>=>DateAndTimeRej,<<"Act id">>=>Act#stage3Activity.id},
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Last device failure", NewLastFailureAtrVal),
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Device status", "Down: "++Reason),
					F = fun(Val)->Val+1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Nr sensor failures", F)
			end;
		"Interface process state changed (RD)"->
			Result = maps:get(<<"Result">>,S2Data),
			case (Result=="Success") or (Result == <<"Success">>) of
				true->
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Interface process status", "Running");
				_->
					TimeRejected = Act#stage3Activity.execution_info#execution_info.tstart,
					DateAndTimeRej = base_time:base_time_to_date_and_time_string(TimeRejected),
					Reason = maps:get(<<"Reason">>,S2Data),
					NewLastFailureAtrVal = #{<<"Date and time">>=>DateAndTimeRej,<<"Act id">>=>Act#stage3Activity.id},
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Last interface process failure", NewLastFailureAtrVal),
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Interface process status", "Down: "++Reason),
					F = fun(Val)->Val+1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Nr of interface process failures", F)
			end
	end,
	Variables.

analyse_each_delivery(AtrRecep,_Act,[],_S2,NrRejected)->
	case NrRejected>0 of
		true->
			F2 = fun(Val)->Val+NrRejected end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Nr of deliveries rejected", F2);
		_->
			ok
	end;
analyse_each_delivery(AtrRecep,Act,[ClientContract|T],S2Data,NrRejected)->
	C = maps:get(ClientContract,S2Data),
	Result = maps:get(<<"Result">>,C),
	case (Result==<<"Success">>) or (Result=="Success") of
		true->
			NewRejected = NrRejected;
		_->
			error_log:log(?MODULE,0,unknown,"\nERROR:Interface holon data delivery rejected"),
			NewRejected = NrRejected+1,
			TimeRejected = Act#stage3Activity.execution_info#execution_info.tstart,
			DateAndTimeRej = base_time:base_time_to_date_and_time_string(TimeRejected),
			NewLastFailureAtrVal = #{<<"Date and time">>=>DateAndTimeRej,<<"Act id">>=>Act#stage3Activity.id},
			{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Last delivery rejected", NewLastFailureAtrVal)
	end,
	analyse_each_delivery(AtrRecep,Act,T,S2Data,NewRejected).
					
			
			
	

