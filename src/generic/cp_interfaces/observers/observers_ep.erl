
-module(observers_ep).
-author("Daniel van Niekerk").

-behaviour(gen_server).

-export([start_link/3,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-export([clients_to_contracts/3]).
-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).
-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).
-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).

-record(ep_state, {factory_ready,reception_map,coordinator,my_id,act_handlers,acts_que}).				

%%%===================================================================

start_link(PluginDetailsMap,CoordinatorPid,PluginSup) ->
  {ok,PID} = gen_server:start_link(?MODULE, [PluginDetailsMap,CoordinatorPid,PluginSup], []),
  {ok,PID}.

stop() ->
  gen_server:stop(self()).

init([PluginDetailsMap,CoordinatorPid,PluginSup]) ->
	{ok,ReceptionMap} = custom_erlang_functions:myGenServCall(CoordinatorPid,{register_plugin_and_get_receptions,PluginDetailsMap,PluginSup}),
	MyPid = self(),
	spawn_monitor(fun()->register_for_acts(ReceptionMap,MyPid)end),
	CommsPid = maps:get(comms,ReceptionMap),
	{ok,MyBc} = comms_api:get_bc(CommsPid),
	{ok,ID} = business_cards:get_id(MyBc),
    {ok, #ep_state{factory_ready = false,reception_map=ReceptionMap,coordinator = CoordinatorPid,my_id=ID,act_handlers=[],acts_que=[]}}. 

%-----------------------------------------------------------------

handle_call({acts_ready,Acts},From,State)->
	PermittedList = [maps:get(sched,State#ep_state.reception_map),self()],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(PermittedList, From) of
		true->
			MyPid = self(),
			case State#ep_state.factory_ready of
				true->
					spawn_monitor(fun()->handle_acts(Acts,State#ep_state.reception_map,MyPid)end),						
					{reply,ok,State};
				_->
					NewQue = lists:append(State#ep_state.acts_que, Acts),
					{reply,ok,State#ep_state{acts_que=NewQue}}
			end;

		_->
			{reply,{error,"Only the schedule of this instance can make this call"},State}
	end;

handle_call({updated_receptions,ReceptionMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#ep_state.coordinator, From) of
		true->
			%error_log:log(?MODULE,0,unknown,"Execution plugin named ~p got updated receptions.",[?MODULE]),
			SchedPid = maps:get(sched,ReceptionMap),
			case SchedPid == maps:get(sched,State#ep_state.reception_map) of
				true->
					ok;
				_->
					sched_api:register_execution_plugin(SchedPid, self(), "Add client", 0)					
			end,
			NewState = State#ep_state{reception_map = ReceptionMap},
			{reply,ok,NewState};
		_->
			{reply,{error,"Only the coordinator of this instance can make this call"},State}
	end;

handle_call({cancel_service,Contract,Reason,_Requester},From,State)->%%from client or from own process when client is seen to be dead
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list([self(),maps:get(comms,State#ep_state.reception_map)], From) of
		true->%%no need to check requester - comms has already done this
			F = fun(Clients)->
				Contracts = sensor_ep:clients_to_contracts(maps:keys(Clients),Clients,#{}),
				case maps:is_key(Contract, Contracts) of
					true->
						Identifier = maps:get(Contract,Contracts),
						IDContracts = maps:get(Identifier,Clients),
						NewIDContracts = lists:delete(Contract, IDContracts),
						case NewIDContracts of
							[]->
								NewClients = maps:remove(Identifier,Clients);
							_->
								NewClients = maps:update(Identifier, NewIDContracts, Clients)
						end;
					_->
						NewClients = Clients
				end,
				NewClients
			end,
			AtrRecep = maps:get(atr,State#ep_state.reception_map),
			CommsRecep = maps:get(comms,State#ep_state.reception_map),
			atr_api:apply_function_to_atr_val(AtrRecep, "Clients",F),
			spawn_monitor(fun()->custom_erlang_functions:myGenServCall(CommsRecep, {deregister_plugin_for_contract,Contract})end),
			spawn_monitor(fun()->exe_api:start_and_finish_new_act(maps:get(exe,State#ep_state.reception_map), "Remove client", #{<<"Contract">>=>Contract}, #{<<"Reason">>=>Reason})end),
			{reply,ok,State};
		_->
			{reply,{error,"Only my comms recep can make this call"},State}
	end;
handle_call({share_with_clients,IDClients,DataMap,AH},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			share_with_clients(IDClients,DataMap,State#ep_state.reception_map,self(),AH),
			NewAHS = [AH|State#ep_state.act_handlers],
			{reply,ok,State#ep_state{act_handlers=NewAHS}};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;

handle_call({data_deliveries_finished,AH},From,State)->
	%error_log:log(?MODULE,0,unknown,"\nData deliveries finished received"),
	case lists:member(AH, State#ep_state.act_handlers) andalso security_functions:from_is_process_or_monitored_by_process(AH, From) of
		true->
			act_handler_api:done(AH, []),
			NewAHS = lists:delete(AH, State#ep_state.act_handlers),
			{reply,ok,State#ep_state{act_handlers = NewAHS}};
		_->
			{reply,{error,"Not expecting call from you"},State}
	end;

handle_call(Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"\nExecution plugin named ~p got unrecognized call ~p",[?MODULE,Request]),
  {reply, unknown, State}.


handle_cast(factory_ready,State)->%%so that the sensor only starts sending data when all clients are ready
	case State#ep_state.factory_ready of
		true->
			{noreply,State};
		_->
			MyPid = self(),
			
			spawn_monitor(fun()->handle_acts(State#ep_state.acts_que,State#ep_state.reception_map,MyPid)end),	
			{noreply,State#ep_state{factory_ready=true,acts_que=[]}}
	end;

handle_cast(_Request, State) ->
  {noreply, State}.


handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Internal functions

register_for_acts(ReceptionMap,MyPid)->
	SchedPid = maps:get(sched,ReceptionMap),
	timer:sleep(20),%%give coordinator time to pass this plugins pid around
	{Res,UnfinishedActs}=sched_api:register_execution_plugin(SchedPid, MyPid,"Add client", 0),
	case Res of 
		ok->
			{ok,UnfinishedDeliverActs} = sched_api:register_execution_plugin(SchedPid, MyPid, "Deliver data (SPA)", 0),
			%error_log:log(?MODULE,0,unknown,"\nUFDA:~p",[UnfinishedDeliverActs]),
			handle_unfinished_add_client(UnfinishedActs,ReceptionMap,MyPid),
			custom_erlang_functions:myGenServCall(MyPid, {acts_ready,UnfinishedDeliverActs}),
			AtrPid = maps:get(atr,ReceptionMap),
			{ok,Clients} = atr_api:get_attribute_value(AtrPid, "Clients"),
			register_with_comms_for_each_contract(maps:keys(Clients),Clients,maps:get(comms,ReceptionMap),MyPid);			
		_->%%sometimes this plugin's pid has not yet been sent by coordinator to core components and they do not permit the registration
			timer:sleep(500),
			register_for_acts(ReceptionMap,MyPid)
	end.

register_with_comms_for_each_contract([],_Clients,_CommsRecep,_MyPid)->
	ok;
register_with_comms_for_each_contract([Identifier|T],Clients,CommsRecep,MyPid)->
	Contracts = maps:get(Identifier,Clients),
	register_with_comms_for_each_contract(Contracts,CommsRecep,MyPid),
	register_with_comms_for_each_contract(T,Clients,CommsRecep,MyPid).

register_with_comms_for_each_contract([],_CommsRecep,_MyPid)->
	ok;
register_with_comms_for_each_contract([Contract|T],CommsRecep,MyPid)->
	{ok,ClientBC} = contracts:get_client_bc(Contract),
	{ok,ClientAdr} = business_cards:get_address(ClientBC),
	comms_api:register_plugin_for_contract(CommsRecep, Contract, ClientAdr, MyPid),
	register_with_comms_for_each_contract(T,CommsRecep,MyPid).

  
handle_unfinished_add_client([],_ReceptionMap,_MyPid)->
	ok;
handle_unfinished_add_client([Act|T],ReceptionMap,MyPid)->
	ExePid = maps:get(exe,ReceptionMap),		
	CommsPid = maps:get(comms,ReceptionMap),
	{ok,AddAH} = exe_api:start_act_handler(ExePid, Act, MyPid),
	S1Data = Act#stage2Activity.schedule_info#schedule_info.s1data,
	Contract = maps:get(<<"Contract">>,S1Data),
	{ok,ClientBC} = contracts:get_client_bc(Contract),
	{ok,ClientAdr} = business_cards:get_address(ClientBC),
	ok = comms_api:register_plugin_for_contract(CommsPid, Contract, ClientAdr,MyPid),
	Identifier = maps:get(<<"Identifier">>,S1Data),%%normally EID or ID or a product identifier for something like a wool batch
	F = fun(Clients)->
		case maps:is_key(Identifier, Clients) of
			true->
				CurrentVal = maps:get(Identifier,Clients),
				case lists:member(Contract,CurrentVal) of
					true->
						NewClients = Clients;
					_->
						NewClients = maps:update(Identifier, [Contract|CurrentVal], Clients)
				end;
			_->
				NewClients = maps:put(Identifier,[Contract],Clients)
		end,
		NewClients
	end,
	AtrRecep = maps:get(atr,ReceptionMap),
	ok = atr_api:apply_function_to_atr_val(AtrRecep, "Clients", F),
	act_handler_api:done(AddAH, #{<<"Result">>=>"Success"}),
	handle_unfinished_add_client(T,ReceptionMap,MyPid).

handle_acts([],_ReceptionMap,_MyPid)->
	ok;
handle_acts([H|T],ReceptionMap,MyPid)->
	case is_record(H,stage1Activity) of
		true->
			ActType = H#stage1Activity.type;
		_->
			ActType = H#stage2Activity.type
	end,
	case ActType of
		"Add client"->
			handle_add_client_act(H,ReceptionMap,MyPid);
		"Deliver data (SPA)"->
			handle_deliver_data(H,ReceptionMap,MyPid)
	end,
	handle_acts(T,ReceptionMap,MyPid).


handle_add_client_act(Act,ReceptionMap,MyPid)->%%serv_prov_mod has already checked if request args contain the needed args
	SchedPid = maps:get(sched,ReceptionMap),		
	CommsPid = maps:get(comms,ReceptionMap),
	{ok,AddAH} = sched_api:start_act_and_get_act_handler(SchedPid, Act#stage1Activity.id, MyPid),
	S1Data = Act#stage1Activity.schedule_info#schedule_info.s1data,
	Contract = maps:get(<<"Contract">>,S1Data),
	{ok,ClientBC} = contracts:get_client_bc(Contract),
	{ok,ClientAdr} = business_cards:get_address(ClientBC),
	ok = comms_api:register_plugin_for_contract(CommsPid, Contract, ClientAdr,MyPid),
	Identifier = maps:get(<<"Identifier">>,S1Data),%%normally EID or ID or a product identifier for something like a wool batch
	F = fun(Clients)->
		case maps:is_key(Identifier, Clients) of
			true->
				CurrentVal = maps:get(Identifier,Clients),
				NewClients = maps:update(Identifier, [Contract|CurrentVal], Clients);
			_->
				NewClients = maps:put(Identifier,[Contract],Clients)
		end,
		NewClients
	end,
	AtrRecep = maps:get(atr,ReceptionMap),
	ok = atr_api:apply_function_to_atr_val(AtrRecep, "Clients", F),
	act_handler_api:done(AddAH, #{<<"Result">>=>"Success"}).

handle_deliver_data(Act,ReceptionMap,MyPid)->
	SchedPid = maps:get(sched,ReceptionMap),
	ExePid = maps:get(exe,ReceptionMap),
	case is_record(Act,stage1Activity) of
		true->
			{ok,AH} = sched_api:start_act_and_get_act_handler(SchedPid, Act#stage1Activity.id, MyPid),
			S1Data = Act#stage1Activity.schedule_info#schedule_info.s1data,
			S2Data = #{};
		_->
			{ok,AH} = exe_api:start_act_handler(ExePid, Act, MyPid),
			S1Data = Act#stage2Activity.schedule_info#schedule_info.s1data,
			S2Data = Act#stage2Activity.execution_info#execution_info.s2data
	end,
	{ok,Clients} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Clients"),
	
	Identifier = maps:get(<<"Identifier">>,S1Data),
	DataMap = maps:get(<<"DataMap">>,S1Data),
	case maps:is_key(Identifier, Clients) of
		true->
			case (S2Data==#{}) or (S2Data==[]) of
				true->
					IDClients = maps:get(Identifier,Clients),
					case IDClients of
						[]->
							act_handler_api:done(AH, "No clients for identifier");
						_->
							ClientsMap = make_client_map(IDClients,#{}),
							act_handler_api:update_progress(AH, ClientsMap)
					end;
				_->
					IDClients = maps:keys(S2Data)
			end,
			custom_erlang_functions:myGenServCall(MyPid, {share_with_clients,IDClients,DataMap,AH});
		_->
			act_handler_api:done(AH, "No clients for identifier")
	end.

make_client_map([],Map)->
	Map;
make_client_map([Client|T],Map)->
	make_client_map(T,maps:put(Client,pending,Map)).
	
	
share_with_clients([],_DataMap,_ReceptionMap,_MyPid,_AH)->
	ok;
share_with_clients([Contract|T],DataMap,ReceptionMap,MyPid,AH)->
	spawn_monitor(fun()->deliver_package(Contract,DataMap,ReceptionMap,MyPid,AH)end),
	share_with_clients(T,DataMap,ReceptionMap,MyPid,AH).
	

deliver_package(Contract,DataMap,ReceptionMap,MyPid,AH)->
	{ok,RequestArgs} = contracts:get_request_arguments(Contract),
	case maps:is_key(<<"Topics">>, RequestArgs) of
		true->
			DataFields = maps:get(<<"Topics">>,RequestArgs),
			ClientData = delete_unrequested_data_names(DataFields,maps:keys(DataMap),DataMap);
		_->
			ClientData = delete_unrequested_data_names(all,maps:keys(DataMap),DataMap)
	end,
	CommsPid = maps:get(comms,ReceptionMap),
	StartT = erlang:system_time(microsecond),
	case (ClientData==#{}) of
		true->
			DeliveryRes = {error,"No fields match what client asked for. No delivery attempted"};
			
		_->	
			DeliveryRes = custom_erlang_functions:myGenServCall(CommsPid,{deliver_package_from_plugin,Contract,ClientData})%%must go through comms first
	end,
	EndT = erlang:system_time(microsecond),
	Duration = StartT-EndT,
	case DeliveryRes of
		accept->
			S2Data = #{<<"Result">>=>"Success",<<"Delivery duration (microseconds)">>=>Duration};
		{reject,Reason}->
			S2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"Reason provided by client: "++Reason};
		Err->
			ErrS = lists:flatten(io_lib:format("~p",[Err])),
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientAdr} = business_cards:get_address(ClientBC),
			case is_atom(ClientAdr) of
				true->
					Pid = whereis(ClientAdr),
					case is_pid(Pid) of
						true->
							ClientAlive = is_process_alive(Pid);
						_->
							ClientAlive = false
					end;
				_->
					case is_pid(ClientAdr) of
						true->
							ClientAlive = is_process_alive(ClientAdr);
						_->
							ClientAlive = false
					end
			end,
			S2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"Caught: "++ErrS,<<"Client still alive">>=>ClientAlive},
			case ClientAlive of
				true->
					ok;
				_->
					custom_erlang_functions:myGenServCall(MyPid,{cancel_service,Contract,"Client not alive anymore",me})
			end			
	end,
	F = fun(AllS2Data)->NewAllS2Data = maps:update(Contract, S2Data, AllS2Data),
						case lists:member(pending,maps:values(NewAllS2Data)) of
							true->
								ok;
							_->
								spawn_monitor(fun()->custom_erlang_functions:myGenServCall(MyPid, {data_deliveries_finished,AH})end)
						end,
						NewAllS2Data
							end,
	act_handler_api:update_progress(AH, F).

delete_unrequested_data_names(_RequestedDataNames,[],DataMap)->
	DataMap;
delete_unrequested_data_names(RequestedDataNames,[PackageDataN|T],DataMap)->
	case is_binary(PackageDataN) of
		true->
			DataField = binary_to_list(PackageDataN);
		_->
			DataField = PackageDataN
	end,
	CleanDataMap = maps:remove(PackageDataN, DataMap),
	case (RequestedDataNames==all) orelse (lists:member(DataField,RequestedDataNames)) of
		true->
			NewDataMap = maps:put(DataField, maps:get(PackageDataN,DataMap), CleanDataMap),
			delete_unrequested_data_names(RequestedDataNames,T,NewDataMap);
		_->
			delete_unrequested_data_names(RequestedDataNames,T,CleanDataMap)
	end.

clients_to_contracts([],_Clients,ContractsMap)->
	ContractsMap;
clients_to_contracts([Identifier|T],Clients,ContractsMap)->
	IDContracts = maps:get(Identifier,Clients),
	NewContractsMap = add_each_contract(IDContracts,Identifier,ContractsMap),
	clients_to_contracts(T,Clients,NewContractsMap).

add_each_contract([],_Identifier,Contracts)->
	Contracts;
add_each_contract([Contract|T],Identifier,Contracts)->
	NewContracts = maps:put(Contract,Identifier,Contracts),
	add_each_contract(T,Identifier,NewContracts).


	
	