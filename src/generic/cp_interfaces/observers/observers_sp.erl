
-module(observers_sp).
-author("Daniel van Niekerk").

-behaviour(gen_server).

-export([start_link/3,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).


-record(sp_state, {factory_ready,reception_map,coordinator,interface_pid,interface_monitor_ref,interface_mod,my_id,last_interface_failure,min_time_between_failures}).				

%%%===================================================================

start_link(PluginDetailsMap,CoordinatorPid,PluginSup) ->
  {ok,PID} = gen_server:start_link(?MODULE, [PluginDetailsMap,CoordinatorPid,PluginSup], []),
  {ok,PID}.

stop() ->
  gen_server:stop(self()).

init([PluginDetailsMap,CoordinatorPid,PluginSup]) ->
	{ok,ReceptionMap} = custom_erlang_functions:myGenServCall(CoordinatorPid,{register_plugin_and_get_receptions,PluginDetailsMap,PluginSup}),
	MyPid = self(),
	spawn_monitor(fun()->register_for_acts(ReceptionMap,MyPid)end),
	CommsPid = maps:get(comms,ReceptionMap),
	{ok,MyBc} = comms_api:get_bc(CommsPid),
	{ok,ID} = business_cards:get_id(MyBc),
	InterfaceMod = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))++"_interface_mod"),
    {ok, #sp_state{factory_ready = false,reception_map=ReceptionMap,coordinator = CoordinatorPid,min_time_between_failures=30000,last_interface_failure=0,interface_mod=InterfaceMod,my_id=ID,interface_pid=pending,interface_monitor_ref=pending}}. 

%-----------------------------------------------------------------
handle_call(restart_interface,From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case is_pid(State#sp_state.interface_pid) andalso (is_process_alive(State#sp_state.interface_pid)) of
				true->
					demonitor(State#sp_state.interface_monitor_ref),
					exit(State#sp_state.interface_pid,kill);
				_->
					ok
			end,
			InterfaceModName = State#sp_state.interface_mod,
			{InterfacePid,MonitorRef,MinTime} = try_to_restart_interface(InterfaceModName,State#sp_state.reception_map,self(),State#sp_state.my_id),
			{reply,ok,State#sp_state{interface_pid=InterfacePid,interface_monitor_ref=MonitorRef,min_time_between_failures=MinTime}};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;

handle_call({acts_ready,Acts},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(maps:get(sched,State#sp_state.reception_map), From) of
		true->
			MyPid = self(),
			spawn_monitor(fun()->handle_acts(Acts,State#sp_state.reception_map,MyPid)end),
			{reply,ok,State};
		_->
			{reply,{error,"Only the schedule of this instance can make this call"},State}
	end;

handle_call({updated_receptions,ReceptionMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#sp_state.coordinator, From) of
		true->
			%error_log:log(?MODULE,0,unknown,"Scheduling plugin named ~p got updated receptions.",[?MODULE]),
			SchedPid = maps:get(sched,ReceptionMap),
			case SchedPid == maps:get(sched,State#sp_state.reception_map) of
				true->
					ok;
				_->
					sched_api:register_execution_plugin(SchedPid, self(), "Add client", 0)					
			end,
			NewState = State#sp_state{reception_map = ReceptionMap},
			{reply,ok,NewState};
		_->
			{reply,{error,"Only the coordinator of this instance can make this call"},State}
	end;
							

handle_call({new_data,Identifier,DataMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case is_map(DataMap) andalso custom_erlang_functions:isListOfStrings(maps:keys(DataMap)) of
				true->
					sched_api:new_act(maps:get(sched,State#sp_state.reception_map), "Deliver data (SPA)", base_time:now(), #{<<"Identifier">>=>Identifier,<<"DataMap">>=>DataMap}),
					{reply,ok,State};
				_->
					{reply,{error,"DataMap must be a map with strings as keys, each key for each data field"},State}
			end;
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;

handle_call(sensor_running,From,State)->
	case is_pid(State#sp_state.interface_pid) of
		true->
			case security_functions:from_is_process_or_monitored_by_process(State#sp_state.interface_pid, From) orelse security_functions:from_is_process_or_monitored_by_process(self(), From) of
				true->
					spawn_monitor(fun()->exe_api:start_and_finish_new_act(maps:get(exe,State#sp_state.reception_map), "Device state changed (RD)", #{}, #{<<"Result">>=>"Success"}) end),
					{reply,ok,State};
				_->
					{reply,{error,"Only the interface pid or one of its spawned processes can make this call"},State}
			end;
		_->%%incase state has not yet been updated by initialization process
			MyPid = self(),
			spawn_monitor(fun()->timer:sleep(5000),custom_erlang_functions:myGenServCall(MyPid,sensor_running)end),
			{reply,ok,State}
	end;

handle_call({sensor_down,Reason},From,State)->
	case is_pid(State#sp_state.interface_pid) of
		true->
			case security_functions:from_is_process_or_monitored_by_process(State#sp_state.interface_pid, From) orelse security_functions:from_is_process_or_monitored_by_process(self(), From) of
				true->
					case custom_erlang_functions:is_string(Reason) of
						true->
							spawn_monitor(fun()->exe_api:start_and_finish_new_act(maps:get(exe,State#sp_state.reception_map), "Device state changed (RD)", #{}, #{<<"Result">>=>"Failed",<<"Reason">>=>Reason}) end),
							{reply,ok,State};
						_->
							spawn_monitor(fun()->exe_api:start_and_finish_new_act(maps:get(exe,State#sp_state.reception_map), "Device state changed (RD)", #{}, #{<<"Result">>=>"Failed",<<"Reason">>=>"unknown"}) end),
							{reply,{error,"Reason must be a string"},State}
					end;
					
				_->
					{reply,{error,"Only the interface pid or one of its spawned processes can make this call"},State}
			end;
		_->%%incase state has not yet been updated by initialization process
			MyPid = self(),
			spawn_monitor(fun()->timer:sleep(5000),custom_erlang_functions:myGenServCall(MyPid,{sensor_down,Reason})end),
			{reply,ok,State}
	end;
			

handle_call(Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"\nScheduling plugin named ~p got unrecognized call ~p",[?MODULE,Request]),
  {reply, unknown, State}.


handle_cast(factory_ready,State)->%%so that the sensor only starts sending data when all clients are ready
	case State#sp_state.factory_ready of
		true->
			{noreply,State};
		_->
			MyPid = self(),
			spawn_monitor(fun()->custom_erlang_functions:myGenServCall(MyPid,restart_interface)end),
			{noreply,State#sp_state{factory_ready=true}}
	end;

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info({'DOWN', MonitorRef, process, _Object, Info},State)->%%Sheep data receiver's service provider failed. Will now finish receiving activity and remove this plugin
	case MonitorRef==State#sp_state.interface_monitor_ref of
		true->
			InterfaceModName = State#sp_state.interface_mod,
			error_log:log(?MODULE,0,unknown,"\n~p's interface process failed",[InterfaceModName]),
			InfoS = lists:flatten(io_lib:format("~p",[Info])),
			spawn_monitor(fun()->exe_api:start_and_finish_new_act(maps:get(exe,State#sp_state.reception_map), "Interface process state changed (RD)",#{<<"Interface mod">>=>InterfaceModName}, #{<<"Result">>=>"Failed",<<"Reason">>=>InfoS})end),
			LastFailure = State#sp_state.last_interface_failure,
			MinTime = State#sp_state.min_time_between_failures,
			case ((LastFailure+MinTime)>(base_time:now())) of
				true->
					spawn_monitor(fun()->exe_api:start_and_finish_new_act(maps:get(exe,State#sp_state.reception_map), "Interface process state changed (RD)",#{<<"Interface mod">>=>InterfaceModName}, #{<<"Result">>=>"Failed",<<"Reason">>=>"Minimum time between failures was breached. Not automatically restarting interface now.",<<"Minimum time">>=>MinTime})end);
				_->
					MyPid = self(),
					spawn_monitor(fun()->custom_erlang_functions:myGenServCall(MyPid,restart_interface)end)
			end,
			{noreply,State#sp_state{last_interface_failure=base_time:now()}};
		_->
			{noreply,State}
	end;

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Internal functions

register_for_acts(ReceptionMap,MyPid)->
	SchedPid = maps:get(sched,ReceptionMap),
	timer:sleep(20),%%give coordinator time to pass this plugins pid around
	{Res,UnfinishedRestartActs} = sched_api:register_execution_plugin(SchedPid, MyPid, "Restart interface", 0),
	case Res of 
		ok->
			ignore_all_unfinished_restart_acts(SchedPid,UnfinishedRestartActs);		
		_->%%sometimes this plugin's pid has not yet been sent by coordinator to core components and they do not permit the registration
			timer:sleep(500),
			register_for_acts(ReceptionMap,MyPid)
	end.


handle_acts([],_ReceptionMap,_MyPid)->
	ok;
handle_acts([H|T],ReceptionMap,MyPid)->
	ActType = H#stage1Activity.type,
	case ActType of
		"Restart interface"->
			handle_restart_act(H,ReceptionMap,MyPid)
	end,
	handle_acts(T,ReceptionMap,MyPid).

handle_restart_act(Act,ReceptionMap,MyPid)->
	SchedPid = maps:get(sched,ReceptionMap),
	custom_erlang_functions:myGenServCall(MyPid,restart_interface),
	{ok,RestartAH} = sched_api:start_act_and_get_act_handler(SchedPid, Act#stage1Activity.id, MyPid),
	act_handler_api:done(RestartAH, #{<<"Result">>=>"Success"}).


ignore_all_unfinished_restart_acts(_SchedPid,[])->
	ok;
ignore_all_unfinished_restart_acts(SchedPid,[Act|T])->
	{ok,AHPid}=sched_api:start_act_and_get_act_handler(SchedPid, Act#stage1Activity.id, self()),
	act_handler_api:done(AHPid, #{<<"Result">>=>"Cancelled",<<"Reason">>=>"At startup interface is anyways restarted"}),
	ignore_all_unfinished_restart_acts(SchedPid,T).

try_to_restart_interface(InterfaceModName,ReceptionMap,MyPid,MyId)->
	try
		{ok,InterfaceDetails,MinTime} = InterfaceModName:get_gateway_details_and_min_time_between_failures(MyId),
		{ok,_Msg} = atr_api:update_attribute_value(maps:get(atr,ReceptionMap), "Gateway details", InterfaceDetails),
		case is_integer(MinTime) of
			true->
				FinalMinTime = MinTime;
			_->
				FinalMinTime = 30000
		end,
		{ok,InterfacePid} = InterfaceModName:start_interface(MyPid,MyId,ReceptionMap),
		MonitorRef = monitor(process,InterfacePid),
		ok = exe_api:start_and_finish_new_act(maps:get(exe,ReceptionMap), "Interface process state changed (RD)", #{<<"Interface mod">>=>InterfaceModName}, #{<<"Result">>=>"Success"}),
		{InterfacePid,MonitorRef,FinalMinTime}
	catch
		A:B:StackTrace->
			error_log:log(?MODULE,0,unknown,"\nCaught ~p:~p:~p when trying to restart interface via ~p",[A,B,StackTrace,InterfaceModName]),
			ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
			exe_api:start_and_finish_new_act(maps:get(exe,ReceptionMap), "Interface process state changed (RD)", #{<<"Interface mod">>=>InterfaceModName}, #{<<"Result">>=>"Failed",<<"Reason">>=>"Could not restart",<<"Error and stacktrace">>=>ErrorAndStackTrace}),
			{none,none,30000}
	end.

	
	