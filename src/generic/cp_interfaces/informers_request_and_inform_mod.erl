%% @author Daniel
%% @doc @todo Add description to human_workers_request_and_inform_mod.


-module(informers_request_and_inform_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([inform/4]).



%% ====================================================================
%% Internal functions
%% ====================================================================


inform(ReceptionMap,InformTopic,InformDetails,From)->
	case InformTopic of
		"Inform"->
			{ok,MyBc} = comms_api:get_bc(maps:get(comms,ReceptionMap)),
			{ok,MyId} = business_cards:get_id(MyBc),
			InterfaceMod = erlang:list_to_atom(string:lowercase(string:replace(MyId, " ", "_",all))++"_interface_mod"),
			{ok,InterfaceProc}=vs_api:get_value(maps:get(vs,ReceptionMap), "Interface process (CV)"),
			try
				case is_pid(InterfaceProc) of
					true->
						InformRes = custom_erlang_functions:myNormalProcCall(InterfaceProc, {inform,ReceptionMap,From,InformDetails});
					_->
						InformRes = InterfaceMod:inform(ReceptionMap,From,InformDetails)
				end,
				InformRes
			catch 
				A:B:C->
					Reason = lists:flatten(io_lib:format("~p:~p:~p",[A,B,C])),
					{error,Reason}
			end;			
		_->
			{error,"No informs allowed for this resource"}
	end.




