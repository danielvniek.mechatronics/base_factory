%% @author Daniel
%% @doc @todo Add description to sensor_standard_ap_mod.


-module(informers_ap).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,analyse_old_acts/4,analyse_act/3]).

%-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	["Deliver data (SPA)","Device state changed (RD)","Interface processs state changed (RD)"].

analyse_old_acts(ActType,Acts,ReceptionMap,Variables)->%assume already analysed
	observers_ap:analyse_old_acts(ActType, Acts, ReceptionMap, Variables).

analyse_act(Act,ReceptionMap,Variables)->
	ActType = Act#stage3Activity.type,
	AtrRecep = maps:get(atr,ReceptionMap),
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	case ActType of
		"Inform (SPA)"->
			Result = maps:get(<<"Result">>,S2Data),
			case (Result=="Success") or (Result == <<"Success">>) of
				true->
					F = fun(Val)->Val+1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Nr of messages delivered (including rejected)", F),
					{ok,StartupDateAndTime} = atr_api:get_attribute_value(AtrRecep, "Startup date and time"),
					DateTimeList = string:split(StartupDateAndTime," ",all),
					Date = lists:nth(1, DateTimeList),Time = lists:nth(2, DateTimeList),
					T = base_time:date_and_time_strings_to_base_time(Date, Time),
					TimeDif = base_time:now()-T,
					{ok,NrDataEntriesDel} = atr_api:get_attribute_value(AtrRecep, "Nr of messages delivered (including rejected)"),
					case (TimeDif>0) of
						true->
							RatePerS = NrDataEntriesDel/TimeDif;
						_->
							RatePerS = 0
					end,
					RatePerH = RatePerS*3600,
					{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "Interfacing rate (messages per hour)", RatePerH);
				_->
					error_log:log(?MODULE,0,unknown,"\nERROR:Informer holon data delivery rejected"),
					TimeRejected = Act#stage3Activity.execution_info#execution_info.tstart,
					DateAndTimeRej = base_time:base_time_to_date_and_time_string(TimeRejected),
					NewLastFailureAtrVal = #{<<"Date and time">>=>DateAndTimeRej,<<"Act id">>=>Act#stage3Activity.id},
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Last delivery rejected", NewLastFailureAtrVal),
					F2 = fun(Val)->Val+1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Nr deliveries rejected", F2)
			end;
		_->
			observers_ap:analyse_act(Act, ReceptionMap, Variables)
	end.

					
			
			
	

