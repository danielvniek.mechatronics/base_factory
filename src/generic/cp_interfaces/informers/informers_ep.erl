
-module(informers_ep).
-author("Daniel van Niekerk").

-behaviour(gen_server).

-export([start_link/3,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).


-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).
-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).
-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).

-record(ep_state, {factory_ready,reception_map,coordinator,my_id,act_handlers,acts_que}).				

%%%===================================================================

start_link(PluginDetailsMap,CoordinatorPid,PluginSup) ->
  {ok,PID} = gen_server:start_link(?MODULE, [PluginDetailsMap,CoordinatorPid,PluginSup], []),
  {ok,PID}.

stop() ->
  gen_server:stop(self()).

init([PluginDetailsMap,CoordinatorPid,PluginSup]) ->
	{ok,ReceptionMap} = custom_erlang_functions:myGenServCall(CoordinatorPid,{register_plugin_and_get_receptions,PluginDetailsMap,PluginSup}),
	MyPid = self(),
	spawn_monitor(fun()->register_for_acts(ReceptionMap,MyPid)end),
	CommsPid = maps:get(comms,ReceptionMap),
	{ok,MyBc} = comms_api:get_bc(CommsPid),
	{ok,ID} = business_cards:get_id(MyBc),
    {ok, #ep_state{factory_ready = false,reception_map=ReceptionMap,coordinator = CoordinatorPid,my_id=ID,act_handlers=[],acts_que=[]}}. 

%-----------------------------------------------------------------

handle_call({acts_ready,Acts},From,State)->
	PermittedList = [maps:get(sched,State#ep_state.reception_map),self()],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(PermittedList, From) of
		true->
			MyPid = self(),
			case State#ep_state.factory_ready of
				true->
					spawn_monitor(fun()->handle_acts(Acts,State#ep_state.reception_map,MyPid,none)end),						
					{reply,ok,State};
				_->
					NewQue = lists:append(State#ep_state.acts_que, Acts),
					{reply,ok,State#ep_state{acts_que=NewQue}}
			end;
		_->
			{reply,{error,"Only the schedule of this instance can make this call"},State}
	end;

handle_call({updated_receptions,ReceptionMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#ep_state.coordinator, From) of
		true->
			%error_log:log(?MODULE,0,unknown,"Execution plugin named ~p got updated receptions.",[?MODULE]),
			SchedPid = maps:get(sched,ReceptionMap),
			case SchedPid == maps:get(sched,State#ep_state.reception_map) of
				true->
					ok;
				_->
					sched_api:register_execution_plugin(SchedPid, self(), "Add client", 0)					
			end,
			NewState = State#ep_state{reception_map = ReceptionMap},
			{reply,ok,NewState};
		_->
			{reply,{error,"Only the coordinator of this instance can make this call"},State}
	end;


handle_call(Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"\nExecution plugin named ~p got unrecognized call ~p",[?MODULE,Request]),
  {reply, unknown, State}.


handle_cast(factory_ready,State)->%%so that the sensor only starts sending data when all clients are ready
	case State#ep_state.factory_ready of
		true->
			{noreply,State};
		_->
			MyPid = self(),
			spawn_monitor(fun()->handle_acts(State#ep_state.acts_que,State#ep_state.reception_map,MyPid,none)end),	
			{noreply,State#ep_state{factory_ready=true,acts_que=[]}}
	end;

handle_cast(_Request, State) ->
  {noreply, State}.


handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Internal functions

register_for_acts(ReceptionMap,MyPid)->
	SchedPid = maps:get(sched,ReceptionMap),
	timer:sleep(20),%%give coordinator time to pass this plugins pid around
	{Res,UnfinishedActs}=sched_api:register_execution_plugin(SchedPid, MyPid,"Inform (SPA)", 0),
	case Res of 
		ok->
			handle_unfinished_acts(UnfinishedActs,ReceptionMap,MyPid,none);		%TODO, change none
		_->%%sometimes this plugin's pid has not yet been sent by coordinator to core components and they do not permit the registration
			timer:sleep(500),
			register_for_acts(ReceptionMap,MyPid)
	end.


handle_unfinished_acts([],_ReceptionMap,_MyPid,_InterfaceProc)->
	ok;
handle_unfinished_acts([H|T],ReceptionMap,MyPid,InterfaceProc)->
	case H#stage1Activity.type of
		"Inform (SPA)"->
			SchedPid = maps:get(sched,ReceptionMap),		
			CommsPid = maps:get(comms,ReceptionMap),
			{ok,AH} = sched_api:start_act_and_get_act_handler(SchedPid, H#stage1Activity.id, MyPid),
			S1Data = H#stage1Activity.schedule_info#schedule_info.s1data,
			Contract = maps:get(<<"Contract">>,S1Data),
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientAdr} = business_cards:get_address(ClientBC),
			ok = comms_api:register_plugin_for_contract(CommsPid, Contract, ClientAdr,MyPid),
			try_to_inform(CommsPid,AH,Contract,InterfaceProc)		
	end,
	handle_acts(T,ReceptionMap,MyPid,InterfaceProc).

handle_acts([],_ReceptionMap,_MyPid,_InterfaceProc)->
	ok;
handle_acts([H|T],ReceptionMap,MyPid,InterfaceProc)->
	case H#stage1Activity.type of
		"Inform (SPA)"->
			SchedPid = maps:get(sched,ReceptionMap),		
			CommsPid = maps:get(comms,ReceptionMap),
			{ok,AH} = sched_api:start_act_and_get_act_handler(SchedPid, H#stage1Activity.id, MyPid),
			S1Data = H#stage1Activity.schedule_info#schedule_info.s1data,
			Contract = maps:get(<<"Contract">>,S1Data),
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientAdr} = business_cards:get_address(ClientBC),
			ok = comms_api:register_plugin_for_contract(CommsPid, Contract, ClientAdr,MyPid),
			try_to_inform(CommsPid,AH,Contract,InterfaceProc)		
	end,
	handle_acts(T,ReceptionMap,MyPid,InterfaceProc).

try_to_inform(ReceptionMap,AH,Contract,InterfaceProc)->
	Comms = maps:get(comms,ReceptionMap),
	try
		{ok,MyBC} = contracts:get_service_bc(Contract),
		{ok,MyId} = business_cards:get_id(MyBC),
		{ok,ClientBC} = contracts:get_client_bc(Contract),
		{ok,ClientAdr} = business_cards:get_address(ClientBC),
		{ok,RequestArgs} = contracts:get_request_arguments(Contract),
		InterfaceMod = erlang:list_to_atom(string:lowercase(string:replace(MyId, " ", "_",all))++"_interface_mod"),
		case is_pid(InterfaceProc) of
			true->
				InformRes = custom_erlang_functions:myNormalProcCall(InterfaceProc, {inform,ReceptionMap,ClientAdr,RequestArgs});
			_->
				InformRes = InterfaceMod:inform(ReceptionMap,ClientAdr,RequestArgs)
		end,
		case InformRes of
			ok->
				act_handler_api:done(AH, #{<<"Result">>=>"Success"}),
				comms_api:service_stopped(Comms, Contract, "Done");
			_->
				{error,Err} = InformRes,
				act_handler_api:done(AH, #{<<"Result">>=>"Failed",<<"Reason">>=>Err}),
				comms_api:service_stopped(Comms, Contract, "Failed")
		end
	catch 
		A:B:C->
			Reason = lists:flatten(io_lib:format("~p:~p:~p",[A,B,C])),
			act_handler_api:done(AH, #{<<"Result">>=>"Failed",<<"Reason">>=>Reason}),
			comms_api:service_stopped(Comms, Contract, "Failed")
	end.		



	
	