%% @author Daniel
%% @doc @todo Add description to sensors_attributes.


-module(informers_attributes).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get()->
	ObsAtrs = observers_attributes:get(),
	ClientsAtr = lists:nth(4,ObsAtrs),
	L = lists:delete(ClientsAtr, ObsAtrs),
	{ok,InterfProc} = vs_api:create("Interface process (CV)", "PID", none),
	[InterfProc|L].

