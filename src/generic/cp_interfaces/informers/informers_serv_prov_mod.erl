%% @author Daniel
%% @doc @todo Add description to sensors_serv_prov_mod.


-module(informers_serv_prov_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_services/0,handle_request/3]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_services()->
	{monitored_by,[CommsRecep]} = process_info(self(),monitored_by),
	{ok,MyBc} = comms_api:get_bc(CommsRecep),
	{ok,ID} = business_cards:get_id(MyBc),
	InterfaceMod = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))++"_interface_mod"),
	{InterfaceDescription,Resources} = InterfaceMod:get_interface_info(ID),
	{ok,ServiceDetails} = cp_interface_creation:create_details(informer,InterfaceDescription, Resources),
	#{"Observe"=>{ServiceDetails,0}}.

handle_request(ReceptionMap,ServType,Contract)->
	case ServType of
		"Inform"->
			SchedRecep = maps:get(sched,ReceptionMap),
			{ok,MyBc} = comms_api:get_bc(maps:get(comms,ReceptionMap)),
			{ok,Services} = business_cards:get_services(MyBc),
			{ok,ServDescr} = business_cards:get_service_description(Services, "Inform"),
			Resources = maps:get(<<"Resources">>,ServDescr),
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			case maps:is_key(all, Resources) of
				true->
					{ok,_ActId}=sched_api:new_act(SchedRecep, "Inform (SPA)", base_time:now(), #{<<"Contract">>=>Contract}),
					{accept,[],#{},false};
				_->
					ResourceTypes = maps:keys(Resources),
					{ok,ClientType} = business_cards:get_type(ClientBC),
					case lists:member(ClientType,ResourceTypes) of
						true->
							IdsAndIdentifierType = maps:get(ClientType,Resources),
							AvailableIds = maps:get(<<"Available identifiers">>,IdsAndIdentifierType),
							{ok,ClientID} = business_cards:get_id(ClientBC),
							case lists:member(ClientID, AvailableIds) of
								true->
									{ok,_ActId}=sched_api:new_act(SchedRecep, "Inform (SPA)", base_time:now(), #{<<"Contract">>=>Contract}),
									{accept,[],#{},false};
								_->
									{{reject,"This client's ID is not in the available identifiers list."},0,0,0}
							end;
						_->
							{{reject,"This informer cannot provide this service because the client's type is not one of the keys under 'Resources' in this informers service description."},0,0,0}
					end
			end
	end.

	
