%% @author Daniel
%% @doc @todo Add description to 'SheepDataProvidersSupport'.


-module(cp_interface_creation).

%% ====================================================================
%% API functions
%% ====================================================================
-export([create_details/3,make_available_for_all_resources/3,add_to_resources/6,add_observer_topic/4,add_informer_topic/2,add_interactor_topic/2]).

%% ====================================================================
%% Internal functions
%% ====================================================================

create_details(InterfaceType,InterfaceDescription,Resources)->
	case (is_map(Resources) andalso valid_resources(InterfaceType,maps:keys(Resources),Resources))  of
		true->
			{ok,#{<<"Interface Description">>=>InterfaceDescription,<<"Resources">>=>Resources}};
		_->
			{error,"Invalid Resources"}
	end.

make_available_for_all_resources(InterfaceType,IdentifierType,Topics)->
	case ((custom_erlang_functions:is_string(IdentifierType)) or (IdentifierType==all)) and valid_topics(InterfaceType,Topics) of
		true->
			{ok,#{all=>#{<<"Identifier type">>=>IdentifierType,<<"Topics">>=>Topics}}};
		_->
			{error,"Identifier type must either be all or a string"}
	end.

add_to_resources(InterfaceType,ExistingResources,ResourceType,AvailableIdentifiers,IdentifierType,Topics)->
	case (is_map(ExistingResources)) andalso (valid_resources(InterfaceType,maps:keys(ExistingResources),ExistingResources)) and valid_topics(InterfaceType,Topics) of
		true->
			NewResources = maps:put(ResourceType, #{<<"Available identifiers">>=>AvailableIdentifiers,<<"Identifier type">>=>IdentifierType,<<"Topics">>=>Topics}, ExistingResources),
			case valid_resources(InterfaceType,maps:keys(NewResources),NewResources) of
				true->
					case InterfaceType of
						observer->
							{ok,NewResources};
						_->
							case IdentifierType of
								"ID"->
									{ok,NewResources};
								_->
									{error,"Informers and Interactors can only have 'ID' as IdentifierType"}
							end
					end;
				_->
					{error,"Invalid arguments. ResourceType must be all or a string, AvailableIdentifiers must be all or a list of strings, IdentifierType must be a string and Topics must be constructed using cp_interface_creation:add_observer_topic,:add_informer_topic, or :add_interactor_topic"}
			end;
		_->
			{error,"Invalid ExistingResources. Must be a map. If ExistingResources==all, no other Resources need to be added"}
	end.

add_observer_topic(Topics,Topic,DataAccuracy,DataUnit)->
	case (is_map(Topics)) andalso (valid_observer_topics(maps:keys(Topics),Topics)) of
		true->
			NewTopics = maps:put(Topic, #{<<"Accuracy">>=>DataAccuracy,<<"Unit">>=>DataUnit}, Topics),
			case valid_observer_topics(maps:keys(NewTopics),NewTopics) of
				true->
					{ok,NewTopics};
				_->
					{error,"Invalid topic arguments. Topic must be string, Accuracy must be an interger between 0 and 100 and Unit must be a string"}
			end;
		_->
			{error,"Invalid original Topics. Must be constructed using this function"}
	end.

add_informer_topic(Topics,Topic)->
	case valid_topics(informer,Topics) of
		true->
			case custom_erlang_functions:is_string(Topic) of
				true->
					{ok,[Topic|Topics]};
				_->
					{error,"Topic must be a string"}
			end;
		_->
			{error,"Topics must be a list of strings"}
	end.

add_interactor_topic(Topics,Topic)->
	add_informer_topic(Topics,Topic).

valid_resources(_InterfaceType,[],_Resources)->
	true;
valid_resources(InterfaceType,[ResourceType|T],Resources)->
	case custom_erlang_functions:is_string(ResourceType) of
		true->
			IdsAndIdentifierType = maps:get(ResourceType,Resources),
			case is_map(IdsAndIdentifierType) of
				true->
					case (maps:is_key(<<"Available identifiers">>, IdsAndIdentifierType)) andalso (maps:is_key(<<"Identifier type">>, IdsAndIdentifierType)) andalso (maps:is_key(<<"Topics">>, IdsAndIdentifierType)) of
						true->%%Available identifiers can be anything with the atom all being a special case
							IdentifierType = maps:get(<<"Identifier type">>,IdsAndIdentifierType),
							AvailableIdentifiers = maps:get(<<"Available identifiers">>,IdsAndIdentifierType),
							Topics = maps:get(<<"Topics">>,IdsAndIdentifierType),
							case custom_erlang_functions:is_string(IdentifierType) and valid_topics(InterfaceType,Topics) and ((AvailableIdentifiers==all) or (is_list(AvailableIdentifiers) and (not(custom_erlang_functions:is_string(AvailableIdentifiers))))) of
								true->
									valid_resources(InterfaceType,T,Resources);
								_->
									false
							end;
						_->
							false
					end;
				_->
					false
			end;
		_->
			case ResourceType of
				all->
					IdsAndIdentifierType = maps:get(ResourceType,Resources),
					case is_map(IdsAndIdentifierType) of
						true->
							case (maps:is_key(<<"Identifier type">>, IdsAndIdentifierType)) of
								true->
									IdentifierType = maps:get(<<"Identifier type">>,IdsAndIdentifierType),
									Topics = maps:get(<<"Data fields">>,IdsAndIdentifierType),
									case (custom_erlang_functions:is_string(IdentifierType) or (IdentifierType==all)) and (valid_topics(InterfaceType,Topics)) of
										true->
											true;%%when all is present no other resource details needed
										_->
											false
									end;
								_->
									false
							end;
						_->
							false
					end;
				_->
					false
			end			
	end.

valid_topics(InterfaceType,Topics)->
	case InterfaceType of
		observer->
			case is_map(Topics) of
				true->
					valid_observer_topics(maps:keys(Topics),Topics);
				_->
					{error,"Topics must be an empty map or a map constructed using cp_interface_creation:add_observer_topic"}
			end;
		_->
			custom_erlang_functions:isListOfStrings(Topics)
	end.
		
valid_observer_topics([],_Topics)->
	true;
valid_observer_topics([Topic|T],Topics)->
	case custom_erlang_functions:is_string(Topic) of
		true->
			Details = maps:get(Topic,Topics),
			case is_map(Details) andalso (maps:is_key(<<"Accuracy">>, Details)) andalso (maps:is_key(<<"Unit">>, Details)) of
				true->
					case valid_accuracy(maps:get(<<"Accuracy">>, Details)) and custom_erlang_functions:is_string(maps:get(<<"Unit">>,Details)) of
						true->
							valid_observer_topics(T,Topics);
						_->
							false
					end;
				_->
					false
			end;
		_->
			false
	end.

valid_accuracy(Accuracy)->
	is_integer(Accuracy) andalso (Accuracy=<100) andalso (Accuracy>=0).
