%% @author Daniel
%% @doc @todo Add description to computation_resources_tests.


-module(cr).

%% ====================================================================
%% API functions
%% ====================================================================
-export([add_nr_of_sheep/1,calc_difference/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================

add_nr_of_sheep(Nr)->
	StartT = erlang:timestamp(),
	{ok,BC} = business_cards:create_std_base_bc("RFID Sheep Scale", "Observers"),
	ok = base_resource_creator:new_base_resource(top_sup, BC),
	ok=loop(Nr),
	EndT = erlang:timestamp(),
	SecondsDif = calc_difference(StartT,EndT).
	%io:format("\nTimeDif (s):~p",[SecondsDif]),
	%timer:sleep(5000),
	%io:format("\nTotal RAM usage: ~p",[erlang:memory(total)*0.000001]),
	%io:format("\nETS RAM usage: ~p",[erlang:memory(ets)*0.000001]).
	
loop(0)->
	ok;
loop(Nr)->
	{ok,BC} = business_cards:create_std_base_bc(integer_to_list(Nr), "Sheep"),
	ok = base_resource_creator:new_base_resource(top_sup, BC),
	loop(Nr-1).

calc_difference({MegS,S,Sm},{MegE,E,Em})->
	MicrS = Sm+1000000*S+1000000*1000000*MegS,
	MicrE = Em+1000000*E+1000000*1000000*MegE,
	MicrDif = MicrE-MicrS,
	MicrDif.