
-module(ui_state).
-author("Daniel van Niekerk ").
-behaviour(gen_server).

-include_lib("stdlib/include/ms_transform.hrl").
-export([start_link/0]).
-export([get_clients/0,add_client/2,get_notifications/0,allow/1,allow/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(ui_state, {clients,notifications,monitors}).%%note that for now the default mod is always used but in future developments this can be changed to be more like the core components' receptions where the storage method can be changed

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================
get_clients()->
	custom_erlang_functions:myGenServCall(ui_state,get_clients).
add_client(PermLevel,UserN)->
	MonitorList = security_functions:get_monitors(self()),
	ClientPid = lists:nth(1, MonitorList),
	custom_erlang_functions:myGenServCall(ui_state,{add_client,PermLevel,ClientPid,UserN}).
get_notifications()->
	custom_erlang_functions:myGenServCall(ui_state,get_notifications).
allow(MaxPermLevel)->
	MonitorList = security_functions:get_monitors(self()),
	ClientPid = lists:nth(1, MonitorList),
	custom_erlang_functions:myGenServCall(ui_state,{allow,MaxPermLevel,ClientPid}).

allow(MaxPermLevel,Pid)->
	MonitorList = security_functions:get_monitors(Pid),
	ClientPid = lists:nth(1, MonitorList),
	custom_erlang_functions:myGenServCall(ui_state,{allow,MaxPermLevel,ClientPid}).

username_available([],_UserN)->
	true;
username_available([H|T],UserN)->
	{_PL,UserName} = H,
	case UserName of
		UserN->
			false;
		_->
			username_available(T,UserN)
	end.
start_link() ->
  gen_server:start_link({local, ui_state}, ?MODULE, [], []).

init([]) ->
  yes = global:register_name(ui_state,self()),
  {ok, #ui_state{clients=#{},notifications=[],monitors=#{}}}.

handle_call(get_clients,_From,State)->
	{reply,maps:keys(State#ui_state.clients),State};

handle_call({add_client,PermLevel,ClientPid,UserN},_From,State)->%%TODO add security check
	AllClients = maps:values(State#ui_state.clients),
	case username_available(AllClients,UserN) of
		true->
			PermLevelInt = binary_to_integer(PermLevel),
			NewClients = maps:put(ClientPid, {PermLevelInt,UserN}, State#ui_state.clients),
			MonitorRef = monitor(process,ClientPid),
			NewMonitors = maps:put(MonitorRef, ClientPid, State#ui_state.monitors),
			{reply,ok,State#ui_state{clients = NewClients,monitors = NewMonitors}};
		_->
			{reply,{error,"Username busy"},State}
	end;

handle_call(get_notifications,_From,State)->
	{reply,State#ui_state.notifications,State};

handle_call({allow,MaxPermLevel,ClientPid},_From,State)->
	case is_binary(MaxPermLevel) of
		true->
			MaxInt = binary_to_integer(MaxPermLevel);
		_->
			MaxInt = MaxPermLevel
	end,
	%error_log:log(?MODULE,0,unknown,"\nClientPid:~p",[ClientPid]),
	%error_log:log(?MODULE,0,unknown,"\nClients:~p",[State#ui_state.clients]),
	case maps:is_key(ClientPid, State#ui_state.clients) of
		true->
			{PermLevel,_UserN} = maps:get(ClientPid,State#ui_state.clients),
			case PermLevel>MaxInt of
				true->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Action not permitted. Your do not have the authority to do this">>},
					{reply,ReplyMap,State};
				_->
					{reply,allow,State}
			end;
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Your session has timed out (5 minutes of no requests to base_factory). Please refresh the webpage and reenter your login details">>},
			{reply,ReplyMap,State}
	end;

handle_call(Unknown, _From, State) ->
  {reply, {bad_request,Unknown}, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info({'DOWN', MonitorRef, process, _Object, _Info},State)->
	case maps:is_key(MonitorRef, State#ui_state.monitors) of
		true->
			Pid = maps:get(MonitorRef,State#ui_state.monitors),
			NewClients = maps:remove(Pid, State#ui_state.clients),
			NewMonitors = maps:remove(MonitorRef, State#ui_state.monitors),
			{noreply,State#ui_state{clients = NewClients,monitors = NewMonitors}};
		_->
			{noreply,State}
	end;

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%SLOC:89
			
			
