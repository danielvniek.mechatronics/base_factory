%% @author Daniel
%% @doc @todo Add description to notify.


-module(notify).

%% ====================================================================
%% API functions
%% ====================================================================
-export([new/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================
new(Message)->
	error_log:log(?MODULE,0,unknown,"Notify message to management UI:~p",[Message]).

