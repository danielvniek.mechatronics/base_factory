-module(dash_socket).

-export([init/2]).
-export([websocket_init/1]).
-export([websocket_handle/2]).
-export([websocket_info/2]).


init(Req, Opts) ->
  {cowboy_websocket, Req, Opts,#{idle_timeout=>3600000}}.%60 min timeout

websocket_init(State) ->
	%error_log:log(?MODULE,0,unknown,"\nNew websocket ~p",[self()]),
	
  {[], State}.

websocket_handle({text, Msg_in}, State) ->
	%error_log:log(?MODULE,0,unknown,"Msg_in:~p",[Msg_in]),
  SearchMap = jsone:decode(Msg_in,[{object_format, map}]),
 % error_log:log(?MODULE,0,unknown,"SearchMap:~p",[SearchMap]),
  Request = maps:get(<<"Request">>,SearchMap),
  Content = maps:get(<<"Content">>,SearchMap),
  MyPid = self(),
  case Request of
	  <<"request">>->
		  spawn_monitor(fun()->request_and_inform:request(Content,MyPid)end);
	  <<"inform">>->
		  spawn_monitor(fun()->request_and_inform:inform(Content,MyPid)end);
	  <<"search_resources">>->
		  spawn_monitor(fun()->search_resources:search_resources(Content,MyPid) end);
	  <<"get_resource_info">>->
		  spawn_monitor(fun()->get_resource_info:get_resource_info(Content,MyPid) end);
	  <<"edit_resource">>->
		  spawn_monitor(fun()->edit_resource:edit_resource(Content,MyPid) end);
	  <<"new_resource">>->
		  spawn_monitor(fun()->add_and_remove_resources:add(Content,MyPid) end);
	  <<"remove_resource">>->
		  spawn_monitor(fun()->add_and_remove_resources:remove(Content,MyPid)end);
	  <<"validate_user">>->
		  spawn_monitor(fun()->user_management:validate_user(Content,MyPid)end);
	  <<"add_user">>->
		  spawn_monitor(fun()->user_management:add_user(Content, MyPid)end);
	  <<"change_password">>->
		  spawn_monitor(fun()->user_management:change_password(Content,MyPid)end);
	  <<"remove_user">>->
		  spawn_monitor(fun()->user_management:remove_user(Content, MyPid)end);
	  <<"get_users">>->
		  spawn_monitor(fun()->user_management:get_users(Content,MyPid)end);
	  <<"plugin_functions">>->
		  spawn_monitor(fun()->plugin_dash_functions:handle(Content,MyPid)end);
	  <<"basic_functions">>->
		  spawn_monitor(fun()->basic_dash_functions:handle(Content,MyPid)end);
	  <<"warehouse_functions">>->
		  spawn_monitor(fun()->warehouse_functions:handle(Content,MyPid)end);
	  <<"holon_interactions">>->
		  spawn_monitor(fun()->holon_interactions:get_holon_interactions(Content, MyPid)end);
	  _->
		  error_log:log(?MODULE,0,unknown,"\nWebsocket got unknown request")
  end,
  {ok, State};

websocket_handle(_Data, State) ->
	%error_log:log(?MODULE,0,unknown,"\nws handle ~p",[self()]),
  {[], State}.
	
websocket_info({timeout, _Ref, Msg}, State) ->
	%error_log:log(?MODULE,0,unknown,"\nws timeout info ~p",[self()]),
  erlang:start_timer(1000, self(), <<"How' you doin'?">>),
  {[{text, Msg}], State};

websocket_info({send,Msg},State)->
	%error_log:log(?MODULE,0,unknown,"\nMsg:~p",[Msg]),
  {[{text,Msg}], State};
websocket_info(_Info, State) ->
	%error_log:log(?MODULE,0,unknown,"\nws info ~p for pid ~p",[Info,self()]),
  {[], State}.

%%SLOC:61



	