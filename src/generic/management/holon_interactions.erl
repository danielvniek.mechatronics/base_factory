%% @author Daniel
%% @doc @todo Add description to holon_interactions.


-module(holon_interactions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_holon_interactions/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_holon_interactions(Content,SocketProcess)->
	Holons = custom_erlang_functions:binary_list_to_str_list(maps:get(<<"ids">>,Content),[]),
	StartD = binary_to_list(maps:get(<<"StartDate">>,Content)),
	StartT = binary_to_list(maps:get(<<"StartTime">>,Content)),
	EndD = binary_to_list(maps:get(<<"EndDate">>,Content)),
	EndT = binary_to_list(maps:get(<<"EndTime">>,Content)),
	StartBT = base_time:date_and_time_strings_to_base_time(StartD, StartT),
	EndBT = base_time:date_and_time_strings_to_base_time(EndD, EndT),
	{ok,Text} = custom_erlang_functions:myGenServCall(holon_interactions, {get_text,StartBT,EndBT,Holons}),
	case (Text=="") of
		true->
			BinaryT = <<"">>;
		_->
			BinaryT = list_to_binary(Text)
	end,
	ReplyMap = #{<<"SUBJECT">>=><<"Holon interactions">>,<<"CONTENT">>=>BinaryT},
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
%%SLOC:18


