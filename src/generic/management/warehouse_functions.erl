%% @author Daniel
%% @doc @todo Add description to warehouse_functions.


-module(warehouse_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([handle/2]).

-record(product,{id,type,group,suppliers,production_date,delivery_date,expiration_date,promised_content,analysed_content,usage,amount_used,amount_left,unit,location_log,temperature_log,empty}).


%% ====================================================================
%% Internal functions
%% ====================================================================
handle(Content,SocketProcess)->
	ContentMap = jsone:decode(Content,[{object_format, map}]),
	WarehouseId = binary_to_list(maps:get(<<"wh_id">>,ContentMap)),
	{ok,BCs}=doha_api:get_bcs_by_ids([WarehouseId]),
	case BCs of
		[]->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"No warehouse with given id">>};
		_->
			BC = lists:nth(1, BCs),
			{ok,Address} = business_cards:get_address(BC),
			{ok,WH_BC} = business_cards:create_std_base_bc("Management_UI", "UI"),
			{ok,Contract}=contracts:create_contract(WH_BC, BC, self(), "Store products", once_off, maps:remove(<<"wh_id">>, ContentMap), #{}),
			Res = comms_api:request_service(Address, Contract),
			case Res of
				{ok,"success"}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Product added/removed/edited successfully">>};
				{ok,"none"}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Product removed successfully">>};
				{ok,Array}->
					Products = build_product_map(Array,#{}),
					ReplyMap = #{<<"SUBJECT">>=><<"Warehouse products">>,<<"CONTENT">>=>Products};
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
				{not_allowed,ReplyMap}->
					ok;
				Err->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)}
			end
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

build_product_map([],ProdMap)->
	case length(maps:keys(ProdMap)) of
		0->
			<<"">>;
		_->
			ProdMap
	end;
build_product_map([Prod|T],ProdMap)->
	Id = Prod#product.id,
	
	ProdBinMap = #{<<"type">>=>Prod#product.type,<<"group">>=>Prod#product.group,<<"suppliers">>=>Prod#product.suppliers,<<"production_date">>=>Prod#product.production_date,<<"delivery_date">>=>Prod#product.delivery_date,<<"expiration_date">>=>Prod#product.expiration_date,<<"promised_content">>=>Prod#product.promised_content,<<"analysed_content">>=>Prod#product.analysed_content,<<"usage">>=>Prod#product.usage,<<"amount_used">>=>Prod#product.amount_used,<<"amount_left">>=>Prod#product.amount_left,<<"unit">>=>Prod#product.unit,<<"location_log">>=>Prod#product.location_log,<<"temperature_log">>=>Prod#product.temperature_log,<<"empty">>=>Prod#product.empty},
	NewProdMap = maps:put(Id,ProdBinMap, ProdMap),
	build_product_map(T,NewProdMap).

%%SLOC:45
