%% @author Daniel
%% @doc @todo Add description to user_management.


-module(user_management).

%% ====================================================================
%% API functions
%% ====================================================================
-export([validate_user/2,add_user/2,remove_user/2,change_password/2,get_users/2]).

-define(USERS_PATH,"MUI BackEnd/Users.txt").

%% ====================================================================
%% Internal functions
%% ====================================================================
get_users(_Content,SocketProcess)->
	case ui_state:allow(2) of
		allow->
			{ok,FileContent} = file:read_file(?USERS_PATH),
			Users = jsone:decode(FileContent,[{object_format, map}]),
			UserNamesB = maps:keys(Users),
			ReplyMap = #{<<"SUBJECT">>=><<"users">>,<<"CONTENT">>=>UserNamesB},
			ReplyJSON = jsone:encode(ReplyMap);
		ReplyMap->
			ReplyJSON = jsone:encode(ReplyMap)
	end,
	SocketProcess!{send,ReplyJSON}.

validate_user(Content,SocketProcess)->
	ContentMap = jsone:decode(Content,[{object_format, map}]),
	UserName = maps:get(<<"user_name">>,ContentMap),
	Password = maps:get(<<"password">>,ContentMap),
	{ok,FileContent} = file:read_file(?USERS_PATH),
	Users = jsone:decode(FileContent,[{object_format, map}]),
	case maps:is_key(UserName, Users) of
		true->
			UserMap = maps:get(UserName,Users),
			case maps:get(<<"password">>,UserMap) of
				Password->
					PermissionLevel = maps:get(<<"permission_level">>,UserMap),
					AddRes = ui_state:add_client(PermissionLevel,UserName),
					case AddRes of
						ok->
							ReplyMap = #{<<"SUBJECT">>=><<"Valid user">>,<<"CONTENT">>=>PermissionLevel};
						_->
							ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"This username has another session open. Each user can only have one session open at a time.">>}
					end;
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Invalid password">>}
			end;
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Invalid user">>}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
 	SocketProcess!{send,ReplyJSON}.

add_user(Content,SocketProcess)->
	ContentMap = jsone:decode(Content,[{object_format, map}]),
	NewUser = maps:get(<<"new_user">>,ContentMap),
	NewPswd = maps:get(<<"new_pswd">>,ContentMap),
	PermissionLevel = maps:get(<<"permission_level">>,ContentMap),
	case ui_state:allow(PermissionLevel) of%%only level 1 users can add level 1 and level 2 can add rest
		allow->
			{ok,FileContent} = file:read_file(?USERS_PATH),
			Users = jsone:decode(FileContent,[{object_format, map}]),
			case maps:is_key(NewUser, Users) of
				true->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"User name is already taken">>};
				_->
					NewUserMap = #{<<"password">>=>NewPswd,<<"permission_level">>=>PermissionLevel},
					NewUsers = maps:put(NewUser, NewUserMap, Users),
					NewFileContent = jsone:encode(NewUsers),
					file:write_file(?USERS_PATH, NewFileContent),
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"New user added successfully">>}
			end,
			ReplyJSON = jsone:encode(ReplyMap);
		ReplyMap->
			ReplyJSON = jsone:encode(ReplyMap)
	end,
 	SocketProcess!{send,ReplyJSON},
	get_users("content",SocketProcess).

remove_user(Content,SocketProcess)->
	ContentMap = jsone:decode(Content,[{object_format, map}]),
	UserRem = maps:get(<<"user_rem">>,ContentMap),
	{ok,FileContent} = file:read_file(?USERS_PATH),
	Users = jsone:decode(FileContent,[{object_format, map}]),
	case maps:is_key(UserRem, Users) of
		true->
			UserRemMap = maps:get(UserRem,Users),
			PermissionLevel = maps:get(<<"permission_level">>,UserRemMap),
			case ui_state:allow(PermissionLevel) of
				allow->
					NewUsers = maps:remove(UserRem, Users),
					NewFileContent = jsone:encode(NewUsers),
					file:write_file(?USERS_PATH, NewFileContent),
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"User removed successfully">>};
				ReplyMap->
					ok
			end;
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"The user you are trying to remove does not exist">>}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
 	SocketProcess!{send,ReplyJSON},
	get_users("content",SocketProcess).

change_password(Content,SocketProcess)->
	case ui_state:allow(5) of
		allow->
			ContentMap = jsone:decode(Content,[{object_format, map}]),
			UserName = maps:get(<<"user_name">>,ContentMap),
			Password = maps:get(<<"old_pswd">>,ContentMap),
			{ok,FileContent} = file:read_file(?USERS_PATH),
			Users = jsone:decode(FileContent,[{object_format, map}]),
			case maps:is_key(UserName, Users) of
				true->
					UserMap = maps:get(UserName,Users),
					case maps:get(<<"password">>,UserMap) of
						Password->
							NewUserMap = maps:update(<<"password">>, maps:get(<<"new_pswd">>,ContentMap), UserMap),
							NewUsers = maps:update(UserName, NewUserMap, Users),
							NewFileContent = jsone:encode(NewUsers),
							file:write_file(?USERS_PATH, NewFileContent),
							ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Password changed successfully">>};
						_->
							ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Invalid old password">>}
					end;
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Invalid user">>}
			end;			
		ReplyMap->
			ok
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

%%SLOC:117