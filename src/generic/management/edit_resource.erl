%% @author Daniel
%% @doc @todo Add description to edit_resource.


-module(edit_resource).

%% ====================================================================
%% API functions
%% ====================================================================
-export([edit_resource/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
edit_resource(Content,SocketProcess)->
	ContentMap = jsone:decode(Content,[{object_format, map}]),
	case maps:is_key(<<"id">>, ContentMap) of
		true->
			Id = binary_to_list(maps:get(<<"id">>,ContentMap)),
			Address = business_cards:id_to_adr(Id),
			BinaryAddress = atom_to_binary(Address,latin1);
		_->
			BinaryAddress = maps:get(<<"address">>,ContentMap),
			Address = erlang:binary_to_atom(BinaryAddress,latin1)
	end,
	Component = binary_to_list(maps:get(<<"component">>,ContentMap)),
	Action = binary_to_list(maps:get(<<"action">>,ContentMap)),
	Details = maps:get(<<"details">>,ContentMap),
	DetailsMap = jsone:decode(Details,[{object_format, map}]),
	case Component of
		"plugin"->
			case ui_state:allow(1) of
				allow->
					ReplyMap = edit_plugins(Address,Action,DetailsMap);
				ReplyMap->
					ok
			end;
		"schedule"->
			case ui_state:allow(2) of
				allow->
					ReplyMap = edit_schedule(Address,Action,DetailsMap);
				ReplyMap->
					ok
			end;
		"attributes"->
			case ui_state:allow(2) of
				allow->
					ReplyMap = edit_attributes(Address,Action,DetailsMap);
				ReplyMap->
					ok
			end;
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Unrecognized component">>}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON},
	case maps:is_key(<<"send_updated_resource_info">>, ContentMap) of
		true->
			Update = maps:get(<<"send_updated_resource_info">>,ContentMap),
			case Update of
				<<"true">>->
					get_resource_info:get_resource_info(BinaryAddress, SocketProcess);
				_->
					ok
			end;
		_->
			ok
	end.

edit_plugins(Address,Action,DetailsMap)->
	ModuleStr = binary_to_list(maps:get(<<"module">>,DetailsMap)),
	Module = list_to_atom(string:lowercase(ModuleStr)),
	Args = maps:get(<<"arguments">>,DetailsMap),
	case Action of
		"add"->
			Res = comms_api:add_plugin(Address, Module, Args);
		"remove"->
			Res = comms_api:remove_plugin(Address, Module, Args);
		_->
			Res = {error,"Invalid action specified"}
	end,
	case Res of
		ok->
			case Action of
				"add"->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Plugin added successfully">>};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Plugin removed successfully">>}
			end;
		{error,Err}->
			ErrS = lists:flatten(io_lib:format("~p", [Err])),
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Unrecognized error occured">>}
	end,
	ReplyMap.

edit_schedule(Address,Action,DetailsMap)->
 	case Action of
 		"add"->
			ActType = binary_to_list(maps:get(<<"type">>,DetailsMap)),
			Date = binary_to_list(maps:get(<<"date">>,DetailsMap)),
			Time = binary_to_list(maps:get(<<"time">>,DetailsMap)),
			Tsched = base_time:date_and_time_strings_to_base_time(Date, Time),
			S1Data = maps:get(<<"data">>,DetailsMap),
	
			Call = {new_act,ActType,Tsched,S1Data},
			Reply = comms_api:forward_call(Address, sched, Call),
			case Reply of
				{ok,_S}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"New activity added to schedule">>};
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Unrecognized error occured">>}
			end;
 		"remove"->
			case maps:is_key(<<"ids">>, DetailsMap) of
				true->
					BinaryIds = maps:get(<<"ids">>,DetailsMap),
					Ids = custom_erlang_functions:binary_list_to_str_list(BinaryIds, []),
					SchedRequest = {get_del_or_pop_sched_by_ids,"del",Ids},
					Call = {storage_request,SchedRequest},
					Reply = comms_api:forward_call(Address, sched, Call);
				_->
					case maps:is_key(<<"types">>, DetailsMap) of
						true->
							BinaryTypes = maps:get(<<"types">>,DetailsMap),
							Types = custom_erlang_functions:binary_list_to_str_list(BinaryTypes, []),
							SchedRequest = {get_del_or_pop_sched_by_types,"del",Types},
							Call = {storage_request,SchedRequest},
							Reply = comms_api:forward_call(Address, sched, Call);
						_->
							case maps:is_key(<<"all">>, DetailsMap) of
								true->
									SchedRequest = {get_del_or_pop_all_sched,"del"},
									Call = {storage_request,SchedRequest},
									Reply = comms_api:forward_call(Address, sched, Call);
								_->
									Reply = {error,"No schedule remove details given"}
							end
					end
			end,
			case Reply of
				{ok,"success"}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Activities removed successfully from schedule">>};
				{ok,"none"}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"No activities to remove">>};
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Unrecognized error">>}
			end
	end,
	ReplyMap.

edit_attributes(Address,Action,DetailsMap)->
 	case Action of
 		"add"->
			ID = binary_to_list(maps:get(<<"id">>,DetailsMap)),
			Type = binary_to_list(maps:get(<<"type">>,DetailsMap)),
			Context = binary_to_list(maps:get(<<"context">>,DetailsMap)),
			TempValue = maps:get(<<"value">>,DetailsMap),
			case maps:is_key(<<"value_conv">>, DetailsMap) of
				true->
					DataT = maps:get(<<"value_conv">>,DetailsMap),
					case DataT of
						<<"binary_to_list">>->
							Value = binary_to_list(TempValue);
						<<"bin_list_to_str_list">>->
							Value = custom_erlang_functions:binary_list_to_str_list(TempValue, []);
						_->
							Value = TempValue
					end;
				_->
					Value = TempValue
			end,
			{ok,Atr} = attribute_functions:create_attribute(ID, Type, Context, Value),
 			AtrRequest = {save_attributes,[Atr]},
			Call = {storage_request,AtrRequest},
			Reply = comms_api:forward_call(Address, atr, Call),
			case Reply of
				{ok,"success"}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"New attribute added to attributes">>};
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Unrecognized error occured">>}
			end;
 		"remove"->
			case maps:is_key(<<"ids">>, DetailsMap) of
				true->
					BinaryIds = maps:get(<<"ids">>,DetailsMap),
					Ids = custom_erlang_functions:binary_list_to_str_list(BinaryIds, []),
					AtrRequest = {get_del_or_pop_atr_by_ids,"del",Ids},
					Call = {storage_request,AtrRequest},
					Reply = comms_api:forward_call(Address, atr, Call);
				_->
					case maps:is_key(<<"types">>, DetailsMap) of
						true->
							BinaryTypes = maps:get(<<"types">>,DetailsMap),
							Types = custom_erlang_functions:binary_list_to_str_list(BinaryTypes, []),
							AtrRequest = {get_del_or_pop_atr_by_types,"del",Types},
							Call = {storage_request,AtrRequest},
							Reply = comms_api:forward_call(Address,atr, Call);
						_->
							case maps:is_key(<<"all">>, DetailsMap) of
								true->
									AtrRequest = {get_del_or_pop_all_atr,"del"},
									Call = {storage_request,AtrRequest},
									Reply = comms_api:forward_call(Address, atr, Call);
								_->
									Reply = {error,"No schedule remove details given"}
							end
					end
			end,
			case Reply of
				{ok,"success"}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Attributes removed successfully">>};
				{ok,"none"}->
					ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"No attributes to remove">>};
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Unrecognized error">>}
			end
	end,
	ReplyMap.
%%SLOC:215
