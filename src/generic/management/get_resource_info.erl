%% @author Daniel
%% @doc @todo Add description to get_resource_info.


-module(get_resource_info).
-include("../base_core/support/activity_records.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([get_all_info/1,get_resource_info/2,get_specific_info/2,map_resource_info/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================
%%functions called by dash_socket
get_all_info(Address)->
	Reply = custom_erlang_functions:myGenServCall(Address, get_resource_info),
	case Reply of
		{ok,Return}->
			case maps:is_key(basic_bio, Return) of
				true->
					MappedResourceInfo = map_basic_resource_info(Return);
				_->
					MappedResourceInfo = map_resource_info(Return)
			end;
		_->
			MappedResourceInfo = {error,"Resource with given address did not share its info. Ensure the address in front end code is correct and that all basic resources are correctly implemented"}
	end,
	MappedResourceInfo.

get_resource_info(Content,SocketProcess)->
	case ui_state:allow(5) of
		allow->
			ContentMap = jsone:decode(Content,[{object_format, map}]),
			Components = maps:get(<<"components">>,ContentMap),
			case maps:is_key(<<"id">>, ContentMap) of
				true->
					Id = binary_to_list(maps:get(<<"id">>,ContentMap)),
					{ok,BCs}=doha_api:get_bcs_by_ids([Id]),
					case length(BCs)>0 of
						true->
							BC = lists:nth(1, BCs),
							{ok,Architecture} = business_cards:get_architecture(BC),
							case not(Components==<<"all">>) and (Architecture==base) of
								true->
									ComponentsMap = jsone:decode(Components,[{object_format, map}]),
									ResourceInfo = get_specific_info(BC,ComponentsMap);
								_->
									{ok,Address} = business_cards:get_address(BC),
									ResourceInfo = get_all_info(Address)
							end,
							case ResourceInfo of
								{error,Err}->
									ErrS = lists:flatten(io_lib:format("~p", [Err])),
									ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
								_->
									ReplyMap = #{<<"SUBJECT">>=><<"Resource info">>,<<"CONTENT">>=>ResourceInfo}
							end;
						_->
							ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"No resource with such an id">>}
					end;
				_->
					case maps:is_key(<<"address">>, ContentMap) of
						true->
							BinaryAddress = maps:get(<<"address">>,ContentMap),
							Address = binary_to_atom(BinaryAddress,latin1),
							case is_pid(whereis(Address)) of
								true->%%ensuring a resource with this address exists	
									case not(Components==<<"all">>) of
										true->
											Res = comms_api:get_bc(Address),
											case is_tuple(Res) and business_cards:valid_base_bc(element(2,Res)) of
												true->
													{ok,BC} = Res,
													{ok,Architecture} = business_cards:get_architecture(BC),
													case Architecture of
														basic->%basic_resource
															ResourceInfo = get_all_info(Address),
															case ResourceInfo of
																{error,Err}->
																	ErrS = lists:flatten(io_lib:format("~p", [Err])),
																	ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
																_->
																	ReplyMap = #{<<"SUBJECT">>=><<"Resource info">>,<<"CONTENT">>=>ResourceInfo}
															end;
														_->
															ComponentsMap = jsone:decode(Components,[{object_format, map}]),
															ResourceInfo = get_specific_info(BC,ComponentsMap),
															ReplyMap = #{<<"SUBJECT">>=><<"Resource info">>,<<"CONTENT">>=>ResourceInfo}
													end;
												_->
													ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"No base resource with the given address. Also note that specific resource info cannot be requested from basic resources, only all info">>}
											end;
										_->
											ResourceInfo = get_all_info(Address),
											case ResourceInfo of
												{error,Err}->
													ErrS = lists:flatten(io_lib:format("~p", [Err])),
													ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
												_->
													ReplyMap = #{<<"SUBJECT">>=><<"Resource info">>,<<"CONTENT">>=>ResourceInfo}
											end
									end;
								_->
									ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"No resource with the given address">>}
							end;
						_->
							ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"No id or address given for specific resource info request">>}
					end
			end;
		ReplyMap->
			ok
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

%%internal functions
map_basic_resource_info(Map)->
	case maps:is_key(basic_bio, Map) and maps:is_key(atr, Map) and maps:is_key(id, Map) and maps:is_key(type, Map) and maps:is_key(address, Map) of
		true->
			AtrList = maps:get(atr,Map),
			AtrMap = build_atr_map(AtrList,#{}),
			Temp = term_to_json(Map),
			ReturnMap = maps:put(<<"atr">>,AtrMap,Temp),
			ReturnMap;
		_->
			{error,"Basic resource provided invalid resource info. Fix basic resource's code"}
	end.

map_resource_info(OriginalMap)->
	AtrList = maps:get(atr,OriginalMap),
	SchedList = maps:get(sched,OriginalMap),
	ExeList = maps:get(exe,OriginalMap),
	BioList = maps:get(bio,OriginalMap),
	Plugins = maps:get(plugins,OriginalMap),
	PluginsMap = plugins_to_json(maps:keys(Plugins),Plugins,#{}),
	AtrMap = build_atr_map(AtrList,#{}),
	SchedMap = build_sched_map(SchedList,#{}),
	ExeMap = build_exe_map(ExeList,#{}),
	BioMap = build_bio_map(BioList,#{}),
	Temp = term_to_json(OriginalMap),
	ReturnMap = maps:merge(Temp, #{<<"atr">>=>AtrMap,<<"sched">>=>SchedMap,<<"exe">>=>ExeMap,<<"bio">>=>BioMap,<<"plugins">>=>PluginsMap}),
	ReturnMap.
	
build_atr_map([],AtrMap)->
	case length(maps:keys(AtrMap)) of
		0->
			<<"">>;
		_->
			AtrMap
	end;
build_atr_map([Atr|T],AtrMap)->
	{ok,Id} = attribute_functions:get_atr_id(Atr),
	{ok,Type} = attribute_functions:get_atr_type(Atr),
	{ok,Context} = attribute_functions:get_atr_context(Atr),
	{ok,Value} = attribute_functions:get_atr_value(Atr),
	AtrBinMap = #{<<"type">>=>list_to_binary(Type),<<"context">>=>list_to_binary(Context),<<"value">>=>term_to_json(Value)},
	NewAtrMap = maps:put(list_to_binary(Id),AtrBinMap, AtrMap),
	build_atr_map(T,NewAtrMap).

build_sched_map([],SchedMap)->
	case length(maps:keys(SchedMap)) of
		0->
			<<"">>;
		_->
			SchedMap
	end;
build_sched_map([Sched|T],SchedMap)->
	Id = Sched#stage1Activity.id,
	Type = Sched#stage1Activity.type,
	TschedRaw = Sched#stage1Activity.schedule_info#schedule_info.tsched,
	Tsched = base_time:base_time_to_date_and_time_string(TschedRaw),
	S1data = Sched#stage1Activity.schedule_info#schedule_info.s1data,
	S1dataB = term_to_json(S1data),
	SchedBinMap = #{<<"id">>=>list_to_binary(Id),<<"type">>=>list_to_binary(Type),<<"tsched">>=>list_to_binary(Tsched),<<"s1data">>=>S1dataB},
	Keys = maps:keys(SchedMap),
	Key = erlang:integer_to_binary(length(Keys)+1),
	NewSchedMap = maps:put(Key,SchedBinMap, SchedMap),
	build_sched_map(T,NewSchedMap).

build_exe_map([],ExeMap)->
	case length(maps:keys(ExeMap)) of
		0->
			<<"">>;
		_->
			ExeMap
	end;
build_exe_map([Exe|T],ExeMap)->
	Id = Exe#stage2Activity.id,
	Type = Exe#stage2Activity.type,
	TschedRaw = Exe#stage2Activity.schedule_info#schedule_info.tsched,
	Tsched = base_time:base_time_to_date_and_time_string(TschedRaw),
	S1data = Exe#stage2Activity.schedule_info#schedule_info.s1data,
	S1dataB = term_to_json(S1data),
	TstartRaw = Exe#stage2Activity.execution_info#execution_info.tstart,
	Tstart = base_time:base_time_to_date_and_time_string(TstartRaw),
	S2data = Exe#stage2Activity.execution_info#execution_info.s2data,
	S2dataB = term_to_json(S2data),
	ExeBinMap = #{<<"id">>=>list_to_binary(Id),<<"type">>=>list_to_binary(Type),<<"tsched">>=>erlang:list_to_binary(Tsched),<<"s1data">>=>S1dataB
				   ,<<"tstart">>=>erlang:list_to_binary(Tstart),<<"s2data">>=>S2dataB},
	Keys = maps:keys(ExeMap),
	Key = erlang:integer_to_binary(length(Keys)+1),
	NewExeMap = maps:put(Key,ExeBinMap, ExeMap),
	build_exe_map(T,NewExeMap).
build_bio_map([],BioMap)->
	case length(maps:keys(BioMap)) of
		0->
			<<"">>;
		_->
			BioMap
	end;
build_bio_map([Bio|T],BioMap)->
	Id = Bio#stage3Activity.id,
	Type = Bio#stage3Activity.type,
	TschedRaw = Bio#stage3Activity.schedule_info#schedule_info.tsched,
	Tsched = base_time:base_time_to_date_and_time_string(TschedRaw),
	S1data = Bio#stage3Activity.schedule_info#schedule_info.s1data,
	S1dataB = term_to_json(S1data),
	TstartRaw = Bio#stage3Activity.execution_info#execution_info.tstart,
	Tstart = base_time:base_time_to_date_and_time_string(TstartRaw),
	S2data = Bio#stage3Activity.execution_info#execution_info.s2data,
	S2dataB = term_to_json(S2data),
	TendRaw = Bio#stage3Activity.biography_info#biography_info.tend,
	Tend = base_time:base_time_to_date_and_time_string(TendRaw),
	S3data = Bio#stage3Activity.biography_info#biography_info.s3data,
	S3dataB = term_to_json(S3data),
	BioBinMap = #{<<"id">>=>list_to_binary(Id),<<"type">>=>list_to_binary(Type),<<"tsched">>=>erlang:list_to_binary(Tsched),<<"s1data">>=>S1dataB
				   ,<<"tstart">>=>erlang:list_to_binary(Tstart),<<"s2data">>=>S2dataB
				   ,<<"tend">>=>erlang:list_to_binary(Tend),<<"s3data">>=>S3dataB},
	Keys = maps:keys(BioMap),
	Key = erlang:integer_to_binary(length(Keys)+1),
	NewBioMap = maps:put(Key,BioBinMap, BioMap),
	build_bio_map(T,NewBioMap).


plugins_to_json(PluginMap)->
	Keys = maps:keys(PluginMap),
	plugins_to_json(Keys,PluginMap,#{}).
plugins_to_json([],_PluginMap,JsonMap)->
	case length(maps:keys(JsonMap)) of
		0->
			<<"">>;
		_->
			JsonMap
	end;
plugins_to_json([Key|T],PluginMap,JsonMap)->
	Module = maps:get(<<"module">>,Key),
	Args = maps:get(<<"args">>,Key),
	BinaryArgs = term_to_json(Args),
	Status = maps:get(Key,PluginMap),
	BinaryMod = erlang:atom_to_binary(Module,latin1),
	case is_tuple(Status) of
		true->
			BinaryStat = <<"Running">>;
		_->
			BinaryStat = <<"Failed">>
	end,
	PluginJsonMap = #{<<"module">>=>BinaryMod,<<"args">>=>BinaryArgs,<<"status">>=>BinaryStat},
	Keys = maps:keys(JsonMap),
	PluginNr = length(Keys)+1,
	PluginBinaryKey = integer_to_binary(PluginNr),
	NewJsonMap = maps:put(PluginBinaryKey,PluginJsonMap,JsonMap),
	plugins_to_json(T,PluginMap,NewJsonMap).

term_to_json(Term)->
	case custom_erlang_functions:is_string(Term) of
		true->
			TermB = list_to_binary(Term);
		_->
			case is_binary(Term) or is_number(Term) or is_boolean(Term) of
				true->
					TermB = Term;
				_->
					case is_atom(Term) of
						true->
							TermB = atom_to_binary(Term,latin1);
						_->
							case is_map(Term) of
								true->
									Temp = ensure_all_keys_are_json_keys(maps:keys(Term),Term),
									TermB = ensure_all_vals_are_json(maps:keys(Temp),Temp);
								_->
									case is_list(Term) of
										true->
											TermB=ensure_all_entries_are_json(Term,[]);
										_->
											S = lists:flatten(io_lib:format("~p",[Term])),
											TermB = list_to_binary(S)
									end
							end
					end
			end
	end,
	TermB.

ensure_all_keys_are_json_keys([],Map)->
	Map;
ensure_all_keys_are_json_keys([Key|T],Map)->
	case custom_erlang_functions:is_binary_string(Key) of
		true->
			JsonKey = Key;
		_->
			case custom_erlang_functions:is_string(Key) of
				true->
					JsonKey = list_to_binary(Key);
				_->
					S = lists:flatten(io_lib:format("~p",[Key])),
					JsonKey = list_to_binary(S)
			end
	end,
	Val = maps:get(Key,Map),
	Temp = maps:remove(Key, Map),
	NewMap = maps:put(JsonKey, Val, Temp),
	ensure_all_keys_are_json_keys(T,NewMap).

ensure_all_vals_are_json([],Map)->
	Map;
ensure_all_vals_are_json([Key|T],Map)->
	OrigVal = maps:get(Key,Map),
	NewVal = term_to_json(OrigVal),
	NewMap = maps:update(Key, NewVal, Map),
	ensure_all_vals_are_json(T,NewMap).

ensure_all_entries_are_json([],FinalList)->
	lists:reverse(FinalList);
ensure_all_entries_are_json([H|T],FinalList)->
	case is_number(H) or is_binary(H) of
		true->
			NewList = [H|FinalList];
		_->
			NewList = [term_to_json(H)|FinalList]
	end,
	ensure_all_entries_are_json(T,NewList).
			

get_specific_info(BC,ComponentsMap)->
	JsonBcMap = term_to_json(BC),
	Address = maps:get(<<"address">>,BC),
	case maps:is_key(<<"plugins">>, ComponentsMap) of
		true->
			PluginRes = custom_erlang_functions:myGenServCall(Address,get_plugins_with_status),
			case PluginRes of
				{ok,PluginsWithStatus}->
					BinaryJsonPlugins = plugins_to_json(PluginsWithStatus),
					PJsonBcMap = maps:put(<<"plugins">>, BinaryJsonPlugins, JsonBcMap);
				_->
					PJsonBcMap = JsonBcMap
			end;
		_->
			PJsonBcMap = JsonBcMap
	end,
	case maps:is_key(<<"services">>, ComponentsMap) of
		true->
			ServJsonBcMap = PJsonBcMap;
		_->
			ServJsonBcMap = maps:remove(<<"services">>, PJsonBcMap)
	end,
	case maps:is_key(<<"atr">>,ComponentsMap) of
		true->
			BinaryAtrTypes = maps:get(<<"atr">>,ComponentsMap),
			case is_list(BinaryAtrTypes) of
				true->
					AtrTypes = custom_erlang_functions:binary_list_to_str_list(BinaryAtrTypes,[]),
					AtrRequest = {get_del_or_pop_atr_by_types,"get",AtrTypes};
				_->%%<<"all">>
					AtrRequest = {get_del_or_pop_all_atr,"get"}
			end,
			{ok,Attributes} = comms_api:forward_call(Address, atr, {storage_request,AtrRequest}),
			BinaryAttributes = build_atr_map(Attributes, #{}),
			AJsonBcMap = maps:put(<<"atr">>, BinaryAttributes, ServJsonBcMap);
		_->
			AJsonBcMap = ServJsonBcMap
	end,
	%%bio
	case maps:is_key(<<"bio">>,ComponentsMap) of
		true->
			BioFilter = maps:get(<<"bio">>,ComponentsMap),
			case jsone_decodable(BioFilter) of
				true->
					BioFilterMap = jsone:decode(BioFilter,[{object_format, map}]);
				_->
					BioFilterMap = BioFilter
			end,
			case is_map(BioFilterMap) of
				true->
					case maps:is_key(<<"types">>,BioFilterMap) and maps:is_key(<<"days">>, BioFilterMap) of
					true->
						BinaryBioTypes = maps:get(<<"types">>,BioFilterMap),
						BioTypes = custom_erlang_functions:binary_list_to_str_list(BinaryBioTypes,[]),
						Days = maps:get(<<"days">>,BioFilterMap),
						case is_integer(Days) of
							true->
								TLow = base_time:now()-3600*24*Days;
							_->
								TLow = 0,
								error_log:log(?MODULE,0,unknown,"\nERROR: Days in get resource info for bio not an integer. Fix front end code")
						end,					
						BioRequest = {get_del_or_pop_bio_by_types_and_times,"get",BioTypes,0,"all",TLow,"all",0,"all"};
					_->
						case maps:is_key(<<"types">>,BioFilterMap) of
							true->
								BinaryBioTypes = maps:get(<<"types">>,BioFilterMap),
								BioTypes = custom_erlang_functions:binary_list_to_str_list(BinaryBioTypes,[]),
								BioRequest = {get_del_or_pop_bio_by_types,"get",BioTypes};
							_->
								case maps:is_key(<<"days">>,BioFilterMap) of
									true->
										Days = maps:get(<<"days">>,BioFilterMap),
										case is_integer(Days) of
											true->
												TLow = base_time:now()-3600*24*Days;
											_->
												TLow = 0,
												error_log:log(?MODULE,0,unknown,"\nERROR: Days in get resource info for bio not an integer. Fix front end code")
										end,
										BioRequest = {get_del_or_pop_bio_by_times,"get",0,"all",TLow,"all",0,"all"};
									_->
										BioRequest = {get_del_or_pop_all_bio,"get"}
								end
						end
				end;
			_->
				BioRequest = {get_del_or_pop_all_bio,"get"}
			end,
			{ok,Biography} = comms_api:forward_call(Address, bio, {storage_request,BioRequest}),
			BinaryBiography = build_bio_map(Biography, #{}),
			BJsonBcMap = maps:put(<<"bio">>, BinaryBiography, AJsonBcMap);
		_->
			BJsonBcMap = AJsonBcMap
	end,
	%%sched						
	case maps:is_key(<<"sched">>,ComponentsMap) of
		true->
			SchedFilter = maps:get(<<"sched">>,ComponentsMap),
			case jsone_decodable(SchedFilter) of
				true->
					SchedFilterMap = jsone:decode(SchedFilter,[{object_format, map}]);
				_->
					SchedFilterMap = SchedFilter
			end,
			case is_map(SchedFilterMap) of
				true->
					case maps:is_key(<<"types">>,SchedFilterMap) and maps:is_key(<<"days">>, SchedFilterMap) of
						true->
							BinarySchedTypes = maps:get(<<"types">>,SchedFilterMap),
							SchedTypes = custom_erlang_functions:binary_list_to_str_list(BinarySchedTypes,[]),
							SDays = maps:get(<<"days">>,SchedFilterMap),
							case is_integer(SDays) of
								true->
									THigh = base_time:now()+3600*24*SDays;
								_->
									THigh = "all",
									error_log:log(?MODULE,0,unknown,"\nERROR: Days in get resource info for sched not an integer. Fix front end code")
							end,
							SchedRequest = {get_del_or_pop_sched_by_types_and_times,"get",SchedTypes,0,THigh};
						_->
							case maps:is_key(<<"types">>,SchedFilterMap) of
								true->
									BinarySchedTypes = maps:get(<<"types">>,SchedFilterMap),
									SchedTypes = custom_erlang_functions:binary_list_to_str_list(BinarySchedTypes,[]),
									SchedRequest = {get_del_or_pop_sched_by_types,"get",SchedTypes};
								_->
									case maps:is_key(<<"days">>,SchedFilterMap) of
										true->
											SDays = maps:get(<<"days">>,SchedFilterMap),
											case is_integer(SDays) of
												true->
													THigh = base_time:now()+3600*24*SDays;
												_->
													THigh = "all",
													error_log:log(?MODULE,0,unknown,"\nERROR: Days in get resource info for sched not an integer. Fix front end code")
											end,
											SchedRequest = {get_del_or_pop_sched_by_times,"get",0,THigh};
										_->
											SchedRequest = {get_del_or_pop_all_sched,"get"}
									end
							end
					end;
				_->
					SchedRequest = {get_del_or_pop_all_sched,"get"}
			end,
			{ok,Schedule} = comms_api:forward_call(Address, sched, {storage_request,SchedRequest}),
			BinarySched = build_sched_map(Schedule, #{}),
			SJsonBcMap = maps:put(<<"sched">>, BinarySched, BJsonBcMap);
		_->
			SJsonBcMap = BJsonBcMap
	end,
	%%exe
	case maps:is_key(<<"exe">>,ComponentsMap) of
		true->
			BinaryExeTypes = maps:get(<<"exe">>,ComponentsMap),
			case is_list(BinaryExeTypes) of
				true->
					ExeTypes = custom_erlang_functions:binary_list_to_str_list(BinaryExeTypes,[]),
					ExeRequest = {get_del_or_pop_exe_by_types,"get",ExeTypes};
				_->
					ExeRequest = {get_del_or_pop_all_exe,"get"}
			end,
			{ok,Execution} = comms_api:forward_call(Address, exe, {storage_request,ExeRequest}),
			BinaryExe = build_exe_map(Execution, #{}),
			EJsonBcMap = maps:put(<<"exe">>, BinaryExe, SJsonBcMap);
		_->
			EJsonBcMap = SJsonBcMap
	end,
	EJsonBcMap.
	

jsone_decodable(Content)->
	try
		jsone:decode(Content,[{object_format, map}]),
		true
	catch 
		_:_->
			false
	end.

%%SLOc:481
				
				
				
				
				
				
				
				
				
				

	
	
	
	
	