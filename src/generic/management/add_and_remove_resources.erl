%% @author Daniel
%% @doc @todo Add description to new_resource.


-module(add_and_remove_resources).

%% ====================================================================
%% API functions
%% ====================================================================
-export([add/2,remove/2]).

%% ====================================================================
%% Internal functions
%% ====================================================================
add(Content,SocketProcess)->
	TS = erlang:timestamp(),
	case ui_state:allow(2) of
		allow->
			ContentMap = jsone:decode(Content,[{object_format, map}]),
			ID = binary_to_list(maps:get(<<"id">>,ContentMap)),
			Type = binary_to_list(maps:get(<<"type">>,ContentMap)),
			TwinType = maps:get(<<"twin_type">>,ContentMap),
			case TwinType of
				<<"base">>->
					{ok,BC} = business_cards:create_std_base_bc(ID, Type),
					Res = base_resource_creator:new_base_resource(top_sup, BC);
				_->%%basic
					ResourceDetailsMap = ContentMap,
					Res = base_resource_creator:new_basic_resource(ResourceDetailsMap)
			end,
			case Res of
				ok->
					system_restarts:log("New resource created"),
					ReplyMap = #{<<"SUBJECT">>=><<"Resource added">>,<<"CONTENT">>=><<"New resource added successfully">>};
				{ok,_S}->
					ReplyMap = #{<<"SUBJECT">>=><<"Resource added">>,<<"CONTENT">>=><<"New resource added successfully">>};
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
				error->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not create basic resource">>}
			end;
		ReplyMap->
			ok
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

remove(Content,SocketProcess)->
	TS = erlang:timestamp(),
	%error_log:log(?MODULE,0,unknown,"Remove timestamp start: ~p",[TS]),
	case ui_state:allow(2) of
		allow->
			Res = base_resource_creator:remove_resource(binary_to_list(Content)),
			TS2 = erlang:timestamp(),
			%error_log:log(?MODULE,0,unknown,"Remove timestamp done: ~p",[TS2]),
			case Res of
				ok->
					system_restarts:log(),
					ReplyMap = #{<<"SUBJECT">>=><<"Resource removed">>,<<"CONTENT">>=><<"Resource removed successfully">>};
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not remove resource">>}
			end;
		ReplyMap->
			ok
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
%%SLOc:57
