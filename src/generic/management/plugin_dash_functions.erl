%% @author Daniel
%% @doc @todo Add description to plugin_dash_functions.


-module(plugin_dash_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([handle/2,str_list_to_binary_list/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
%% ====================================================================
handle(Content,SocketProcess)->
	case ui_state:allow(1) of
		allow->
			ContentMap = jsone:decode(Content,[{object_format, map}]),
			BinaryAction = maps:get(<<"action">>,ContentMap),
			case BinaryAction of
				<<"get_directory_contents">>->
					get_directory_contents(ContentMap,SocketProcess);
				<<"create_plugin">>->
					create_plugin(ContentMap,SocketProcess);
				<<"create_folder">>->
					create_folder(ContentMap,SocketProcess);
				<<"delete_plugin">>->
					delete_plugin(ContentMap,SocketProcess);
				<<"get_plugin_settings_files">>->
					get_plugin_settings_files(ContentMap,SocketProcess);
				<<"get_plugin_settings">>->
					get_plugin_settings(ContentMap,SocketProcess);
				<<"remove_from_settings_file">>->
					remove_from_settings_file(ContentMap,SocketProcess);
				<<"add_to_settings_file">>->
					add_to_settings_file(ContentMap,SocketProcess);
				<<"delete_settings_file">>->
					delete_settings_file(ContentMap,SocketProcess);
				<<"create_settings_file">>->
					create_settings_file(ContentMap,SocketProcess);
				_->
					error_log:log(?MODULE,0,unknown,"\nUnknown plugin request from dash")
			end;
		ReplyMap->
			ReplyJSON = jsone:encode(ReplyMap),
			SocketProcess!{send,ReplyJSON}
	end.
		
get_directory_contents(ContentMap,SocketProcess)->
	Directory = binary_to_list(maps:get(<<"Directory">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	FullDir = string:concat(CurDir,Directory),
	{ok,FilesAndFolders} = file:list_dir(FullDir),
	FilesAndFoldersBin = str_list_to_binary_list(FilesAndFolders,[]),
	ContentMap = #{<<"Directory">>=>maps:get(<<"Directory">>,ContentMap),<<"Directory contents">>=>FilesAndFoldersBin},
	ReplyMap = #{<<"SUBJECT">>=><<"Directory contents">>,<<"CONTENT">>=>ContentMap},
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
create_plugin(ContentMap,SocketProcess)->
	FileNameAndExt = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	Directory = binary_to_list(maps:get(<<"Directory">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	FullDir = string:concat(CurDir,Directory),
	Temp = string:concat(FullDir,"/"),
	DirAndFile = string:concat(Temp,FileNameAndExt),
	FileContents = maps:get(<<"Contents">>,ContentMap),
	SrcFileList = string:split(FileNameAndExt, "."),
	SrcFileName = lists:nth(1, SrcFileList),
	PredictedName = string:concat(SrcFileName,".beam"),
	case module_checker:will_module_clash_with_existing(PredictedName) of
		true->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Another module in the core,plugins or basic resources has the same name. Rename your resource file if you want to use it or remove the old version first if that is causing the clash">>};
		_->
			Res = file:write_file(DirAndFile, FileContents),
			case Res of
				ok->
					CompRes = c:c(DirAndFile,[{outdir,FullDir}]),
					case CompRes of
						error->
							ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not compile the source file you provided">>};
						_->
							{ok,FilesAndFolders} = file:list_dir(FullDir),
							FilesAndFoldersBin = str_list_to_binary_list(FilesAndFolders,[]),
							ContentMap = #{<<"Directory">>=>maps:get(<<"Directory">>,ContentMap),<<"Directory contents">>=>FilesAndFoldersBin},
							ReplyMap = #{<<"SUBJECT">>=><<"Directory contents">>,<<"CONTENT">>=>ContentMap}
					end;
				{error,Err}->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>ErrS}
			end
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
create_folder(ContentMap,SocketProcess)->
	FolderName = binary_to_list(maps:get(<<"FolderName">>,ContentMap)),
	Directory = binary_to_list(maps:get(<<"Directory">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	FullDir = string:concat(CurDir,Directory),
	Temp = string:concat(FullDir,"/"),
	DirAndFolder = string:concat(Temp,FolderName),
	Res = file:make_dir(DirAndFolder),
	case Res of
		ok->
			{ok,FilesAndFolders} = file:list_dir(FullDir),
			ReplyMap = #{<<"SUBJECT">>=><<"Directory contents">>,<<"CONTENT">>=>FilesAndFolders};
		{error,Err}->
			ErrS = lists:flatten(io_lib:format("~p", [Err])),
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>ErrS}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
delete_plugin(ContentMap,SocketProcess)->
	FileNameAndExt = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	Directory = binary_to_list(maps:get(<<"Directory">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	FullDir = string:concat(CurDir,Directory),
	Temp = string:concat(FullDir,"/"),
	DirAndFile = string:concat(Temp,FileNameAndExt),
	case string:find(FileNameAndExt, ".") of
		nomatch->
			RawRes = file:del_dir(DirAndFile),
			case RawRes of
				{error,Error}->
					case Error of 
						eexist->
							Res = {error,<<"Folders must be empty before you delete them">>};
						_->
							Res = RawRes
					end;
				_->
					Res = RawRes
			end;
		_->
			Res = file:delete(DirAndFile),
			SrcFileList = string:split(FileNameAndExt, "."),
			SrcFileName = lists:nth(1, SrcFileList),
			SrcFileNameAndExt = string:concat(SrcFileName, ".erl"),
			DirAndSrcFile = string:concat(Temp, SrcFileNameAndExt),
			file:delete(DirAndSrcFile)
	end,
	case Res of
		ok->
			{ok,FilesAndFolders} = file:list_dir(FullDir),
			FilesAndFoldersBin = str_list_to_binary_list(FilesAndFolders,[]),
			ContentMap = #{<<"Directory">>=>maps:get(<<"Directory">>,ContentMap),<<"Directory contents">>=>FilesAndFoldersBin},
			ReplyMap = #{<<"SUBJECT">>=><<"Directory contents">>,<<"CONTENT">>=>ContentMap};
		{error,Err}->
			ErrS = lists:flatten(io_lib:format("~p", [Err])),
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>ErrS}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

get_plugin_settings_files(_ContentMap,SocketProcess)->
	{ok,CurDir} = file:get_cwd(),
	PluginSettingsDir = string:concat(CurDir, "/plugin_settings"),
	{ok,FilesAndFolders} = file:list_dir(PluginSettingsDir),
	FilesAndFoldersBin = str_list_to_binary_list(FilesAndFolders,[]),
	ReplyMap = #{<<"SUBJECT">>=><<"Plugin settings files">>,<<"CONTENT">>=>FilesAndFoldersBin},
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

get_plugin_settings(ContentMap,SocketProcess)->
	FileName = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	SettingsDir = string:concat(CurDir,"/plugin_settings/"),
	DirAndFile = string:concat(SettingsDir,FileName),
	Res = file:read_file(DirAndFile),
	case Res of
		{ok,FileContent}->
			SettingsMap = jsone:decode(FileContent,[{object_format, map}]),
			ReplyMap = #{<<"SUBJECT">>=><<"Plugin settings">>,<<"CONTENT">>=>SettingsMap};
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not get settings file contents">>}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

remove_from_settings_file(ContentMap,SocketProcess)->
	FileName = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	SettingNr = maps:get(<<"setting_nr">>,ContentMap),
	{ok,CurDir} = file:get_cwd(),
	SettingsDir = string:concat(CurDir,"/plugin_settings/"),
	DirAndFile = string:concat(SettingsDir,FileName),
	Res = file:read_file(DirAndFile),
	case Res of
		{ok,FileContent}->
			SettingsMap = jsone:decode(FileContent,[{object_format, map}]),
			NewSettingsMap = maps:remove(SettingNr, SettingsMap),
			SettingsKeys = maps:keys(NewSettingsMap),
			NewNewSettingsMap = order_new_settings_map(SettingsKeys,binary_to_integer(SettingNr),NewSettingsMap),
			NewFileContent = jsone:encode(NewNewSettingsMap),
			SaveRes = file:write_file(DirAndFile, NewFileContent),
			case SaveRes of
				ok->
					ReplyMap = #{<<"SUBJECT">>=><<"Plugin settings">>,<<"CONTENT">>=>NewNewSettingsMap};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not save new settings file">>}
			end;
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not get settings file contents">>}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

add_to_settings_file(ContentMap,SocketProcess)->
	FileOrFolder = binary_to_list(maps:get(<<"FileOrFolder">>,ContentMap)),
	case string:find(FileOrFolder, "/") of
		nomatch->
			%this is a file 
			NewSetting = #{<<"type">>=><<"file">>,<<"file_or_folder">>=>list_to_binary(FileOrFolder),<<"args">>=>maps:get(<<"args">>,ContentMap)};
		_->
			NewSetting = #{<<"type">>=><<"folder">>,<<"file_or_folder">>=>list_to_binary(FileOrFolder)}
	end,
	FileName = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	SettingsDir = string:concat(CurDir,"/plugin_settings/"),
	DirAndFile = string:concat(SettingsDir,FileName),
	Res = file:read_file(DirAndFile),
	case Res of
		{ok,FileContent}->
			SettingsMap = jsone:decode(FileContent,[{object_format, map}]),
			SettingsKeys = maps:keys(SettingsMap),
			NewKeyNr = length(SettingsKeys)+1,
			NewKey = integer_to_binary(NewKeyNr),
			NewSettingsMap = maps:put(NewKey, NewSetting, SettingsMap),
			NewFileContent = jsone:encode(NewSettingsMap),
			SaveRes = file:write_file(DirAndFile, NewFileContent),
			case SaveRes of
				ok->
					ReplyMap = #{<<"SUBJECT">>=><<"Plugin settings">>,<<"CONTENT">>=>NewSettingsMap};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not save new settings file">>}
			end;
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not get settings file contents">>}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
			
delete_settings_file(ContentMap,SocketProcess)->
	FileName = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	SettingsDir = string:concat(CurDir,"/plugin_settings/"),
	DirAndFile = string:concat(SettingsDir,FileName),
	Res = file:delete(DirAndFile),
	case Res of
		ok->
			PluginSettingsDir = string:concat(CurDir, "/plugin_settings"),
			{ok,FilesAndFolders} = file:list_dir(PluginSettingsDir),
			FilesAndFoldersBin = str_list_to_binary_list(FilesAndFolders,[]),
			ReplyMap = #{<<"SUBJECT">>=><<"Plugin settings files">>,<<"CONTENT">>=>FilesAndFoldersBin};
		_->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not delete settings file">>}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

create_settings_file(ContentMap,SocketProcess)->
	FileName = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	SettingsDir = string:concat(CurDir,"/plugin_settings/"),
	DirAndFile = string:concat(SettingsDir,FileName),
	EmptySettings = jsone:encode(#{}),
	PluginSettingsDir = string:concat(CurDir, "/plugin_settings"),
	{ok,FilesAndFolders} = file:list_dir(PluginSettingsDir),
	
	case lists:member(FileName, FilesAndFolders) of
		true->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not add settings file. Another settings file already has this name">>};
		_->
			Res = file:write_file(DirAndFile, EmptySettings),
			case Res of
				ok->
					{ok,NewFilesAndFolders} = file:list_dir(PluginSettingsDir),
					FilesAndFoldersBin = str_list_to_binary_list(NewFilesAndFolders,[]),
					ReplyMap = #{<<"SUBJECT">>=><<"Plugin settings files">>,<<"CONTENT">>=>FilesAndFoldersBin};
				_->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not create new settings file">>}
			end
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
order_new_settings_map([],_SettingsNr,SettingsMap)->
	SettingsMap;
order_new_settings_map([Key|T],SettingNr,SettingsMap)->
	KeyNr = binary_to_integer(Key),
	case (KeyNr>SettingNr) of
		true->
			NewKeyNr = KeyNr-1,
			NewBKey = integer_to_binary(NewKeyNr),
			Val = maps:get(Key, SettingsMap),
			CleanMap = maps:remove(Key, SettingsMap),
			NewMap = maps:put(NewBKey, Val, CleanMap),
			order_new_settings_map(T,SettingNr,NewMap);
		_->
			order_new_settings_map(T,SettingNr,SettingsMap)
	end.

str_list_to_binary_list([],BinList)->
	BinList;
str_list_to_binary_list([H|T],BinList)->
	NewH = list_to_binary(H),
	str_list_to_binary_list(T,[NewH|BinList]).

%%SLOC:281
