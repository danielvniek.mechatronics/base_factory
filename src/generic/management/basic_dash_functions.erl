%% @author Daniel
%% @doc @todo Add description to plugin_dash_functions.


-module(basic_dash_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([handle/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
%% ====================================================================
handle(Content,SocketProcess)->
	case ui_state:allow(1) of
		allow->
			ContentMap = jsone:decode(Content,[{object_format, map}]),
			BinaryAction = maps:get(<<"action">>,ContentMap),
			case BinaryAction of
				<<"create_basic_file">>->
					create_basic_file(ContentMap,SocketProcess);
				<<"create_basic_folder">>->
					create_basic_folder(ContentMap,SocketProcess);
				<<"delete_file_or_folder">>->
					delete_file_or_folder(ContentMap,SocketProcess);
				<<"get_basic_folders">>->
					get_basic_folders(ContentMap,SocketProcess);
				<<"get_folder_contents">>->
					get_folder_contents(ContentMap,SocketProcess);
				_->
					error_log:log(?MODULE,0,unknown,"\nUnknown plugin request from dash")
			end;
		ReplyMap->
			ReplyJSON = jsone:encode(ReplyMap),
			SocketProcess!{send,ReplyJSON}
	end.	
		
create_basic_folder(ContentMap,SocketProcess)->
	FolderName = binary_to_list(maps:get(<<"FolderName">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	BasicDir = string:concat(CurDir, "/basic_resources"),
	Temp = string:concat(BasicDir,"/"),
	NewDir = string:concat(Temp, FolderName),
	Res = file:make_dir(NewDir),
	case Res of
		ok->
			{ok,Folders} = file:list_dir(BasicDir),
			BinFolders = plugins_dash_functions:str_list_to_binary_list(Folders,[]),
			ReplyMap = #{<<"SUBJECT">>=><<"Basic folders">>,<<"CONTENT">>=>BinFolders};
		{error,Err}->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>Err}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

create_basic_file(ContentMap,SocketProcess)->
	FileNameAndExt = binary_to_list(maps:get(<<"FileName">>,ContentMap)),
	Directory = binary_to_list(maps:get(<<"Directory">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	FullDir = string:concat(CurDir,Directory),
	Temp = string:concat(FullDir,"/"),
	DirAndFile = string:concat(Temp,FileNameAndExt),
	FileContents = maps:get(<<"Contents">>,ContentMap),
	SrcFileList = string:split(FileNameAndExt, "."),
	SrcFileName = lists:nth(1, SrcFileList),
	PredictedName = string:concat(SrcFileName,".beam"),
	case module_checker:will_module_clash_with_existing(PredictedName) of
		true->
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Another module in the core,plugins or basic resources has the same name. Rename your resource file if you want to use it or remove the old version first if that is causing the clash">>};
		_->
			Res = file:write_file(DirAndFile, FileContents),
			case Res of
				ok->
					CompRes = c:c(DirAndFile,[{outdir,FullDir}]),
					case CompRes of
						error->
							ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=><<"Could not compile the source file you provided">>};
						_->
							{ok,FilesAndFolders} = file:list_dir(FullDir),
							BinFiles = plugin_dash_functions:str_list_to_binary_list(FilesAndFolders, []),
							ReturnContentMap = #{<<"files">>=>BinFiles,<<"Directory">>=>list_to_binary(Directory)},
							ReplyMap = #{<<"SUBJECT">>=><<"Basic folder contents">>,<<"CONTENT">>=>ReturnContentMap}
					end;
				{error,Err}->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>Err}
			end
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

delete_file_or_folder(ContentMap,SocketProcess)->
	FileNameAndExt = binary_to_list(maps:get(<<"FileOrFolder">>,ContentMap)),
	Directory = binary_to_list(maps:get(<<"Directory">>,ContentMap)),
	{ok,CurDir} = file:get_cwd(),
	FullDir = string:concat(CurDir,Directory),
	Temp = string:concat(FullDir,"/"),
	DirAndFile = string:concat(Temp,FileNameAndExt),
	case string:find(FileNameAndExt, ".") of
		nomatch->
			RawRes = file:del_dir(DirAndFile),
			case RawRes of
				{error,Error}->
					case Error of 
						eexist->
							Res = {error,<<"Folders must be empty before you delete them">>};
						_->
							Res = RawRes
					end;
				_->
					Res = RawRes
			end,
			case Res of
				ok->
					{ok,Folders} = file:list_dir(FullDir),
					BinFolders = plugin_dash_functions:str_list_to_binary_list(Folders, []),
					ReplyMap = #{<<"SUBJECT">>=><<"Basic folders">>,<<"CONTENT">>=>BinFolders};
				{error,Err}->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>Err}
			end;
		_->
			Res = file:delete(DirAndFile),
			SrcFileList = string:split(FileNameAndExt, "."),
			SrcFileName = lists:nth(1, SrcFileList),
			SrcFileNameAndExt = string:concat(SrcFileName, ".erl"),
			DirAndSrcFile = string:concat(Temp, SrcFileNameAndExt),
			file:delete(DirAndSrcFile),
			case Res of
				ok->
					{ok,FilesAndFolders} = file:list_dir(FullDir),
					BinFilesAndFolders = plugin_dash_functions:str_list_to_binary_list(FilesAndFolders, []),
					ReturnContentMap = #{<<"files">>=>BinFilesAndFolders,<<"Directory">>=>list_to_binary(Directory)},
					ReplyMap = #{<<"SUBJECT">>=><<"Basic folder contents">>,<<"CONTENT">>=>ReturnContentMap};
				{error,Err}->
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>Err}
			end
	end,
	
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

get_basic_folders(_ContentMap,SocketProcess)->
	{ok,CurDir} = file:get_cwd(),
	BasicDir = string:concat(CurDir, "/basic_resources"),
	{ok,Folders} = file:list_dir(BasicDir),
	FoldersBin = plugin_dash_functions:str_list_to_binary_list(Folders, []),
	ReplyMap = #{<<"SUBJECT">>=><<"Basic folders">>,<<"CONTENT">>=>FoldersBin},
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

get_folder_contents(ContentMap,SocketProcess)->
	{ok,CurDir} = file:get_cwd(),
	BasicDir = string:concat(CurDir, "/basic_resources/"),
	FolderName = binary_to_list(maps:get(<<"FolderName">>,ContentMap)),
	FullDir = string:concat(BasicDir,FolderName),
	{ok,Files} = file:list_dir(FullDir),
	BinFiles = plugin_dash_functions:str_list_to_binary_list(Files, []),
	Directory = string:concat("/basic_resources/",FolderName),
	ReturnContentMap = #{<<"files">>=>BinFiles,<<"Directory">>=>list_to_binary(Directory)},
	ReplyMap = #{<<"SUBJECT">>=><<"Basic folder contents">>,<<"CONTENT">>=>ReturnContentMap},
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.
%%SLOC:142

