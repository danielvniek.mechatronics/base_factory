%% @author Daniel
%% @doc @todo Add description to search_resources.


-module(search_resources).

%% ====================================================================
%% API functions
%% ====================================================================
-export([search_resources/2]).

search_resources(Content,SocketProcess)->
	case ui_state:allow(5) of
		allow->
			ContentMap = jsone:decode(Content,[{object_format, map}]),
			SearchBy = maps:get(<<"SearchBy">>,ContentMap),
			SearchParameters = maps:get(<<"SearchParameters">>,ContentMap),
			case SearchBy of
				<<"all">>->
					SearchList = [];
				_->
					case is_list(SearchParameters) and not(SearchBy==<<"service">>) and not(SearchBy==<<"all">>) of
						true->
							SearchList = custom_erlang_functions:binary_list_to_str_list(SearchParameters, []);
						_->
							SearchList = [],
							error_log:log(?MODULE,0,unknown,"\nERROR: SearchParameters is not a list: fix front end code")
					end
			end,
			case SearchBy of
				<<"ids">>->
					{Res,Return}=doha_api:get_bcs_by_ids(SearchList);
				<<"types">>->
					{Res,Return} = doha_api:get_bcs_by_types(SearchList);
				<<"ids_and_type">>->
					Type = binary_to_list(maps:get(<<"type">>,ContentMap)),
					{Res,Return} = doha_api:get_bcs_by_ids_and_type(SearchList, Type);
				<<"service">>->
					{Res,Return}=doha_api:get_bcs_by_service(SearchParameters);
				<<"all">>->
					{Res,Return} = doha_api:get_all_bcs();
				_->
					error_log:log(?MODULE,0,unknown,"\nDash Socket: Unknown searchby parameter"),
					Res = error,
					Return = "Unknown SEARCHBY parameter"
			end,
			case Res of
				ok->
					case maps:is_key(<<"components">>,ContentMap) of
						true->
							Components = maps:get(<<"components">>,ContentMap),
							ComponentsMap = jsone:decode(Components, [{object_format, map}]);
						_->
							ComponentsMap = #{}
					end,
					JsonBCs = bcs_to_json(Return,#{},ComponentsMap),
					ReplyMap = #{<<"SUBJECT">>=><<"Existing Resources">>,<<"CONTENT">>=>JsonBCs};
				Err->
					ErrS = lists:flatten(io_lib:format("~p", [Err])),
					ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)}
			end;
		ReplyMap->
			ok
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.


bcs_to_json([],JsonMap,_ComponentsMap)->
	case length(maps:keys(JsonMap)) of
		0->
			<<"">>;
		_->
			JsonMap
	end;
bcs_to_json([BC|T],JsonMap,ComponentsMap)->
	{ok,Architecture} = business_cards:get_architecture(BC),
	case Architecture of
		basic->
			{ok,Address} = business_cards:get_address(BC),
			JsonBcMap = get_resource_info:get_all_info(Address);
		_->
			JsonBcMap = get_resource_info:get_specific_info(BC,ComponentsMap)
	end,
	Keys = maps:keys(JsonMap),
	BCnr = length(Keys)+1,
	BCBinaryKey = erlang:integer_to_binary(BCnr),
	NewJsonMap = maps:put(BCBinaryKey, JsonBcMap, JsonMap),
	bcs_to_json(T,NewJsonMap,ComponentsMap).

%%76

