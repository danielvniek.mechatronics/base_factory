%% @author Daniel
%% @doc @todo Add description to request_and_inform.


-module(request_and_inform).

%% ====================================================================
%% API functions
%% ====================================================================
-export([request/2,inform/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================

request(Content,SocketProcess)->
	ContentMap = jsone:decode(Content,[{object_format, map}]),
	ID = binary_to_list(maps:get(<<"id">>,ContentMap)),
	ResourceAdr = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
	InfoRequested = binary_to_list(maps:get(<<"type">>,ContentMap)),
	Dets = maps:get(<<"details">>,ContentMap),
	Reply = comms_api:request_info(ResourceAdr, InfoRequested, Dets),
	case Reply of
		{ok,ReplyM}->
			ReplyMap = #{<<"SUBJECT">>=><<"Request reply">>,<<"CONTENT">>=>maps:put(<<"id">>,list_to_binary(ID),ReplyM)};
		Err->
			ErrS = lists:flatten(io_lib:format("~p", [Err])),
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

inform(Content,SocketProcess)->
	ContentMap = jsone:decode(Content,[{object_format, map}]),
	ID = binary_to_list(maps:get(<<"id">>,ContentMap)),
	ResourceAdr = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
	InformType = binary_to_list(maps:get(<<"type">>,ContentMap)),
	Dets = maps:get(<<"details">>,ContentMap),
	Reply = comms_api:inform(ResourceAdr, InformType, Dets),
	case Reply of
		ok->
			ReplyMap = #{<<"SUBJECT">>=><<"Notify">>,<<"CONTENT">>=><<"Resource accepted information">>};
		Err->
			ErrS = lists:flatten(io_lib:format("~p", [Err])),
			ReplyMap = #{<<"SUBJECT">>=><<"Error">>,<<"CONTENT">>=>list_to_binary(ErrS)}
	end,
	ReplyJSON = jsone:encode(ReplyMap),
	SocketProcess!{send,ReplyJSON}.

%%SLOC:33