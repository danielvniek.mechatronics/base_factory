%% @author Daniel
%% @doc @todo Add description to bf.


-module(bf).

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
start()->
	{ok,CurDir} = file:get_cwd(),
	module_checker:add_all_subdirectories_to_path(CurDir),
	ok = application:start(crypto),
	ok = application:start(asn1),
	ok = application:start(public_key),
	ok = application:start(ssl),
	ok = application:start(ranch),
	ok = application:start(cowlib),
	ok = application:start(cowboy),
	ok = application:start(base_factory).
	

