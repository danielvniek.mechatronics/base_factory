-module(business_cards).
-author("Daniel van Niekerk (original bc by Dale Sparrow)").

-export([
  id_to_adr/1,valid_base_bc/1,valid_services/1,create_std_base_bc/2,create_basic_bc/1,create_new_bc/5,get_service_description/2,update_address/2,get_address/1,get_id/1,
  get_type/1,get_services/1,get_service_types/1,get_architecture/1]).

%A business card has the following structure:
%#{<<"id">>=>Id,<<"type">>=>Type,<<"address">>=>atom_address,<<"services">>=>#{"ServType1"=>ServDescr(any term),<<"ServType2">>=>...},<<"architecture">>=>base/basic}
id_to_adr(ID)->
	Adr = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
	Adr.

valid_base_bc(BC)->
	case erlang:is_map(BC) of
		true->
			case maps:is_key(<<"id">>, BC) and maps:is_key(<<"type">>, BC) and maps:is_key(<<"address">>, BC) and maps:is_key(<<"services">>, BC) and maps:is_key(<<"architecture">>, BC) of
				true->
					IdRes = custom_erlang_functions:is_string(maps:get(<<"id">>, BC)),
					TypeRes = custom_erlang_functions:is_string(maps:get(<<"type">>,BC)),
					AdrRes = is_atom(maps:get(<<"address">>, BC)),
					ServRes = valid_services(maps:get(<<"services">>, BC)),
					ArchRes = (maps:get(<<"architecture">>,BC)==base) or (maps:get(<<"architecture">>,BC)==basic),
					case IdRes and TypeRes and AdrRes and ServRes and ArchRes of 
						true->
							true;
						_->
							io:format("\nBC ~p invalid: \nIDRes: ~p,TypeRes:~p,AdrRes:~p,ServRes:~p,ArchRes:~p",[BC,IdRes,TypeRes,AdrRes,ServRes,ArchRes]),
							false
					end;
				_->
					false
			end;
		_->
			false
	end.

create_std_base_bc(ID,Type)->
	Adr = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
    create_new_bc(ID,Type,Adr,#{},base).

create_basic_bc(ResourceDetailsMap)->
	try
		ID = binary_to_list(maps:get(<<"id">>,ResourceDetailsMap)),
		Type = binary_to_list(maps:get(<<"type">>,ResourceDetailsMap)),
		Adr = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
		create_new_bc(ID,Type,Adr,#{},basic)
	catch
		A:B:StackTrace->
			io:format("\nCould not create basic bc. Caught ~p:~p:~p",[A,B,StackTrace]),
			{error,"Invalid resource details map"}
	end.

create_new_bc(ID,Type,Address,Services,Architecture)->
  case custom_erlang_functions:is_string(ID) of
	  true->
		  case custom_erlang_functions:is_string(Type) of
			  true->
				  case is_atom(Address) of
					  true->
						  case valid_services(Services) of
							  true->
								  case (Architecture==base) or (Architecture==basic) of
									  true->
					  			  		{ok,#{<<"id">> => ID, <<"type">> => Type,<<"address">> => Address, <<"services">> => Services,<<"architecture">>=>Architecture}};
									  _->
										  {error,"Invalid business card architecture"}
								  end;
							  _->
								  {error, "Invalid business card services"}
						  end;
					  _->
						  {error, "Business card address must be an atom"}
				  end;
			  _->
				  {error, "Business card type must be string"}
		  end;
	  _->
		  {error,"Business card id must be string"}
  end.

get_service_description(Services,ServiceType)->
	case valid_services(Services) of
		true->
			{ok,maps:get(ServiceType,Services)};
		_->
			{error,"Invalid services"}
	end.

valid_services(Services)->
	case is_map(Services) of
		true->
			ServTypes = maps:keys(Services),
			custom_erlang_functions:isListOfStrings(ServTypes);
		_->
			false
	end.

			
update_address(BC,Address)->
	case valid_base_bc(BC) and is_atom(Address) of
		true->
		  NewBc = maps:update(<<"address">>, Address, BC),
		  {ok,NewBc};
	   _->
		  {error,"Invalid BC or ErlAdr"}
	end.

get_id(BC)->
  case valid_base_bc(BC)of
	  true->
		  {ok,maps:get(<<"id">>,BC)};
	  _->
		   {error, "Invalid BC"}
  end.

get_type(BC)->
  case valid_base_bc(BC)of
	  true->
		  {ok,maps:get(<<"type">>,BC)};
	  _->
		   {error, "Invalid BC"}
  end.

get_address(BC)->
	case valid_base_bc(BC)of
		true->
		  {ok,maps:get(<<"address">>,BC)};
		_->
			{error, "Invalid BC"}
	end.

get_services(BC)->
	case valid_base_bc(BC) of
		true->
			{ok,maps:get(<<"services">>,BC)};
		_->
			{error,"Invalid BC"}
	end.

get_service_types(BC)->
	case valid_base_bc(BC) of
		true->
			Services = maps:get(<<"services">>,BC),
			{ok,maps:keys(Services)};
		_->
			{error,"Invalid BC"}
	end.

get_architecture(BC)->
	case valid_base_bc(BC) of
		true->
			{ok,maps:get(<<"architecture">>,BC)};
		_->
			{error,"Invalid BC"}
	end.

%%SLOC:133
