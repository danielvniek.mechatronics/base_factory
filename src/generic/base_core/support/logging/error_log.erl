
-module(error_log).
-author("Daniel van Niekerk").

-behaviour(gen_server).
-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([start_link/0,log/4,log/5,clear/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(hi_state, {ets_table::term(),file_path="",key_nr,start_time}).
-record(error_record,{time,module,line,resource_id,error}).
%%%===================================================================
log(Module,Line,ResourceId,Error,Terms)->
	Err = lists:flatten(io_lib:format(Error,Terms)),
	spawn(fun()->timer:sleep(10000),custom_erlang_functions:myGenServCall(?MODULE, {log,Module,Line,ResourceId,Err})end).
log(Module,Line,ResourceId,Error)->
	spawn(fun()->timer:sleep(10000),custom_erlang_functions:myGenServCall(?MODULE, {log,Module,Line,ResourceId,Error})end).
clear()->
	gen_server:call(error_log,clear).
start_link() ->
  {ok,_PID} = gen_server:start_link({local,?MODULE},?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks

init([]) ->
  	NewState = create_or_load_table(), 
   	yes = global:register_name(?MODULE,self()),
  	{ok, NewState}.
%%--------------------------------------------------------------------

handle_call({log,Module,Line,ResourceId,Error},_From,State)->
	io:format("\nLogging error. Error:~p \nModule:~p \nLine: ~p \nResourceId:~p \n",[Error,Module,Line,ResourceId]),
	NewRecord = #error_record{time=base_time:now(),module=Module,line=Line,resource_id=ResourceId,error=Error},
	{ok,"success"} = def_storage_functions:save(State#hi_state.ets_table,[NewRecord],State#hi_state.file_path),
	{reply,ok,State};

handle_call(clear,_From,State)->
	true = ets:delete_all_objects(State#hi_state.ets_table),
	io:format("\nError log cleared"),
	{reply,ok,State};

handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.


handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Holon interactions ets got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
%%--------------------------------------------------------------------

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Table functions:
create_or_load_table()->
	TableName = ?MODULE,
	{ok, CurrentDirectory} = file:get_cwd(),
	FileDir = string:concat(CurrentDirectory,"/Storage/logging/"),
	ok = filelib:ensure_dir(FileDir),
	FilePath = string:concat(FileDir,"error_log.txt"),
	ETS_table = def_storage_functions:claim_or_create_ets(TableName,FilePath),
	NrObj = ets:info(ETS_table,size),
	State = #hi_state{ets_table = ETS_table,file_path = FilePath,key_nr=NrObj},
	State.

%%SLOC:36


	


