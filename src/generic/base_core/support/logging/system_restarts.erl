
-module(system_restarts).
-author("Daniel van Niekerk").

-behaviour(gen_server).
-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([start_link/0,finished/2,log/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(hi_state, {ets_table::term(),file_path="",key_nr,start_time}).
-record(restart_record,{time,event,duration,nr_resources,nr_tabs,nr_procs,total_mem,ets_mem}).
%%%===================================================================
log(Event)->
	spawn(fun()->timer:sleep(10000),custom_erlang_functions:myGenServCall(?MODULE, {log,Event})end).

finished(MicroSecond,NrResources)->
	spawn(fun()->custom_erlang_functions:myGenServCall(?MODULE, {finished,MicroSecond,NrResources})end).

start_link() ->
  {ok,_PID} = gen_server:start_link({local,?MODULE},?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks

init([]) ->
  	NewState = create_or_load_table(), 
   	yes = global:register_name(?MODULE,self()),
  	{ok, NewState#hi_state{start_time=erlang:system_time(microsecond)}}.
%%--------------------------------------------------------------------

handle_call({finished,SysT,NrResources}, _From, State)->
	Duration = SysT - State#hi_state.start_time,
	NrProcesses = erlang:system_info(process_count),
	NrTables = length(ets:all()),
	{TotMem,EtsMem} = get_total_and_ets_memory(),
	NewRecord = #restart_record{time=base_time:now(),event = "System restarted",duration=Duration,nr_resources=NrResources,nr_tabs=NrTables,nr_procs = NrProcesses,total_mem = TotMem,ets_mem=EtsMem},
	{ok,"success"}=def_storage_functions:save(State#hi_state.ets_table,[NewRecord],State#hi_state.file_path),
	spawn_link(fun()->logger()end),
	{reply,ok,State};

handle_call({log,Event},_From,State)->
	NrProcesses = erlang:system_info(process_count),
	NrTables = length(ets:all()),
	{TotMem,EtsMem} = get_total_and_ets_memory(),
	{ok,BCs} = doha_api:get_all_bcs(),
	NrResources = length(BCs),
	NewRecord = #restart_record{time=base_time:now(),event = Event,duration=na,nr_resources=NrResources,nr_tabs=NrTables,nr_procs = NrProcesses,total_mem = TotMem,ets_mem=EtsMem},
	{ok,"success"} = def_storage_functions:save(State#hi_state.ets_table,[NewRecord],State#hi_state.file_path),
	{reply,ok,State};

handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.


handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Holon interactions ets got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
%%--------------------------------------------------------------------

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Table functions:
create_or_load_table()->
	TableName = ?MODULE,
	{ok, CurrentDirectory} = file:get_cwd(),
	FileDir = string:concat(CurrentDirectory,"/Storage/logging/"),
	ok = filelib:ensure_dir(FileDir),
	FilePath = string:concat(FileDir,"system_log.txt"),
	ETS_table = def_storage_functions:claim_or_create_ets(TableName,FilePath),
	NrObj = ets:info(ETS_table,size),
	State = #hi_state{ets_table = ETS_table,file_path = FilePath,key_nr=NrObj},
	State.

get_total_and_ets_memory()->
	TotMem = erlang:memory(total)*0.000001,
	EtsMem = erlang:memory(ets)*0.000001,
	{TotMem,EtsMem}.
	
logger()->
	timer:sleep(30*60*1000),
	ok = custom_erlang_functions:myGenServCall(?MODULE, {log,none}),
	logger().
	


