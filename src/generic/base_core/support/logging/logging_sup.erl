%% @author Daniel
%% @doc @todo Add description to holon_interactions_sup.


-module(logging_sup).

-behaviour(supervisor).

-export([start_link/0, init/1]).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  ETS1 = #{id => 'system_restarts',
    start => {system_restarts, start_link, []},
    restart => temporary,
    shutdown => 2000,
    type => worker,
    modules => [system_restarts]},
  ETS2 = #{id => 'error_log',
    start => {error_log, start_link, []},
    restart => temporary,
    shutdown => 2000,
    type => worker,
    modules => [error_log]},
  

  {ok, {#{strategy => one_for_one,
    intensity => 5,
    period => 30},
    [ETS1,ETS2]}
  }.
%%SLOC:20