%% @author Daniel
%% @doc @todo Add description to def_storage_functions.


-module(def_storage_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([save/3,get_del_or_pop/6,claim_or_create_ets/2,create_or_load_table/3]).



%% ====================================================================
%% Internal functions
%% ====================================================================
save(Table, [],FilePath)->
	  ok = ets:tab2file(Table,FilePath),%%There should be no reason why this fails, thus no need for result checking
	  {ok,"success"};

save(Table,[H|T],FilePath)->
	ets:insert(Table,H),
    save(Table,T,FilePath).

get_del_or_pop(Table,GetDelOrPop, MatchSpecTuple,Content, ReturnVal,FilePath)->
	{MatchSpecMod,GetSpecFunc,DelSpecFunc} = MatchSpecTuple,
	case Content of
		none->
			case GetDelOrPop of
				"get"->
					GetSpec = MatchSpecMod:GetSpecFunc(),
					DelSpec = none;
				"del"->
					DelSpec = MatchSpecMod:DelSpecFunc(),
					GetSpec = none;
				_->
					GetSpec = MatchSpecMod:GetSpecFunc(),
					DelSpec = MatchSpecMod:DelSpecFunc()
			end,
			RemainderList = [];
		_->
			case GetDelOrPop of
				"get"->
					{GetSpec,RemainderList} = MatchSpecMod:GetSpecFunc(Content),
					DelSpec = none;
				"del"->
					{DelSpec,RemainderList} = MatchSpecMod:DelSpecFunc(Content),
					GetSpec = none;
				_->
					{GetSpec,RemainderList} = MatchSpecMod:GetSpecFunc(Content),
					{DelSpec,_RemainderList} = MatchSpecMod:DelSpecFunc(Content)
			end
	end,
	case GetDelOrPop of
		"pop"->
			Activities = ets:select(Table,GetSpec),
			NewReturnVal = lists:append(ReturnVal, Activities),
	  		ets:select_delete(Table, DelSpec);
		"get"->
			Activities = ets:select(Table,GetSpec),
			NewReturnVal = lists:append(ReturnVal, Activities);
		"del"->
			NrDeleted = ets:select_delete(Table,DelSpec),
			case NrDeleted of
				  0->
					  NewReturnVal = ReturnVal;
				  _->
					  NewReturnVal = "success"
			end
	end,
	case RemainderList of
		[]->
			  ok = ets:tab2file(Table,FilePath),
			  case (NewReturnVal == []) and (GetDelOrPop == "del") of
			     true->
				     {ok,"none"};
			    _->
				     {ok,NewReturnVal}
			  end;
		_->
			get_del_or_pop(Table,GetDelOrPop, MatchSpecTuple, RemainderList,NewReturnVal,FilePath)
	end.

create_or_load_table(ID,Type,StorageType)->
	StorageTypeTable = string:concat(string:concat("_",StorageType),"_table"),
	TableName = erlang:list_to_atom(string:concat(string:lowercase(string:replace(ID, " ", "_",all)), StorageTypeTable)),
	{ok, CurrentDirectory} = file:get_cwd(),
	Temp = string:concat(CurrentDirectory,"/Storage/default_storage/"),
	Temp1 = string:concat(Temp,Type),
	TypeFolder = string:concat(Temp1,"/"),
	Temp2 = string:concat(TypeFolder,ID),
	IDFolder = string:concat(Temp2,"/"),
	ok = filelib:ensure_dir(TypeFolder),
	ok = filelib:ensure_dir(IDFolder),
	FileName = string:concat(IDFolder, StorageType),
	FilePath = string:concat(FileName,".txt"),
	ETS_table = claim_or_create_ets(TableName,FilePath),
	{ETS_table,FilePath}.

claim_or_create_ets(TableName,FilePath)->
  {Result,FileTable} = ets:file2tab(FilePath),
  case Result of 
	  ok->
		  %%table loaded from file
		ETS_table = FileTable;
	  _->
		  %%new table created
		ETS_table = ets:new(TableName,[ordered_set,{keypos,2},named_table]),
		ok = ets:tab2file(ETS_table,FilePath)
  end,	      
  ETS_table.

%%SLOC:92
