%%%-------------------------------------------------------------------
%%% @author Dale Sparrow
%%% @copyright (C) 2019, <Stellenbosch University MnM Department>
%%% @doc
%%% Records used for communication between BASE core components
%%% @end
%%% Created : 25. Nov 2019 5:20 PM
%%%-------------------------------------------------------------------
-author("Dale Sparrow and Daniel van Niekerk").
-record(base_settings,{bio_settings::list(),
  attr_settings::list(),
  sched_settings::list(),
  exe_settings::list(),
  comms_settings::list()}).
%% A General message sent to a base component
-record(base_message,
{ reply_format :: erl_term|gen_server_call|json|xml,
  convo_id :: string(),
  sender_bc :: term(),
  target_component:: comms_manager|biography|attributes|schedule|execution|ap|ep|sp|rp,
  payload :: term()}).

-record(base_login_request, {user_name::string(),password::string()}).

-record(base_query,
{sender_address :: string(),
  reply_address :: string(),
  convo_id :: string(),
  sender_name :: string(),
  question :: term()}).

-record(base_attribute,
{
  id :: string(),
  type:: string(),
  context :: string(),%%can be used for units if the designer wants or anything else that gives the value context
  value:: term()
}).
-record(base_attr_desc,
{
  id :: string(),
  type:: personal|contextual,
  context :: term()
}).

-record(base_state_var,
{
  id :: string(),
  type:: term(),
  context :: term(),
  value:: term()
}).

%This trecord is used to define a search pattern for shells.
%The BASE docks will check that a search query is valid before sending it off to the docked BASE component.
-record(base_shell_query,
{
  stage = 3,
  id  = any,
  type = any,
  earliest_tstart  = any,
  latest_tstart = any,
  earliest_tsched = any,
  latest_tsched = any,
  earliest_tend = any,
  latest_tend = any
}).


-record(vct_variable,
{
  var_name :: term(),
  value :: term(),
  confidence :: number(),
  time :: integer()
}).


-record(monitor,
{
  type :: atom(), %The type of monitor to be set, either exist, match, or stream.
  timout :: integer(),
  reportAddress :: term(),
  var_id :: term(),
  update_rate :: integer(), %the rate to send updates for a stream type monitor
  match_value :: term() %the value of the monitored variable should match for a match type monitor
}).

-type address() ::{atom(),atom(),term()}.
-record(address,
{
  type:: atom(), %The type of the address can be pid|ip|url
  value:: term() %The actual value which will be a PID or an IP and port
}).

-type service_desc() ::{string(),term()}.
-record(service_desc,
{
  service_name::string(),
  service_deets::term()
}).

-record(instance_pids,
{
  atr_recep::pid(),
  atr_def_storage::pid(),
  bio_recep::pid(),
  bio_def_storage::pid(),
  exe_recep::pid(),
  exe_def_storage::pid(),
  sched_recep::pid(),
  sched_def_storage::pid(),
  plugin_sup::pid(),
  coordinator::pid(),
  comms_recep::pid()
}).
-record(shell_query,{field::tstart|tend|tsched|id|type,range::{number(),number()}|term()}).


