%% @author Daniel
%% @doc @todo Add description to startup.


-module(startup).
-behaviour(application).
-export([start/2, stop/1]).

%% ====================================================================
%% Internal functions
%% ====================================================================
start(_StartType, _StartArgs) ->%this function must ensure that: 1) the factory sup with the factory underneath it is started (factory does not register itself with DOHA) 2) DOHA is started 3) all services in the services folder are starte

	{ok,_RL} = logging_sup:start_link(),
	ui_state:start_link(),
	web_dash_app:start(bla,bla),
	%true = erlang:set_cookie(node(),base),
	true = code:add_pathz("./External Code"),
	true = code:add_pathz("./MUI FrontEnd"),
	{ok,CurDir} = file:get_cwd(),
	module_checker:add_all_subdirectories_to_path(CurDir),
	io:format("\nStarting BASE factory\n"),
	ClashRes = module_checker:any_module_name_clashes(),
	case ClashRes of
		false->%%change back to ClashRes...this is just for development
			case top_sup:start_link({base_sup,start_link,[]}) of
		    {ok, Pid} ->
				yes = global:register_name(top_sup,Pid),
		    	%Start DOHA under top_sup: 
				case doha_sup:start_link() of
					{ok, _DohaPid}->
						io:format("\nDOHA started. Starting existing resources if any"),
						{ok,_HI} = holon_interactions_sup:start_link(),
						
						{ok,AllBcs} = doha_api:get_all_bcs(),
						spawn(fun()->start_existing_resources(AllBcs)end),
						{ok, Pid};
					DohaError->
						DohaError
				end;
		    Error ->
			  io:format("\nTop sup not found"),
		      Error
		  	end;
		_->
			error_log:log(?MODULE,0,unknown,"\nSome module names clash. Cannot start"),
			{error,"Some module names clash. Cannot start"}
	end.
stop(_State)->
	io:format("\nApplication stopped").

restart_base_resource(BC)->
	SupPid = whereis(top_sup),
	Architecture = maps:get(<<"architecture">>,BC),
	case Architecture of
		basic->
			base_resource_creator:start_basic_resource(SupPid, BC,false,na);
		_->
			base_resource_creator:start_base_resource(SupPid, BC,false)
	end.

start_existing_resources([])->
	ok;
start_existing_resources([BC|T])->
	restart_base_resource(BC),
	start_existing_resources(T).


	