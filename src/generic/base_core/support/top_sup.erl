-module(top_sup).
-author("Daniel van Niekerk").

-behaviour(supervisor).

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).


start_link(MFA = {_,_,_}) ->
  supervisor:start_link({local, ?MODULE}, ?MODULE,MFA).


init({M,F,A}) ->
	MaxRestart = 5,
	MaxTime = 3600,
	{ok, {{simple_one_for_one, MaxRestart, MaxTime},
	[{base_sup,
	{M,F,A},
	temporary, 5000, supervisor, [M]}]}}.
 
%%SLOc:9