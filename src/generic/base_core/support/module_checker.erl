%% @author Daniel
%% @doc @todo Add description to module_checker.


-module(module_checker).

%% ====================================================================
%% API functions
%% ====================================================================
-export([any_module_name_clashes/0,will_module_clash_with_existing/1,get_all_beam_files/1,add_all_subdirectories_to_path/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================
add_all_subdirectories_to_path(Dir)->
	SubDirs = get_all_subdirectories(Dir),
	add_each_dir_to_path(SubDirs).

add_each_dir_to_path([])->
	ok;
add_each_dir_to_path([Dir|T])->
	true = code:add_pathz(Dir),
	add_each_dir_to_path(T).
get_all_subdirectories(Dir)->
	{ok,FilesAndFolders} = file:list_dir(Dir),
	{_Beam,Folders} = get_beam_and_folders(FilesAndFolders,[],[]),
	FullDirectories = folders_to_directories(Dir,Folders,[]),
	SubDirs = get_each_subdirectory(FullDirectories,[]),
	AllDirs = lists:append(FullDirectories, SubDirs),
	AllDirs.

get_each_subdirectory([],SubDirs)->
	SubDirs;
get_each_subdirectory([Dir|T],SubDirs)->
	SD = get_all_subdirectories(Dir),
	NewSubDirs = lists:append(SubDirs, SD),
	get_each_subdirectory(T,NewSubDirs).
will_module_clash_with_existing(Module)->
	AllBeam = get_all_beam_files(),
	lists:member(Module, AllBeam).
any_module_name_clashes()->
	AllBeam = get_all_beam_files(),
	not(erlang:length(AllBeam) == sets:size(sets:from_list(AllBeam))).
get_all_beam_files()->
	{ok,CurDir} = file:get_cwd(),
	{ok,EbinFilesAndFolders} = file:list_dir(CurDir),
	{EbinBeamFiles,_Folders} = get_beam_and_folders(EbinFilesAndFolders,[],[]),
 	PluginDir = string:concat(CurDir, "/External Code"),
	PluginBeam = get_all_beam_files(PluginDir),
	AllBeam = lists:append(EbinBeamFiles,PluginBeam),
	AllBeam.

get_all_beam_files(Dir)->
	{ok,FilesAndFolders} = file:list_dir(Dir),
	{Beam,Folders} = get_beam_and_folders(FilesAndFolders,[],[]),
	FullDirectories = folders_to_directories(Dir,Folders,[]),
	FoldersBeam = get_beam_from_each_directory(FullDirectories,[]),
	AllBeam = lists:append(Beam, FoldersBeam),
	AllBeam.

get_beam_from_each_directory([],BeamFiles)->
	BeamFiles;
get_beam_from_each_directory([Dir|T],BeamFiles)->
	Beams = get_all_beam_files(Dir),
	get_beam_from_each_directory(T,lists:append(Beams, BeamFiles)).

get_beam_and_folders([],Beams,Folders)->
	{Beams,Folders};
get_beam_and_folders([File|T],Beams,Folders)->
	SplitRes = string:split(File,"."),
	case length(SplitRes) of
		1->
			%folder
			get_beam_and_folders(T,Beams,[File|Folders]);
		2->
			Extention = lists:nth(2, SplitRes),
			case Extention of
				"beam"->
					get_beam_and_folders(T,[File|Beams],Folders);
				_->
					get_beam_and_folders(T,Beams,Folders)
			end;
		_->
			get_beam_and_folders(T,Beams,Folders)
	end.
folders_to_directories(_Dir,[],Directories)->
	Directories;
folders_to_directories(Dir,[Folder|T],Directories)->
	Temp = string:concat(Dir, "/"),
	NewDir = string:concat(Temp,Folder),
	folders_to_directories(Dir,T,[NewDir|Directories]).

%%SLOC:72
