%% @author Daniel
%% @doc @todo Add description to base_resource_creator.


-module(base_resource_creator).
-author("Daniel van Niekerk").
-include("base_records.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([new_base_resource/2,start_base_resource/3,remove_resource/1,new_basic_resource/1,start_basic_resource/4]).

new_base_resource(Raw_Sup_Pid, BC)->%%kept like this for incase resources get started underneath under resources in a next iteration of the factory then Raw_Sup_Pid will not just be top_sup
	case id_available(BC) of
		true->
			case business_cards:valid_base_bc(BC) and (erlang:is_pid(Raw_Sup_Pid) or is_atom(Raw_Sup_Pid)) of 
				true->
					case is_atom(Raw_Sup_Pid) of
						true->
							Sup_Pid = whereis(Raw_Sup_Pid);
						_->
							Sup_Pid = Raw_Sup_Pid
					end,
					case is_process_alive(Sup_Pid) of
						true->
							ok = start_base_resource(Sup_Pid, BC,true),
							ok;
						_->
							{error, "base_resource_creator: Supervisor provided to create new resource under is not alive"}
					end;
				_->
					{error, "base_resource_creator: Invalid business card or invalid supervisor PID"}
			end;
		Reason->
			{error,Reason}
	end.

%% ====================================================================
%% Internal functions
%% ====================================================================
start_base_resource(ParentSupPid,CompleteBC,FactoryReady)->
  	{ok, MySupPid} = supervisor:start_child(ParentSupPid,[]),%%top sup is a simple_one_for_one sup so no need for child specs	
	{ok,CoordPid} = startCoordinator(MySupPid,CompleteBC),
	{ok, _ComsSupPid}= startCommsManager(MySupPid,CoordPid,CompleteBC,FactoryReady),%%only comms reception has a atom name and not just a pid
	{ok, _SchedSupPid}=startSchedule(MySupPid,CoordPid,CompleteBC),%%BC_ID used by components to start their respective ets tables since id is unique
	{ok,_AtrSupPid}=startAttributes(MySupPid,CoordPid,CompleteBC),
	{ok,_BioSupPid}=startBiography(MySupPid,CoordPid,CompleteBC),
	{ok,_ExeSupPid}=startExecution(MySupPid,CoordPid,CompleteBC),
	{ok,_PluginSupPid}=startPluginsSup(MySupPid,CoordPid),
	ok.

startCoordinator(InstanceSup,BC)->
	CoordinatorID = coordinator,
    CoordMFA = {coordinator, start_link, [BC,InstanceSup]},
	Restart = permanent,
	Shutdown = 2000,
  	Type = worker,
    Coordinator = {CoordinatorID, CoordMFA, Restart, Shutdown, Type, [coordinator]},
	supervisor:start_child(InstanceSup, Coordinator).
startCommsManager(InstanceSup,CoordPid,BC,FactoryReady) ->
  Restart = permanent,
  Shutdown = 2000,
  Type = worker,
  CommsID = comms_sup,
  CommsMFargs = {comms_sup, start_link, [CoordPid,BC,FactoryReady]},
  Comms = {CommsID, CommsMFargs, Restart, Shutdown, Type, [comms_sup]},
  supervisor:start_child(InstanceSup,Comms).

startSchedule(Base_sup_Pid,CoordPid,BC)->
  Restart = permanent,
  Shutdown = 2000,
  Type = worker,
  ScheduleID = sched_sup,
  ScheduleMFargs = {sched_sup, start_link, [CoordPid,BC]},
  Schedule = {ScheduleID, ScheduleMFargs, Restart, Shutdown, Type, [sched_sup]},
  supervisor:start_child(Base_sup_Pid,Schedule).

startBiography(Base_sup_Pid,CoordPid,BC)->
  Restart = permanent,
  Shutdown = 2000,
  Type = worker,
  BiographyID = bio_sup,
  BiographyMFargs = {bio_sup, start_link, [CoordPid,BC]},
  Biography = {BiographyID, BiographyMFargs, Restart, Shutdown, Type, [bio_sup]},
  supervisor:start_child(Base_sup_Pid,Biography).

startExecution(Base_sup_Pid,CoordPid,BC)->
  Restart = permanent,
  Shutdown = 2000,
  Type = worker,
  ExecutionID = exe_sup,
  ExecutionMFargs = {exe_sup, start_link, [CoordPid,BC]},
  Execution = {ExecutionID,  ExecutionMFargs, Restart, Shutdown, Type, [exe_sup]},
  supervisor:start_child(Base_sup_Pid,Execution).

startAttributes(Base_sup_Pid,CoordPid,BC)->
  Restart = permanent,
  Shutdown = 2000,
  Type = worker,
  AttributesID = atr_sup,
  AttributesMFargs = {atr_sup, start_link, [CoordPid,BC]},
  Attributes = {AttributesID, AttributesMFargs, Restart, Shutdown, Type, [atr_sup]},
  supervisor:start_child(Base_sup_Pid,Attributes).

startPluginsSup(InstanceSup,CoordPid)->
	Restart = permanent,
	Shutdown = 2000,
	Type = supervisor,
	PluginSupID = plugins_sup,
	Args = {plugins_sup, start_link, [CoordPid]},
	PluginSup = {PluginSupID,  Args, Restart, Shutdown, Type, [plugins_sup]},
	supervisor:start_child(InstanceSup, PluginSup).

id_available(BC)->
	{ok,Id} = business_cards:get_id(BC),
	{ok,GetRes}= doha_api:get_bcs_by_ids(Id),
	case GetRes of
		[]->
			{ok,Removed} = doha_api:get_removed_resources_by_ids(Id),
			case Removed of
				[]->
					{ok,Adr} = business_cards:get_address(BC),
					case lists:member(Adr,global:registered_names()) of
						true->
							"One of the system processes uses this id";%for example doha, ui_state etc.
						_->
							true
					end;
				_->
					"A removed factory resource has this id"
			end;
		_->
			"Another factory resource already has this id"
	end.

remove_resource(ID)->
	Res = doha_api:get_bcs_by_ids(ID),
	case Res of
		{ok,BCList}->
			case length(BCList) of
				1->
					BC = lists:nth(1, BCList),
					{ok,Architecture} = business_cards:get_architecture(BC),
					case Architecture of
						basic->%%indication that this is a basic resource
							{ok,Adr} = business_cards:get_address(BC),
							custom_erlang_functions:terminate_spawned_processes(Adr);
						_->
							ok%%not deleting ets anymore. This is kept for "Removed resources"
					end,
					{ok,ErlAdr} = business_cards:get_address(BC),
					{ok,AllInstancePids}=custom_erlang_functions:myGenServCall(ErlAdr, prepare_for_removal),
					ProcsToTerminate = get_removed_resources_sub_processes(AllInstancePids,[]),
					SupPid = custom_erlang_functions:myGenServCall(ErlAdr,get_instance_sup),
					supervisor:terminate_child(top_sup, SupPid),
					doha_api:remove_resource(ID),
					global:unregister_name(ErlAdr),
					terminate_sub_procs(ProcsToTerminate),
					ok;
				_->
					{error,"No business cards with this id"}
			end;
		_->
			{error,"No resource has this id"}
	end.
	
%%basic resources:
new_basic_resource(ResourceDetailsMap)->
	{Res,BC} = business_cards:create_basic_bc(ResourceDetailsMap),
	case Res of
		ok->
			case id_available(BC) of
				true->
					CustomArgs = maps:get(<<"custom_args">>,ResourceDetailsMap),
					start_basic_resource(top_sup,BC,true,CustomArgs);
				Reason->
					{error,Reason}
			end;
		_->
			error_log:log(?MODULE,0,unknown,"\nERROR:Invalid resource details map"),
			{error,"Invalid ResourceDetailsMap"}
	end.

start_basic_resource(ParentSupPid,BasicBC,FactoryReady,CustomArgs)->
		{ok,Type} = business_cards:get_type(BasicBC),
		StartupMod = erlang:list_to_atom(string:lowercase(string:replace(Type, " ", "_",all))),
		{ok, ResourceSupPid} = supervisor:start_child(ParentSupPid,[]),%%top sup is a simple_one_for_one sup so no need for child specs	
		Restart = permanent,
		Shutdown = 2000,
		ProcType = worker,
		Args = {StartupMod, start_link, [BasicBC,ResourceSupPid,CustomArgs,FactoryReady]},
		NewRes = {StartupMod, Args, Restart, Shutdown, ProcType, [StartupMod]},
		{Res,ReasonOrPid} = supervisor:start_child(ResourceSupPid,NewRes),
		{Res,ReasonOrPid}.

get_removed_resources_sub_processes([],AllSubProcs)->
	AllSubProcs;
get_removed_resources_sub_processes([InstancePid|T],AllSubProcs)->
	{monitors,Mons} = process_info(InstancePid,monitors),
	SubProcs = get_sub_procs(Mons,[]),
	get_removed_resources_sub_processes(T,lists:append(AllSubProcs, SubProcs)).
	
get_sub_procs([],SubProcs)->
	SubProcs;
get_sub_procs([{process,Pid}|T],SubProcs)->
	{links,Links} = process_info(Pid,links),
	case Links of
		[]->
			get_sub_procs(T,[Pid|SubProcs]);
		_->
			get_sub_procs(T,SubProcs)
	end.

terminate_sub_procs([])->
	ok;
terminate_sub_procs([SubP|T])->
	exit(SubP,kill),
	terminate_sub_procs(T).

%%SLOC:188
