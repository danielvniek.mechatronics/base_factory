%% @author Daniel
%% @doc @todo Add description to attribute_functions.


-module(attribute_functions).
-include("base_records.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([create_attribute/4,is_attribute/1,is_attributes/1,get_atr_id/1,get_atr_type/1,get_atr_context/1,get_atr_value/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================
create_attribute(ID,Type,Context,Value)->
	case custom_erlang_functions:is_string(ID) and custom_erlang_functions:is_string(Type) of
		true->
			{ok,#base_attribute{id=ID,type=Type,context=Context,value=Value}};
		_->
			{error,"Invalid attribute arguments: ID, Type and Context must all be of type string"}
	end.

is_attribute(Attribute)->
	case erlang:is_record(Attribute, base_attribute, 5) of
		true->
			case (custom_erlang_functions:is_string(Attribute#base_attribute.id) and custom_erlang_functions:is_string(Attribute#base_attribute.type)) of
				true->
					true;
				_->
					false
			end;
		_->
			false
	end.
is_attributes([])->
	true;
is_attributes(AttributeList)->
	case is_list(AttributeList) of
		true->
			[H|T] = AttributeList,
			Res = is_attribute(H),
			case Res of
				true->
					is_attributes(T);
				_->
					Res
			end;
		_->
			false
	end.

get_atr_id(Attribute)->
	case is_attribute(Attribute) of
		true->
			{ok,Attribute#base_attribute.id};
		_->
			{error,"Invalid attribute"}
	end.
get_atr_type(Attribute)->
	case is_attribute(Attribute) of
		true->
			{ok,Attribute#base_attribute.type};
		_->
			{error,"Invalid attribute"}
	end.
get_atr_context(Attribute)->
	case is_attribute(Attribute) of
		true->
			{ok,Attribute#base_attribute.context};
		_->
			{error,"Invalid attribute"}
	end.
get_atr_value(Attribute)->
	case is_attribute(Attribute) of
		true->
			{ok,Attribute#base_attribute.value};
		_->
			{error,"Invalid attribute"}
	end.

%%SLOC:63
