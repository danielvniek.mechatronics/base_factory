%% @author Daniel
%% @doc @todo Add description to security_functions.


-module(security_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_monitors/1,from_is_in_process_list_or_monitored_by_a_process_in_process_list/2,from_is_process_or_monitored_by_process/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
from_is_process_or_monitored_by_process(Process,From)->
	case is_atom(Process) of
		true->
			FinalProcess = whereis(Process);
		_->
			FinalProcess = Process
	end,
	{FromPid,_Ref} = From,
	MonitorRes = process_info(FromPid,monitored_by),
	case MonitorRes of
		undefined->
			MonitoredByList = [];
		_->
			{monitored_by,MonitoredByList} = MonitorRes
	end,
	FromList = [FromPid|MonitoredByList],
	lists:member(FinalProcess, FromList).

from_is_in_process_list_or_monitored_by_a_process_in_process_list(ProcessList,From)->
	{FromPid,_Ref} = From,
	MonitorRes = process_info(FromPid,monitored_by),
	case MonitorRes of
		undefined->
			MonitoredByList = [];
		_->
			{monitored_by,MonitoredByList} = MonitorRes
	end,
	FromList = [FromPid|MonitoredByList],
	compare_lists(FromList,ProcessList).
	
get_monitors(From)->
	case is_pid(From) of
		true->
			FromPid = From;
		_->
			{FromPid,_Ref} = From
	end,
	MonitorRes = process_info(FromPid,monitored_by),
	case MonitorRes of
		undefined->
			MonitoredByList = [];
		_->
			{monitored_by,MonitoredByList} = MonitorRes
	end,
	MonitoredByList.

compare_lists([],_ReceptionList)->
	false;
compare_lists([H|T],ReceptionList)->
	case is_atom(H) of
		true->
			FinalProcess = whereis(H);
		_->
			FinalProcess = H
	end,
	case lists:member(FinalProcess, ReceptionList) of
		true->
			true;
		_->
			compare_lists(T,ReceptionList)
	end.

%%SLOC:59
