%% @author Daniel
%% @doc @todo Add description to plugin_registration_functions.


-module(plugin_registration_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([register/5,deregister/5]).



%% ====================================================================
%% Internal functions
%% ====================================================================
register(CurrentPluginMap,MultiplePluginsPerActType,Plugin_Pid,ActType,SchedGatesTimeAndPid)->
	case MultiplePluginsPerActType of
		true->
			case maps:is_key(ActType, CurrentPluginMap) of
				true->
					PluginList = maps:get(ActType,CurrentPluginMap),
					AlivePluginList = check_if_all_alive(PluginList,[]),%%there is a possibility that the reason for the registration is because one of the plugins died and was restarted
					case lists:keyfind(Plugin_Pid, 1, AlivePluginList) of
						false->
							CleanPluginList = lists:delete(Plugin_Pid, AlivePluginList),
							NewPluginList = [Plugin_Pid|CleanPluginList],
							NewPluginMap = maps:update(ActType, NewPluginList, CurrentPluginMap),
							Reply = ok;							
						_->		
							NewPluginMap = CurrentPluginMap,
							Reply = {error,"Pid already registered."}							
					end;
				_->
					NewPluginList = [Plugin_Pid],
					NewPluginMap = maps:put(ActType, NewPluginList, CurrentPluginMap),
					Reply = ok
			end,
			NewGateMap = none;
		_->
			case maps:is_key(ActType, CurrentPluginMap) of
				true->
					PluginPid = maps:get(ActType,CurrentPluginMap),
					case is_process_alive(PluginPid) and not(Plugin_Pid==PluginPid) of
						true->
							NewPluginMap = CurrentPluginMap,
							Reply = {error,"Activity type already registered for another plugin that is still alive"},
							case not(SchedGatesTimeAndPid==none) of
								true->
									{GateMap,_Time,_SchedPid} = SchedGatesTimeAndPid,
									NewGateMap = GateMap;
								_->
									NewGateMap = none
							end;
						_->
							case not(SchedGatesTimeAndPid==none) of
								true->
									NewGateMap = update_gates(SchedGatesTimeAndPid,Plugin_Pid,ActType);
								_->
									NewGateMap = none
							end,
							NewPluginMap = maps:update(ActType,Plugin_Pid, CurrentPluginMap),
							Reply = ok
					end;
				_->
					case not(SchedGatesTimeAndPid==none) of
						true->
							NewGateMap = update_gates(SchedGatesTimeAndPid,Plugin_Pid,ActType);
						_->
							NewGateMap = none
					end,
					NewPluginMap = maps:put(ActType,Plugin_Pid, CurrentPluginMap),
					Reply = ok
			end
	end,
	{NewPluginMap,Reply,NewGateMap}.


deregister(CurrentPluginMap,MultiplePluginsPerActType,Plugin_Pid,ActType,GateMap)->
	case MultiplePluginsPerActType of
		true->		
			case maps:is_key(ActType, CurrentPluginMap) of
				true->
					PluginList = maps:get(ActType,CurrentPluginMap),
					case lists:keymember(Plugin_Pid, 1, PluginList) of
						true->
							NewPluginList = lists:keydelete(Plugin_Pid, 1, PluginList),
							case NewPluginList of
								[]->
									NewPluginMap = maps:remove(ActType, CurrentPluginMap);
								_->
									NewPluginMap = maps:update(ActType, NewPluginList, CurrentPluginMap)
							end,
							Reply = ok;
						_->
							NewPluginMap = CurrentPluginMap,
							Reply = {error, "Not registered"}
					end;
				_->
					NewPluginMap = CurrentPluginMap,
					Reply = {error,"Not registered"}
			end,
			NewGateMap = none;
		_->
			case maps:is_key(ActType, CurrentPluginMap) of
				true->
					Registered_Pid = maps:get(ActType,CurrentPluginMap),
					case Registered_Pid of
						Plugin_Pid->
							NewPluginMap = maps:remove(ActType, CurrentPluginMap),
							case GateMap of
								none->
									NewGateMap = none;
								_->
									delete_old_gate(GateMap,ActType),
									NewGateMap = maps:remove(ActType, GateMap)
							end,
							Reply = ok;
						_->
							NewPluginMap = CurrentPluginMap,
							NewGateMap = GateMap,
							Reply = {error,"Not registered"}
					end;
				_->
					NewPluginMap = CurrentPluginMap,
					NewGateMap = GateMap,
					Reply = {error,"Not registered"}
			end
	end,
	{NewPluginMap,Reply,NewGateMap}.


delete_old_gate(GateMap,ActType)->
	OldGatePid = maps:get(ActType,GateMap),%the act type should be in this map otherwise there is an error in the code
	gen_server:stop(OldGatePid).

update_gates(SchedGateInfo,Plugin_Pid,ActType)->
	{GateMap,TimeBeforeSched,Acts,RecepPid} = SchedGateInfo,
	case maps:is_key(ActType,GateMap) of
		true->
			delete_old_gate(GateMap,ActType);
		_->
			ok
	end,
	Scheds=sched_processes:get_scheds([],Acts),
	OrderedList = lists:sort(Scheds),
	OrderedTimeList = lists:reverse(OrderedList),
	{ok,Pid} = sched_gate:start_link(Plugin_Pid, ActType, TimeBeforeSched, OrderedTimeList, RecepPid),
	MonitorRef = monitor(process,Pid),
	NewGateMap = maps:put(ActType,Pid, GateMap),
	{MonitorRef,NewGateMap}.

check_if_all_alive([],ReturnList)->
	ReturnList;
check_if_all_alive([PluginPid|T],ReturnList)->
	case is_process_alive(PluginPid) of
		true->
			check_if_all_alive(T,[PluginPid|ReturnList]);
		_->
			check_if_all_alive(T,ReturnList)
	end.

%%SLOC:139
							
						
