%% @author Daniel
%% @doc @todo Add description to holon_interactions_sup.


-module(holon_interactions_sup).

-behaviour(supervisor).

-export([start_link/0, init/1]).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  ETS = #{id => 'holon_interactions_ets',
    start => {holon_interactions_ets, start_link, []},
    restart => temporary,
    shutdown => 2000,
    type => worker,
    modules => [holon_interactions_ets]},

  {ok, {#{strategy => one_for_one,
    intensity => 5,
    period => 30},
    [ETS]}
  }.