
-module(holon_interactions_ets).
-author("Daniel van Niekerk").

-behaviour(gen_server).
-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([start_link/0,new/3,new_contract/3,get_match_spec/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(hi_state, {ets_table::term(),file_path="",key_nr}).
-record(hi_record,{id,time,sender,receiver,event}).
%%%===================================================================
new_contract(Contract,Receiver,Event)->
	{ok,Client} = contracts:get_client_bc(Contract),
	{ok,Server} = contracts:get_service_bc(Contract),
	{ok,ServType} = contracts:get_service_type(Contract),
	case Receiver of 
		client->
			custom_erlang_functions:myGenServCall(holon_interactions, {new,Server,Client,Event++": "++ServType});
		_->
			custom_erlang_functions:myGenServCall(holon_interactions, {new,Client,Server,Event++": "++ServType})
	end.

new(Sender,Receiver,Event)->
	custom_erlang_functions:myGenServCall(holon_interactions, {new,Sender,Receiver,Event}).

start_link() ->
  {ok,_PID} = gen_server:start_link({local,holon_interactions},?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks

init([]) ->
  	NewState = create_or_load_table(), 
   	yes = global:register_name(holon_interactions,self()),
  	{ok, NewState}.
%%--------------------------------------------------------------------


handle_call({new, Sender,Receiver,Event}, _From, State)->%%assume doha_recep already checked that BC valid
	try
		case custom_erlang_functions:is_string(Event) of
			true->
				{ok,SenderId} = get_holon_id(Sender),
				{ok,ReceiverId} = get_holon_id(Receiver),
				NewKeyNr = State#hi_state.key_nr+1,
				NewRecord = #hi_record{id=NewKeyNr,time=base_time:now(),sender=SenderId,receiver=ReceiverId,event=Event},
				Reply = def_storage_functions:save(State#hi_state.ets_table,[NewRecord],State#hi_state.file_path),
				{reply,Reply,State#hi_state{key_nr=NewKeyNr}};
			_->
				{reply,{error,"Event must be a string"},State}
		end
	catch
		A:B:StackTrace->
			error_log:log(?MODULE,0,unknown,"\nERROR: Holon interactions could not add new record. Caught ~p:~p:~p",[A,B,StackTrace]),
			Err = lists:flatten(io_lib:format("~p:~p:~p", [A,B,StackTrace])),
			{reply,{error,Err},State}
	end;

handle_call({get_text,StartT,EndT,Holons}, _From, State)->
	case is_integer(StartT) and is_integer(EndT) and custom_erlang_functions:isListOfStrings(Holons) of
		true->
			case length(Holons)>0 of
				true->
					MatchSpecTuple = {holon_interactions_ets,get_match_spec,del_match_spec},
					{ok,Records} = def_storage_functions:get_del_or_pop(State#hi_state.ets_table,"get",MatchSpecTuple,lists:append([StartT,EndT], Holons),[],State#hi_state.file_path),
					%error_log:log(?MODULE,0,unknown,"Records:~p",[Records]),
					Text = records_to_text(Records,"",0,none,Holons),
					{reply,{ok,Text},State};
				_->
					{reply,{ok,""},State}
			end;
		_->
			{reply,{error,"Invalid arguments"},State}
	end;

handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.


handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Holon interactions ets got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
%%--------------------------------------------------------------------

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Table functions:
create_or_load_table()->
	TableName = hi_table,
	{ok, CurrentDirectory} = file:get_cwd(),
	FileDir = string:concat(CurrentDirectory,"/Storage/holon_interactions/"),
	ok = filelib:ensure_dir(FileDir),
	FilePath = string:concat(FileDir,"holon_interactions.txt"),
	ETS_table = def_storage_functions:claim_or_create_ets(TableName,FilePath),
	NrObj = ets:info(ETS_table,size),
	State = #hi_state{ets_table = ETS_table,file_path = FilePath,key_nr=NrObj},
	State.

get_holon_id(Holon)->
	case business_cards:valid_base_bc(Holon) of
		true->
			business_cards:get_id(Holon);
		_->
			FromMonitors = security_functions:get_monitors(Holon),
			FromCommsRecep = lists:nth(1, FromMonitors),
			{ok,BC} = comms_api:get_bc(FromCommsRecep),
			business_cards:get_id(BC)
	end.

records_to_text([],Text,_LastT,_FirstHolon,_Holons)->
	Text;
records_to_text([Record|T],Text,LastT,FirstHolon,Holons)->
	Time = Record#hi_record.time,
	Sender = holons(Record#hi_record.sender,Holons),
	Receiver = holons(Record#hi_record.receiver,Holons),
	case (Sender=="Other") and(Receiver=="Other") of
		true->
			records_to_text(T,Text,LastT,FirstHolon,Holons);
		_->
			Event = Record#hi_record.event,
			DateAndTString = base_time:base_time_to_date_and_time_string(Time),
			case FirstHolon of
				none->
					TimeNote = Text ++ "\nNote left of "++Sender ++": " ++DateAndTString,
					NewText = TimeNote++"\n"++Sender++ "->" ++Receiver++": "++Event,
					NewTime = LastT,
					NewFirstHolon = Sender;
				_->
					case Time>(LastT+60) of %%if more than minute spacing show new time
						true->
							TimeNote = Text ++ "\nNote left of "++FirstHolon ++": " ++DateAndTString,
							NewText = TimeNote++"\n"++Sender++ "->" ++Receiver++": "++Event,
							NewTime = Time;
						_->
							NewTime = LastT,
							NewText = Text++"\n"++Sender++ "->" ++Receiver++": "++Event
					end,
					NewFirstHolon = FirstHolon
			end,
			records_to_text(T,NewText,NewTime,NewFirstHolon,Holons)
	end.

holons(Holon,Holons)->
	case lists:member(Holon,Holons) of
		true->
			Holon;
		_->
			"Other"
	end.
  
  
get_match_spec(Info)->
	StartT = lists:nth(1, Info),
	EndT = lists:nth(2, Info),
	Holons = lists:delete(StartT, lists:delete(EndT, Info)),
	L = length(Holons),
	case L>=10 of
		true->
			List = lists:sublist(Holons, 1, 10),
			RList = lists:append([StartT,EndT], lists:sublist(Holons, 11, L)),
			case length(RList) of
				2->
					RemainderList = [];
				_->
					RemainderList =RList
			end;
		_->
			RemainderList = [],
			Element1 = lists:nth(1, Holons),
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Holons, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #hi_record{time=Time,sender=Sender,receiver=Receiver})
      when (Time>=StartT) andalso (Time=<EndT) andalso ((Sender ==E1 orelse Sender==E2 orelse Sender==E3 orelse Sender==E4 orelse Sender==E5 orelse Sender==E6 orelse Sender==E7 orelse Sender==E8 orelse Sender==E9 orelse Sender==E10) or (Receiver ==E1 orelse Receiver==E2 orelse Receiver==E3 orelse Receiver==E4 orelse Receiver==E5 orelse Receiver==E6 orelse Receiver==E7 orelse Receiver==E8 orelse Receiver==E9 orelse Receiver==E10))->
      A
    end),
	{MatchSpec,RemainderList}.
					
%%SLOC:162

