%% @author Daniel
%% @doc @todo Add description to storage_functions.


-module(storage_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([request/6,add_id_and_type/3,get_content_if_save/1,
		 valid_atr_storage_request/1,valid_sched_storage_request/1,valid_exe_storage_request/1,valid_bio_storage_request/1,inform_cloud_backup_plugins/5]).

-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
inform_cloud_backup_plugins([],_RId,_RType,_SC,_MyR)->
	ok;
inform_cloud_backup_plugins([PluginPid|T],ResourceId,ResourceType,StorageComponent,MyRecep)->
	Res = custom_erlang_functions:myGenServCall(PluginPid, {stored_content_changed,ResourceId,ResourceType,StorageComponent},20000),
	case Res of 
		timeout->
			custom_erlang_functions:myGenServCall(MyRecep, {deregister_cloud_backup_plugin,PluginPid},20000);
		_->
			ok
	end,
	inform_cloud_backup_plugins(T,ResourceId,ResourceType,StorageComponent,MyRecep).

inform_cloud_backup_plugins_if_save_del_or_pop(Call,CloudBackupPlugins,MyRecep)->
	Instruction = element(1,Call),
	InstructionS = atom_to_list(Instruction),
	case string:find(InstructionS,"save") of
		nomatch->
			GetDelOrPop = element(4,Call),
			case GetDelOrPop of
				"get"->
					ok;
				_->
					Id = element(2,Call),
					Type = element(3,Call),
					case string:find(InstructionS,"sched") of
						nomatch->
							case string:find(InstructionS,"exe") of
								nomatch->
									case string:find(InstructionS,"bio") of
										nomatch->
											StorageComponent = "atr";
										_->
											StorageComponent = "bio"
									end;
								_->
									StorageComponent = "exe"
							end;
						_->
							StorageComponent = "sched"
					end,
					inform_cloud_backup_plugins(CloudBackupPlugins,Id,Type,StorageComponent,MyRecep)
			end;
		_->
			Id = element(2,Call),
			Type = element(3,Call),
			case string:find(InstructionS,"sched") of
				nomatch->
					case string:find(InstructionS,"exe") of
						nomatch->
							case string:find(InstructionS,"bio") of
								nomatch->
									StorageComponent = "atr";
								_->
									StorageComponent = "bio"
							end;
						_->
							StorageComponent = "exe"
					end;
				_->
					StorageComponent = "sched"
			end,
			inform_cloud_backup_plugins(CloudBackupPlugins,Id,Type,StorageComponent,MyRecep)
	end.

request(StorageServiceAdr,DefaultStoragePid,Call,From,PostRequestProcess,{CloudBackupPlugins,MyRecep})->	
	case StorageServiceAdr of
		default->
			%%use default module
			{Res,ResMsg}=custom_erlang_functions:myGenServCall(DefaultStoragePid, Call);
		_->
			{Res,ResMsg}=custom_erlang_functions:myGenServCall(StorageServiceAdr, Call)
	end,
	case Res of 
		ok->
			case PostRequestProcess of 
				none->
					ok;
				_->
					{Mod,Function,Args} = PostRequestProcess,
					Mod:Function(Args)
			end,
			case Call of
				{save_sched_activities,_ID,_HolonType,[Act]}->%%indicating one act being saved
					gen_server:reply(From, {ok,Act#stage1Activity.id});
				_->
					gen_server:reply(From, {Res,ResMsg})
			end,
			case CloudBackupPlugins of
				[]->
					ok;
				_->
					inform_cloud_backup_plugins_if_save_del_or_pop(Call,CloudBackupPlugins,MyRecep)
			end;
		timeout->
			error_log:log(?MODULE,0,unknown,"\nStorage request timed out. Retrying in 5 seconds\n"),
			timer:sleep(5000),
			request(StorageServiceAdr,DefaultStoragePid,Call,From,PostRequestProcess,{CloudBackupPlugins,MyRecep});
		retry->
			error_log:log(?MODULE,0,unknown,"\nSomething went wrong, retrying in 5 seconds"),			
			timer:sleep(5000),
			request(StorageServiceAdr,DefaultStoragePid,Call,From,PostRequestProcess,{CloudBackupPlugins,MyRecep});
		error->%%TODO: if the default storage is NOT used and the storage service keeps returning error after a few retries, then use default storage and call change storage in recep
			error_log:log(?MODULE,0,unknown,"\nSome unforseen error occured. Check code"),
			request(StorageServiceAdr,DefaultStoragePid,Call,From,PostRequestProcess,{CloudBackupPlugins,MyRecep});
		_->
			error_log:log(?MODULE,0,unknown,"\nUnexpected result, check the code in the service"),
			gen_server:reply(From,{error,"Storage service is faulty"})
			
	end.
get_content_if_save(Request)->
	AtomRequest = erlang:element(1, Request),
	StringRequest = erlang:atom_to_list(AtomRequest),
	case string:slice(StringRequest, 0, 4) of
		"save"->
			ActsOrAtrs = erlang:element(2, Request),
			ActsOrAtrs;
		_->
			false
	end.
valid_atr_storage_request(Request)->
	case erlang:is_tuple(Request) of
		true->
			RequestName = element(1, Request),
			case is_atom(RequestName) of
				true->
					case RequestName of
						save_attributes->
							case size(Request)==2 of
								true->
									case attribute_functions:is_attributes(element(2,Request)) of
										true->
											true;
										_->
											{false,"Invalid attributes"}
									end;
								_->
									{false,"Invalid storage request"}
							end;
						get_del_or_pop_all_atr->
							is_get_del_or_pop_all(Request);
						get_del_or_pop_atr_by_ids->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_atr_by_types->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_atr_by_contexts->
							is_get_del_or_pop(Request);
						_->
							{false,"Invalid storage request"}
					end;
				_->
					{false,"Invalid storage request"}
			end;
		_->
			{false,"Invalid storage request"}
	end.

valid_sched_storage_request(Request)->
	case erlang:is_tuple(Request) of
		true->
			RequestName = element(1, Request),
			case is_atom(RequestName) of
				true->
					case RequestName of
						save_sched_activities->
							case size(Request)==2 of
								true->
									case activity_functions:is_stage1_acts(element(2,Request)) of
										true->
											true;
										_->
											{false,"Invalid Activities"}
									end;
								_->
									{false,"Invalid storage request"}
							end;
					
						get_del_or_pop_all_sched->
							is_get_del_or_pop_all(Request);
						get_del_or_pop_sched_by_ids->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_sched_by_types->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_sched_by_times->
							is_get_del_or_pop_by_times(Request,4);
						get_del_or_pop_sched_by_types_and_times->
							is_get_del_or_pop_by_types_and_times(Request,5);
						_->
							{false,"Invalid storage request"}
					end;
				_->
					{false,"Invalid storage request"}
			end;
		_->
			{false,"Invalid storage request"}
	end.
valid_exe_storage_request(Request)->
	case erlang:is_tuple(Request) of
		true->
			RequestName = element(1, Request),
			case is_atom(RequestName) of
				true->
					case RequestName of
						save_exe_activities->
							case size(Request)==2 of
								true->
									case activity_functions:is_stage2_acts(element(2,Request)) of
										true->
											true;
										_->
											{false,"Invalid Activities"}
									end;
								_->
									{false,"Invalid storage request"}
							end;
						get_del_or_pop_all_exe->
							is_get_del_or_pop_all(Request);
						get_del_or_pop_exe_by_ids->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_exe_by_types->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_exe_by_times->
							is_get_del_or_pop_by_times(Request,6);
						get_del_or_pop_exe_by_types_and_times->
							is_get_del_or_pop_by_types_and_times(Request,7);
						_->
							{false,"Invalid storage request"}
					end;
				_->
					{false,"Invalid storage request"}
			end;
		_->
			{false,"Invalid storage request"}
	end.
valid_bio_storage_request(Request)->
	case erlang:is_tuple(Request) of
		true->
			RequestName = element(1, Request),
			case is_atom(RequestName) of
				true->
					case RequestName of
						save_bio_activities->
							case size(Request)==2 of
								true->
									case activity_functions:is_stage3_acts(element(2,Request)) of
										true->
											true;
										_->
											{false,"Invalid Activities"}
									end;
								_->
									{false,"Invalid storage request"}
							end;
						get_del_or_pop_all_bio->
							is_get_del_or_pop_all(Request);
						get_del_or_pop_bio_by_ids->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_bio_by_types->
							is_get_del_or_pop_by_string(Request);
						get_del_or_pop_bio_by_times->
							is_get_del_or_pop_by_times(Request,8);
						get_del_or_pop_bio_by_types_and_times->
							is_get_del_or_pop_by_types_and_times(Request,9);
						get_pending_s3data_acts->
							case size(Request)==2 of
								true->
									case custom_erlang_functions:is_string(element(2,Request)) of
										true->
											true;
										_->
											{false,"Act type must be a string"}
									end;
								_->
									{false,"Incorrect request tuple size"}
							end;
						_->
							{false,"Invalid storage request"}
					end;
				_->
					{false,"Invalid storage request"}
			end;
		_->
			{false,"Invalid storage request"}
	end.

add_id_and_type(Request,ID,Type)->
	Temp = erlang:insert_element(2,Request,Type),
	Call = erlang:insert_element(2,Temp,ID),
	Call.

is_get_del_or_pop_by_string(Request)->
	case size(Request)==3 of
		true->
			case (element(2,Request)=="get")or(element(2,Request)=="del")or(element(2,Request)=="pop") of
				true->
					case custom_erlang_functions:isListOfStrings(element(3,Request)) of
						true->
							true;
						_->
							{false,"Not list of strings"}
					end;
				_->
					{false,"Only get, del or pop accepted"}
			end;
		_->
			{false,"Incorrect request tuple size"}
	end.
is_get_del_or_pop(Request)->
	case size(Request)==3 of
		true->
			case (element(2,Request)=="get")or(element(2,Request)=="del")or(element(2,Request)=="pop") of
				true->
					case is_list(element(3,Request)) of
						true->
							true;
						_->
							{false,"Not list"}
					end;
				_->
					{false,"Only get, del or pop accepted"}
			end;
		_->
			{false,"Incorrect request tuple size"}
	end.

is_get_del_or_pop_all(Request)->
	case size(Request)==2 of
		true->
			case (element(2,Request)=="get")or(element(2,Request)=="del")or(element(2,Request)=="pop") of
				true->
					true;
				_->
					{false,"Only get, del or pop accepted"}
			end;
		_->
			{false,"Incorrect request tuple size"}
	end.
is_get_del_or_pop_by_times(Request,Length)->
	case size(Request)==Length of
		true->
			case (element(2,Request)=="get")or(element(2,Request)=="del")or(element(2,Request)=="pop") of
				true->
					true;
				_->
					{false,"Only get, del or pop accepted"}
			end;
		_->
			{false,"Incorrect request tuple size"}
	end.
is_get_del_or_pop_by_types_and_times(Request,Length)->
	case size(Request)==Length of
		true->
			case (element(2,Request)=="get")or(element(2,Request)=="del")or(element(2,Request)=="pop") of
				true->
					case custom_erlang_functions:isListOfStrings(element(3,Request)) of
						true->
							true;
						_->
							{false,"Not list of strings"}
					end;
				_->
					{false,"Only get, del or pop accepted"}
			end;
		_->
			{false,"Incorrect request tuple size"}
	end.

%%SLOc:358