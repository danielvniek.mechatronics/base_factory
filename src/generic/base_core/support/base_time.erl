%% @author Daniel
%% @doc @todo Add description to base_time.


-module(base_time).

%% ====================================================================
%% API functions
%% ====================================================================
-export([days_to_base_time/1,hours_to_base_time/1,minutes_to_base_time/1,seconds_to_base_time/1,now/0,date_and_time_strings_to_base_time/2,base_time_to_date_and_time_string/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================
now()->
	calendar:datetime_to_gregorian_seconds(calendar:local_time()).


date_and_time_strings_to_base_time(Date,Time)->
	case (string:slice(Date,4,1)=="/") and (string:slice(Date, 7,1)=="/") of
		true->
			YMD = string:split(Date,"/",all);
		_->
			case (string:slice(Date,2,1)=="/") and (string:slice(Date, 5,1)=="/") of
				true->
					DMY = string:split(Date,"/",all),
					YMD = [lists:nth(3, DMY),lists:nth(2, DMY),lists:nth(1, DMY)];
				_->
					case (string:slice(Date,4,1)=="-") and (string:slice(Date, 7,1)=="-") of
							true->
								YMD = string:split(Date, "-", all);
							_->
								DMY = string:split(Date,"-",all),
								YMD = [lists:nth(3, DMY),lists:nth(2, DMY),lists:nth(1, DMY)]
					end
			end
	end,
	Year = list_to_integer(lists:nth(1, YMD)),
	Month = list_to_integer(lists:nth(2, YMD)),
	Day = list_to_integer(lists:nth(3, YMD)),
	DateT = {Year,Month,Day},
	HMS = string:split(Time, ":", all),
	case length(HMS) of
		3->
			Secs = lists:nth(3,HMS),
			case string:find(Secs,".") of
				nomatch->
					Second = list_to_integer(Secs);
				_->
					Second = round(list_to_float(lists:nth(3,HMS)))
			end;
		_->
			Second = 0
	end,
	Hour = list_to_integer(lists:nth(1, HMS)),
	Minute = list_to_integer(lists:nth(2, HMS)),
	
	TimeT = {Hour,Minute,Second},
	DateTime = {DateT,TimeT},
	calendar:datetime_to_gregorian_seconds(DateTime).

base_time_to_date_and_time_string(BaseTime)->
	{{Y,M,D},{H,Min,S}} = calendar:gregorian_seconds_to_datetime(BaseTime),
	Year = string:concat(number_to_two_digit_str(Y), "-"),
	YandM = string:concat(Year,number_to_two_digit_str(M)),
	Temp = string:concat(YandM,"-"),
	YMD = string:concat(Temp, number_to_two_digit_str(D)),
	Hour = string:concat(number_to_two_digit_str(H), ":"),
	HandM = string:concat(Hour,number_to_two_digit_str(Min)),
	Temp1 = string:concat(HandM,":"),
	HMS = string:concat(Temp1, number_to_two_digit_str(S)),
	Temp2 = string:concat(YMD," "),
	string:concat(Temp2, HMS).

number_to_two_digit_str(Number)->
	case Number>=10 of
		true->
			integer_to_list(Number);
		_->
			string:concat("0", integer_to_list(Number))
	end.
days_to_base_time(Days)->
	Days*24*60*60.

hours_to_base_time(Hours)->
	Hours*60*60.

minutes_to_base_time(Minutes)->
	Minutes*60.

seconds_to_base_time(Seconds)->
	Seconds*60.

%%SLOC:71

