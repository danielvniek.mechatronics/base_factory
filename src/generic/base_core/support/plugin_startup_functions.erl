	%% @author Daniel
%% @doc @todo Add description to plugin_startup_functions.


-module(plugin_startup_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([findPluginsFromSettings/3,start_plugins/3]).

-include("base_records.hrl").

%% ====================================================================
%% Internal functions
%% ====================================================================

findPluginsFromSettings([],_SettingsMap,PluginListOfMaps)->
	PluginListOfMaps;
findPluginsFromSettings([Key|T],SettingsMap,PluginListOfMaps)->
	Setting = maps:get(Key,SettingsMap),
	Module = erlang:binary_to_atom(maps:get(<<"module">>,Setting), latin1),
	case maps:is_key(<<"args">>, SettingsMap) of
		true->
			Args = maps:get(<<"args">>,Setting);
		_->
			Args = none
	end,
	PluginMap = #{<<"module">> => Module, <<"args">>=> Args},
	NewPluginListOfMaps = [PluginMap|PluginListOfMaps],
	findPluginsFromSettings(T,SettingsMap,NewPluginListOfMaps).

start_plugins([],_PluginsTopSup, CoordinatorPid)->
	custom_erlang_functions:myGenServCall(CoordinatorPid,all_plugins_started),
	ok;
start_plugins(List, PluginsTopSup, CoordinatorPid)->
  [PluginDetailsMap|T] = List,
  {ok, PluginSup} = supervisor:start_child(PluginsTopSup,[]),
  StartRes = start_plugin(PluginDetailsMap,PluginSup,CoordinatorPid),
  case StartRes of
	  {error,_Reason}->
		  supervisor:terminate_child(PluginsTopSup, PluginSup),
  		  custom_erlang_functions:myGenServCall(CoordinatorPid, {failed_plugin_startup,PluginDetailsMap}),
	     error_log:log(?MODULE,0,unknown,"\nERROR: Could not start plugin with module name ~p",[maps:get(<<"module">>,PluginDetailsMap)]);
	  _->
		  custom_erlang_functions:myGenServCall(CoordinatorPid,{new_plugin_sup,PluginDetailsMap,PluginSup}),
		  ok
  end,
  start_plugins(T,PluginsTopSup,CoordinatorPid).

start_plugin(PluginDetailsMap,PluginSup,CoordinatorPid)->
  Module = maps:get(<<"module">>, PluginDetailsMap),
  %%TODO: function to check that there are no modules (.beam) in new directory with same name as other modules in root module directory
  case is_atom(Module) of
	  true->
		  Restart = permanent,
		  Shutdown = 2000,
		  Type = worker,
		  ID = PluginDetailsMap,
		  ChildArgs = {Module, start_link, [PluginDetailsMap,CoordinatorPid,PluginSup]},
		  Child = {ID,  ChildArgs, Restart, Shutdown, Type, [Module]},
		  StartRes = supervisor:start_child(PluginSup,Child),
		  case StartRes of
			  {ok,ChildPid}->
					{ok,ChildPid};
			  _->
				  {error,"Could not start plugin"}
		  end;
	_->
		{error,"Module must be an atom name"}
  end.

%%SLOC:68

