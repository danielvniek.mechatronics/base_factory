%%%-------------------------------------------------------------------
%%% @author Dale Sparrow
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Mar 2019 1:59 PM
%%%-------------------------------------------------------------------
-author("Dale Sparrow (Small changes by Daniel van Niekerk)").

-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).
-record(biography_info,{tend::integer(),s3data::term()}).
-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).

-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

%%SLOC:9