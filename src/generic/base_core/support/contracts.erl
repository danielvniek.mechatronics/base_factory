
-module(contracts).
-author("Dale Sparrow (changes made by Daniel van Niekerk)").

%% API
-export([create_contract/7,create_contract/8,deliver_package/2,deliver_proposal/2,service_stopped/2,valid_contract/1,
		 get_client_bc/1,get_service_bc/1,get_delivery_address/1,get_proposal/1,find_contract_with_serv_type/2,
		 get_service_type/1,get_interval/1,get_request_arguments/1,get_package_blueprint/1,
		 add_proposal_field/4,get_proposal_fields/1,get_proposal_field_description/1,get_proposal_field_entry_type/1,get_proposal_field_unit/1,insert_proposal_reply/2,get_proposal_field_value/2]).

deliver_package(Contract,Package)->
	case valid_contract(Contract) of
		true->
			{ok,DeliveryAddr} = get_delivery_address(Contract),
			case is_pid(DeliveryAddr) of
				true->
					custom_erlang_functions:myGenServCall(DeliveryAddr, {service_package_delivery,Contract,Package,from});
				_->
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,ClientAdr} = business_cards:get_address(ClientBC),
					PluginIdentifier = DeliveryAddr,
					custom_erlang_functions:myGenServCall(ClientAdr, {deliver_package_to_plugin,PluginIdentifier,Contract,Package})
			end;
		_->
			{error,"Invalid contract"}
	end.

deliver_proposal(Contract,Proposal)->
	case valid_contract(Contract) of
		true->
			{ok,DeliveryAddr} = get_delivery_address(Contract),
			case is_pid(DeliveryAddr) of
				true->
					custom_erlang_functions:myGenServCall(DeliveryAddr, {proposal,Contract,Proposal,from});
				_->
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,ClientAdr} = business_cards:get_address(ClientBC),
					PluginIdentifier = DeliveryAddr,
					custom_erlang_functions:myGenServCall(ClientAdr, {deliver_proposal_to_plugin,PluginIdentifier,Contract,Proposal})
			end;
		_->
			{error,"Invalid contract"}
	end.

service_stopped(Contract,Reason)->
	case valid_contract(Contract) of
		true->
			{ok,DeliveryAddr} = get_delivery_address(Contract),
			case is_pid(DeliveryAddr) of
				true->
					custom_erlang_functions:myGenServCall(DeliveryAddr, {service_stopped,Contract,Reason,from});
				_->
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,ClientAdr} = business_cards:get_address(ClientBC),
					PluginIdentifier = DeliveryAddr,
					custom_erlang_functions:myGenServCall(ClientAdr, {deliver_service_stopped_to_plugin,PluginIdentifier,Contract,Reason})
			end;
		_->
			{error,"Invalid contract"}
	end.

create_contract(ClientBC,ServiceBC,DeliveryAddr,ServType,ServInterval,RequestArgsMap,PackageBlueprintMap)->
	Parties = #{client_bc => ClientBC,service_bc => ServiceBC,delivery_addr => DeliveryAddr},
	Requirements = #{service_type=>ServType,interval =>ServInterval,request_arguments=>RequestArgsMap,package_blueprint=>PackageBlueprintMap},
	  case valid_parties(Parties) of
	    true-> 
			case valid_requirements(Requirements) of
				true->
					{ok,#{parties => Parties, requirements => Requirements,time_created=>base_time:now(),proposal=>none}};
				_->
					{error,"Invalid contract arguments: Invalid requirements"}
			end;
	    _-> 
			{error,"Invalid contract arguments: Invalid parties"}
	  end.			

valid_proposal(Proposal)->
	case is_map(Proposal) of
		true->
			Fields = maps:keys(Proposal),
			valid_fields(Fields);
		_->
			case Proposal==none of
				true->
					true;
				_->
					false
			end
	end.

valid_fields([])->
	true;
valid_fields([Field|T])->
	case valid_proposal_field(Field) of
		true->
			valid_fields(T);
		_->
			false
	end.

valid_proposal_field(Field)->
	case is_map(Field) andalso maps:is_key(description, Field) andalso maps:is_key(entry_type, Field) andalso maps:is_key(unit, Field) of
		true->
  			case custom_erlang_functions:is_string(maps:get(description,Field)) and valid_entry_type(maps:get(entry_type,Field)) and custom_erlang_functions:is_string(maps:get(unit,Field)) of
				true->
					true;
				_->
					false
			end;
		_->
			false
	end.

valid_entry_type(EntryType)->
	(EntryType==string) or (EntryType==number) or (EntryType==integer) or (EntryType==boolean) or (EntryType==map) or (EntryType==list).
			
			
add_proposal_field(ProposalTemplate,FieldDescription,FieldEntryType,FieldUnit)->
	case valid_proposal(ProposalTemplate) of
		true->
			Key = #{description=>FieldDescription,entry_type=>FieldEntryType,unit=>FieldUnit},
			case is_map(ProposalTemplate) of
				true->
					{ok,maps:put(Key,pending,ProposalTemplate)};
				_->
					{ok,#{Key=>pending}}
			end;
		_->
			{error,"Invalid proposal"}
	end.

create_contract(ClientBC,ServiceBC,DeliveryAddr,ServType,ServInterval,RequestArgsMap,PackageBlueprintMap,Proposal)->
	Parties = #{client_bc => ClientBC,service_bc => ServiceBC,delivery_addr => DeliveryAddr},
	Requirements = #{service_type=>ServType,interval =>ServInterval,request_arguments=>RequestArgsMap,package_blueprint=>PackageBlueprintMap},
	  case valid_parties(Parties) of
	    true-> 
			case valid_requirements(Requirements) of
				true->
					case valid_proposal(Proposal) of
						true->
							{ok,#{parties => Parties, requirements => Requirements,time_created=>base_time:now(),proposal=>Proposal}};
						_->
							{error,"Invalid contract arguments: Invalid proposal"}
					end;
				_->
					{error,"Invalid contract arguments: Invalid requirements"}
			end;
	    _-> 
			{error,"Invalid contract arguments: Invalid parties"}
	  end.

valid_contract(C)->

	case is_map(C) of
		true->
			case maps:is_key(parties, C) and maps:is_key(requirements, C) and maps:is_key(time_created, C) and maps:is_key(proposal, C) of
				true->
					case valid_parties(maps:get(parties,C)) and valid_requirements(maps:get(requirements,C)) and is_integer(maps:get(time_created,C)) and valid_proposal(maps:get(proposal,C)) of
						true->
							true;
						_->
							false
					end;
				_->
					false
			end;
		_->
			false
	end.

valid_parties(P)->
  case maps:is_key(client_bc, P) and maps:is_key(service_bc, P) and maps:is_key(delivery_addr,P) of
      true->
		  case business_cards:valid_base_bc(maps:get(client_bc,P)) of
			  true->
				  case business_cards:valid_base_bc(maps:get(service_bc,P))  of
					  true->
						  case (is_pid(maps:get(delivery_addr,P))or(is_map(maps:get(delivery_addr,P))))  of
							  true->
								  true;
							  _->
								  io:format("\nInvalid delivery details"),
								  false
						  end;
					  _->
						  io:format("\nInvalid service BC in contract"),
						  false
				  end;
			  _->
				  io:format("\nInvalid client BC in contract"),
				  false
		  end;
	  _->
		  false
  end.

valid_requirements(R)->
  case maps:is_key(service_type,R) and maps:is_key(interval, R) and maps:is_key(request_arguments, R) and maps:is_key(package_blueprint, R) of
	  true->
		  case custom_erlang_functions:is_string(maps:get(service_type,R)) and valid_interval(maps:get(interval,R)) of
			  true->
				  true;
			  _->
				  false
		  end;
	  _->
		  false
  end.

valid_interval(Interval)->
	case Interval of
		once_off->
			true;
		{periodically,Map}->
			case maps:is_key(start, Map) and maps:is_key(interval, Map) and maps:is_key(stop, Map) of
				true->
					Strt = maps:get(start,Map), Interv = maps:get(interval,Map), Stop = maps:get(stop,Map),%%when interv=0 the service will send data as frequently as it can
					case is_integer(Strt) and is_integer(Interv) and ((is_integer(Stop)) or (Stop == never)) of
						true->
							true;
						_->
							false
					end;
				_->
					false
			end;
		_->
			false
	end.
%Parties get functions: service_type,interval,request_arguments,package_blueprint	


get_client_bc(Contract)->
  case valid_contract(Contract) of
	  true->
		  Parties = maps:get(parties,Contract),
		  Return = maps:get(client_bc,Parties),
		  {ok,Return};
	  _->
		  
		  {error,"Invalid contract"}
  end.
get_service_bc(Contract)->
  case valid_contract(Contract) of
	  true->
		  Parties = maps:get(parties,Contract),
		  Return = maps:get(service_bc,Parties),
		  {ok,Return};
	  _->
		  {error,"Invalid contract"}
  end.

get_delivery_address(Contract)->
  case valid_contract(Contract) of
	  true->
		  Parties = maps:get(parties,Contract),
		  Return = maps:get(delivery_addr,Parties),
		  {ok,Return};
	  _->
		  {error,"Invalid contract"}
  end.
%Requirements get functions: service_type,interval,request_arguments,package_blueprint

get_service_type(Contract)->
  case valid_contract(Contract) of
	  true->
		  Requirements = maps:get(requirements,Contract),
		  Return = maps:get(service_type,Requirements),
		  {ok,Return};
	  _->
		  {error,"Invalid contract"}
  end.
get_interval(Contract)->
  case valid_contract(Contract) of
	  true->
		  Requirements = maps:get(requirements,Contract),
		  Return = maps:get(interval,Requirements),
		  {ok,Return};
	  _->
		  {error,"Invalid contract"}
  end.
get_request_arguments(Contract)->
  case valid_contract(Contract) of
	  true->
		  Requirements = maps:get(requirements,Contract),
		  Return = maps:get(request_arguments,Requirements),
		  {ok,Return};
	  _->
		  {error,"Invalid contract"}
  end.
get_package_blueprint(Contract)->
  case valid_contract(Contract) of
	  true->
		  Requirements = maps:get(requirements,Contract),
		  Return = maps:get(package_blueprint,Requirements),
		  {ok,Return};
	  _->
		  {error,"Invalid contract"}
  end.

get_proposal(Contract)->
	case valid_contract(Contract) of
		true->
			Proposal = maps:get(proposal,Contract),
			{ok,Proposal};
		_->
			{error,"Invalid contract"}
	end.

get_proposal_fields(Contract)->
	case valid_contract(Contract) of
		true->
			Proposal = maps:get(proposal,Contract),
			{ok,maps:keys(Proposal)};
		_->
			{error,"Invalid contract"}
	end.

get_proposal_field_description(ProposalField)->
	case valid_proposal_field(ProposalField) of
		true->
			{ok,maps:get(description,ProposalField)};
		_->
			{error,"Invalid proposal field"}
	end.

get_proposal_field_entry_type(ProposalField)->
	case valid_proposal_field(ProposalField) of
		true->
			{ok,maps:get(entry_type,ProposalField)};
		_->
			{error,"Invalid proposal field"}
	end.

get_proposal_field_unit(ProposalField)->
	case valid_proposal_field(ProposalField) of
		true->
			{ok,maps:get(unit,ProposalField)};
		_->
			{error,"Invalid proposal field"}
	end.

insert_proposal_reply(Contract,Proposal)->
	case valid_contract(Contract)  of
		true->
			case valid_proposal(Proposal) of
				true->
					{ok,CurrentProposalFields} = get_proposal_fields(Contract),
					NewContract = update_each_field(CurrentProposalFields,Contract,Proposal,#{}),
					{ok,NewContract};
				_->
					NewContract = maps:update(proposal, invalid, Contract),
					{ok,NewContract}
			end;
		_->
			{error,"Invalid contract or proposal"}
	end.

update_each_field([],Contract,_Proposal,FinalProposal)->
	maps:update(proposal, FinalProposal, Contract);
update_each_field([Field|T],Contract,Proposal,FinalProposal)->
	case maps:is_key(Field, Proposal) of
		true->
			NewFinalProposal = maps:put(Field,maps:get(Field,Proposal),FinalProposal);
		_->
			NewFinalProposal = maps:put(Field,none,FinalProposal)
	end,
	update_each_field(T,Contract,Proposal,NewFinalProposal).
			
get_proposal_field_value(FieldDescription,Contract)->
	case valid_contract(Contract) of
		true->
			Proposal = maps:get(proposal,Contract),
			Fields = maps:keys(Proposal),
			Field = find_field_with_description(Fields,FieldDescription),
			case Field of
				none->
					{error,"No field with this description"};
				_->
					{ok,maps:get(Field,Proposal)}
					
			end;
		_->
			{error,"Invalid contract"}
	end.


find_field_with_description([],_FieldDescription)->
	none;
find_field_with_description([Field|T],FieldDescription)->
	Descr = maps:get(description,Field),
	case Descr==FieldDescription of
		true->
			Field;
		_->
			find_field_with_description(T,FieldDescription)
	end.

find_contract_with_serv_type([],_ServType)->
	{error,"No contract found with this service type"};

find_contract_with_serv_type([Contract|T],ServType)->
	{ok,CServType} = contracts:get_service_type(Contract),
	case (CServType==ServType) of
		true->
			{ok,Contract};
		_->
			find_contract_with_serv_type(T,ServType)
	end.

%%SLOC:367		
	
