%%%-------------------------------------------------------------------
%%% @author Dale Sparrow
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Mar 2019 1:13 PM
%%%-------------------------------------------------------------------
-module(activity_functions).
-author("Daniel van Niekerk (adapted from original by Dale Sparrow)").
-include("activity_records.hrl").

%% API
-export([makeStage1Activity/4,
  makeStage2Activity/6,
  makeStage3Activity/8,
  promoteStage1/3,
  promoteStage2/3,
  is_stage1_act/1,is_stage1_acts/1,
  is_stage2_act/1,is_stage2_acts/1,
  is_stage3_act/1,is_stage3_acts/1,
		 get_ids/1
]).
%=====Creation functions=====%

%The following function creates a stage1Activity record: 
%#stage1Activity{#activity_shell{id,type,tsched,tstart,tend}
%, s1data}
%
%ID::string() - must be unique 
%Type::string()
%Tsched::integer() - use base_time functions
%S1Data::term() - data about how activity should be done
%
%Result -> {ok,#stage1Activity}|{error,Reason::string()}
%***IMPORTANT: take note of Result->{ok, #stage1Activity} 
%and not Result->#stage1Activity
-spec makeStage1Activity(ID::string(),Type::string(),Tsched::integer(),S1Data::term()) -> Result when
	Result :: {ok, Stage1Activity} | {error, Reason},
	Stage1Activity :: term(),
	Reason :: string().
makeStage1Activity(ID,Type,Tsched,S1Data)->
  case custom_erlang_functions:is_string(ID) and custom_erlang_functions:is_string(Type) and is_integer(Tsched) of
	  true->
		  SchedInfo = #schedule_info{tsched = Tsched,s1data = S1Data},
		  S1A = #stage1Activity{id = ID,type = Type,schedule_info = SchedInfo},
		  {ok,S1A};
	  _->
		  {error,"One of the activity arguments is invalid"}
  end.

%The following function creates a stage2Activity record: 
%#stage2Activity{#activity_shell{id,type,tsched,tstart,tend}
%, s1data,s2data}
%
%ID::string() - must be unique 
%Type::string()
%Tsched::integer() - use base_time functions
%Tstart::integer() - use base_time functions
%S1Data::term() - data about how activity should be done
%S2Data::term() - data about how activity was done. Updated 
%by execution plugin as activity is executed
%
%Result -> {ok,#stage2Activity}|{error,Reason::string()} 
%***IMPORTANT: take note of Result->{ok, #stage2Activity} 
%and not Result->#stage2Activity
-spec makeStage2Activity(ID::string(),Type::string(),Tsched::integer(),Tstart::integer(),S1Data::term(),S2Data::term()) -> Result when
	Result :: {ok, Stage2Activity} | {error, Reason},
	Stage2Activity :: term(),
	Reason :: string().
makeStage2Activity(ID,Type,Tsched,Tstart,S1Data,S2Data)->
  case custom_erlang_functions:is_string(ID) and custom_erlang_functions:is_string(Type) and is_integer(Tsched) and is_integer(Tstart) of
	  true->
		  SchedInfo = #schedule_info{tsched = Tsched,s1data = S1Data},
		  ExeInfo = #execution_info{tstart = Tstart,s2data = S2Data},
		  S2A = #stage2Activity{id = ID,type = Type,schedule_info = SchedInfo,execution_info = ExeInfo},
		  {ok,S2A};
	  _->
		  {error,"One of the activity arguments is invalid"}
  end.

%The following function creates a stage3Activity record: 
%#stage3Activity{#activity_shell{id,type,tsched,tstart,tend}
%, s1data,s2data,s3data}
%
%ID::string() - must be unique 
%Type::string()
%Tsched::integer() - use base_time functions
%Tstart::integer() - use base_time functions
%Tend::integer() - use base_time functions
%S1Data::term() - data about how activity should be done
%S2Data::term() - data about how activity was done. Updated 
%by execution plugin as activity is executed
%S3Data::term() - post execution reflection data. Created by
%reflection plugin after which that rp can update the s3data
%of an activity in bio. The activity will already be in bio
%
%Result -> {ok,#stage3Activity}|{error,Reason::string()} 
%***IMPORTANT: take note of Result->{ok, #stage3Activity} 
%and not Result->#stage3Activity
-spec makeStage3Activity(ID::string(),Type::string(),Tsched::integer(),Tstart::integer(),Tend::integer(),S1Data::term(),S2Data::term(),S3Data::term()) -> Result when
	Result :: {ok, Stage2Activity} | {error, Reason},
	Stage2Activity :: term(),
	Reason :: string().
makeStage3Activity(ID,Type,Tsched,Tstart,Tend,S1Data,S2Data,S3Data)->
	case custom_erlang_functions:is_string(ID) and custom_erlang_functions:is_string(Type) and is_integer(Tsched) and is_integer(Tstart) and is_integer(Tend) of
		true->
			SchedInfo = #schedule_info{tsched = Tsched,s1data = S1Data},
			ExeInfo = #execution_info{tstart = Tstart,s2data = S2Data},
			BioInfo = #biography_info{tend = Tend,s3data = S3Data},
			S3A = #stage3Activity{id = ID,type = Type,schedule_info = SchedInfo,execution_info = ExeInfo,biography_info=BioInfo},
			{ok,S3A};
		_->
			{error,"One of the activity arguments is invalid"}
	end.

%=====Promotion functions=====%

%This function takes a stage1Activity record and promotes it 
%to a stage2Activity
%
%S1Activity::#stage1Activity
%Tstart::integer() - use base_time functions
%S2Data::term() - activity execution data
%
%Result -> {ok,#stage2Activity}|{error,Reason::string()} 
%***IMPORTANT: take note of Result->{ok, #stage2Activity} 
%and not Result->#stage2Activity
-spec promoteStage1(S1Activity::term(),Tstart::integer(),S2Data::term()) -> Result when
	Result :: {ok, Stage2Activity} | {error, Reason},
	Stage2Activity :: term(),
	Reason :: string().
promoteStage1(S1Activity,Tstart,S2Data)->
	case is_stage1_act(S1Activity) and is_integer(Tstart) of
		true->
		  ID = S1Activity#stage1Activity.id, Type = S1Activity#stage1Activity.type,
		  SchedInfo = S1Activity#stage1Activity.schedule_info,
		  ExeInfo = #execution_info{tstart = Tstart,s2data = S2Data},
		  S2A = #stage2Activity{id = ID,type = Type,schedule_info = SchedInfo,execution_info = ExeInfo},
		  {ok,S2A};
		_->
			{error,"Invalid act or tstart"}
	end.


%This function takes a stage2Activity record and promotes it 
%to a stage3Activity
%
%S2Activity::#stage2Activity
%Tend::integer() - use base_time functions
%S3Data::term() - activity reflection data
%
%Result -> {ok,#stage3Activity}|{error,Reason::string()} 
%***IMPORTANT: take note of Result->{ok, #stage3Activity} 
%and not Result->#stage3Activity
-spec promoteStage2(S2Activity::term(),Tend::integer(),S3Data::term()) -> Result when
	Result :: {ok, Stage3Activity} | {error, Reason},
	Stage3Activity :: term(),
	Reason :: string().
promoteStage2(S2Activity,Tend,S3Data)->
	case is_stage2_act(S2Activity) and is_integer(Tend) of
		true->
		  ID = S2Activity#stage2Activity.id, Type = S2Activity#stage2Activity.type,
		  SchedInfo = S2Activity#stage2Activity.schedule_info,
		  ExeInfo = S2Activity#stage2Activity.execution_info,
		  BioInfo = #biography_info{tend = Tend,s3data = S3Data},
		  S3A = #stage3Activity{id = ID,type = Type,schedule_info = SchedInfo,execution_info = ExeInfo,biography_info = BioInfo},
		  {ok,S3A};
		_->
			{error,"Invalid act or tend"}
	end.

%=====Validation functions=====%
%This function checks if Act is a valid 
%stage 1 activity
-spec is_stage1_act(Act::term()) -> Result when
	Result :: true | false.
is_stage1_act(Act)->
	case erlang:is_record(Act, stage1Activity,4) of
		true->
			try
				case (custom_erlang_functions:is_string(Act#stage1Activity.id) and custom_erlang_functions:is_string(Act#stage1Activity.type) and is_record(Act#stage1Activity.schedule_info, schedule_info,3)) of
					true->
						case is_integer(Act#stage1Activity.schedule_info#schedule_info.tsched) of
							true->
								true;
							_->
								false
						end;
					_->
						false
				end
			catch
				_:_->false
			end;
		_->
			false
	end.
%This functions checks if all entries in
%ActList are valid stage 1 activities
-spec is_stage1_acts(ActList::list()) -> Result when
	Result :: true | false.
is_stage1_acts([])->
	true;
is_stage1_acts(ActList)->
	[H|T] = ActList,
	Res = is_stage1_act(H),
	case Res of
		true->
			is_stage1_acts(T);
		_->
			Res
	end.

%This function checks if Act is a valid 
%stage 2 activity
-spec is_stage2_act(Act::term()) -> Result when
	Result :: true | false.
is_stage2_act(Act)->
	case erlang:is_record(Act, stage2Activity,5) of
		true->
			try
				case (custom_erlang_functions:is_string(Act#stage2Activity.id) and custom_erlang_functions:is_string(Act#stage2Activity.type) and is_record(Act#stage2Activity.schedule_info, schedule_info,3) and is_record(Act#stage2Activity.execution_info, execution_info,3))  of
					true->
						case is_integer(Act#stage2Activity.schedule_info#schedule_info.tsched) and is_integer(Act#stage2Activity.execution_info#execution_info.tstart) of
							true->
								true;
							_->
								false
						end;
					_->
						false
				end
			catch
				_:_->false
			end;
		_->
			false
	end.
%This functions checks if all entries in
%ActList are valid stage 2 activities
-spec is_stage2_acts(ActList::list()) -> Result when
	Result :: true | false.
is_stage2_acts([])->
	true;
is_stage2_acts(ActList)->
	[H|T] = ActList,
	Res = is_stage2_act(H),
	case Res of
		true->
			is_stage2_acts(T);
		_->
			Res
	end.


%This function checks if Act is a valid 
%stage 3 activity
-spec is_stage3_act(Act::term()) -> Result when
	Result :: true | false.
is_stage3_act(Act)->
	case erlang:is_record(Act, stage3Activity,6) of
		true->
			try
				case (custom_erlang_functions:is_string(Act#stage3Activity.id) and custom_erlang_functions:is_string(Act#stage3Activity.type) and is_record(Act#stage3Activity.schedule_info, schedule_info,3) and is_record(Act#stage3Activity.execution_info, execution_info,3) and is_record(Act#stage3Activity.biography_info, biography_info,3))  of
					true->
						case is_integer(Act#stage3Activity.schedule_info#schedule_info.tsched) and is_integer(Act#stage3Activity.execution_info#execution_info.tstart)and is_integer(Act#stage3Activity.biography_info#biography_info.tend) of
							true->
								true;
							_->
								false
						end;
					_->
						false
				end
			catch
				_:_->false
			end;
		_->
			false
	end.
%This functions checks if all entries in
%ActList are valid stage 3 activities
-spec is_stage3_acts(ActList::list()) -> Result when
	Result :: true | false.
is_stage3_acts([])->
	true;
is_stage3_acts(ActList)->
	[H|T] = ActList,
	Res = is_stage3_act(H),
	case Res of
		true->
			is_stage3_acts(T);
		_->
			Res
	end.

get_ids([],Ids)->
	{ok,Ids};
get_ids([Act|T],Ids)->
	ActId = element(2,Act),
	NewIds = [ActId|Ids],
	get_ids(T,NewIds).
get_ids(Acts)->
	case is_list(Acts) of
		true->
			FirstAct = lists:nth(1, Acts),
			case is_tuple(FirstAct) of
				true->
					RecordName = element(1,FirstAct),
					case RecordName of
						stage1Activity->
							case is_stage1_acts(Acts) of
								true->
									get_ids(Acts,[]);
								_->
									{error,"Invalid acts"}
							end;
						stage2Activity->
							case is_stage2_acts(Acts) of
								true->
									get_ids(Acts,[]);
								_->
									{error,"Invalid acts"}
							end;
						stage3Activity->
							case is_stage3_acts(Acts) of
								true->
									get_ids(Acts,[]);
								_->
									{error,"Invalid acts"}
							end;
						_->
							{error,"Invalid activity record"}
					end;
				_->
					{error,"Invalid acts"}
			end;
		_->
			{error,"Acts must be list"}
	end.
	%%SLOC:239
