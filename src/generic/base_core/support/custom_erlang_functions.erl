%% @author Daniel
%% @doc @todo Add description to custom_erlang_functions.


-module(custom_erlang_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([
		 myGenServCall/2,myGenServCall/3,
		 myNormalProcCall/2,myNormalProcCall/3,
		 isListOfStrings/1,is_string/1,isListOfIntegers/1,is_binary_string/1,
		 tryOneArgFunction/3,terminate_spawned_processes/1,isListOfBinaryStrings/1,
		 gen_serv_reply_if_from/2,binary_list_to_str_list/2,
		 remove_keys_from_map/2,
		 gen_serv_reply_if_i_called/2,kill_all_pids/1,get_cvs/1,cv_to_atr/3]
	   	).
-define(CALL_TIMEOUT,3600000).%60 mins


%% ====================================================================
%% Internal functions
%% ====================================================================

myNormalProcCall(ProcessPid,Call)->
	case is_pid(ProcessPid) of
		true->
			ProcessPid!{Call,self()},
			receive
				Result->
					Result
			after 5000->
				timeout
			end;
		_->
			{error,"ProcessPid must be a pid"}
	end.
myNormalProcCall(ProcessPid,Call,Timeout)->
	case is_pid(ProcessPid) and is_integer(Timeout) of
		true->
			ProcessPid!{Call,self()},
			receive
				Result->
					Result
			after Timeout->
				timeout
			end;
		_->
			{error,"ProcessPid must be a pid"}
	end.
myGenServCall(GenServ,Call)->
	case is_atom(GenServ) or is_pid(GenServ) of
		true->
			MyPid = self(),
			try
				gen_server:call(GenServ, Call,?CALL_TIMEOUT)
			catch
				exit:_Something -> error_log:log(?MODULE,0,unknown,"\nERROR:Gen serv call,~p by ~p to ~p timed out",[Call,MyPid,GenServ]),
								   timeout
			end;
		_->
			{error,"GenServ must be an atom or pid"}
	end.
	
myGenServCall(GenServ,Call,Timeout)->
	case is_atom(GenServ) or is_pid(GenServ) and is_integer(Timeout) of
		true->
			MyPid = self(),
			try
				gen_server:call(GenServ, Call, Timeout)
			catch
				exit:_Something -> error_log:log(?MODULE,0,unknown,"\nERROR:Gen serv call,~p by ~p to ~p timed out",[Call,MyPid,GenServ]),
								   timeout
			end;
		_->
			{error,"GenServ must be an atom or pid"}
	end.

is_string(String)->
	((io_lib:printable_list(String) and not(String==[]))).

is_binary_string(BinaryString)->
	is_binary(BinaryString) andalso (is_string(binary_to_list(BinaryString))).

isListOfStrings([])->
	true;
isListOfStrings(List)->
	case is_list(List) of
		true->
			[H|T] = List,
			case is_string(H) of
				true->
					isListOfStrings(T);
				_->
					false
			end;
		_->
			false
	end.

isListOfIntegers([])->
	true;
isListOfIntegers(List)->
	[H|T] = List,
	case is_integer(H) of
		true->
			isListOfIntegers(T);
		_->
			false
	end.

isListOfBinaryStrings([])->
	true;
isListOfBinaryStrings(List)->
	[H|T] = List,
	case is_binary_string(H) of
		true->
			isListOfBinaryStrings(T);
		_->
			false
	end.
tryOneArgFunction(Module,Function,Arg)->
	try
		Module:Function(Arg)
	catch
		_:Something -> error_log:log(?MODULE,0,unknown,"\nCould not call 1 arg function"),
						   {error,Something}
	end.

terminate_spawned_processes(RawPid)->
	case is_atom(RawPid) of
		true->
			Pid = whereis(RawPid);
		_->
			Pid = RawPid
	end,
	{monitors,MonitorList} = erlang:process_info(Pid, monitors),
	delete_each_monitored_process(MonitorList).

delete_each_monitored_process([])->
	ok;
delete_each_monitored_process([MonitoredEntity|T])->
	{Type,ProcessInfo} = MonitoredEntity,
	case Type of
		process->
			case is_pid(ProcessInfo) of
				true->
					Pid = ProcessInfo;
				_->
					{ProcName,_Loc} = ProcessInfo,
					Pid = whereis(ProcName)
			end,
			{initial_call,{ProcLibOrNot,Init_POrNot,_N}} = process_info(Pid,initial_call),
			case (ProcLibOrNot==proc_lib) and (Init_POrNot==init_p) of
				true->
					%error_log:log(?MODULE,0,unknown,"\n~p is a gen_server monitored by the plugin being deleted so it will not be terminated",[Pid]),
					ok;
				_->
					exit(Pid,kill)
			end;
		_->
			ok
	end,
	delete_each_monitored_process(T).

gen_serv_reply_if_from(ReplyTo,Res)->
	case is_tuple(ReplyTo) andalso size(ReplyTo)==2 of
		true->
			{Pid,_Ref} = ReplyTo,
			case is_pid(Pid) andalso is_process_alive(Pid) of
				true->
					gen_server:reply(ReplyTo,Res);
				_->
					ok
			end;
		_->
			ok
	end.

binary_list_to_str_list([],ListOfStrings)->
	ListOfStrings;
binary_list_to_str_list(BinaryList,ListOfStrings)->
	case is_list(BinaryList) of
		true->
			[BinaryItem|T]=BinaryList,
			case is_binary(BinaryItem) of
				true->
					Str = binary_to_list(BinaryItem),
					case io_lib:printable_list(Str) of
						true->
							binary_list_to_str_list(T,[Str|ListOfStrings]);
						_->
							binary_list_to_str_list(T,ListOfStrings)
					end;
				_->
					binary_list_to_str_list(T,ListOfStrings)
			end;
		_->
			error_log:log(?MODULE,0,unknown,"\nERROR:Binary list of strings ~p is not a list. Check front end code",[BinaryList]),
			[]
	end.

remove_keys_from_map([],Map)->
	Map;
remove_keys_from_map([Key|T],Map)->
	remove_keys_from_map(T,maps:remove(Key, Map)).

gen_serv_reply_if_i_called(From,Reply)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			gen_serv_reply_if_from(From,Reply);
		_->
			ok
	end.

kill_all_pids([])->
	ok;
kill_all_pids([Pid|T])->
	case is_process_alive(Pid) of
		true->
			exit(Pid,kill);
		_->
			ok
	end,
	kill_all_pids(T).

get_cvs(AtrRecep)->
	{ok,Atrs} = atr_api:get_attributes_by_types(AtrRecep, ["Calculation variables"]),
	atrs_to_cvs_map(Atrs,#{}).

atrs_to_cvs_map([],CVsMap)->
	CVsMap;
atrs_to_cvs_map([Atr|T],CVsMap)->
	{ok,AtrId} = attribute_functions:get_atr_id(Atr),
	{ok,AtrVal} = attribute_functions:get_atr_value(Atr),
	atrs_to_cvs_map(T,maps:put(AtrId, AtrVal, CVsMap)).

cv_to_atr(CVId,CVVal,CVActType)->
	{ok,Atr}=attribute_functions:create_attribute(CVId, "Calculation variables", CVActType, CVVal),
	Atr.
%%SLOc:196
	