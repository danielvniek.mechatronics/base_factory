%% @author Daniel
%% @doc @todo Add description to ets_match_spec.


-module(exe_match_spec).
-include("../support/activity_records.hrl").
-include_lib("stdlib/include/ms_transform.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([get_match10IDs/1,get_match10Types/1,get_match10TypesWithTimes/1,get_matchTimes/1,get_matchAll/0,del_match10IDs/1,del_match10Types/1,del_match10TypesWithTimes/1,del_matchTimes/1,del_matchAll/0]).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_matchAll()->
	MatchSpec = ets:fun2ms(
    fun(A = #stage2Activity{})
      ->
      A
    end),
	MatchSpec.
get_match10IDs(IDs)->
	L = length(IDs),
	case L>=10 of
		true->
			List = lists:sublist(IDs, 1, 10),
			RemainderList = lists:sublist(IDs, 11, L);
		_->
			RemainderList = [],
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, IDs)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(IDs, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #stage2Activity{id = ID})
      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
      A
    end),
	{MatchSpec,RemainderList}.
get_match10Types(Types)->
	L = length(Types),
	case L>=10 of
		true->
			List = lists:sublist(Types, 1, 10),
			RemainderList = lists:sublist(Types, 11, L);
		_->
			RemainderList = [],
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, Types)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #stage2Activity{type = Type})
      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
      A
    end),
	{MatchSpec,RemainderList}.


get_matchTimes({TschedLow, TschedHigh, TstartLow, TstartHigh})->
	case erlang:is_integer(TschedLow) of
		true->
			SchedLow = TschedLow;
		_->
			SchedLow = 0
	end,
	case erlang:is_integer(TstartLow) of
		true->
			StartLow = TstartLow;
		_->
			StartLow = 0
	end,
	case erlang:is_integer(TschedHigh) of
		true->
			case erlang:is_integer(TstartHigh)of
				true->						
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) ->
				      A
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart) ->
				      A
				    end)
			end;
		_->
			case erlang:is_integer(TstartHigh)of
				true->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) ->
				      A
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart)  ->
				      A
				    end)
			end
	end,
	{MatchSpec,[]}.
get_match10TypesWithTimes({Types, TschedLow, TschedHigh, TstartLow, TstartHigh})->
	L = length(Types),
	case L>=10 of
		true->
			List = lists:sublist(Types, 1, 10),
			RemainderList = {lists:sublist(Types, 11, L),TschedLow,TschedHigh,TstartLow,TstartHigh};
		_->
			RemainderList = [],
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, Types)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	case erlang:is_integer(TschedLow) of
		true->
			SchedLow = TschedLow;
		_->
			SchedLow = 0
	end,
	case erlang:is_integer(TstartLow) of
		true->
			StartLow = TstartLow;
		_->
			StartLow = 0
	end,
	case erlang:is_integer(TschedHigh) of
		true->
			case erlang:is_integer(TstartHigh)of
				true->						
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      A
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart) andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      A
				    end)
			end;
		_->
			case erlang:is_integer(TstartHigh)of
				true->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      A
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart)  andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      A
				    end)
			end
	end,
	{MatchSpec,RemainderList}.

del_matchAll()->
	MatchSpec = ets:fun2ms(
    fun(A = #stage2Activity{})
      ->
      A
    end),
	MatchSpec.
del_match10IDs(IDs)->
	L = length(IDs),
	case L>=10 of
		true->
			List = lists:sublist(IDs, 1, 10),
			RemainderList = lists:sublist(IDs, 11, L);
		_->
			RemainderList = [],
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, IDs)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(IDs, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #stage2Activity{id = ID})
      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
      true
    end),
	{MatchSpec,RemainderList}.
del_match10Types(Types)->
	L = length(Types),
	case L>=10 of
		true->
			List = lists:sublist(Types, 1, 10),
			RemainderList = lists:sublist(Types, 11, L);
		_->
			RemainderList = [],
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, Types)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #stage2Activity{type = Type})
      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
      true
    end),
	{MatchSpec,RemainderList}.


del_matchTimes({TschedLow, TschedHigh, TstartLow, TstartHigh})->
	case erlang:is_integer(TschedLow) of
		true->
			SchedLow = TschedLow;
		_->
			SchedLow = 0
	end,
	case erlang:is_integer(TstartLow) of
		true->
			StartLow = TstartLow;
		_->
			StartLow = 0
	end,
	case erlang:is_integer(TschedHigh) of
		true->
			case erlang:is_integer(TstartHigh)of
				true->						
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) ->
				      true
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart) ->
				      true
				    end)
			end;
		_->
			case erlang:is_integer(TstartHigh)of
				true->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) ->
				      true
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart)  ->
				      true
				    end)
			end
	end,
	{MatchSpec,[]}.
del_match10TypesWithTimes({Types, TschedLow, TschedHigh, TstartLow, TstartHigh})->
	L = length(Types),
	case L>=10 of
		true->
			List = lists:sublist(Types, 1, 10),
			RemainderList = {lists:sublist(Types, 11, L),TschedLow,TschedHigh,TstartLow,TstartHigh};
		_->
			RemainderList = [],
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, Types)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	case erlang:is_integer(TschedLow) of
		true->
			SchedLow = TschedLow;
		_->
			SchedLow = 0
	end,
	case erlang:is_integer(TstartLow) of
		true->
			StartLow = TstartLow;
		_->
			StartLow = 0
	end,
	case erlang:is_integer(TschedHigh) of
		true->
			case erlang:is_integer(TstartHigh)of
				true->						
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      true
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched andalso Tsched=<TschedHigh) andalso 
								(StartLow=<Tstart) andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      true
				    end)
			end;
		_->
			case erlang:is_integer(TstartHigh)of
				true->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart andalso Tstart=<TstartHigh) andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      true
				    end);
				_->
					MatchSpec = ets:fun2ms(
				    fun(A = #stage2Activity{type = Type,schedule_info = #schedule_info{tsched = Tsched},execution_info = #execution_info{tstart = Tstart}})
				      when  (SchedLow=<Tsched) andalso 
								(StartLow=<Tstart)  andalso 
										(Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10)->
						      true
				    end)
			end
	end,
	{MatchSpec,RemainderList}.
%%412