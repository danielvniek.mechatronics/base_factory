%% @author Daniel
%% @doc @todo Add description to atr_api.


-module(atr_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([register_scheduling_plugin/3,deregister_scheduling_plugin/3,
		 change_storage/2,
		 save/2,
		 get_all_attributes/1,get_attributes_by_ids/2,get_attributes_by_types/2,get_attributes_by_contexts/2,
		 del_all_attributes/1,del_attributes_by_ids/2,del_attributes_by_types/2,del_attributes_by_contexts/2,
		 pop_all_attributes/1,pop_attributes_by_ids/2,pop_attributes_by_types/2,pop_attributes_by_contexts/2,
		get_attribute_value/2,update_attribute_value/3,apply_function_to_atr_val/3]).
-record(base_attribute,
{
  id :: string(),
  type:: string(),
  context :: string(),%%can be used for units if the designer wants or anything else that gives the value context
  value:: term()
}).

%% ====================================================================
%% Internal functions
%% ====================================================================
register_scheduling_plugin(ReceptionPid,SP_Pid,ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {register_scheduling_plugin,SP_Pid,ActType}).

deregister_scheduling_plugin(ReceptionPid,SP_Pid,ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {deregister_scheduling_plugin,SP_Pid,ActType}).

change_storage(ReceptionPid,NewStoragePid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {change_storage, NewStoragePid}).

save(ReceptionPid,Attributes)->
	case is_list(Attributes) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{save_attributes,Attributes}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{save_attributes,[Attributes]}})
	end.

apply_function_to_atr_val(ReceptionPid,AtrId,Fun)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {apply_function_to_atr_val,AtrId,Fun}).
%--------------------GET--------------------%
get_all_attributes(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_atr,"get"}}).

update_attribute_value(ReceptionPid,AtrId,NewVal)->
	{Res,AtrsOrMsg} = custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"get",[AtrId]}}),
	case (Res==ok) of 
		true->
			case (length(AtrsOrMsg)==1) of
				true->
					[Atr] = AtrsOrMsg,
					NewAtr = Atr#base_attribute{value=NewVal},
					save(ReceptionPid,[NewAtr]);
				_->
					{error,"No attribute with this id"}
			end;
		_->
			{Res,AtrsOrMsg}
	end.
get_attribute_value(ReceptionPid,AtrId)->
	{Res,AtrsOrMsg} = custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"get",[AtrId]}}),
	case Res of 
		ok->
			case (length(AtrsOrMsg)==1) of
				true->
					[Atr] = AtrsOrMsg,
					attribute_functions:get_atr_value(Atr);
				_->
					{error,"No attribute with this id"}
			end;
		_->
			{Res,AtrsOrMsg}
	end.
get_attributes_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"get",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"get",[IDs]}})
	end.

get_attributes_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_types,"get",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_types,"get",[Types]}})
	end.

get_attributes_by_contexts(ReceptionPid,Contexts)->
	case is_list(Contexts) and not(io_lib:printable_list(Contexts)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_contexts,"get",Contexts}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_contexts,"get",[Contexts]}})
	end.
%------------------DELETE---------------------%
del_all_attributes(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_atr,"del"}}).

del_attributes_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"del",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"del",[IDs]}})
	end.

del_attributes_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_types,"del",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_types,"del",[Types]}})
	end.

del_attributes_by_contexts(ReceptionPid,Contexts)->
	case is_list(Contexts) and not(io_lib:printable_list(Contexts)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_contexts,"del",Contexts}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_contexts,"del",[Contexts]}})
	end.
%------------------POP----------------------%
pop_all_attributes(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_atr,"pop"}}).

pop_attributes_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"pop",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_ids,"pop",[IDs]}})
	end.

pop_attributes_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_types,"pop",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_types,"pop",[Types]}})
	end.

pop_attributes_by_contexts(ReceptionPid,Contexts)->
	case is_list(Contexts) and not(io_lib:printable_list(Contexts)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_contexts,"pop",Contexts}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_atr_by_contexts,"pop",[Contexts]}})
	end.
%%SLOC:113