
-module(atr_sup).
-author("Dale Sparrow (Daniel van Niekerk made small changes from original)").

-behaviour(supervisor).

%% API
-export([start_link/2
  ]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

start_link(CoordPid,BC) ->
  supervisor:start_link(?MODULE, [CoordPid,BC]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([CoordPid,BC]) ->
  RestartStrategy = one_for_one,
  MaxRestarts = 10,
  MaxSecondsBetweenRestarts = 3,

  SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

  Restart = permanent,
  Shutdown = 2000,
  Type = worker,

  ReceptionID = atr_recep,
  RecepMFargs = {atr_recep, start_link, [CoordPid,BC]},
  Reception = {ReceptionID, RecepMFargs, Restart, Shutdown, Type, [atr_recep]},

  AmoduleID = atr_def_storage,
  AmoduleMFargs = {atr_def_storage, start_link, [CoordPid,BC]},
  Amodule = {AmoduleID, AmoduleMFargs, Restart, Shutdown, Type, [atr_def_storage]},

  {ok, {SupFlags, [Reception, Amodule]}}.

%%SLOC:17
