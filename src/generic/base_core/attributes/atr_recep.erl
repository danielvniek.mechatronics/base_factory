-module(atr_recep).
-author("Daniel van Niekerk (adapted from the original by Dale Sparrow)").

-behaviour(gen_server).

%% ATTRIBUTES API

-export([start_link/2,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).


-include("../support/base_records.hrl").
-include("../support/activity_records.hrl").
-record(atr_state, {status::ready|not_ready,
					default_storage_pid::pid(),storage_service_address::default|pid(),	%%storage related records
					coord_pid::pid(),	reception_map::term(),	instance_plugins::list(),										%%instance pids needed for functionality
					resource_id::string(),resource_type::string(),						%%id and type needed for storage services
					registered_scheduling_plugins::map(),cloud_backup_plugins}).								%%atr shares attributes with registered scheduling plugins

%%%===================================================================

start_link(CoordPid,BC) ->
  {ok,AtrPID} = gen_server:start_link(?MODULE, [CoordPid,BC], []),
  {ok,AtrPID}.

stop() ->
  gen_server:stop(self()).

init([CoordPid,BC]) ->
 	{ok,ID} = business_cards:get_id(BC),
	{ok,Type} = business_cards:get_type(BC),
	custom_erlang_functions:myGenServCall(CoordPid,{core_component_started,atr_recep}),
    {ok, #atr_state{status = not_ready,coord_pid = CoordPid,reception_map = #{},instance_plugins=[],
					resource_id=ID,resource_type=Type,
					storage_service_address = default,registered_scheduling_plugins=#{},cloud_backup_plugins=[]}}. 

%-----------------------------------------------------------------
%%cloud backup plugins
handle_call({register_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#atr_state.reception_map),State#atr_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#atr_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					NewCloudBackupPlugins = [PluginPid|CleanCloudBackupPlugins],
					{reply,ok,State#atr_state{cloud_backup_plugins=NewCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
handle_call({deregister_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#atr_state.reception_map),State#atr_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#atr_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					{reply,ok,State#atr_state{cloud_backup_plugins=CleanCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
%Activity handling related calls
handle_call({register_scheduling_plugin,SP_Pid, AtrType},From,State)->
	FullPermittedList = lists:append(maps:values(State#atr_state.reception_map),State#atr_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case io_lib:printable_list(AtrType) and is_pid(SP_Pid) of
				true->
					{NewSP_Map,RegResult,_GateMap} = plugin_registration_functions:register(State#atr_state.registered_scheduling_plugins,true,SP_Pid,AtrType,none),
					Request = {get_del_or_pop_atr_by_types, "get",[AtrType]},
					Call = storage_functions:add_id_and_type(Request,State#atr_state.resource_id,State#atr_state.resource_type),
					case State#atr_state.storage_service_address of
						default->
							%%use default module
							{ok,Atrs}=custom_erlang_functions:myGenServCall(State#atr_state.default_storage_pid, Call);
						_->
							{ok,Atrs}=custom_erlang_functions:myGenServCall(State#atr_state.storage_service_address, Call)
					end,
					case RegResult of
						ok->
							Reply = {ok,Atrs};
						_->
							Reply = RegResult
					end,
					NewState = State#atr_state{registered_scheduling_plugins = NewSP_Map};
				_->
					NewState = State,
					Reply = {error,"Invalid input arguments"}
			end,
			{reply,Reply,NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
handle_call({deregister_scheduling_plugin,SP_Pid,AtrType},From,State)->
	FullPermittedList = lists:append(maps:values(State#atr_state.reception_map),State#atr_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case io_lib:printable_list(AtrType) and is_pid(SP_Pid) of
				true->
					{NewSPMap,Reply,_GateMap} = plugin_registration_functions:deregister(State#atr_state.registered_scheduling_plugins, true, SP_Pid, AtrType, none),
					NewState = State#atr_state{registered_scheduling_plugins = NewSPMap};
				_->
					NewState = State,
					Reply = {error,"Invalid input arguments"}
			end,
			{reply,Reply,NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
%This call is used to change storage method of an instance. For example using some cloud storage service
handle_call({change_storage, NewStoragePid},From,State)->
	FullPermittedList = lists:append(maps:values(State#atr_state.reception_map),State#atr_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case erlang:is_pid(NewStoragePid) of
				true->
					NewState = State#atr_state{storage_service_address = NewStoragePid},
					Reply = ok;
				_->
					NewState = State,
					Reply = {error, "Invalid NewStoragePid"}
			end,
			{reply, Reply, NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

%Save, get, delete and pop requests all go through the next call:
handle_call({storage_request,Request},From,State)->
	FullPermittedList = lists:append(maps:values(State#atr_state.reception_map),State#atr_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#atr_state.status of
				ready->
					ValidReq = storage_functions:valid_atr_storage_request(Request),
					case ValidReq of
						true->
							Atrs = storage_functions:get_content_if_save(Request),
							case Atrs of
								false->
									PostRequestProcess = none;
								_->
									PostRequestProcess = {atr_processes,pass_to_sps,{Atrs,State#atr_state.registered_scheduling_plugins,self()}}
							end,
							Call = storage_functions:add_id_and_type(Request,State#atr_state.resource_id,State#atr_state.resource_type),
							MyPid = self(),
							spawn_monitor(storage_functions,request,[State#atr_state.storage_service_address, State#atr_state.default_storage_pid, Call, From, PostRequestProcess,{State#atr_state.cloud_backup_plugins,MyPid}]),
							{noreply,State};
						_->
							{false,Reason} = ValidReq,
							Reply = {error,Reason},
							{reply,Reply,State}
					end;
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({apply_function_to_atr_val,AtrId,Fun},From,State)->
	FullPermittedList = lists:append(maps:values(State#atr_state.reception_map),State#atr_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#atr_state.status of
				ready->
					case State#atr_state.storage_service_address of
						default->
							%%use default module
							Adr = State#atr_state.default_storage_pid;						
						_->
							Adr = State#atr_state.storage_service_address
					end,
					{ok,Atrs}=custom_erlang_functions:myGenServCall(Adr, {get_del_or_pop_atr_by_ids,State#atr_state.resource_id,State#atr_state.resource_type,"get",[AtrId]}),
					case length(Atrs) of
						1->
							Atr = lists:nth(1, Atrs),
									AtrVal = Atr#base_attribute.value,
							try
								NewAtrVal = Fun(AtrVal),
								NewAtr = Atr#base_attribute{value = NewAtrVal},
								{ok,"success"} = custom_erlang_functions:myGenServCall(State#atr_state.default_storage_pid, {save_attributes,State#atr_state.resource_id,State#atr_state.resource_type,[NewAtr]}),
								MyPid = self(),
								spawn_monitor(fun()->storage_functions:inform_cloud_backup_plugins(State#atr_state.cloud_backup_plugins,State#atr_state.resource_id,State#atr_state.resource_type,"atr",MyPid)end),
								{reply,ok,State}
							catch
								A:B:StackTrace->
									ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
									{reply,{error,"Caught "++ErrorAndStackTrace}}
							end;
						_->
							{reply,{error,"No attribute with this id"},State}
					end;
					
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this intance's processes can make this call"},State}
	end;
			
%Sent by coordinator
handle_call({instance_pids, ReceptionMap, DefStorage},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#atr_state.coord_pid, From) of
		true->
			
			NewState = State#atr_state{default_storage_pid = DefStorage,status = ready,reception_map = ReceptionMap},
			setup_start_attributes(NewState),
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;
handle_call({instance_plugins, InstancePlugins},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#atr_state.coord_pid, From) of
		true->
			NewState = State#atr_state{instance_plugins = InstancePlugins},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;
handle_call(_Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"Atr_recep:unknown request"),
  {reply, unknown, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

setup_start_attributes(State)->
	timer:sleep(20),
	GetRequest = {get_del_or_pop_all_atr,"get"},
	GetCall = storage_functions:add_id_and_type(GetRequest,State#atr_state.resource_id,State#atr_state.resource_type),
	
	case State#atr_state.storage_service_address of
		default->
			%%use default module
			{Res,Atrs}=custom_erlang_functions:myGenServCall(State#atr_state.default_storage_pid, GetCall);
		_->
			{Res,Atrs}=custom_erlang_functions:myGenServCall(State#atr_state.storage_service_address, GetCall)
	end,
	
	case Res of 
		ok->
			
			case length(Atrs)=<2 of 
				true->
					ModName = State#atr_state.resource_type++"_attributes",
					Mod = erlang:list_to_atom(string:lowercase(string:replace(ModName, " ", "_",all))),
					try
						StartupAttributes = Mod:get(),
						Request = {save_attributes,[StartupAttributes]},
						Call = storage_functions:add_id_and_type(Request,State#atr_state.resource_id,State#atr_state.resource_type),
						case State#atr_state.storage_service_address of
							default->
								%%use default module
								{ok,"success"}=custom_erlang_functions:myGenServCall(State#atr_state.default_storage_pid, Call);
							_->
								{ok,"success"}=custom_erlang_functions:myGenServCall(State#atr_state.storage_service_address, Call)
						end
					catch 
						A:B:C->
							io:format("ERROR: Could not add any attributes to new holon. Caught ~p:~p:~p",[A,B,C])
					end;
				_->
					io:format("L:~p",[length(Atrs)]),
					ok
			end;
			
		_->%%default storage does not recognize its reception yet
			%error_log:log(?MODULE,0,unknown,"\nDef storage not ready yet"),
			timer:sleep(500),
			setup_start_attributes(State)
	end.

%%SLOC:234