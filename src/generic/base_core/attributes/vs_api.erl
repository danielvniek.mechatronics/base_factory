%% @author Daniel
%% @doc @todo Add description to atr_api.


-module(vs_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([
		 save/2,create/3,
		 get_all/1,
		 delete_all/1,delete_by_id/2,
		get_value/2,update_value/3,apply_function_to_val/3]).

-record(base_attribute,
{
  id :: string(),
  type:: string(),
  context :: string(),%%can be used for units if the designer wants or anything else that gives the value context
  value:: term()
}).

%% ====================================================================
%% Internal functions
%% ====================================================================
ensure_variable_id(ID)->
	L = length(ID),
	CVString = string:slice(ID, L-5),
	case (CVString==" (CV)") of
		true->
			ID;
		_->
			ID++" (CV)"
	end.

create(ID,Context,Value)->
	case custom_erlang_functions:is_string(ID) and custom_erlang_functions:is_string(Context) of
		true->
			VID = ensure_variable_id(ID),	
			{ok,#base_attribute{id=VID,type="Calculation variables",context = Context,value=Value}};
		_->
			{error,"ID and Context must both be strings"}
	end.

save(VS_PID,Variables)->
	case is_list(Variables) of
		true->
			custom_erlang_functions:myGenServCall(VS_PID, {storage_request,{save_attributes,Variables}});
		_->
			custom_erlang_functions:myGenServCall(VS_PID, {storage_request,{save_attributes,[Variables]}})
	end.

apply_function_to_val(VS_PID,ID,Fun)->
	custom_erlang_functions:myGenServCall(VS_PID, {apply_function_to_atr_val,ID,Fun}).
%--------------------GET--------------------%
atrs_to_cvs_map([],CVsMap)->
	CVsMap;

atrs_to_cvs_map([Atr|T],CVsMap)->
	{ok,AtrId} = attribute_functions:get_atr_id(Atr),
	{ok,AtrVal} = attribute_functions:get_atr_value(Atr),
	atrs_to_cvs_map(T,maps:put(AtrId, AtrVal, CVsMap)).


get_all(VS_PID)->
	{ok,Atrs}=atr_api:get_attributes_by_types(VS_PID, ["Calculation variables"]),
	A = atrs_to_cvs_map(Atrs,#{}),
	{ok,A}.

update_value(VS_PID,ID,NewVal)->
	VID = ensure_variable_id(ID),	 
	{Res,AtrsOrMsg} = custom_erlang_functions:myGenServCall(VS_PID, {storage_request,{get_del_or_pop_atr_by_ids,"get",[VID]}}),
	case (Res==ok) of 
		true->
			case (length(AtrsOrMsg)==1) of
				true->
					[Atr] = AtrsOrMsg,
					NewAtr = Atr#base_attribute{value=NewVal},
					save(VS_PID,[NewAtr]);
				_->
					{error,"No attribute with this id"}
			end;
		_->
			{Res,AtrsOrMsg}
	end.

get_value(VS_PID,ID)->
	VID = ensure_variable_id(ID),	
	{Res,AtrsOrMsg} = custom_erlang_functions:myGenServCall(VS_PID, {storage_request,{get_del_or_pop_atr_by_ids,"get",[VID]}}),
	case Res of 
		ok->
			case (length(AtrsOrMsg)==1) of
				true->
					[Atr] = AtrsOrMsg,
					attribute_functions:get_atr_value(Atr);
				_->
					{error,"No attribute with this id"}
			end;
		_->
			{Res,AtrsOrMsg}
	end.

%------------------DELETE---------------------%
delete_all(VS_PID)->
	custom_erlang_functions:myGenServCall(VS_PID, {storage_request,{get_del_or_pop_atr_by_types,"del",["Calculation variables"]}}).

delete_by_id(VS_PID,ID)->
	VID = ensure_variable_id(ID),
	custom_erlang_functions:myGenServCall(VS_PID, {storage_request,{get_del_or_pop_atr_by_ids,"del",[VID]}}).


