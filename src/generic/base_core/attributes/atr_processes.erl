%% @author Daniel
%% @doc @todo Add description to atr_processes.


-module(atr_processes).
-include("../support/base_records.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([pass_to_sps/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================
pass_to_sps({Attributes,SpMap,MyRecep})->
	AtrTypes = maps:keys(SpMap),
	case (length(AtrTypes)>0) and (length(Attributes)>0) of
		true->
			pass_to_sps(Attributes,SpMap,AtrTypes,MyRecep);
		_->
			ok
	end.
pass_to_sps(Attributes,SpMap,AtrTypes,MyRecep)->
	[AtrTypeKey|T] = AtrTypes,
	SP_List = maps:get(AtrTypeKey,SpMap),
	pass_to_sp(SP_List,AtrTypeKey,Attributes,[],Attributes,MyRecep),
	case (length(T)==0) of
		true->
			ok;
		_->
			pass_to_sps(Attributes,SpMap,T,MyRecep)
	end.
pass_to_sp(SP_List,AtrType,[],FoundAtrs,NewAtrs,MyRecep)->
	case (SP_List==[]) or (FoundAtrs==[]) of
		true->
			NewAtrs;
		_->
			[H|T] = SP_List,
			pass_to_each_sp(H,AtrType,FoundAtrs,MyRecep),
			pass_to_sp(T,AtrType,[],FoundAtrs,NewAtrs,MyRecep)
	end;
pass_to_sp(SP_Pid,AtrType,[Atr|T],FoundAtrs,NewAtrs,MyRecep)->
	case Atr#base_attribute.type of
		AtrType->
			NewNewAtrs = lists:delete(Atr, NewAtrs),
			pass_to_sp(SP_Pid,AtrType,T,[Atr|FoundAtrs],NewNewAtrs,MyRecep);
		_->
			pass_to_sp(SP_Pid,AtrType,T,FoundAtrs,NewAtrs,MyRecep)
	end.
pass_to_each_sp(SP_Pid,AtrType,FoundAtrs,MyRecep)->
	Res = custom_erlang_functions:myGenServCall(SP_Pid,{attributes_updated,FoundAtrs}),
	case Res of 
		ok->
			ok;
		_->
			io:format("\nSp did not respond or responded with wrong reply. Will kill sp so that its supervisor can restart it. Will delete sp's pid from atr_recep state"),
			ok=custom_erlang_functions:myGenServCall(MyRecep,{deregister_scheduling_plugin,SP_Pid,AtrType}),
			exit(SP_Pid,kill)		
	end.
%%SLOC:45

