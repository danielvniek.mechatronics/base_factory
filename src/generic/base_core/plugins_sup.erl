%%%-------------------------------------------------------------------
%%% @author Daniel van Niekerk
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%% This module acts as the supervisor and functional access to plugin services
%%% @end
%%% Created : 8 August 2020
%%%-------------------------------------------------------------------
-module(plugins_sup).
-author("Daniel van Niekerk").

-behaviour(supervisor).

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

start_link(CoordPid) ->
  supervisor:start_link(?MODULE, [CoordPid]).

%%%===================================================================

init([CoordPid]) ->
    custom_erlang_functions:myGenServCall(CoordPid,{core_component_started,plugin_sup}),
  	MaxRestart = 3,
	MaxTime = 3600,
	{ok, {{simple_one_for_one, MaxRestart, MaxTime},
	[{plugin_sup,
	{plugin_sup,start_link,[]},
	temporary, 5000, supervisor, [plugin_sup]}]}}.

%%SLOC:10
