
-module(bio_sup).
-author("Dale Sparrow (Daniel van Niekerk made small changes from original)").

-behaviour(supervisor).

%% API
-export([start_link/2]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

start_link(CoordPid,BC) ->
  supervisor:start_link(?MODULE, [CoordPid,BC]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([CoordPid,BC]) ->
  RestartStrategy = one_for_one,
  MaxRestarts = 10,
  MaxSecondsBetweenRestarts = 3,

  SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

  Restart = permanent,
  Shutdown = 2000,
  Type = worker,

  ReceptionID = bio_recep,
  RecepMFargs = {bio_recep, start_link, [CoordPid,BC]},
  Reception = {ReceptionID, RecepMFargs, Restart, Shutdown, Type, [bio_recep]},

  BmoduleID = bio_def_storage,
  BmoduleMFargs = {bio_def_storage, start_link, [CoordPid,BC]},
  Bmodule = {BmoduleID, BmoduleMFargs, Restart, Shutdown, Type, [bio_def_storage]},

  {ok, {SupFlags, [Reception, Bmodule]}}.

%%SLOC:15
