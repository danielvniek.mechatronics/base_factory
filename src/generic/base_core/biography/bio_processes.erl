%% @author Daniel
%% @doc @todo Add description to bio_processes.


-module(bio_processes).
-include("../support/activity_records.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([update_s3_data/5,pass_to_aps/1]).
-record(bio_state, {status::ready|not_ready,
					default_storage_pid::pid(),storage_service_address::default|pid(),	%%storage related records
					coord_pid::pid(),	reception_map::term(),	instance_plugins::list(),										%%instance pids needed for functionality
					resource_id::string(),resource_type::string(),						%%id and type needed for storage services
					registered_analysis_plugins::map(),cloud_backup_plugins								%%bio shares all acts by type with registered analysis plugins				
					}).	


%% ====================================================================
%% Internal functions
%% ====================================================================
pass_to_aps({Acts,ApMap,MyRecep})->
	ActTypes = maps:keys(ApMap),
	case (length(ActTypes)>0) and (length(Acts)>0) of
		true->
			pass_to_aps(Acts,ApMap,ActTypes,MyRecep);
		_->
			ok
	end.
pass_to_aps(Acts,ApMap,ActTypes,MyRecep)->
	[ActTypeKey|T] = ActTypes,
	AP_Pid = maps:get(ActTypeKey,ApMap),
	NewActs = pass_to_ap(AP_Pid,ActTypeKey,Acts,[],Acts,MyRecep),
	case (length(NewActs)==0) or (length(T)==0) of
		true->
			ok;
		_->
			pass_to_aps(NewActs,ApMap,T,MyRecep)
	end.
pass_to_ap(AP_List,ActType,[],FoundActs,NewActs,MyRecep)->
	case (AP_List==[]) or (FoundActs==[]) of
		true->
			NewActs;
		_->
			[H|T] = AP_List,
			pass_to_each_ap(H,ActType,FoundActs,MyRecep),
			pass_to_ap(T,ActType,[],FoundActs,NewActs,MyRecep)
	end;
pass_to_ap(AP_List,ActType,[Act|T],FoundActs,NewActs,MyRecep)->
	case ActType of
		all->
			pass_to_ap(AP_List,ActType,[],[Act|T],NewActs,MyRecep);
		_->
			case Act#stage3Activity.type of
				ActType->
					%NewNewActs = lists:delete(Act, NewActs),%this was removed to accomodate the added ActType == all functionality
					pass_to_ap(AP_List,ActType,T,[Act|FoundActs],NewActs,MyRecep);
				_->
					pass_to_ap(AP_List,ActType,T,FoundActs,NewActs,MyRecep)
			end
	end.
pass_to_each_ap(AP_Pid,ActType,FoundActs,MyRecep)->
	Res = custom_erlang_functions:myGenServCall(AP_Pid,{new_bio_entries,FoundActs}),
	case Res of 
		ok->
			ok;
		_->
			error_log:log(?MODULE,0,unknown,"\nERROr:Ap did not respond or responded with wrong reply. Will kill ap so that its supervisor can restart it. Will delete ap's pid from bio_recep state"),
			%{ok,_DeregRes}=custom_erlang_functions:myGenServCall(MyRecep,{deregister_analysis_plugin,AP_Pid,ActType}),
			exit(AP_Pid,kill)		
	end.

update_s3_data(ActId,S3Data,BioRecep,State,From)->
	{ok,GetRes} = custom_erlang_functions:myGenServCall(BioRecep,{storage_request,{get_del_or_pop_bio_by_ids,"get",[ActId]}}),
	case length(GetRes) of
		0->
			Reply = {error,"No activities in bio with id: ~p",[ActId]},
			gen_server:reply(From, Reply);
		1->
			Act = lists:nth(1,GetRes),
			NewBioInfo = Act#stage3Activity.biography_info#biography_info{s3data = S3Data},
			NewAct = Act#stage3Activity{biography_info = NewBioInfo},
			
			update_act(NewAct,State,From,BioRecep);
		_->
			error_log:log(?MODULE,0,unknown,"\nERROR: There is an error in the code of the bio ets tables or the storage service being used because only one activity should have been returned"),
			Act = lists:nth(1,GetRes),
			NewBioInfo = Act#stage3Activity.biography_info#biography_info{s3data = S3Data},
			NewAct = Act#stage3Activity{biography_info = NewBioInfo},
			update_act(NewAct,State,From,BioRecep)
	end.
	

update_act(NewAct,State,From,MyPid)->
	PostRequestProcess = {bio_processes,pass_to_aps,{[NewAct],State#bio_state.registered_analysis_plugins,maps:get(bio,State#bio_state.reception_map)}},
	Call = storage_functions:add_id_and_type({save_bio_activities,[NewAct]},State#bio_state.resource_id,State#bio_state.resource_type),
	storage_functions:request(State#bio_state.storage_service_address, State#bio_state.default_storage_pid, Call, From, PostRequestProcess,{State#bio_state.cloud_backup_plugins,MyPid}).

%%SLOC:73