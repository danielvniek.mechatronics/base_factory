
-module(bio_recep).
-author("Daniel van Niekerk (concept by Dale Sparrow)").

-behaviour(gen_server).

%% ATTRIBUTES API

-export([start_link/2,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-include("../support/base_records.hrl").
-include("../support/activity_records.hrl").
-record(bio_state, {status::ready|not_ready,
					default_storage_pid::pid(),storage_service_address::default|pid(),	%%storage related records
					coord_pid::pid(),	reception_map::term(),	instance_plugins::list(),										%%instance pids needed for functionality
					resource_id::string(),resource_type::string(),						%%id and type needed for storage services
					registered_analysis_plugins::map(),cloud_backup_plugins							%%bio shares all acts by type with registered analysis plugins				
					}).								
%%%===================================================================

start_link(CoordPid,BC) ->
  {ok,BioPID} = gen_server:start_link(?MODULE, [CoordPid,BC], []),
  {ok,BioPID}.

stop() ->
  gen_server:stop(self()).

init([CoordPid,BC]) ->
  {ok,ID} = business_cards:get_id(BC),
  {ok,Type} = business_cards:get_type(BC),
  custom_erlang_functions:myGenServCall(CoordPid,{core_component_started,bio_recep}),
  {ok, #bio_state{status = not_ready,coord_pid = CoordPid,resource_id=ID,resource_type=Type,reception_map = #{},instance_plugins=[],
				  storage_service_address = default,registered_analysis_plugins=#{},cloud_backup_plugins=[]}}. 

%-----------------------------------------------------------------
%cloud backup plugins
handle_call({register_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#bio_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					NewCloudBackupPlugins = [PluginPid|CleanCloudBackupPlugins],
					{reply,ok,State#bio_state{cloud_backup_plugins=NewCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
handle_call({deregister_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#bio_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					{reply,ok,State#bio_state{cloud_backup_plugins=CleanCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
%Activity handling related calls
handle_call({update_s3_data,ActId,S3Data},From,State)->
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case io_lib:printable_list(ActId) of
				true->
					MyPid = self(),
					spawn_monitor(bio_processes,update_s3_data,[ActId,S3Data,MyPid,State,From]),
					{noreply,State};
				_->
					{reply,{error,"ActId must be a string"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({register_analysis_plugin,AP_Pid, ActType},From,State)->
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case (io_lib:printable_list(ActType) orelse(ActType==all)) and is_pid(AP_Pid) of
				true->
					{NewAP_Map,RegResult,_GateMap} = plugin_registration_functions:register(State#bio_state.registered_analysis_plugins,true,AP_Pid,ActType,none),
					case ActType of
						all->
							Request = {get_del_or_pop_all_bio,"get"};
						_->
							Request = {get_del_or_pop_bio_by_types, "get",[ActType]}
					end,
					Call = storage_functions:add_id_and_type(Request,State#bio_state.resource_id,State#bio_state.resource_type),
					case State#bio_state.storage_service_address of
						default->
							%%use default module
							{ok,OldActs}=custom_erlang_functions:myGenServCall(State#bio_state.default_storage_pid, Call);
						_->
							{ok,OldActs}=custom_erlang_functions:myGenServCall(State#bio_state.storage_service_address, Call)
					end,
					case RegResult of
						ok->
							Reply = {ok,OldActs};
						_->
							Reply = RegResult
					end,
					NewState = State#bio_state{registered_analysis_plugins = NewAP_Map};
				_->
					NewState = State,
					Reply = {error,"Invalid input arguments"}
			end,
			{reply,Reply,NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
			
handle_call({deregister_analysis_plugin,AP_Pid,ActType},From,State)->
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case (io_lib:printable_list(ActType) orelse (ActType==all)) and is_pid(AP_Pid) of
				true->
					{NewAPMap,Reply,_GateMap} = plugin_registration_functions:deregister(State#bio_state.registered_analysis_plugins, true, AP_Pid, ActType, none),
					NewState = State#bio_state{registered_analysis_plugins = NewAPMap};
				_->
					NewState = State,
					Reply = {error,"Invalid input arguments"}
			end,
			{reply,Reply,NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

%This call is used to change storage method of an instance. For example using some cloud storage service
handle_call({change_storage, NewStoragePid},From,State)->%%this call will come from a service_finder:start spawned process if more than one service is found
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case erlang:is_pid(NewStoragePid) of
				true->
					NewState = State#bio_state{storage_service_address = NewStoragePid},
					Reply = ok;
				_->
					NewState = State,
					Reply = {error, "Invalid NewStoragePid"}
			end,
			{reply, Reply, NewState};	
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({get_pending_s3data_acts,ActType},From,State)->
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#bio_state.status of
				ready->
					Call = {get_del_or_pop_bio_by_types, State#bio_state.resource_id, State#bio_state.resource_type, "get",[ActType]},
					case State#bio_state.storage_service_address of
						default->
							%%use default module
							{ok,Acts}=custom_erlang_functions:myGenServCall(State#bio_state.default_storage_pid, Call);
						_->%%TODO: if service does not respond use default like in storage functions
							{ok,Acts}=custom_erlang_functions:myGenServCall(State#bio_state.storage_service_address, Call)
					end,
					PendingActs = find_pending_acts(Acts,[]),
					{reply,{ok,PendingActs},State};
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
%Save, get, delete and pop requests all go through the next call:
handle_call({storage_request,Request},From,State)->
	FullPermittedList = lists:append(maps:values(State#bio_state.reception_map),State#bio_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) or true of
		true->
			case State#bio_state.status of
				ready->
					ValidReq = storage_functions:valid_bio_storage_request(Request),
					case ValidReq of
						true->
							Acts = storage_functions:get_content_if_save(Request),
							case Acts of
								false->
									PostRequestProcess = none;
								_->
									PostRequestProcess = {bio_processes,pass_to_aps,{Acts,State#bio_state.registered_analysis_plugins,self()}}	
							end,
							Call = storage_functions:add_id_and_type(Request,State#bio_state.resource_id,State#bio_state.resource_type),
							MyPid = self(),
							spawn_monitor(storage_functions,request,[State#bio_state.storage_service_address, State#bio_state.default_storage_pid, Call, From, PostRequestProcess,{State#bio_state.cloud_backup_plugins,MyPid}]),
							{noreply,State};
						_->
							{false,Reason} = ValidReq,
							Reply = {error,Reason},
							{reply,Reply,State}
					end;
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
handle_call({instance_pids, ReceptionMap, DefStorage},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_state.coord_pid, From) of
		true->
			NewState = State#bio_state{default_storage_pid = DefStorage,status = ready,reception_map = ReceptionMap},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;
handle_call({instance_plugins, InstancePlugins},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_state.coord_pid, From) of
		true->
			NewState = State#bio_state{instance_plugins = InstancePlugins},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;			

handle_call(_Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"Bio_recep:unknown request"),
  {reply, unknown, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

find_pending_acts([],PendingActs)->
	PendingActs;
find_pending_acts([Act|T],PendingActs)->
	case (Act#stage3Activity.biography_info#biography_info.s3data=="pending") or (Act#stage3Activity.biography_info#biography_info.s3data=="none") of
		true->
			find_pending_acts(T,[Act|PendingActs]);
		_->
			find_pending_acts(T,PendingActs)
	end.

%%SLOC:213
