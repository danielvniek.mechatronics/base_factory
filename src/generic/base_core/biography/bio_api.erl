%% @author Daniel
%% @doc @todo Add description to bio_api.

%%Note we do not give plugins the option to specify the timeout for requests. This is fixed to allow the core always has sufficient time to complete the requested action. Plugins do not know how long this is
%%also note: api's do not check for valid args, this is done in reception since reception should anyways check for if calls do  not come via api
-module(bio_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([update_s3_data/3,
		 register_analysis_plugin/3,deregister_analysis_plugin/3,
		 change_storage/2,
		 save/2,
		 get_all_acts/1,get_acts_by_ids/2,get_acts_by_types/2,get_acts_by_times/7,get_acts_by_types_and_times/8,get_pending_s3data_acts/2,
		 del_all_acts/1,del_acts_by_ids/2,del_acts_by_types/2,del_acts_by_times/7,del_acts_by_types_and_times/8,
		 pop_all_acts/1,pop_acts_by_ids/2,pop_acts_by_types/2,pop_acts_by_times/7,pop_acts_by_types_and_times/8]).

%% ====================================================================
%% Internal functions
%% ====================================================================
update_s3_data(ReceptionPid,Activity_ID,S3Data)->	
	custom_erlang_functions:myGenServCall(ReceptionPid, {update_s3_data,Activity_ID,S3Data}).
	
register_analysis_plugin(ReceptionPid,AP_Pid,ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {register_analysis_plugin,AP_Pid,ActType}).

deregister_analysis_plugin(ReceptionPid,AP_Pid,ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {deregister_analysis_plugin,AP_Pid,ActType}).

change_storage(ReceptionPid,NewStoragePid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {change_storage, NewStoragePid}).	

save(ReceptionPid,Activities)->
	case is_list(Activities) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{save_bio_activities,Activities}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{save_bio_activities,[Activities]}})
	end.
	
%--------------------GET--------------------%
get_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_bio,"get"}}).
	

get_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_ids,"get",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_ids,"get",[IDs]}})
	end.
	

get_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types,"get",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types,"get",[Types]}})
	end.
	
get_acts_by_times(ReceptionPid,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_times,"get",TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}}).
	
get_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types_and_times,"get",Types,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types_and_times,"get",[Types],TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}})
	end.

get_pending_s3data_acts(ReceptionPid, ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {get_pending_s3data_acts,ActType}).
%------------------DELETE---------------------%
del_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_bio,"del"}}).
	

del_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_ids,"del",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_ids,"del",[IDs]}})
	end.
	

del_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types,"del",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types,"del",[Types]}})
	end.
	
del_acts_by_times(ReceptionPid,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_times,"del",TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}}).
	
del_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types_and_times,"del",Types,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types_and_times,"del",[Types],TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}})
	end.
%------------------POP----------------------%
pop_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_bio,"pop"}}).
	

pop_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_ids,"pop",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_ids,"pop",[IDs]}})
	end.
	

pop_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types,"pop",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types,"pop",[Types]}})
	end.
	
pop_acts_by_times(ReceptionPid,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_times,"pop",TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}}).
	
pop_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types_and_times,"pop",Types,TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_bio_by_types_and_times,"pop",[Types],TschedLow,TschedHigh,TstartLow,TstartHigh,TendLow,TendHigh}})
	end.

%%SLOC:102