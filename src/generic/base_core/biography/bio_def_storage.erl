
-module(bio_def_storage).
-author("Daniel van Niekerk (adapted from original by Dale Sparrow)").

-behaviour(gen_server).
-include("../support/base_records.hrl").
-include("../support/activity_records.hrl").
-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([start_link/2]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(bio_def_storage_state, {holon_name,holon_type,file_path = "",ets_table::term(),
		coord_pid::pid(),my_recep::pid()}).

%%%===================================================================

start_link(CoordPid,BC) ->
  {ok,_PID} = gen_server:start_link(?MODULE, [CoordPid,BC], []).

%%%===================================================================
%%% gen_server callbacks

init([CoordPid,BC]) ->
  {ok,ID} = business_cards:get_id(BC),
  {ok,Type} = business_cards:get_type(BC),
  {ETS_Table,FilePath} = def_storage_functions:create_or_load_table(ID,Type,"bio"),
  custom_erlang_functions:myGenServCall(CoordPid,{core_component_started,bio_def_storage}),
  {ok, #bio_def_storage_state{holon_name = ID,holon_type = Type,coord_pid = CoordPid,ets_table = ETS_Table,file_path = FilePath}}.
%%--------------------------------------------------------------------

handle_call({save_bio_activities, _ID, _HolonType, Acts}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_def_storage_state.my_recep, From) of
		true->
			Reply = def_storage_functions:save(State#bio_def_storage_state.ets_table,Acts,State#bio_def_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"},State}
	end;
handle_call({get_del_or_pop_all_bio, _ID, _HolonType,GetDelOrPop}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_def_storage_state.my_recep, From) of
		true->
			MatchSpecTuple = {bio_match_spec,get_matchAll,del_matchAll},
			Reply = def_storage_functions:get_del_or_pop(State#bio_def_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,none,[],State#bio_def_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"},State}
	end;
handle_call({get_del_or_pop_bio_by_ids, _ID, _HolonType, GetDelOrPop,IDs}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_def_storage_state.my_recep, From) of
		true->
			MatchSpecTuple = {bio_match_spec,get_match10IDs,del_match10IDs},
			Reply = def_storage_functions:get_del_or_pop(State#bio_def_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,IDs,[],State#bio_def_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"},State}
	end;
handle_call({get_del_or_pop_bio_by_types, _ID, _HolonType, GetDelOrPop,Types}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_def_storage_state.my_recep, From) of
		true->
			MatchSpecTuple = {bio_match_spec,get_match10Types,del_match10Types},
			Reply = def_storage_functions:get_del_or_pop(State#bio_def_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,Types,[],State#bio_def_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"},State}
	end;
handle_call({get_del_or_pop_bio_by_times, _ID, _HolonType, GetDelOrPop,TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_def_storage_state.my_recep, From) of
		true->
			Content = {TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh},
			MatchSpecTuple = {bio_match_spec,get_matchTimes,del_matchTimes},
			Reply = def_storage_functions:get_del_or_pop(State#bio_def_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,Content,[],State#bio_def_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"},State}
	end;
handle_call({get_del_or_pop_bio_by_types_and_times, _ID, _HolonType, GetDelOrPop,Types,TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_def_storage_state.my_recep, From) of
		true->
			Content = {Types,TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh},
			MatchSpecTuple = {bio_match_spec,get_match10TypesWithTimes,del_match10TypesWithTimes},
			Reply = def_storage_functions:get_del_or_pop(State#bio_def_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,Content,[],State#bio_def_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"},State}
	end;

handle_call({reception_pid,MyRecep},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#bio_def_storage_state.coord_pid, From) of
		true->
			NewState = State#bio_def_storage_state{my_recep = MyRecep},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send reception_pid"},State}
	end;

handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Bio def storage got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

