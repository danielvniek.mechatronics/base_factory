
-module(exe_activity_handler).
-author("Daniel van Niekerk (adapted from original by Dale Sparrow)").

-behaviour(gen_server).
-include("../support/activity_records.hrl").

%% API
-export([start/3]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(act_handler_state, {s2_act,ep_pid,exe_recep,ep_ref}).

%%%===================================================================

start(S2Act,EP_Pid,ExeRecep) ->
  {ok,_PID} = gen_server:start(?MODULE, [S2Act,EP_Pid,ExeRecep], []).

%%%===================================================================
%%% gen_server callbacks

init([S2Act,EP_Pid,ExeRecep]) ->
  MonitorRef = monitor(process,EP_Pid),
  {ok, #act_handler_state{s2_act = S2Act,ep_pid=EP_Pid,exe_recep = ExeRecep,ep_ref = MonitorRef}}.
%%--------------------------------------------------------------------
handle_call({update_progress,NewS2DataOrFunction},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#act_handler_state.ep_pid, From) of
		true->
			S2Act = State#act_handler_state.s2_act,
			S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
			case is_function(NewS2DataOrFunction) of
				true->
					try
						NewS2Data = NewS2DataOrFunction(S2Data),
						NewExeInfo = S2Act#stage2Activity.execution_info#execution_info{s2data = NewS2Data},
						NewAct = S2Act#stage2Activity{execution_info = NewExeInfo},
						{ok,"success"}=custom_erlang_functions:myGenServCall(State#act_handler_state.exe_recep,{storage_request,{save_exe_activities,[NewAct]}}),
						{reply,ok,State#act_handler_state{s2_act = NewAct}}
					catch
						A:B:StackTrace->
							Caught = lists:flatten(io_lib:format("~p:~p:~p", [A,B,StackTrace])),
							Reply = {error,"Could not update progress with the given function. Caught "++Caught},
							{reply,Reply,State}
					end;
				_->
					NewS2Data = NewS2DataOrFunction,
					NewExeInfo = S2Act#stage2Activity.execution_info#execution_info{s2data = NewS2Data},
					NewAct = S2Act#stage2Activity{execution_info = NewExeInfo},
					{ok,"success"}=custom_erlang_functions:myGenServCall(State#act_handler_state.exe_recep,{storage_request,{save_exe_activities,[NewAct]}}),
					{reply,ok,State#act_handler_state{s2_act = NewAct}}
			end;
		_->
			{reply,{error,"Only the ep process that started the act handler and its spawned processes can communicate with it"},State}
	end;
handle_call(get_s2_act,From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#act_handler_state.ep_pid, From) of
		true->
			{reply,{ok,State#act_handler_state.s2_act},State};
		_->
			{reply,{error,"Only the ep process that started the act handler and its spawned processes can communicate with it"},State}
	end;
handle_call({done,FinalS2DataIfChanged},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#act_handler_state.ep_pid, From) of
		true->
			S2Act = State#act_handler_state.s2_act,
			case (FinalS2DataIfChanged==[]) or (FinalS2DataIfChanged==#{}) of
				true->
					CompletedS2Act = S2Act;
				_->
					NewExeInfo = S2Act#stage2Activity.execution_info#execution_info{s2data = FinalS2DataIfChanged},
					CompletedS2Act = S2Act#stage2Activity{execution_info = NewExeInfo},
					{ok,"success"}=custom_erlang_functions:myGenServCall(State#act_handler_state.exe_recep,{storage_request,{save_exe_activities,[CompletedS2Act]}})%%this is done to ensure the latest version of the activity is in execution before trying to pass it along to rps and bio
			end,
			ok = custom_erlang_functions:myGenServCall(State#act_handler_state.exe_recep,{finish_s2_act,CompletedS2Act}),
			MyPid = self(),
			erlang:demonitor(State#act_handler_state.ep_ref),
			spawn(fun()->ok = gen_server:stop(MyPid)end),
			{reply,ok,State#act_handler_state{s2_act = CompletedS2Act}};
			
		_->
			{reply,{error,"Only the ep process that started the act handler and its spawned processes can communicate with it"},State}
	end;
handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.
handle_info({'DOWN', MonitorRef, process, _Object, _Info},State)->
	case MonitorRef==State#act_handler_state.ep_ref of
		true->
			MyPid = self(),
			error_log:log(?MODULE,0,unknown,"ERROR: The execution plugin with pid: ~p's failed. Activity handler terminating",[State#act_handler_state.ep_pid]),
			spawn(fun()->gen_server:stop(MyPid)end),
			{noreply,State};
		_->
			{noreply,State}
	end;
handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Act handler got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%%SLOC:82

