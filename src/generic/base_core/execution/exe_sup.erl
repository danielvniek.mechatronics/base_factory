
-module(exe_sup).
-author("Dale Sparrow (small changes by Daniel van Niekerk)").

-behaviour(supervisor).

%% API
-export([start_link/2]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

start_link(CoordPid,BC) ->
  supervisor:start_link(?MODULE, [CoordPid,BC]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([CoordPid,BC]) ->
  RestartStrategy = one_for_one,
  MaxRestarts = 10,
  MaxSecondsBetweenRestarts = 3,

  SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

  Restart = permanent,
  Shutdown = 2000,
  Type = worker,

  ReceptionID = exe_recep,
  RecepMFargs = {exe_recep, start_link, [CoordPid,BC]},
  Reception = {ReceptionID, RecepMFargs, Restart, Shutdown, Type, [exe_recep]},

  EmoduleID = exe_def_storage,
  EmoduleMFargs = {exe_def_storage, start_link, [CoordPid,BC]},
  Emodule = {EmoduleID, EmoduleMFargs, Restart, Shutdown, Type, [exe_def_storage]},

  {ok, {SupFlags, [Reception, Emodule]}}.

%%SLOC:17
