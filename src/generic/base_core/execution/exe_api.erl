%% @author Daniel
%% @doc @todo Add description to exe_api.

%%Note we do not give plugins the option to specify the timeout for requests. This is fixed to allow the core always has sufficient time to complete the requested action. Plugins do not know how long this is
%%also note: api's do not check for valid args, this is done in reception since reception should anyways check for if calls do  not come via api
-module(exe_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([start_and_finish_new_act/4,start_and_finish_new_act/6,
		 start_act_handler/3,finish_s2_act/2,
		 register_reflection_plugin/3,deregister_reflection_plugin/3,
		 change_storage/2,
		 save/2,
		 get_all_acts/1,get_acts_by_ids/2,get_acts_by_types/2,get_acts_by_times/5,get_acts_by_types_and_times/6,
		 del_all_acts/1,del_acts_by_ids/2,del_acts_by_types/2,del_acts_by_times/5,del_acts_by_types_and_times/6,
		 pop_all_acts/1,pop_acts_by_ids/2,pop_acts_by_types/2,pop_acts_by_times/5,pop_acts_by_types_and_times/6]).



%% ====================================================================
%% Internal functions
%% ====================================================================
start_and_finish_new_act(ReceptionPid,ActType,S1Data,S2Data)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {start_and_finish_new_act,ActType,S1Data,S2Data,base_time:now(),base_time:now()}).
start_and_finish_new_act(ReceptionPid,ActType,S1Data,S2Data,Tsched,Tstart)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {start_and_finish_new_act,ActType,S1Data,S2Data,Tsched,Tstart}).
start_act_handler(ReceptionPid,S2Act,EP_Pid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {start_act_handler,S2Act,EP_Pid}).
finish_s2_act(ReceptionPid,S2Act)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {finish_s2_act,S2Act}).
register_reflection_plugin(ReceptionPid,RP_Pid,ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {register_reflection_plugin,RP_Pid,ActType}).

deregister_reflection_plugin(ReceptionPid,RP_Pid,ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {deregister_reflection_plugin,RP_Pid,ActType}).

change_storage(ReceptionPid,NewStoragePid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {change_storage, NewStoragePid}).

save(ReceptionPid,Activities)->
	case is_list(Activities) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{save_exe_activities,Activities}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{save_exe_activities,[Activities]}})
	end.
%--------------------GET--------------------%
get_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_exe,"get"}}).

get_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_ids,"get",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_ids,"get",[IDs]}})
	end.

get_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types,"get",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types,"get",[Types]}})
	end.

get_acts_by_times(ReceptionPid,TschedLow,TschedHigh,TstartLow,TstartHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_times,"get",TschedLow,TschedHigh,TstartLow,TstartHigh}}).

get_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh,TstartLow,TstartHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types_and_times,"get",Types,TschedLow,TschedHigh,TstartLow,TstartHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types_and_times,"get",[Types],TschedLow,TschedHigh,TstartLow,TstartHigh}})
	end.
%------------------DELETE---------------------%
del_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_exe,"del"}}).

del_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_ids,"del",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_ids,"del",[IDs]}})
	end.

del_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types,"del",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types,"del",[Types]}})
	end.

del_acts_by_times(ReceptionPid,TschedLow,TschedHigh,TstartLow,TstartHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_times,"del",TschedLow,TschedHigh,TstartLow,TstartHigh}}).

del_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh,TstartLow,TstartHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types_and_times,"del",Types,TschedLow,TschedHigh,TstartLow,TstartHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types_and_times,"del",[Types],TschedLow,TschedHigh,TstartLow,TstartHigh}})
	end.
%------------------POP----------------------%
pop_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_exe,"pop"}}).

pop_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_ids,"pop",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_ids,"pop",[IDs]}})
	end.

pop_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types,"pop",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types,"pop",[Types]}})
	end.

pop_acts_by_times(ReceptionPid,TschedLow,TschedHigh,TstartLow,TstartHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_times,"pop",TschedLow,TschedHigh,TstartLow,TstartHigh}}).

pop_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh,TstartLow,TstartHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types_and_times,"pop",Types,TschedLow,TschedHigh,TstartLow,TstartHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_exe_by_types_and_times,"pop",[Types],TschedLow,TschedHigh,TstartLow,TstartHigh}})
	end.
%%SLOC:96
