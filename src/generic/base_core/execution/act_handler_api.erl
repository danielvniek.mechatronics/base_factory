%% @author Daniel
%% @doc @todo Add description to act_handler_api.


-module(act_handler_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([update_progress/2,get_s2_act/1,done/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
update_progress(ActHandler,NewS2DataOrFunction)->
	custom_erlang_functions:myGenServCall(ActHandler, {update_progress,NewS2DataOrFunction}).

get_s2_act(ActHandler)->
	custom_erlang_functions:myGenServCall(ActHandler, get_s2_act).

done(ActHandler,FinalS2DataIfChanged)->
	custom_erlang_functions:myGenServCall(ActHandler, {done,FinalS2DataIfChanged}).

%%SLOC:6