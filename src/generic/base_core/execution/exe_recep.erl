-module(exe_recep).
-author("Daniel van Niekerk (concept by Dale Sparrow)").

-behaviour(gen_server).

-export([start_link/2,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-include("../support/base_records.hrl").
-include("../support/activity_records.hrl").
-record(exe_state, {status::ready|not_ready,
					default_storage_pid::pid(),storage_service_address::default|pid(),	%%storage related records
					coord_pid::pid(),reception_map::term(),	instance_plugins::list(),				%%instance pids needed for functionality
					resource_id::string(),resource_type::string(),						%%id and type needed for storage services
					activity_handlers::map(),											%%activity_handlers are processes responsible for one activity each. They get messages from execution plugins to update data and finish activities
					act_handler_monitors::map(),
					last_time_stamp,
					registered_reflection_plugins::map(),cloud_backup_plugins}).								%%when exeucution gets call from activity handler to give_away_s3_act it first checks is a reflection plugins registered itself to receive acts of this type and then shares the act with that rp.
																						% The activity is still sent to bio and rp can update s3 data in bio.

%%%===================================================================

start_link(CoordPid,BC) ->
  {ok,ExePID} = gen_server:start_link(?MODULE, [CoordPid,BC], []),
  {ok,ExePID}.

stop() ->
  gen_server:stop(self()).

init([CoordPid,BC]) ->
  {ok,ID} = business_cards:get_id(BC),
  {ok,Type} = business_cards:get_type(BC),
  custom_erlang_functions:myGenServCall(CoordPid,{core_component_started,exe_recep}),
  {ok, #exe_state{status=not_ready,coord_pid = CoordPid,reception_map = #{},instance_plugins=[],last_time_stamp = {"",0,0},
				  resource_id=ID,resource_type=Type,storage_service_address = default,activity_handlers=#{},registered_reflection_plugins=#{},act_handler_monitors=#{},cloud_backup_plugins=[]}}. 

%-----------------------------------------------------------------
handle_call({register_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#exe_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					NewCloudBackupPlugins = [PluginPid|CleanCloudBackupPlugins],
					{reply,ok,State#exe_state{cloud_backup_plugins=NewCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
handle_call({deregister_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#exe_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					{reply,ok,State#exe_state{cloud_backup_plugins=CleanCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
%Activity handling related calls

handle_call({start_act_handler,S2Act,EP_Pid},From,State)->%%can come from plugin or from schedule process
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#exe_state.status of
				ready->
					case activity_functions:is_stage2_act(S2Act) of
						true->
							case is_pid(EP_Pid) of
								true->
									{ok,AH_Pid} = exe_activity_handler:start(S2Act, EP_Pid, self()),
									MonitorRef = erlang:monitor(process, AH_Pid),
									ActId = S2Act#stage2Activity.id,
									NewActHandMon = maps:put(ActId, MonitorRef, State#exe_state.act_handler_monitors),
									case maps:is_key(ActId, State#exe_state.activity_handlers) of
										true->
											
											exit(maps:get(ActId,State#exe_state.activity_handlers),kill),
											error_log:log(?MODULE,0,unknown,"\nERROR: Act handler duplicate found. gonna kill~p and I am ~p",[maps:get(ActId,State#exe_state.activity_handlers),self()]);
										_->
											ok
									end,
									New_AH_Map = maps:put(ActId, AH_Pid, State#exe_state.activity_handlers),
									NewState = State#exe_state{activity_handlers=New_AH_Map,act_handler_monitors = NewActHandMon},
									{reply,{ok,AH_Pid},NewState};
								_->
									{reply,{error,"EP_Pid must be a valid pid"},State}
							end;
						_->
							{reply,{error,"S2Act is not a valid stage 2 activity"},State}
					end;
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
			

handle_call({finish_s2_act,S2Act},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#exe_state.status of
				ready->
					case activity_functions:is_stage2_act(S2Act) of
						true->
							ActType = S2Act#stage2Activity.type,
							BioRecep = maps:get(bio,State#exe_state.reception_map),
							ActId = S2Act#stage2Activity.id,
							MyPid = self(),
							case maps:is_key(ActType, State#exe_state.registered_reflection_plugins) of
								true->
									RPinfo = maps:get(ActType,State#exe_state.registered_reflection_plugins),
									
									spawn_monitor(exe_processes,give_to_bio_and_rp,[BioRecep,RPinfo,S2Act,MyPid]);		
								_->
									spawn_monitor(exe_processes,give_to_bio_and_rp,[BioRecep,none,S2Act,MyPid])
							end,
							case maps:is_key(ActId, State#exe_state.activity_handlers) of
								true->
									MonitorRef = maps:get(ActId,State#exe_state.act_handler_monitors),
									demonitor(MonitorRef),
									NewActHandMonitors = maps:remove(ActId, State#exe_state.act_handler_monitors),
									NewActHandlers=maps:remove(ActId, State#exe_state.activity_handlers),
									NewState = State#exe_state{activity_handlers=NewActHandlers,act_handler_monitors=NewActHandMonitors};
								_->
									%error_log:log(?MODULE,0,unknown,"\nA give_away request was received for an activity with no activity handler"),
									NewState = State
							end,
							Reply = ok;
						_->
							Reply = {error, "Invalid S3Act"},
							NewState = State
					end,
					{reply,Reply,NewState};
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
handle_call({register_reflection_plugin,RP_Pid,ActType},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case io_lib:printable_list(ActType) and is_pid(RP_Pid) of
				true->
					BioRecep = maps:get(bio,State#exe_state.reception_map),
					
					{NewRP_Map,RegResult,_GateMap} = plugin_registration_functions:register(State#exe_state.registered_reflection_plugins,false,RP_Pid,ActType,none),
					NewState = State#exe_state{registered_reflection_plugins = NewRP_Map},
					case RegResult of
						ok->
							{reply,bio_api:get_pending_s3data_acts(BioRecep, ActType),NewState};
						_->
							{reply,RegResult,State}
					end;
				_->
					Reply = {error,"Invalid input arguments"},
					{reply,Reply,State}
			end;
			
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
handle_call({deregister_reflection_plugin,RP_Pid,ActType},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case io_lib:printable_list(ActType) and is_pid(RP_Pid) of
				true->
					
					{NewRPMap,Reply,_GateMap} = plugin_registration_functions:deregister(State#exe_state.registered_reflection_plugins, false, RP_Pid, ActType, none),
					NewState = State#exe_state{registered_reflection_plugins = NewRPMap};
				_->
					NewState = State,
					Reply = {error,"Invalid input arguments"}
			end,
			{reply,Reply,NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

%This call is used to change storage method of an instance. For example using some cloud storage service
handle_call({change_storage, NewStoragePid},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case erlang:is_pid(NewStoragePid) of
				true->
					NewState = State#exe_state{storage_service_address = NewStoragePid},
					Reply = ok;
				_->
					NewState = State,
					Reply = {error, "Invalid NewStoragePid"}
			end,
			{reply, Reply, NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
%%for activities that do not need to be scheduled and should start and finish at the same time. For example new sensor data being saved in a new activity.
handle_call({start_and_finish_new_act,ActType,S1Data,S2Data,Tsched,Tstart},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case custom_erlang_functions:is_string(ActType) and is_integer(Tsched) and is_integer(Tstart) of
				true->
					{PrevType,Time,Counter} = State#exe_state.last_time_stamp,
					TimeNow = base_time:now(),
					ActIdRaw = string:concat(string:concat(ActType, "_exe_"),integer_to_list(TimeNow)),
					case (PrevType==ActType) and (Time==TimeNow) of
						true->
							Copy = string:concat(string:concat("(",integer_to_list(Counter+1)),")"),
							ActId = string:concat(ActIdRaw,Copy),
							NewTimeStamp = {ActType,TimeNow,Counter+1};
						_->
							ActId = ActIdRaw,
							NewTimeStamp = {ActType,TimeNow,0}
					end,
					{ok,S2Act} = activity_functions:makeStage2Activity(ActId, ActType, Tsched, Tstart, S1Data, S2Data),
					BioRecep = maps:get(bio,State#exe_state.reception_map),
					MyPid = self(),
					case maps:is_key(ActType, State#exe_state.registered_reflection_plugins) of
						true->
							RPinfo = maps:get(ActType,State#exe_state.registered_reflection_plugins),
							spawn_monitor(exe_processes,give_to_bio_and_rp,[BioRecep,RPinfo,S2Act,MyPid]);		
						_->
							spawn_monitor(exe_processes,give_to_bio_and_rp,[BioRecep,none,S2Act,MyPid])
					end,
					{reply,ok,State#exe_state{last_time_stamp = NewTimeStamp}};
				_->
					{reply,{error,"ActType must be a string and tsched and tstart must be integers"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
%Save, get, delete and pop requests all go through the next call:
handle_call({storage_request,Request},From,State)->
	FullPermittedList = lists:append(maps:values(State#exe_state.reception_map),State#exe_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#exe_state.status of
				ready->
					ValidReq = storage_functions:valid_exe_storage_request(Request),
					case ValidReq of
						true->
							PostRequestProcess = none,
							Call = storage_functions:add_id_and_type(Request,State#exe_state.resource_id,State#exe_state.resource_type),
							MyPid = self(),
							spawn_monitor(storage_functions,request,[State#exe_state.storage_service_address, State#exe_state.default_storage_pid, Call, From, PostRequestProcess,{State#exe_state.cloud_backup_plugins,MyPid}]),
							{noreply,State};
						_->
							{false,Reason} = ValidReq,
							Reply = {error,Reason},
							{reply,Reply,State}
					end;
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({instance_pids, ReceptionMap, DefStorage},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#exe_state.coord_pid, From) of
		true->
			NewState = State#exe_state{default_storage_pid = DefStorage,status = ready,reception_map = ReceptionMap},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;
handle_call({instance_plugins, InstancePlugins},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#exe_state.coord_pid, From) of
		true->
			NewState = State#exe_state{instance_plugins = InstancePlugins},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;

handle_call(_Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"exe_recep:unknown request"),
  {reply, unknown, State}.

handle_cast(_Request, State) ->
  {noreply, State}.
handle_info({'DOWN', MonitorRef, process, _Object, _Info},State)->
	AllMonitorRefs = maps:values(State#exe_state.act_handler_monitors),
	case lists:member(MonitorRef, AllMonitorRefs) of
		true->
			
			error_log:log(?MODULE,0,unknown,"ERROR:One of the execution reception's activity handlers failed"),%%TODO: check which process and do something about it (maybe not necessary. This should just indicate there is something wrong in code)
			ActId = find_key(MonitorRef,maps:keys(State#exe_state.act_handler_monitors),State#exe_state.act_handler_monitors),
			NewMonitors = maps:remove(ActId,State#exe_state.act_handler_monitors),
			NewActHandlers = maps:remove(ActId, State#exe_state.activity_handlers),
			demonitor(MonitorRef),
			NewState = State#exe_state{activity_handlers = NewActHandlers,act_handler_monitors = NewMonitors},
			{noreply,NewState};
		_->
			{noreply,State}
	end;
			
handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

find_key(_Value,[],_Map)->
	error_log:log(?MODULE,0,unknown,"ERROR: no key with matching value");
find_key(Value,[Key|T],Map)->
	Val = maps:get(Key,Map),
	case Val==Value of 
		true->
			Key;
		_->
			find_key(Value,T,Map)
	end.

%%SLOC:286

