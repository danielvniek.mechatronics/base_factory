%% @author Daniel
%% @doc @todo Add description to activity_handlers.


-module(exe_processes).
-include("../support/activity_records.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([give_to_bio_and_rp/4]).

%% ====================================================================
%% Internal functions
%% ====================================================================
	

give_to_bio_and_rp(BioRecep,RP_Pid,S2Act,MyRecep)->%%this process pops act from execution storage and saves in bio
	case RP_Pid of 
		none->
			{ok,S3Act} = activity_functions:promoteStage2(S2Act, base_time:now(), "none"),
			{ok,"success"} = custom_erlang_functions:myGenServCall(BioRecep,{storage_request,{save_bio_activities,[S3Act]}});
		_->
			{ok,S3Act} = activity_functions:promoteStage2(S2Act, base_time:now(), "pending"),
			{ok,"success"} = custom_erlang_functions:myGenServCall(BioRecep,{storage_request,{save_bio_activities,[S3Act]}}),
			Res = custom_erlang_functions:myGenServCall(RP_Pid,{reflect_on_act,S3Act}),
			case Res of 
				ok->
					ok;
				Reply->
					error_log:log(?MODULE,0,unknown,"\nERROR:Rp did not respond or responded with wrong reply: ~p. Will kill rp so that its supervisor can restart it. Will delete rp's pid from exe_recep state",[Reply]),
					ActType =S3Act#stage3Activity.type,
					custom_erlang_functions:myGenServCall(MyRecep,{deregister_reflection_plugin,RP_Pid,ActType}),
					exit(RP_Pid,kill)		
			end
	end,
	S3ActId = S3Act#stage3Activity.id,
	{ok,_DelRes} = custom_erlang_functions:myGenServCall(MyRecep,{storage_request,{get_del_or_pop_exe_by_ids,"del",[S3ActId]}}).%%activity is first sent to bio before deleting in execution
%%SLOC:21