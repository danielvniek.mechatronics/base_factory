%%%-------------------------------------------------------------------
%%% @author Dale Sparrow
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. Jan 2019 12:15 PM
%%%-------------------------------------------------------------------
-module(sched_sup).
-author("Dale Sparrow (Daniel van Niekerk made small changes from original)").

-behaviour(supervisor).

%% API
-export([start_link/2]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

start_link(CoordPid,BC) ->
  supervisor:start_link(?MODULE, [CoordPid,BC]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([CoordPid,BC]) ->
  RestartStrategy = one_for_one,
  MaxRestarts = 10,
  MaxSecondsBetweenRestarts = 3,

  SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

  Restart = permanent,
  Shutdown = 2000,
  Type = worker,

  ReceptionID = sched_recep,
  RecepMFargs = {sched_recep, start_link, [CoordPid,BC]},
  Reception = {ReceptionID, RecepMFargs, Restart, Shutdown, Type, [sched_recep]},

  SmoduleID = sched_def_storae,
  SmoduleMFargs = {sched_def_storage, start_link, [CoordPid,BC]},
  Smodule = {SmoduleID, SmoduleMFargs, Restart, Shutdown, Type, [sched_def_storage]},

  {ok, {SupFlags, [Reception, Smodule]}}.
%%SLOC:17
