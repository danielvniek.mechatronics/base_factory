
-module(sched_gate).
-author("Daniel van Niekerk (adapted from original by Dale Sparrow)").

-behaviour(gen_server).
-include("../support/activity_records.hrl").

%% API
-export([start_link/5]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(gate_state, {ep_pid,act_type,time_before_sched,ordered_time_list,sched_recep,ep_ref,timer_pid,last_acts}).

%%%===================================================================

start_link(EP_Pid,ActType,TimeBeforeSched,OrderedTimeList,SchedRecep) ->
  {ok,_PID} = gen_server:start_link(?MODULE, [EP_Pid,ActType,TimeBeforeSched,OrderedTimeList,SchedRecep], []).

%%%===================================================================
%%% gen_server callbacks

init([EP_Pid,ActType,TimeBeforeSched,OrderedTimeList,SchedRecep]) ->
	case length(OrderedTimeList)>0 of
		true->
			TimeTillPop = lists:last(OrderedTimeList)-base_time:now()-TimeBeforeSched,
			MyPid = self(),
			case (TimeTillPop>0) of
				true->
					TimerPid = spawn(fun()->timer:sleep(TimeTillPop*1000),custom_erlang_functions:myGenServCall(MyPid,pop_last) end);
				_->
					TimerPid = spawn(fun()->custom_erlang_functions:myGenServCall(MyPid,pop_last) end)
			end;
		_->
			TimerPid = none,
			ok
	end,
  MonitorRef = monitor(process,EP_Pid),
  {ok, #gate_state{timer_pid = TimerPid,ep_pid = EP_Pid,act_type=ActType,time_before_sched=TimeBeforeSched,ordered_time_list=OrderedTimeList,sched_recep=SchedRecep,ep_ref=MonitorRef,last_acts=[]}}.
%%--------------------------------------------------------------------
handle_call({new_act_times,Scheds},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#gate_state.sched_recep, From) of
		true->
			L = new_ordered_list(State#gate_state.ordered_time_list,Scheds),
			OL = lists:sort(L),
			NewTimeList = lists:reverse(OL),
			case length(NewTimeList)>0 of
				true->
					case State#gate_state.timer_pid of%%kill old timer before starting new one
						none->
							ok;
						_->
							exit(State#gate_state.timer_pid,kill)
					end,
					TimeTillPop = lists:last(NewTimeList)-base_time:now()-State#gate_state.time_before_sched,
					MyPid = self(),
					case (TimeTillPop>0) of
						true->
							TimerPid = spawn(fun()->timer:sleep(TimeTillPop*1000),custom_erlang_functions:myGenServCall(MyPid,pop_last) end);
						_->
							TimerPid = spawn(fun()->custom_erlang_functions:myGenServCall(MyPid,pop_last) end)
					end,
					{reply,ok,State#gate_state{ordered_time_list = NewTimeList,timer_pid = TimerPid}};
				_->
					{reply,ok,State}
			end;
		_->
			{reply,{error,"Only my sched reception's spawned process can make this call"},State}
	end;
handle_call(pop_last,From,State)->
	{Pid,_Ref} = From,
	case Pid == State#gate_state.timer_pid of
		true->
			TschedToGet = lists:last(State#gate_state.ordered_time_list)+State#gate_state.time_before_sched,
			SchedRecep = State#gate_state.sched_recep,
			ActType = State#gate_state.act_type,
			{ok,AllActs} = custom_erlang_functions:myGenServCall(SchedRecep,{storage_request,{get_del_or_pop_sched_by_types_and_times,"get",[ActType],0,TschedToGet}}),
			
			case AllActs of
				[]->
					error_log:log(?MODULE,0,unknown,"\nGate: No acts to pop on predicted time. Act must have been deleted"),
					{reply,ok,State};
				_->
					Acts = delete_last_acts_if_any(State#gate_state.last_acts,AllActs),
					EP_Pid = State#gate_state.ep_pid,
					Res = custom_erlang_functions:myGenServCall(EP_Pid,{acts_ready,Acts}),
					case Res of
						ok->
							NewTimeList = lists:droplast(State#gate_state.ordered_time_list),
							case length(NewTimeList)>0 of
								true->
									TimeTillPop = lists:last(NewTimeList)-base_time:now()-State#gate_state.time_before_sched,
									MyPid = self(),
									case (TimeTillPop>0) of
										true->
											TimerPid = spawn(fun()->timer:sleep(TimeTillPop*1000),custom_erlang_functions:myGenServCall(MyPid,pop_last) end);
										_->
											TimerPid = spawn(fun()->custom_erlang_functions:myGenServCall(MyPid,pop_last) end)
									end,
									{reply,ok,State#gate_state{ordered_time_list = NewTimeList,timer_pid = TimerPid,last_acts=AllActs}};
								_->
									{reply,ok,State#gate_state{ordered_time_list = [],timer_pid = none,last_acts=AllActs}}
							end;
						_->%%if EP does not respond, terminate this process
							ok=custom_erlang_functions:myGenServCall(SchedRecep,{deregister_execution_plugin,EP_Pid,ActType}),%%no need to terminate this server, the deregistration process will do it
							exit(EP_Pid,kill),
							{reply,ok,State}
					end
			end;
		_->
			{reply,{error,"Only my timer process can make this call"},State}
	end;
handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.
handle_info({'DOWN', MonitorRef, process, _Object, _Info},State)->
	case MonitorRef==State#gate_state.ep_ref of
		true->
			MyPid = self(),
			error_log:log(?MODULE,0,unknown,"ERROR: EP with pid ~p failed. Sched gate terminating",[State#gate_state.ep_pid]),
			spawn(fun()->gen_server:stop(MyPid)end),
			{noreply,State};
		_->
			{noreply,State}
	end;
handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Gate got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

new_ordered_list(OrdList,[])->
	OrdList;
new_ordered_list(OrdList,[Sched|T])->
	case lists:member(Sched, OrdList) of
		true->
			new_ordered_list(OrdList,T);
		_->
			new_ordered_list([Sched|OrdList],T)
	end.

delete_last_acts_if_any([],AllActs)->
	AllActs;
delete_last_acts_if_any([Act|T],AllActs)->
	NewAllActs = lists:delete(Act, AllActs),
	delete_last_acts_if_any(T,NewAllActs).
%%SLOC:125	
