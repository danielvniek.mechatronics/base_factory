%% @author Daniel
%% @doc @todo Add description to sched_gate_functions.


-module(sched_processes).
-include("../support/activity_records.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([update_gates/1,promote_act_and_give_ah/5,get_scheds/2]).

%% ====================================================================
%% Internal functions
%% ====================================================================

update_gates({Acts,GateMap})->
	ActTypes = maps:keys(GateMap),
	case (length(ActTypes)>0) and (length(Acts)>0) of
		true->
			update_gates(Acts,GateMap,ActTypes);
		_->
			ok
	end.
update_gates(Acts,GateMap,ActTypes)->
	case (length(Acts)==0) or (length(ActTypes)==0) of
		true->
			ok;
		_->
			[ActTypeKey|T] = ActTypes,
			GatePid = maps:get(ActTypeKey,GateMap),
			NewActs = update_gate(GatePid,ActTypeKey,Acts,[],Acts),
			update_gates(NewActs,GateMap,T)
	end.
update_gate(GatePid,_ActType,[],FoundActs,NewActs)->
	Scheds = get_scheds([],FoundActs),
	ok = custom_erlang_functions:myGenServCall(GatePid,{new_act_times,Scheds}),
	NewActs;

update_gate(GatePid,ActType,[Act|T],FoundActs,NewActs)->
	case Act#stage1Activity.type of
		ActType->
			NewNewActs = lists:delete(Act, NewActs),
			update_gate(GatePid,ActType,T,[Act|FoundActs],NewNewActs);
		_->
			update_gate(GatePid,ActType,T,FoundActs,NewActs)
	end.
	
get_scheds(Scheds,[])->
	Scheds;
get_scheds(Scheds,[H|T])->
	Tsched = H#stage1Activity.schedule_info#schedule_info.tsched,
	CleanScheds = lists:delete(Tsched, Scheds),%%ensures no duplicate of times, all acts that need to start at that time will be popped together
	NewScheds=[Tsched|CleanScheds],
	
	get_scheds(NewScheds,T).


promote_act_and_give_ah(S1ActId,MyRecep,ExeRecep,EP_Pid,From)->
	{ok,ActList} = custom_erlang_functions:myGenServCall(MyRecep, {storage_request,{get_del_or_pop_sched_by_ids,"get",[S1ActId]}}),
	case ActList of
		[]->
			gen_server:reply(From,{error,"No activity with this id in schedule"});
		_->
			S1Act = lists:nth(1, ActList),
			{ok,S2Act} = activity_functions:promoteStage1(S1Act, base_time:now(), #{}),
			{ok,"success"}=custom_erlang_functions:myGenServCall(ExeRecep,{storage_request,{save_exe_activities,[S2Act]}}),%%this is done to ensure the act is at least somewhere in the core and not just in the activity handler
			{ok,AHPid}=custom_erlang_functions:myGenServCall(ExeRecep,{start_act_handler,S2Act,EP_Pid}),
			{ok,_Res} =custom_erlang_functions:myGenServCall(MyRecep, {storage_request,{get_del_or_pop_sched_by_ids,"del",[S1ActId]}}),
			gen_server:reply(From, {ok,AHPid})
	end.
%%SLOC:53
