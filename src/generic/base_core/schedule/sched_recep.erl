
-module(sched_recep).
-author("Daniel van Niekerk (concept by Dale Sparrow)").

-behaviour(gen_server).

-export([start_link/2,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).


-include("../support/base_records.hrl").
-include("../support/activity_records.hrl").
-record(sched_state, {	status::ready|not_ready,
						default_storage_pid::pid(),storage_service_address::default|pid(),	%%storage related records
						coord_pid::pid(),reception_map::term(),	instance_plugins::list(),					%%instance pids needed for functionality
						resource_id::string(),resource_type::string(),						%%id and type needed for storage services
						gates::map(),gate_monitor_map::map(),														%%gates are processes that each have one activity type that the check for. If the time arrives for some activity to be 
						registered_execution_plugins::map(),	cloud_backup_plugins,							%started the gate will share that act with the registered_execution plugin for that actType
						last_time_stamp}).
%%%===================================================================

start_link(CoordPid,BC) ->
  {ok,SchedPID} = gen_server:start_link(?MODULE, [CoordPid,BC], []),
  {ok,SchedPID}.

stop() ->
  gen_server:stop(self()).

init([CoordPid,BC]) ->
 	{ok,ID} = business_cards:get_id(BC),
	{ok,Type} = business_cards:get_type(BC),
	custom_erlang_functions:myGenServCall(CoordPid,{core_component_started,sched_recep}),
    {ok, #sched_state{status = not_ready,resource_id=ID,coord_pid = CoordPid,reception_map = #{},instance_plugins = [],
					  resource_type=Type,storage_service_address = default,gates = #{},gate_monitor_map = #{},registered_execution_plugins=#{},cloud_backup_plugins=[],last_time_stamp = {"",0,0}}}. 

%-----------------------------------------------------------------
%%cloud backup plugins
handle_call({register_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#sched_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					NewCloudBackupPlugins = [PluginPid|CleanCloudBackupPlugins],
					{reply,ok,State#sched_state{cloud_backup_plugins=NewCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
handle_call({deregister_cloud_backup_plugin,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_pid(PluginPid) of
				true->
					CloudBackupPlugins = State#sched_state.cloud_backup_plugins,
					CleanCloudBackupPlugins = lists:delete(PluginPid, CloudBackupPlugins),
					{reply,ok,State#sched_state{cloud_backup_plugins=CleanCloudBackupPlugins}};
				_->
					{reply,{error,"PluginPid must be a valid pid"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
%Activity handling related calls
handle_call({start_act_and_get_act_handler,S1ActId,PluginPid},From,State)->
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#sched_state.status of
				ready->
					case custom_erlang_functions:is_string(S1ActId) of
						true->
							ExeRecep = maps:get(exe,State#sched_state.reception_map),
							MyPid = self(),
							spawn_monitor(sched_processes,promote_act_and_give_ah,[S1ActId,MyPid,ExeRecep,PluginPid,From]),	
							{noreply,State};
						_->
							{reply,{error,"S1ActId must be a string"},State}
					end;
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({register_execution_plugin,EP_Pid,ActType,TimeBeforeSched},From,State)->
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case custom_erlang_functions:is_string(ActType) and is_pid(EP_Pid) and is_integer(TimeBeforeSched) of
				true->
					ExeRecep = maps:get(exe,State#sched_state.reception_map),
					{ok,UnfinishedActs} = exe_api:get_acts_by_types(ExeRecep, [ActType]),
					Request = {get_del_or_pop_sched_by_types,"get",[ActType]},
					Call = storage_functions:add_id_and_type(Request,State#sched_state.resource_id,State#sched_state.resource_type),
					case State#sched_state.storage_service_address of
						default->
							%%use default module
							{ok,Acts}=custom_erlang_functions:myGenServCall(State#sched_state.default_storage_pid, Call);
						_->
							{ok,Acts}=custom_erlang_functions:myGenServCall(State#sched_state.storage_service_address, Call)
					end,
					SchedGatesTimeAndPid = {State#sched_state.gates,TimeBeforeSched,Acts,self()},
					{NewEP_Map,RegResult,GateMapAndMonitor} = plugin_registration_functions:register(State#sched_state.registered_execution_plugins,false,EP_Pid,ActType,SchedGatesTimeAndPid),
					case RegResult of
						ok->
							{MonitorRef,NewGateMap} = GateMapAndMonitor,
							NewGateMonitors = maps:put(MonitorRef,ActType,State#sched_state.gate_monitor_map),
							Reply = {ok,UnfinishedActs};
						_->
							NewGateMonitors = State#sched_state.gate_monitor_map,
							NewGateMap = State#sched_state.gates,
							Reply = RegResult
					end,
					NewState = State#sched_state{registered_execution_plugins = NewEP_Map,gates = NewGateMap,gate_monitor_map=NewGateMonitors};
				_->
					NewState = State,
					Reply = {error,"Invalid input arguments"}
			end,
			{reply,Reply,NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
handle_call({deregister_execution_plugin,EP_Pid,ActType},From,State)->%%used by gate itself and eps that do not want to receive acts anymore
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case custom_erlang_functions:is_string(ActType) and is_pid(EP_Pid) of
				true->
					GateMap = State#sched_state.gates,
					{NewEPMap,Reply,NewGateMap} = plugin_registration_functions:deregister(State#sched_state.registered_execution_plugins, false, EP_Pid, ActType, GateMap),
					NewState = State#sched_state{registered_execution_plugins = NewEPMap,gates = NewGateMap};
				_->
					NewState = State,
					Reply = {error,"Invalid input arguments"}
			end,
			{reply,Reply,NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

%This call is used to change storage method of an instance. For example using some cloud storage service
handle_call({change_storage, NewStoragePid},From,State)->
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case erlang:is_pid(NewStoragePid) of
				true->
					NewState = State#sched_state{storage_service_address = NewStoragePid},
					Reply = ok;
				_->
					NewState = State,
					Reply = {error, "Invalid NewStoragePid"}
			end,
			{reply, Reply, NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({new_act,ActType,Tsched,S1Data},From,State)->
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case custom_erlang_functions:is_string(ActType) and is_integer(Tsched) of
				true->	
					{PrevType,Time,Counter} = State#sched_state.last_time_stamp,
					TimeNow = base_time:now(),
					ActIdRaw = string:concat(string:concat(ActType, "_sched_"),integer_to_list(TimeNow)),
					case (PrevType==ActType) and (Time==TimeNow) of
						true->
							Copy = string:concat(string:concat("(",integer_to_list(Counter+1)),")"),
							ActId = string:concat(ActIdRaw,Copy),
							NewTimeStamp = {ActType,TimeNow,Counter+1};
						_->
							ActId = ActIdRaw,
							NewTimeStamp = {ActType,TimeNow,0}
					end,
					{ok,Act} = activity_functions:makeStage1Activity(ActId, ActType, Tsched, S1Data),
					Request = {save_sched_activities,[Act]},
					Call = storage_functions:add_id_and_type(Request,State#sched_state.resource_id,State#sched_state.resource_type),
					PostRequestProcess = {sched_processes,update_gates,{[Act],State#sched_state.gates}},
					MyPid = self(),
					spawn_monitor(storage_functions,request,[State#sched_state.storage_service_address, State#sched_state.default_storage_pid, Call, From, PostRequestProcess,{State#sched_state.cloud_backup_plugins,MyPid}]),
					{noreply,State#sched_state{last_time_stamp = NewTimeStamp}};
				_->
					{reply,{error,"Invalid ActType or Tsched"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;
%Save, get, delete and pop requests all go through the next call:
handle_call({storage_request,Request},From,State)->
	FullPermittedList = lists:append(maps:values(State#sched_state.reception_map),State#sched_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case State#sched_state.status of 
				ready->
					ValidReq = storage_functions:valid_sched_storage_request(Request),
					case ValidReq of
						true->
							Acts = storage_functions:get_content_if_save(Request),
							case Acts of
								false->
									PostRequestProcess = none;
								_->
									PostRequestProcess = {sched_processes,update_gates,{Acts,State#sched_state.gates}}
							end,
							Call = storage_functions:add_id_and_type(Request,State#sched_state.resource_id,State#sched_state.resource_type),
							MyPid = self(),
							spawn_monitor(storage_functions,request,[State#sched_state.storage_service_address, State#sched_state.default_storage_pid, Call, From, PostRequestProcess,{State#sched_state.cloud_backup_plugins,MyPid}]),
							{noreply,State};
						_->
							{false,Reason} = ValidReq,
							Reply = {error,Reason},
							{reply,Reply,State}
					end;
				_->
					{reply,{error,"Reception not ready"},State}
			end;
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({instance_pids, ReceptionMap, DefStorage},From,State)->
	
	case security_functions:from_is_process_or_monitored_by_process(State#sched_state.coord_pid, From) of
		true->
			NewState = State#sched_state{default_storage_pid = DefStorage,status = ready,reception_map = ReceptionMap},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;
handle_call({instance_plugins, InstancePlugins},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#sched_state.coord_pid, From) of
		true->
			NewState = State#sched_state{instance_plugins = InstancePlugins},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;
handle_call(_Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"Sched_recep:unknown request"),
  {reply, unknown, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info({'DOWN', MonitorRef, process, _Object, Info},State)->
	AllMonitorRefs = maps:keys(State#sched_state.gate_monitor_map),
	case lists:member(MonitorRef, AllMonitorRefs) of
		true->
			error_log:log(?MODULE,0,unknown,"ERROR:One of the sched reception's gates stopped because of reason: ~p",[Info]),
			ActType = maps:get(MonitorRef,State#sched_state.gate_monitor_map),
			NewGateMonitors = maps:remove(MonitorRef,State#sched_state.gate_monitor_map),
			NewGates = maps:remove(ActType, State#sched_state.gates),
			NewEPs = maps:remove(ActType,State#sched_state.registered_execution_plugins),
			demonitor(MonitorRef),
			NewState = State#sched_state{gates = NewGates,gate_monitor_map = NewGateMonitors,registered_execution_plugins = NewEPs},
			{noreply,NewState};
		_->
			{noreply,State}
	end;

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%%SLOC:238



