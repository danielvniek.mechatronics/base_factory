%% @author Daniel
%% @doc @todo Add description to sched_api.

%%Note we do not give plugins the option to specify the timeout for requests. This is fixed to allow the core always has sufficient time to complete the requested action. Plugins do not know how long this is
%%also note: api's do not check for valid args, this is done in reception since reception should anyways check for if calls do  not come via api
-module(sched_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([start_act_and_get_act_handler/3,
		 register_execution_plugin/4,deregister_execution_plugin/3,
		 change_storage/2,
		 new_act/4,save/2,
		 get_all_acts/1,get_acts_by_ids/2,get_acts_by_types/2,get_acts_by_times/3,get_acts_by_types_and_times/4,
		 del_all_acts/1,del_acts_by_ids/2,del_acts_by_types/2,del_acts_by_times/3,del_acts_by_types_and_times/4,
		 pop_all_acts/1,pop_acts_by_ids/2,pop_acts_by_types/2,pop_acts_by_times/3,pop_acts_by_types_and_times/4]).


%% ====================================================================
%% Internal functions
%% ====================================================================
start_act_and_get_act_handler(ReceptionPid,Activity_ID,EP_Pid)->
	custom_erlang_functions:myGenServCall(ReceptionPid,{start_act_and_get_act_handler,Activity_ID,EP_Pid}).
	
register_execution_plugin(ReceptionPid,EP_Pid,ActType,TimeBeforeSched)->
	custom_erlang_functions:myGenServCall(ReceptionPid,{register_execution_plugin,EP_Pid,ActType,TimeBeforeSched}).

deregister_execution_plugin(ReceptionPid,EP_Pid,ActType)->
	custom_erlang_functions:myGenServCall(ReceptionPid,{deregister_execution_plugin,EP_Pid,ActType}).

change_storage(ReceptionPid,NewStoragePid)->
	custom_erlang_functions:myGenServCall(ReceptionPid,{change_storage, NewStoragePid}).
%%this call will return {ok,ActId} if successfull
new_act(ReceptionPid,ActType,Tsched,S1Data)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {new_act,ActType,Tsched,S1Data}).
%%this call will return {ok,"success"} if successful
save(ReceptionPid,Activities)->
	case is_list(Activities) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid,{storage_request,{save_sched_activities,Activities}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid,{storage_request,{save_sched_activities,[Activities]}})
	end.
%--------------------GET--------------------%
get_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_sched,"get"}}).

get_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_ids,"get",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_ids,"get",[IDs]}})
	end.

get_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types,"get",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types,"get",[Types]}})
	end.

get_acts_by_times(ReceptionPid,TschedLow,TschedHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_times,"get",TschedLow,TschedHigh}}).

get_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types_and_times,"get",Types,TschedLow,TschedHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types_and_times,"get",[Types],TschedLow,TschedHigh}})
	end.
%------------------DELETE---------------------%
del_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_sched,"del"}}).

del_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_ids,"del",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_ids,"del",[IDs]}})
	end.

del_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types,"del",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types,"del",[Types]}})
	end.

del_acts_by_times(ReceptionPid,TschedLow,TschedHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_times,"del",TschedLow,TschedHigh}}).

del_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types_and_times,"del",Types,TschedLow,TschedHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types_and_times,"del",[Types],TschedLow,TschedHigh}})
	end.
%------------------POP----------------------%
pop_all_acts(ReceptionPid)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_all_sched,"pop"}}).

pop_acts_by_ids(ReceptionPid,IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_ids,"pop",IDs}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_ids,"pop",[IDs]}})
	end.

pop_acts_by_types(ReceptionPid,Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types,"pop",Types}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types,"pop",[Types]}})
	end.

pop_acts_by_times(ReceptionPid,TschedLow,TschedHigh)->
	custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_times,"pop",TschedLow,TschedHigh}}).

pop_acts_by_types_and_times(ReceptionPid,Types,TschedLow,TschedHigh)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types_and_times,"pop",Types,TschedLow,TschedHigh}});
		_->
			custom_erlang_functions:myGenServCall(ReceptionPid, {storage_request,{get_del_or_pop_sched_by_types_and_times,"pop",[Types],TschedLow,TschedHigh}})
	end.

