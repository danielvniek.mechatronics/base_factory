%% @author Daniel
%% @doc @todo Add description to resource_acts_helper_funcs.


-module(ra_helper_funcs).

%% ====================================================================
%% API functions
%% ====================================================================
-export([add_contracts/3,remove_old_contracts/4,get_added_act_types_map/5,deregister_old/4,send_service_request/5,
		 register_for_acts/5,get_file_and_register_loop/4,handle_ra_acts/6,handle_unfinished_acts/4,handle_done/5,busy_acts_and_contracts/3,
		 get_left_over_rfps/3,add_left_over_contracts_to_proposals/2,remove_all_rfps_for_act/3,remove_proposal/3]).


-export([handle_mod_reply/10,handle_mod_catch/9]).


-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================

get_file_and_register_loop(RAMod,ReceptionMap,MyPid,ActTypesMap)->	
	try
		NewActTypesMap = RAMod:get_act_types(),
		case NewActTypesMap==ActTypesMap of
			true->
				ok;
			_->
				deregister_old(maps:keys(ActTypesMap),maps:keys(NewActTypesMap),maps:get(sched,ReceptionMap),MyPid),
				AddedActTypesMap = get_added_act_types_map(maps:keys(ActTypesMap),maps:keys(NewActTypesMap),ActTypesMap,NewActTypesMap,#{}),
				NewActTypes = maps:keys(AddedActTypesMap),NewTimes = maps:values(AddedActTypesMap),
				register_for_acts(ReceptionMap,MyPid,NewActTypes,NewTimes,RAMod)
		end,
		timer:sleep(30000),
		get_file_and_register_loop(RAMod,ReceptionMap,MyPid,NewActTypesMap)
	catch
		_:_Something -> 
			%error_log:log(?MODULE,0,unknown,"\nERROR: resource_acts_ep could not get the required information from ~p",[RAMod]),
		timer:sleep(30000),
		get_file_and_register_loop(RAMod,ReceptionMap,MyPid,ActTypesMap)
	end.

register_for_acts(_ReceptionMap,_MyPid,[],[],_RAMod)->
	ok;
register_for_acts(ReceptionMap,MyPid,[ActType|T1],[TimeBeforeSched|T2],RAMod)->
	SchedPid = maps:get(sched,ReceptionMap),
	timer:sleep(20),%%give coordinator time to pass this plugins pid around
	{Res,UnfinishedActs}=sched_api:register_execution_plugin(SchedPid, MyPid, ActType, TimeBeforeSched),
	case Res of 
		ok->
			handle_unfinished_acts(UnfinishedActs,ReceptionMap,MyPid,RAMod),
			register_for_acts(ReceptionMap,MyPid,T1,T2,RAMod);
		_->%%sometimes this plugin's pid has not yet been sent by coordinator to core components and they do not permit the registration
			timer:sleep(500),
			register_for_acts(ReceptionMap,MyPid,[ActType|T1],[TimeBeforeSched|T2],RAMod)
	end.

add_contracts([],ContractDetails,_ActId)->
	ContractDetails;
add_contracts([Contract|T],ContractDetails,ActId)->
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,Adr} = business_cards:get_address(ServBC),
	NewContractsDetails = maps:put(Contract,{Adr,ActId},ContractDetails),
	add_contracts(T,NewContractsDetails,ActId).

remove_old_contracts(_CommsPid,[],ContractDetails,_Reason)->
	ContractDetails;
remove_old_contracts(CommsPid,[Contract|T],ContractDetails,Reason)->
	spawn_monitor(fun()->comms_api:send_cancel_service(CommsPid, Contract, Reason)end),
	remove_old_contracts(CommsPid,T,maps:remove(Contract, ContractDetails),Reason).

get_added_act_types_map(_OldTypes,[],_OldMap,_NewMap,AddedMap)->
	AddedMap;
get_added_act_types_map(OldTypes,[Type|T],OldMap,NewMap,AddedMap)->
	case lists:member(Type, OldTypes) of
		true->
			NewVal = maps:get(Type,NewMap),
			OldVal = maps:get(Type,OldMap),
			case NewVal==OldVal of
				true->
					get_added_act_types_map(OldTypes,T,OldMap,NewMap,AddedMap);
				_->
					get_added_act_types_map(OldTypes,T,OldMap,NewMap,maps:put(Type, NewVal, AddedMap))
			end;
		_->
			NewVal = maps:get(Type,NewMap),
			get_added_act_types_map(OldTypes,T,OldMap,NewMap,maps:put(Type, NewVal, AddedMap))
	end.

deregister_old([],_NewActTypes,_SchedRecep,_MyPid)->
	ok;
deregister_old([OldType|T],NewActTypes,SchedRecep,MyPid)->
	case lists:member(OldType, NewActTypes) of
		true->
			ok;
		_->
			sched_api:deregister_execution_plugin(SchedRecep, MyPid, OldType)
	end,
	deregister_old(T,NewActTypes,SchedRecep,MyPid).

send_service_request(MyPid,ActId,Comms,Contract,From)->
	Reply = comms_api:send_service_request(Comms, Contract),
	case Reply of
		accept->
			custom_erlang_functions:myGenServCall(MyPid,{new_sub_contract,ActId,Contract});
		_->
			ok
	end,
	custom_erlang_functions:gen_serv_reply_if_from(From, {Contract,Reply}).

handle_ra_acts([],_ReceptionMap,_MyRecep,_RAMod,ActiveActs,BusyActs)->
	{ActiveActs,BusyActs};
handle_ra_acts([H|T],ReceptionMap,MyRecep,RAMod,ActiveActs,BusyActs)->	
	spawn_monitor(fun()->ra_handling_funcs:handle_act(H,ReceptionMap,MyRecep,RAMod)end),
	handle_ra_acts(T,ReceptionMap,MyRecep,RAMod,maps:put(H#stage1Activity.id,{pending_ah_pid,[]},ActiveActs),maps:put(H#stage1Activity.id,[{acts_ready,[H]}],BusyActs)).



handle_unfinished_acts([],_CommsRecep,_MyRecep,_RAMod)->
	ok;
handle_unfinished_acts([Act|T],CommsRecep,MyRecep,RAMod)->
	custom_erlang_functions:myGenServCall(MyRecep,{spawn_process,ra_handling_funcs,handle_unfinished_act,[Act,CommsRecep,MyRecep,RAMod]}),
	handle_unfinished_acts(T,CommsRecep,MyRecep,RAMod).


%%handling functions:

create_pending_proposals([],_ActRfps,Proposals)->
	Proposals;
create_pending_proposals([RfpIdentifier|T],ActRfps,Proposals)->
	{Contracts,_Pid} = maps:get(RfpIdentifier,ActRfps),
	NewProposals = maps:put(RfpIdentifier, Contracts, Proposals),
	create_pending_proposals(T,ActRfps,NewProposals).

handle_mod_reply(AcceptOrReject,Done,S2Data,OldS2Data,MyPid,ActId,AHPid,RAMod,ActType,StartContinueHandlePackageOrHandleServStopped)->
	case is_map(S2Data) and ((Done==true)or(Done==false)) and ((AcceptOrReject==accept)orelse(is_tuple(AcceptOrReject) andalso size(AcceptOrReject)==2 andalso(element(1,AcceptOrReject)==reject))) of
		true->
			{ok,Contracts,ActRfps} = custom_erlang_functions:myGenServCall(MyPid,{get_act_contracts_and_give_ah,ActId,AHPid}),
			NewS2Data = maps:put(<<"Contracts">>, Contracts, S2Data),
			case ActRfps==#{} of
				true->
					NNewS2Data = NewS2Data;
				_->
					PendingProposals = create_pending_proposals(maps:keys(ActRfps),ActRfps,#{}),
					NNewS2Data = maps:put(<<"Pending proposals">>,PendingProposals,NewS2Data)
			end,
			case maps:is_key(<<"Proposals">>, OldS2Data) of
				true->
					Proposals = maps:get(<<"Proposals">>,OldS2Data),
					FinalS2Data = maps:put(<<"Proposals">>, Proposals, NNewS2Data);
				_->
					FinalS2Data = NNewS2Data
			end,
			case Done of
				true->
					ok = act_handler_api:done(AHPid, FinalS2Data),
					case maps:is_key(<<"Reason">>, S2Data) of
						true->
							custom_erlang_functions:myGenServCall(MyPid,{clear_act,ActId,maps:get(<<"Reason">>,S2Data),false});
						_->
							custom_erlang_functions:myGenServCall(MyPid,{clear_act,ActId,"Done",false})
					end;
					
				_->
					ok = act_handler_api:update_progress(AHPid, FinalS2Data)
			end;
		_->
			error_log:log(?MODULE,0,unknown,"\nERROR: ~p returned incorrect arguments when used to "++StartContinueHandlePackageOrHandleServStopped++" activity of type ~p. Finishing this activity with failed in s2data.",[RAMod,ActType]),
			Reason = atom_to_list(RAMod) ++" returned incorrect arguments when used to "++StartContinueHandlePackageOrHandleServStopped++" this activity.",
			FailedS2Data = #{<<"Result">>=><<"Failed">>,<<"Reason">>=>Reason},
			ok = act_handler_api:done(AHPid, FailedS2Data),
			custom_erlang_functions:myGenServCall(MyPid,{clear_act,ActId,"Failed",false})
	end.

handle_mod_catch(A,B,StackTrace,MyPid,ActId,AHPid,RAMod,ActType,StartContinueHandlePackageOrHandleServStopped)->
	error_log:log(?MODULE,0,unknown,"\nERROR: Caught: ~p:~p:~p when trying to let ~p "++StartContinueHandlePackageOrHandleServStopped++" resource act (RA) of type  ~p. Finishing this activity with failed in s2data.",[A,B,StackTrace,RAMod,ActType]),
	ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
	FailedS2Data = #{<<"Result">>=><<"Failed">>,<<"Reason">>=>atom_to_list(RAMod) ++" could not "++StartContinueHandlePackageOrHandleServStopped++" activity",<<"Error and stacktrace">>=>ErrorAndStackTrace},
	act_handler_api:done(AHPid,FailedS2Data),
	custom_erlang_functions:myGenServCall(MyPid,{clear_act,ActId,"Failed",false}).

handle_done(ActId,AHPid,MyPid,FinalS2Data,ClearSched)->
	case is_pid(AHPid) andalso (is_process_alive(AHPid)) of
		true->
			ok = act_handler_api:done(AHPid, FinalS2Data);
		_->
			ok
	end,
	custom_erlang_functions:myGenServCall(MyPid,{clear_act,ActId,"Done",ClearSched}),
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_act,ActId}).

busy_acts_and_contracts(ID,ActsOrContracts,MyTuple)->
	case maps:is_key(ID, ActsOrContracts)  of
		true->	
			
			CurrentVal = maps:get(ID,ActsOrContracts),
			case CurrentVal of
				[]->
					error_log:log(?MODULE,0,unknown,"\nERROR empty currrentval in busy_acts_and_contracts functions in ra_helper_funcs.erl"),
					timer:sleep(10000),
					{false,ActsOrContracts};
				_->
					CurrentCall = lists:last(CurrentVal),
					case (CurrentCall==MyTuple) of
						true->

							{true,ActsOrContracts};
						_->
							NewVal = [MyTuple|CurrentVal],
							NewActsOrContracts = maps:update(ID, NewVal, ActsOrContracts),
							{false,NewActsOrContracts}
					end
			end;
		_->
			NewActsOrContracts = maps:put(ID, [MyTuple], ActsOrContracts),
			{true,NewActsOrContracts}
	end.

get_left_over_rfps([],AllLeftOverContracts,LeftOver)->
	{AllLeftOverContracts,LeftOver};
get_left_over_rfps([Contract|T],AllLeftOverContracts,LeftOver)->
	case lists:member(Contract, AllLeftOverContracts) of
		true->
			get_left_over_rfps(T,lists:delete(Contract, AllLeftOverContracts),[Contract|LeftOver]);
		_->
			get_left_over_rfps(T,AllLeftOverContracts,LeftOver)
	end.

add_left_over_contracts_to_proposals([],CurProposals)->
	CurProposals;
add_left_over_contracts_to_proposals([Contract|T],CurProposals)->
	{ok,ServType} = contracts:get_service_type(Contract),
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,ServId} = business_cards:get_id(ServBC),
	NewProposal = #{<<"Service provider id">>=>ServId,<<"Proposal">>=>timeout},
	case maps:is_key(ServType, CurProposals) of
		true->
			CurServProps = maps:get(ServType,CurProposals),
			NewProposals = maps:update(ServType, [NewProposal|CurServProps], CurProposals);
		_->
			NewProposals = maps:put(ServType, [NewProposal], CurProposals)
	end,
	add_left_over_contracts_to_proposals(T,NewProposals).
	
remove_all_rfps_for_act([],_ActRfps,Rfps)->
	Rfps;

remove_all_rfps_for_act([RfpIdentifier|T],ActRfps,Rfps)->
	{Contracts,Pid} = maps:get(RfpIdentifier,ActRfps),
	exit(Pid,kill),
	NewRfps = custom_erlang_functions:remove_keys_from_map(Contracts, Rfps),
	remove_all_rfps_for_act(T,ActRfps,NewRfps).

remove_proposal([],_Contract,PendingProposals)->
	PendingProposals;
remove_proposal([PendingProposal|T],Contract,PendingProposals)->
	{ok,ServBc} = contracts:get_service_bc(Contract),
	{ok,ServId} = business_cards:get_id(ServBc),
	PropServId = maps:get(<<"Service provider id">>,PendingProposal),
	case PropServId of
		ServId->
			lists:delete(PendingProposal, PendingProposals);
		_->
			remove_proposal(T,Contract,PendingProposals)
	end.

%%SLOC:221


