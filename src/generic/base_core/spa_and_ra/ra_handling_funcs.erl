%% @author Daniel
%% @doc @todo Add description to ra_handling_funcs.


-module(ra_handling_funcs).

%% ====================================================================
%% API functions
%% ====================================================================
-export([handle_act/4,handle_unfinished_act/4,handle_package/7,handle_rfp_timeout/5,handle_proposal/7,handle_serv_stopped/6]).
-define(WAIT_TIME,60000).
-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).
-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
handle_act(Act,ReceptionMap,MyPid,Mod)->
	{ok,AHPid}=sched_api:start_act_and_get_act_handler(maps:get(sched,ReceptionMap), Act#stage1Activity.id, MyPid),
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	ActId = S2Act#stage2Activity.id,
	S1Data = S2Act#stage2Activity.schedule_info#schedule_info.s1data,	
	{ok,_Contracts,_PendingProposals} = custom_erlang_functions:myGenServCall(MyPid,{get_act_contracts_and_give_ah,ActId,AHPid},?WAIT_TIME),%%purely to give ah_pid
	try
		{Done,S2Data}=Mod:start(S2Act,AHPid,ReceptionMap,MyPid),
		ra_helper_funcs:handle_mod_reply(accept,Done,S2Data,#{},MyPid,ActId,AHPid,Mod,Act#stage1Activity.type,"start")
	catch
		A:B:StackTrace->
			ra_helper_funcs:handle_mod_catch(A,B,StackTrace,MyPid,ActId,AHPid,Mod,Act#stage1Activity.type,"start")
	end,
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_act,ActId}),
	case maps:is_key(<<"Contract">>,S1Data) of
			true->
				Contract2 = maps:get(<<"Contract">>,S1Data),
				custom_erlang_functions:myGenServCall(MyPid,{done_working_on_contract,Contract2});
			_->
				ok
	end.

handle_unfinished_act(S2Act,ReceptionMap,MyPid,Mod)->
	ActId = S2Act#stage2Activity.id,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	case maps:is_key(<<"Contracts">>, S2Data) of
		true->
			OldContracts = maps:get(<<"Contracts">>,S2Data);
		_->%%left up to case specific module if they want to finish this act because there are no contracts in s2data indicating that the system maybe shutdown while an activity was being started
			OldContracts = []
	end,
	custom_erlang_functions:myGenServCall(MyPid,{busy_with_unfinished_act,S2Act,OldContracts}),
	{ok,AHPid}=exe_api:start_act_handler(maps:get(exe,ReceptionMap), S2Act, MyPid),	
	{ok,_Contracts,_ActRfps} = custom_erlang_functions:myGenServCall(MyPid,{get_act_contracts_and_give_ah,ActId,AHPid},?WAIT_TIME),%%purely to give ah_pid
	try
		{Done,NewS2Data}=Mod:continue(S2Act,AHPid,ReceptionMap,MyPid),
		ra_helper_funcs:handle_mod_reply(accept,Done,NewS2Data,S2Data,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"continue")
	catch%%if module does not have a continue or the continue is faulty activity is just completed with no new data
		A:B:StackTrace->
			ra_helper_funcs:handle_mod_catch(A,B,StackTrace,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"continue")
	end,
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_act,ActId}).
	
handle_package(Contract,AHPid,Mod,ReceptionMap,MyPid,Package,From)->%%important infO: when a package is rejected, the activity is NOT finished unless the module returns Done==true
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	ActId = S2Act#stage2Activity.id,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	try
		{AcceptOrReject,Done,NewS2Data} = Mod:handle_package(S2Act,AHPid,ReceptionMap,MyPid,Contract,Package),
		gen_server:reply(From,AcceptOrReject),
		ra_helper_funcs:handle_mod_reply(AcceptOrReject,Done,NewS2Data,S2Data,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_package for")
		
	catch
		A:B:StackTrace->
			PackageS = lists:flatten(io_lib:format("~p",[Package])),
			ContractS = lists:flatten(io_lib:format("~p",[Contract])),
			ra_helper_funcs:handle_mod_catch(A,B,StackTrace,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_package for package = "++PackageS++"\n for sub-contract =  "++ContractS++" for"),
			gen_server:reply(From,{reject,"Could not handle package"})
	end,
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_act,ActId}).

handle_rfp_timeout(AHPid,Mod,ReceptionMap,MyPid,RfpIdentifier)->
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	ActId = S2Act#stage2Activity.id,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,		
	try
		case maps:is_key(<<"Proposals">>, S2Data) of
			true->
				Proposals = maps:get(<<"Proposals">>,S2Data),
				case maps:is_key(RfpIdentifier, Proposals) of
					true->
						ProposalContracts = maps:get(RfpIdentifier,Proposals);
					_->
						ProposalContracts = []
				end;
			_->
				ProposalContracts = []
		end,
		PendingProposals = maps:get(<<"Pending proposals">>,S2Data),
		PendingProposalContracts = maps:get(RfpIdentifier,PendingProposals),
		{Done,NewS2Data} = Mod:handle_rfp_timeout(S2Act,AHPid,ReceptionMap,MyPid,RfpIdentifier,ProposalContracts,PendingProposalContracts),
		ra_helper_funcs:handle_mod_reply(accept,Done,NewS2Data,S2Data,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_rfp_timeout for")
	catch
		A:B:StackTrace->
			ra_helper_funcs:handle_mod_catch(A,B,StackTrace,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_rfp_timeout for")
	end,
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_act,ActId}).
	
handle_proposal(RfpIdentifier,Contract,AHPid,Mod,ReceptionMap,MyPid,Proposal)->
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	ActId = S2Act#stage2Activity.id,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	{ok,UpdatedContract} = contracts:insert_proposal_reply(Contract,Proposal),
	case maps:is_key(<<"Proposals">>, S2Data) of
		true->
			Proposals = maps:get(<<"Proposals">>,S2Data),
			case maps:is_key(RfpIdentifier, Proposals) of
				true->
					ProposalContracts = maps:get(RfpIdentifier,Proposals);
				_->
					ProposalContracts = []
			end,
			NewProposalContracts = [UpdatedContract|ProposalContracts],
			NewProposals = maps:put(RfpIdentifier,NewProposalContracts);
		_->
			NewProposalContracts = [UpdatedContract],
			NewProposals = #{RfpIdentifier=>[UpdatedContract]}
	end,
	
	PendingProposals = maps:get(<<"Pending proposals">>,S2Data),
	PendingProposalContracts = maps:get(RfpIdentifier,PendingProposals),
	NewPendingProposalContracts = lists:delete(Contract,PendingProposalContracts),
	NewPendingProposals = maps:update(RfpIdentifier, NewPendingProposalContracts, PendingProposals),
	
	AddedS2Data = #{<<"Proposals">>=>NewProposals,<<"Pending proposals">>=>NewPendingProposals},
	NewS2Data = maps:merge(S2Data,AddedS2Data),
	NewExecutionInfo = S2Act#stage2Activity.execution_info#execution_info{s2data=NewS2Data},
	NewS2Act = S2Act#stage2Activity{execution_info=NewExecutionInfo},	
			
	try
		{CancelOtherRfps,Done,NewNewS2Data} = Mod:handle_proposal(NewS2Act,AHPid,ReceptionMap,MyPid,RfpIdentifier,NewProposalContracts,PendingProposalContracts),
		case CancelOtherRfps of
			true->
				custom_erlang_functions:myGenServCall(MyPid,{cancel_rfps,S2Act#stage2Activity.id,RfpIdentifier});
			_->
				ok
		end,
		ra_helper_funcs:handle_mod_reply(accept,Done,NewNewS2Data,NewS2Data,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_proposal for")
		
	catch
		A:B:StackTrace->
			ProposalS = lists:flatten(io_lib:format("~p",[Proposal])),
			ContractS = lists:flatten(io_lib:format("~p",[Contract])),
			ra_helper_funcs:handle_mod_catch(A,B,StackTrace,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_proposal for proposal = "++ProposalS++"\n for sub-contract =  "++ContractS++" for")
	end,
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_act,ActId}).

handle_serv_stopped(Contract,AHPid,Mod,ReceptionMap,MyPid,Reason)->
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	ActId = S2Act#stage2Activity.id,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	try
		{Done,NewS2Data}=Mod:handle_serv_stopped(S2Act,AHPid,ReceptionMap,MyPid,Contract,Reason),
		ra_helper_funcs:handle_mod_reply(accept,Done,NewS2Data,S2Data,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_serv_stopped ")
	catch
		A:B:StackTrace->
			ContractS = lists:flatten(io_lib:format("~p",[Contract])),
			ra_helper_funcs:handle_mod_catch(A,B,StackTrace,MyPid,ActId,AHPid,Mod,S2Act#stage2Activity.type,"handle_serv_stopped with Reason = "++Reason++"\n for sub-contract=  "++ContractS++" for")
	end,
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_act,ActId}).

%%SLOC:141



	
	

