%% @author Daniel
%% @doc @todo Add description to spa_api.


-module(spa_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([send_rfps/10,send_service_request/7,send_service_request/3,send_cancel/4,spawn_process/4,done/4,deliver_package/3]).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).
-record(schedule_info,{tsched::integer(),s1data::term()}).


%% ====================================================================
%% Internal functions
%% ====================================================================
send_rfps(MyRecep,ActId,ServBCs,ServType,Interval,RequestArgs,PackageBlueprint,ProposalTemplate,ProposalTimeLimit,RfpIdentifier)->
	custom_erlang_functions:myGenServCall(MyRecep,{send_rfps,ActId,ServBCs,ServType,Interval,RequestArgs,PackageBlueprint,ProposalTemplate,ProposalTimeLimit,RfpIdentifier}).

send_service_request(MyRecep,ActId,ServBC,ServType,Interval,RequestArgs,PackageBlueprint)->
	custom_erlang_functions:myGenServCall(MyRecep,{send_service_request,ActId,ServBC,ServType,Interval,RequestArgs,PackageBlueprint,none}).

send_service_request(MyRecep,ActId,Contract)->
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,ServType} = contracts:get_service_type(Contract),
	{ok,Interval} = contracts:get_interval(Contract),
	{ok,RequestArgs} = contracts:get_request_arguments(Contract),
	{ok,PackageBlueprint} = contracts:get_package_blueprint(Contract),
	{ok,Proposal} = contracts:get_proposal(Contract),
	custom_erlang_functions:myGenServCall(MyRecep,{send_service_request,ActId,ServBC,ServType,Interval,RequestArgs,PackageBlueprint,Proposal}).

send_cancel(MyRecep,ActId,SubContract,Reason)->
	custom_erlang_functions:myGenServCall(MyRecep,{send_cancel,ActId,SubContract,Reason}).

spawn_process(MyRecep,Mod,Fun,Args)->
	custom_erlang_functions:myGenServCall(MyRecep,{spawn_process,Mod,Fun,Args}).

done(MyRecep,ActId,FinalS2Data,ClearSched)->
	custom_erlang_functions:myGenServCall(MyRecep,{done,ActId,FinalS2Data,ClearSched}).

deliver_package(S2Act,ReceptionMap,Package)->
	S1Data = S2Act#stage2Activity.schedule_info#schedule_info.s1data,
	Contract = maps:get(<<"Contract">>,S1Data),
	Comms = maps:get(comms,ReceptionMap),
	comms_api:deliver_package(Comms, Contract, Package).

%%SLOC:23
