%% @author Daniel
%% @doc @todo Add description to spa_helper_funcs.


-module(spa_helper_funcs).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_file_and_register_loop/4,handle_spa_acts/10,remove_act_from_contract_acts/3,cancel_sched_acts/5,schedule_new_acts/5,cancel_active_act/4]).
-export([handle_setup_info/6,handle_execution_info/5,fill_in_proposal_template_and_reply/5]).

-record(stage1Activity,
          {id::term(),type::term(),schedule_info::term()}).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).
-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_file_and_register_loop(ServProvMod,ReceptionMap,MyPid,ServMap)->	
	try
		CommsPid = maps:get(comms,ReceptionMap),
		NewServMap = ServProvMod:get_services(),
		
		case NewServMap==ServMap of
			true->
				FinalServMap = ServMap;
			_->
				case custom_erlang_functions:isListOfStrings(maps:keys(NewServMap)) of
					true->
						deregister_old(maps:keys(ServMap),maps:keys(NewServMap),ReceptionMap),
						{ActTypes,Times,ServicesMap} = get_added_serv_types_map(maps:keys(ServMap),maps:keys(NewServMap),ServMap,NewServMap,{[],[],#{}}),
						ra_helper_funcs:register_for_acts(ReceptionMap,MyPid,ActTypes,Times,ServProvMod),
						custom_erlang_functions:myGenServCall(CommsPid,{update_services,ServicesMap}),
						FinalServMap = NewServMap;
					_->
						error_log:log(?MODULE,0,unknown,"\nERROR: The keys of the map returned by ~p:get_services must all be strings",[ServProvMod]),
						FinalServMap = ServMap
				end
		end,
		timer:sleep(30000),
		get_file_and_register_loop(ServProvMod,ReceptionMap,MyPid,FinalServMap)
	catch
		A:B:StackTrace -> error_log:log(?MODULE,0,unknown,"\nERROR: comms_recep could not get the required information from ~p because of \n~p:~p:~p",[ServProvMod,A,B,StackTrace]),
		timer:sleep(30000),
		get_file_and_register_loop(ServProvMod,ReceptionMap,MyPid,ServMap)
	end.

deregister_old([],_NewServTypes,_ReceptionMap)->
	ok;
deregister_old([OldType|T],NewServTypes,ReceptionMap)->
	case lists:member(OldType, NewServTypes) of
		true->
			ok;
		_->
			ActType = OldType++" (SPA)",
			sched_api:deregister_execution_plugin(maps:get(sched,ReceptionMap), maps:get(comms,ReceptionMap), ActType)
	end,
	deregister_old(T,NewServTypes,ReceptionMap).

get_added_serv_types_map(_OldTypes,[],_OldMap,_NewMap,Return)->
	Return;
get_added_serv_types_map(OldTypes,[Type|T],OldMap,NewMap,{ActTypes,Times,ServicesMap})->
	case lists:member(Type, OldTypes) of
		true->
			NewDetails = maps:get(Type,NewMap),
			OldDetails = maps:get(Type,OldMap),
			case NewDetails==OldDetails of
				true->
					get_added_serv_types_map(OldTypes,T,OldMap,NewMap,{ActTypes,Times,ServicesMap});
				_->
					case is_tuple(NewDetails) andalso (size(NewDetails)==2) andalso is_integer(element(2,NewDetails)) of
						true->
							{ServDetails,Time} = NewDetails;
						_->
							ServDetails = NewDetails,
							Time=0
					end,
					ActType = Type ++" (SPA)",
					get_added_serv_types_map(OldTypes,T,OldMap,NewMap,{[ActType|ActTypes],[Time|Times],maps:put(Type,ServDetails,ServicesMap)})
			end;
		_->
			NewDetails = maps:get(Type,NewMap),
			{ServDetails,Time} = NewDetails,
			ActType = Type ++" (SPA)",
			get_added_serv_types_map(OldTypes,T,OldMap,NewMap,{[ActType|ActTypes],[Time|Times],maps:put(Type,ServDetails,ServicesMap)})
	end.

handle_spa_acts([],_ReceptionMap,_MyRecep,_RAMod,ActiveActs,BusyActs,SchedSPActs,ExeSPActs,ActContracts,BusyContracts)->
	{ActiveActs,BusyActs,SchedSPActs,ExeSPActs,ActContracts,BusyContracts};
handle_spa_acts([H|T],ReceptionMap,MyRecep,RAMod,ActiveActs,BusyActs,SchedSPActs,ExeSPActs,ActContracts,BusyContracts)->	
	ActId = H#stage1Activity.id,
	S1Data = H#stage1Activity.schedule_info#schedule_info.s1data,
	Contract = maps:get(<<"Contract">>,S1Data),
	NewSchedSPActs = remove_act_from_contract_acts(SchedSPActs,Contract,ActId),
	
	case maps:is_key(Contract, ExeSPActs) of
		true->
			notify:new("Could not start service provision activity for contract: "++Contract++" because a previous activity for the same contract is still in execution"),
			spawn_monitor(fun()->act_contract_unavailable(H,ReceptionMap)end),
			handle_spa_acts(T,ReceptionMap,MyRecep,RAMod,ActiveActs,BusyActs,NewSchedSPActs,ExeSPActs,ActContracts,BusyContracts);
		_->
			
			{Continue,NewBusyContracts} = ra_helper_funcs:busy_acts_and_contracts(Contract,BusyContracts,{acts_ready,[H]}),
			
			case Continue of%%incase there is new setup info being added OR the act was JUST added to the schedule and the current state in comms does not have this act's id yet
				true->
					
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,ClientAdr} = business_cards:get_address(ClientBC),
					NewExeSPActs = maps:put(Contract,{ClientAdr,H#stage1Activity.id},ExeSPActs),
					NewActContracts = maps:put(H#stage1Activity.id,Contract,ActContracts),
					spawn_monitor(fun()->ra_handling_funcs:handle_act(H,ReceptionMap,MyRecep,RAMod)end),
					handle_spa_acts(T,ReceptionMap,MyRecep,RAMod,maps:put(ActId,{pending_ah_pid,[]},ActiveActs),maps:put(ActId,[{acts_ready,[H]}],BusyActs),NewSchedSPActs,NewExeSPActs,NewActContracts,NewBusyContracts);
				_->
					handle_spa_acts(T,ReceptionMap,MyRecep,RAMod,ActiveActs,BusyActs,NewSchedSPActs,ExeSPActs,ActContracts,NewBusyContracts)
			end
	end.

act_contract_unavailable(Act,ReceptionMap)->
	{ok,AHPid}=sched_api:start_act_and_get_act_handler(maps:get(sched,ReceptionMap), Act#stage1Activity.id, self()),
	FailedS2Data = #{<<"Result">>=><<"Failed">>,<<"Reason">>=><<"Previous activity for this contract is still active">>},
	act_handler_api:done(AHPid,FailedS2Data).


remove_act_from_contract_acts(SchedSPActs,Contract,ActId)->
	case maps:is_key(Contract, SchedSPActs) of
		true->
			{Adr,ActIds} = maps:get(Contract,SchedSPActs),
			NewActIds = lists:delete(ActId, ActIds),
			case NewActIds of
				[]->
					maps:remove(Contract, SchedSPActs);
				_->
					maps:update(Contract, {Adr,NewActIds}, SchedSPActs)
			end;
		_->
			SchedSPActs
	end.

cancel_sched_acts(_Sched,_Reason,[],MyPid,Contract)->
	custom_erlang_functions:myGenServCall(MyPid,{done_working_on_contract,Contract});
	
cancel_sched_acts(Sched,Reason,[ActId|T],MyPid,Contract)->
	{Res,AHPid}=sched_api:start_act_and_get_act_handler(Sched, ActId),
	case Res of 
		ok->
			act_handler_api:done(AHPid, "Cancelled by requester for reason"++Reason++" before activity was started");
		_->
			ok
	end,
	cancel_sched_acts(Sched,Reason,T,MyPid,Contract).

schedule_new_acts(_Sched,_ActType,[],_S1Data,IDs)->
	IDs;
schedule_new_acts(Sched,ActType,[Tsched|T],S1Data,IDs)->
	{ok,ActId} = sched_api:new_act(Sched, ActType, Tsched, S1Data),
	schedule_new_acts(Sched,ActType,T,S1Data,[ActId|IDs]).

%%spa_handling_funcs
fill_in_proposal_template_and_reply(_ReceptionMap,[],_Mod,Contract,Proposal)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "propose")end),
	contracts:deliver_proposal(Contract,Proposal);
fill_in_proposal_template_and_reply(ReceptionMap,[ProposalField|T],Mod,Contract,Proposal)->
	{ok,FieldDescription} = contracts:get_proposal_field_description(ProposalField),
	{ok,FieldEntryType} = contracts:get_proposal_field_entry_type(ProposalField),
	{ok,Unit} = contracts:get_proposal_field_unit(ProposalField),
	{ok,ServType} = contracts:get_service_type(Contract),
	try
		ProposalEntry = Mod:propose(ReceptionMap,ServType,FieldDescription,FieldEntryType,Unit,Contract),
		case ProposalEntry of
			{reject,Reason}->
				NewProposal = {reject,Reason},
				OtherFields = [];
			_->
				NewProposal = maps:put(ProposalField,ProposalEntry,Proposal),
				OtherFields = T
		end,
		fill_in_proposal_template_and_reply(ReceptionMap,OtherFields,Mod,Contract,NewProposal)
	catch
		A:B:StackTrace->
			error_log:log(?MODULE,0,unknown,"\nERROR: Caught ~p:~p:~p when trying to let ~p make a proposal for service type ~p",[A,B,StackTrace,Mod,ServType]),
			fill_in_proposal_template_and_reply(ReceptionMap,T,Mod,Contract,maps:put(ProposalField,none,Proposal))
	end.

handle_setup_info(ServProvMod,ReceptionMap,Contract,ActIds,Info,ReplyTo)->
	Sched = maps:get(sched,ReceptionMap),
	{ok,Acts} = sched_api:get_acts_by_ids(Sched, ActIds),
	case Acts of 
		[]->
			gen_server:reply(ReplyTo,{reject,"No acts still in schedule for this contract id"});
		_->
			Act = lists:nth(1, Acts),
			S1Data = Act#stage1Activity.schedule_info#schedule_info.s1data,
			Contract = maps:get(<<"Contract">>,S1Data),
			{ok,ServType} = contracts:get_service_type(Contract),
			try
				{AcceptOrReject,NewCustomS1Data} = ServProvMod:setup_info(ReceptionMap,ServType,Contract,Info,Acts),
				case is_map(NewCustomS1Data) and ((AcceptOrReject==accept)or(is_tuple(AcceptOrReject) and size(AcceptOrReject)==2 and(element(1,AcceptOrReject)==reject))) of
					true->
						case NewCustomS1Data==S1Data of
							true->
								ok;
							_->
								FinalS1Data = maps:put(<<"Contract">>, Contract, NewCustomS1Data),
								overwrite_acts(Sched,Acts,FinalS1Data,[])
						end,
						gen_server:reply(ReplyTo,AcceptOrReject);
					_->
						gen_server:reply(ReplyTo,{reject,"Could not handle setup info correctly"})
				end
			catch
				A:B:StackTrace->
					error_log:log(?MODULE,0,unknown,"\nERROR: Caught ~p:~p:~p when trying to let ~p setup info for service type ~p",[A,B,StackTrace,ServProvMod,ServType]),
					gen_server:reply(ReplyTo,{reject,"Could not handle setup info"})
			end
	end,
	custom_erlang_functions:myGenServCall(maps:get(comms,ReceptionMap),{done_working_on_contract,Contract}).

handle_execution_info(ServProvMod,ReceptionMap,Info,AHPid,ReplyTo)->
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	ActId = S2Act#stage2Activity.id,
	try
		{AcceptOrReject,Done,NewS2Data} = ServProvMod:handle_execution_info(S2Act,AHPid,ReceptionMap,Info),
		gen_server:reply(ReplyTo,AcceptOrReject),
		ra_helper_funcs:handle_mod_reply(AcceptOrReject,Done,NewS2Data,S2Data,maps:get(comms,ReceptionMap),ActId,AHPid,ServProvMod,S2Act#stage2Activity.type,"handle_package for")
	catch
		Caught->
				error_log:log(?MODULE,0,unknown,"\nERROR: Caught ~p when trying to let ~p handle execution info for act type ~p",[Caught,ServProvMod,S2Act#stage2Activity.type]),
			gen_server:reply(ReplyTo,{reject,"Could not handle execution info"})
	end,
	custom_erlang_functions:myGenServCall(maps:get(comms,ReceptionMap),{done_working_on_act,ActId}).

overwrite_acts(Sched,[],_S1Data,OverwrittenActs)->
	sched_api:save(Sched, OverwrittenActs);
overwrite_acts(Sched,[Act|T],S1Data,OverwrittenActs)->
	ScheduleInfo = Act#stage1Activity.schedule_info,
	NewScheduleInfo = ScheduleInfo#schedule_info{s1data = S1Data},
	NewAct = Act#stage1Activity{schedule_info = NewScheduleInfo},
	overwrite_acts(Sched,T,S1Data,[NewAct|OverwrittenActs]).

cancel_active_act(AHPid,Reason,ReceptionMap,Mod)->
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	try
		NewS2Data = Mod:service_cancelled(S2Act,ReceptionMap),
		case is_map(NewS2Data) of
			true->
				FinalS2Data = maps:merge(NewS2Data,#{<<"Result">>=> <<"Cancelled">>,<<"Reason">>=>Reason});
			_->
				FinalS2Data = maps:merge(S2Data,#{<<"Result">>=> <<"Cancelled">>,<<"Reason">>=>Reason})
		end,
		ok = act_handler_api:done(AHPid, FinalS2Data)
	catch
		A:B:StackTrace->
			error_log:log(?MODULE,0,unknown,"\n~p could not handle service cancelled for act ~p. Caught: ~p:~p:~p",[Mod,S2Act,A,B,StackTrace]),
			ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
			FinalS2Data2 = maps:merge(S2Data,#{<<"Result">>=> <<"Cancelled">>,<<"Reason">>=>Reason++" AND "++ atom_to_list(Mod) ++ "could not handle service cancelled",<<"Error and stacktrace">>=>ErrorAndStackTrace}),
			ok = act_handler_api:done(AHPid, FinalS2Data2)
	end,
	custom_erlang_functions:myGenServCall(maps:get(comms,ReceptionMap),{done_working_on_act,S2Act#stage2Activity.id}).
	
%%SLOC:224
