%% @author Daniel
%% @doc @todo Add description to spa_and_ra_calls.


-module(spa_and_ra_calls).

%% ====================================================================
%% API functions
%% ====================================================================
-export([done_working_on_act/3,clear_act/6,acts_ready/4,busy_with_unfinished_act/4,spawn_process/4,send_rfps/14,cancel_rfps/4,
		 rfps_timeout/5,proposal/6,send_service_request/11,send_cancel/6,new_sub_contract/4,service_package_delivery/6,service_stopped/6,get_act_contracts_and_give_ah/4,updated_receptions/5,done/5]).
-export([handle_request/4,done_working_on_contract/3,setup_info/6,execution_info/6,cancel_service/6]).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).
-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
done_working_on_act(ServVariables,From,ActId)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			BusyActs = maps:get(busy_acts,ServVariables),
			CurrentVal = maps:get(ActId,BusyActs),
			LastCall = lists:last(CurrentVal),
			NewVal = lists:delete(LastCall, CurrentVal),
			case NewVal of
				[]->
					NewBusyActs = maps:remove(ActId,BusyActs);
				_->
					PendingCall = lists:last(NewVal),
					MyPid = self(),
					spawn_monitor(fun()->custom_erlang_functions:myGenServCall(MyPid,PendingCall)end),
					NewBusyActs = maps:update(ActId, NewVal,BusyActs)
			end,
			NewServVariables = maps:update(busy_acts, NewBusyActs, ServVariables),
			{ok,NewServVariables};			
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.

clear_act(ServVariables,From,ReceptionMap,ActId,Reason,ClearSched)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			ActiveActs = maps:get(active_acts,ServVariables),
			SCDetails = maps:get(sc_details,ServVariables),
			case maps:is_key(ActId, ActiveActs) of
				true->
					{_AHPid,SubContracts} = maps:get(ActId,ActiveActs),
					NewActiveActs = maps:remove(ActId,ActiveActs),
					NewSCDetails = ra_helper_funcs:remove_old_contracts(maps:get(comms,ReceptionMap),SubContracts,SCDetails,Reason);
				_->
					NewActiveActs = ActiveActs,
					NewSCDetails = SCDetails
			end,
			AllActRfps = maps:get(act_rfps,ServVariables),
			Rfps = maps:get(rfps,ServVariables),
			case maps:is_key(ActId,AllActRfps) of
				true->
					ActRfps = maps:get(ActId,AllActRfps),
					NewRfps = ra_helper_funcs:remove_all_rfps_for_act(maps:keys(ActRfps),ActRfps,Rfps);
				_->
					NewRfps = Rfps
			end,
			NewAllActRfps = maps:remove(ActId, AllActRfps),	
			case maps:is_key(exe_sp_acts, ServVariables) of %%indicating SPA iso RA
				true->
					ActContracts = maps:get(act_contracts,ServVariables),
					case maps:is_key(ActId, ActContracts) of
						true->
							Contract = maps:get(ActId,ActContracts),
							NewActContracts = maps:remove(ActId, ActContracts),
							NewExeSpActs = maps:remove(Contract, maps:get(exe_sp_acts,ServVariables)),
							SchedSPActs = maps:get(sched_sp_acts,ServVariables),
							case ClearSched of
								true->	
									spawn_monitor(fun()->contracts:service_stopped(Contract, Reason)end),
									case maps:is_key(Contract, SchedSPActs) of
										true->
											BusyContracts = maps:get(busy_contracts,ServVariables),
											{Continue,NewBusyContracts} = ra_helper_funcs:busy_acts_and_contracts(Contract,BusyContracts,{clear_sched,Contract,Reason}),
											case Continue of
												true->
													spawn_monitor(fun()->spa_helper_funcs:cancel_sched_acts(maps:get(sched,ReceptionMap),Reason,maps:get(Contract,SchedSPActs))end),
													NewSchedSpActs = maps:remove(Contract, SchedSPActs);
												_->
													NewSchedSpActs = SchedSPActs
											end;
										_->
											NewBusyContracts = maps:get(busy_contracts,ServVariables),
											NewSchedSpActs = SchedSPActs
									end;
								_->
									
									case maps:is_key(Contract, SchedSPActs) of
										true->
											ok;
										_->
											spawn_monitor(fun()->contracts:service_stopped(Contract, Reason)end)
									end,							
									NewSchedSpActs = SchedSPActs,
									NewBusyContracts = maps:get(busy_contracts,ServVariables)
							end;
						_->
							NewActContracts = ActContracts,
							NewExeSpActs = maps:get(exe_sp_acts,ServVariables),
							NewSchedSpActs = maps:get(sched_sp_acts,ServVariables),
							NewBusyContracts = maps:get(busy_contracts,ServVariables)
					end,
					
					ChangedServVariables = #{rfps => NewRfps,act_rfps=>NewAllActRfps,active_acts=>NewActiveActs,sc_details=>NewSCDetails,sched_sp_acts=>NewSchedSpActs,exe_sp_acts=>NewExeSpActs,act_contracts=>NewActContracts,new_busy_contracts => NewBusyContracts};
				_->
					ChangedServVariables = #{rfps=>NewRfps,act_rfps=>NewAllActRfps,active_acts=>NewActiveActs,sc_details=>NewSCDetails}
			end,
			NewServVariables = maps:merge(ServVariables,ChangedServVariables),
			{ok,NewServVariables};
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.

acts_ready(ServVariables,From,ReceptionMap,Acts)->
	Sched = maps:get(sched,ReceptionMap),
	Comms = maps:get(comms,ReceptionMap),
	case security_functions:from_is_process_or_monitored_by_process(Sched, From) or security_functions:from_is_process_or_monitored_by_process(Comms, From) of
		true->
			Mod = maps:get(mod,ServVariables),
			ActiveActs = maps:get(active_acts,ServVariables),
			BusyActs = maps:get(busy_acts,ServVariables),
			case maps:is_key(exe_sp_acts, ServVariables) of
				true->
					
					{NewActiveActs,NewBusyActs,NewSchedSPActs,NewExeSPActs,NewActContracts,NewBusyContracts} = spa_helper_funcs:handle_spa_acts(Acts,ReceptionMap,self(),Mod,ActiveActs,BusyActs,maps:get(sched_sp_acts,ServVariables),maps:get(exe_sp_acts,ServVariables),maps:get(act_contracts,ServVariables),maps:get(busy_contracts,ServVariables)),
					%error_log:log(?MODULE,0,unknown,"\nBusyContracts:~p",[NewBusyContracts]),
					ChangedServVariables = #{active_acts=>NewActiveActs,busy_acts=>NewBusyActs,sched_sp_acts => NewSchedSPActs,exe_sp_acts => NewExeSPActs,act_contracts => NewActContracts,busy_contracts=>NewBusyContracts};
				_->
					{NewActiveActs,NewBusyActs} = ra_helper_funcs:handle_ra_acts(Acts,ReceptionMap,self(),Mod,ActiveActs,BusyActs),
					ChangedServVariables = #{active_acts=>NewActiveActs,busy_acts=>NewBusyActs}
			end,
			NewServVariables = maps:merge(ServVariables,ChangedServVariables),
			{ok,NewServVariables};
		_->
			{{error,"Only the schedule of this instance can make this call"},ServVariables}
	end.

busy_with_unfinished_act(ServVariables,From,Act,OldSubContracts)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			ActId = Act#stage2Activity.id,
			ActiveActs = maps:get(active_acts,ServVariables),
			BusyActs = maps:get(busy_acts,ServVariables),
			SCDetails = maps:get(sc_details,ServVariables),
			NewActiveActs = maps:put(ActId,{pending_ah_pid,OldSubContracts},ActiveActs),
			NewSCDetails = ra_helper_funcs:add_contracts(OldSubContracts,SCDetails,ActId),
			NewBusyActs = maps:put(ActId, [{busy_with_unfinished_act,Act,OldSubContracts}], BusyActs),
			case maps:is_key(exe_sp_acts, ServVariables) of
				true->
					S1Data = Act#stage2Activity.schedule_info#schedule_info.s1data,
					Contract = maps:get(<<"Contract">>,S1Data),
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,ClientAdr} = business_cards:get_address(ClientBC),	
					NewExeSPActs = maps:put(Contract, {ClientAdr,ActId}, maps:get(exe_sp_acts,ServVariables)),
					NewSchedSPActs = spa_helper_funcs:remove_act_from_contract_acts(maps:get(sched_sp_acts,ServVariables),Contract,ActId),
					NewActContracts = maps:put(ActId,Contract,maps:get(act_contracts,ServVariables)),
					ChangedServVariables = #{active_acts=>NewActiveActs,busy_acts=>NewBusyActs,sc_details=>NewSCDetails,sched_sp_acts => NewSchedSPActs,exe_sp_acts => NewExeSPActs,act_contracts => NewActContracts};
				_->
					ChangedServVariables = #{active_acts=>NewActiveActs,busy_acts=>NewBusyActs,sc_details=>NewSCDetails}
			end,
			NewServVariables = maps:merge(ServVariables,ChangedServVariables),
			{ok,NewServVariables};
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.

spawn_process(From,Mod,Fun,Args)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			try
				{Pid,_Ref} = spawn_monitor(Mod,Fun,Args),
				{ok,Pid}
			catch
				A:B:StackTrace->
					ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
					{error,ErrorAndStackTrace}
			end;
			
		_->
			{error,"Only my processes can spawn more processes under me"}
	end.

send_rfps(ServVariables,_From,_R,ActId,[],_D,_S,_I,_RA,_PB,_Pr,TimeLimit,Contracts,RfpIdentifier)->
	AllActRfps = maps:get(act_rfps,ServVariables),
	MyPid = self(),
	{ProposalTimeLimiter,_Ref} = spawn_monitor(fun()->timer:sleep(TimeLimit),custom_erlang_functions:myGenServCall(MyPid,{rfps_timeout,ActId,RfpIdentifier})end),
	case maps:is_key(ActId, AllActRfps) of
		true->
			ActRfps = maps:get(ActId,AllActRfps),
			case maps:is_key(RfpIdentifier, ActRfps) of
				true->
					{CurContracts,_CurTimeLimiter} = maps:get(RfpIdentifier,ActRfps),
					NewContracts = [Contracts|CurContracts],
					NewActRfps = maps:update(RfpIdentifier, {NewContracts,ProposalTimeLimiter}, ActRfps);
				_->
					NewActRfps = maps:put(RfpIdentifier, {Contracts,ProposalTimeLimiter}, ActRfps)
			end,
			NewAllActRfps = maps:update(ActId, NewActRfps, AllActRfps);
		_->
			NewActRfps = #{RfpIdentifier=>{Contracts,ProposalTimeLimiter}},
			NewAllActRfps = maps:put(ActId, NewActRfps, AllActRfps)
	end,
	NewServVariables = maps:update(act_rfps, NewAllActRfps, ServVariables),
	{ok,NewServVariables};

send_rfps(ServVariables,From,ReceptionMap,ActId,[ServBC|T],DeliveryAdr,ServType,Interval,RequestArgs,PackageBlueprint,ProposalTemplate,TimeLimit,Contracts,RfpIdentifier)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case maps:is_key(ActId, maps:get(active_acts,ServVariables)) of
				true->
					Comms = maps:get(comms, ReceptionMap),
					MyBc = maps:get(bc,ServVariables),
					{Res,Contract} = contracts:create_contract(MyBc, ServBC, DeliveryAdr, ServType, Interval, RequestArgs, PackageBlueprint,ProposalTemplate),
					case Res of
						ok->
							spawn_monitor(fun()->comms_api:send_rfp(Comms, Contract)end),
							Rfps = maps:get(rfps,ServVariables),
							NewRfps = maps:put(Contract, {ActId,RfpIdentifier}, Rfps),							
							send_rfps(maps:update(rfps, NewRfps, ServVariables),From,ReceptionMap,ActId,T,DeliveryAdr,ServType,Interval,RequestArgs,PackageBlueprint,ProposalTemplate,TimeLimit,[Contract|Contracts],RfpIdentifier);
						_->
							{{error,Contract},ServVariables}
					end;
				_->
					{{error,"This activity id has not been started by me"},ServVariables}
			end;
		_->
			{{error,"Only my spawned processes can make this call"},ServVariables}
	end.

rfps_timeout(ServVariables,From,ReceptionMap,ActId,RfpIdentifier)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			AllActRfps = maps:get(act_rfps,ServVariables),
			case maps:is_key(ActId, AllActRfps) of
				true->
					BusyActs = maps:get(busy_acts,ServVariables),
					{Continue,NewBusyActs} = ra_helper_funcs:busy_acts_and_contracts(ActId,BusyActs,{rfps_timeout,ActId,RfpIdentifier}),
					case Continue of
						true->
							ActRfps = maps:get(ActId,AllActRfps),
							Rfps = maps:get(rfps,ServVariables),
							case maps:is_key(RfpIdentifier, ActRfps) of
								true->
									{LeftOverContracts,_Pid} = maps:get(RfpIdentifier,ActRfps),
									{AHPid,_SubContracts} = maps:get(ActId,maps:get(active_acts,ServVariables)),
									Mod = maps:get(mod,ServVariables),
									MyPid = self(),
									spawn_monitor(fun()->ra_handling_funcs:handle_rfp_timeout(AHPid,Mod,ReceptionMap,MyPid,RfpIdentifier) end),
									NewActRfps = maps:remove(RfpIdentifier, ActRfps),
									case NewActRfps of
										#{}->
											NewAllActRfps = maps:remove(ActId, AllActRfps);
										_->
											NewAllActRfps = maps:update(ActId, NewActRfps, AllActRfps)
									end,
									NewRfps = custom_erlang_functions:remove_keys_from_map(LeftOverContracts, Rfps);
								_->
									NewAllActRfps = AllActRfps,
									NewRfps = Rfps
							end,
							ChangedServVariables = #{act_rfps=>NewAllActRfps,rfps=>NewRfps,busy_acts=>NewBusyActs},
							NewServVariables = maps:merge(ServVariables,ChangedServVariables);
						_->
							NewServVariables = maps:update(busy_acts, NewBusyActs, ServVariables)
					end,
					Reply = ok;
				_->
					Reply = ok,
					NewServVariables = ServVariables
			end;
		_->
			Reply = {error,"Only my own process can make this call"},
			NewServVariables = ServVariables
	end,
	{Reply,NewServVariables}.

proposal(ServVariables,From,ReceptionMap,Contract,Proposal,ReplyTo)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			AdrToCheck = none;
		_->
			case security_functions:from_is_process_or_monitored_by_process(maps:get(comms,ReceptionMap), From) of
				true->
					AdrToCheck = ReplyTo;
				_->
					AdrToCheck = From
			end
	end,
	Rfps = maps:get(rfps,ServVariables),
	case maps:is_key(Contract, Rfps) of
		true->
			{ok,ServBC} = contracts:get_service_bc(Contract),
			{ok,ServAdr} = business_cards:get_address(ServBC),
			case AdrToCheck==none orelse security_functions:from_is_process_or_monitored_by_process(ServAdr, AdrToCheck) of
				true->
					{ActId,RfpIdentifier} = maps:get(Contract,Rfps),
					AllActRfps = maps:get(act_rfps,ServVariables),
					case maps:is_key(ActId, AllActRfps) of
						true->
							BusyActs = maps:get(busy_acts,ServVariables),
							{Continue,NewBusyActs} = ra_helper_funcs:busy_acts_and_contracts(ActId,BusyActs,{proposal,Contract,Proposal,does_not_matter}),
							case Continue of
								true->
									{AHPid,_SubContracts} = maps:get(ActId,maps:get(active_acts,ServVariables)),
									Mod = maps:get(mod,ServVariables),
									MyPid = self(),
									spawn_monitor(fun()->ra_handling_funcs:handle_proposal(RfpIdentifier,Contract,AHPid,Mod,ReceptionMap,MyPid,Proposal) end),
									NewRfps = maps:remove(Contract, Rfps),
									ActRfps = maps:get(ActId,AllActRfps),
									{Contracts,TimeoutPid} = maps:get(RfpIdentifier,ActRfps),
									NewContracts = lists:delete(Contract, Contracts),
									case NewContracts of
										[]->
											exit(TimeoutPid,kill),
											NewActRfps = maps:remove(RfpIdentifier, ActRfps);
										_->
											NewActRfps = maps:update(RfpIdentifier, {NewContracts,TimeoutPid}, ActRfps)		
									end,
									case NewActRfps of
										#{}->
											NewAllActRfps = maps:remove(ActId,AllActRfps);
										_->
											NewAllActRfps = maps:update(ActId, NewActRfps, AllActRfps)
									end,
									ChangedServVariables = #{busy_acts=>NewBusyActs,rfps=>NewRfps,act_rfps=>NewAllActRfps},
									NewServVariables = maps:merge(ServVariables,ChangedServVariables);
								_->
									NewServVariables = maps:update(busy_acts, NewBusyActs, ServVariables)
							end;
						_->
							NewRfps = maps:remove(Contract,Rfps),
							ChangedServVariables = #{rfps=>NewRfps},
							NewServVariables = maps:merge(ServVariables,ChangedServVariables)
					end,
					Reply = accept;
				_->
					Reply = {reject,"Only the service provider to whom the rfp was sent can provide a proposal"},
					NewServVariables = ServVariables
			end;
		_->
			Reply = {reject,"Not expecting proposal for this contract"},
			NewServVariables = ServVariables
	end,
	{Reply,NewServVariables}.

cancel_rfps(ServVariables,From,ActId,RfpIdentifier)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			AllActRfps = maps:get(act_rfps,ServVariables),
			case maps:is_key(ActId, AllActRfps) of
				true->
					ActRfps = maps:get(ActId,AllActRfps),
					Rfps = maps:get(rfps,ServVariables),
					case maps:is_key(RfpIdentifier, ActRfps) of
						true->
							{Contracts,TimeoutPid} = maps:get(RfpIdentifier,ActRfps),
							exit(TimeoutPid,kill),
							NewRfps = custom_erlang_functions:remove_keys_from_map(Contracts, ActRfps),
							NewActRfps = maps:remove(RfpIdentifier, ActRfps);
						_->
							NewActRfps = ActRfps,
							NewRfps = Rfps
					end,
					case NewActRfps of
						#{}->
							NewAllActRfps = maps:remove(ActId,AllActRfps);
						_->
							NewAllActRfps = maps:update(ActId, NewActRfps, AllActRfps)
					end,
					ChangedServVariables = #{rfps=>NewRfps,act_rfps=>NewAllActRfps},
					NewServVariables = maps:merge(ServVariables, ChangedServVariables);
				_->
					NewServVariables = ServVariables
			end,
			{ok,NewServVariables};
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.

send_service_request(ServVariables,From,ReceptionMap,ActId,ServBC,DeliveryAdr,ServType,Interval,RequestArgs,PackageBlueprint,Proposal)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case maps:is_key(ActId, maps:get(active_acts,ServVariables)) of
				true->
					Comms = maps:get(comms, ReceptionMap),
					MyBc = maps:get(bc,ServVariables),
					{Res,Contract} = contracts:create_contract(MyBc, ServBC, DeliveryAdr, ServType, Interval, RequestArgs, PackageBlueprint,Proposal),
					case Res of
						ok->
							MyPid = self(),
							spawn_monitor(fun()->ra_helper_funcs:send_service_request(MyPid,ActId,Comms,Contract,From)end),
							noreply;
						_->
							{error,"Invalid contract args"}
					end;
				_->
					{error,"This activity id has not been started by me"}
			end;
		_->
			{error,"Only my spawned processes can make this call"}
	end.

send_cancel(ServVariables,From,ReceptionMap,ActId,SubContract,Reason)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case contracts:valid_contract(SubContract) of
				true->
					case custom_erlang_functions:is_string(Reason) of
						true->
							SCDetails = maps:get(sc_details,ServVariables),
							case maps:is_key(SubContract, SCDetails) of
								true->
									{_Adr,C_ActId} = maps:get(SubContract,SCDetails),
									case C_ActId==ActId of
										true->
											spawn_monitor(fun()->comms_api:send_cancel_service(maps:get(comms,ReceptionMap), SubContract, Reason)end),
											NewSCDetails = maps:remove(SubContract, SCDetails),
											ActiveActs = maps:get(active_acts,ServVariables),
											{AHPid,SubContracts} = maps:get(ActId,ActiveActs),
											NewSubContracts = lists:delete(SubContract, SubContracts),
											BusyActs = maps:get(busy_acts,ServVariables),
											case maps:is_key(ActId, BusyActs) or not(is_pid(AHPid)) of
												true->
													ok;
												_->
													{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
													S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
													NewS2Data = maps:put(<<"Contracts">>,NewSubContracts,S2Data),
													act_handler_api:update_progress(AHPid, NewS2Data)
											end,
											NewActiveActs = maps:update(ActId, {AHPid,NewSubContracts}, ActiveActs),
											ChangedServVariables = #{active_acts=>NewActiveActs,sc_details=>NewSCDetails},
											NewServVariables = maps:merge(ServVariables,ChangedServVariables),
											{ok,NewServVariables};
										_->
											{{error,"Wrong act id for this contract given"},ServVariables}
									end;
								_->
									{{error,"Contract not in state"},ServVariables}
							end;
						_->
							{{error,"Reason must be a string"},ServVariables}
					end;
				_->
					{{error,"Contract not valid"},ServVariables}
			end;
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.

new_sub_contract(ServVariables,From,ActId,SubContract)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			ActiveActs = maps:get(active_acts,ServVariables),
			{AHPid,SubContracts} = maps:get(ActId,ActiveActs),
			NewSubContracts = [SubContract|SubContracts],
			NewActiveActs = maps:update(ActId, {AHPid,NewSubContracts}, ActiveActs),
			BusyActs = maps:get(busy_acts,ServVariables),
			case maps:is_key(ActId, BusyActs) or not(is_pid(AHPid)) of
				true->
					ok;
				_->
					{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
					S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
					NewS2Data = maps:put(<<"Contracts">>,NewSubContracts,S2Data),
					act_handler_api:update_progress(AHPid, NewS2Data)
			end,
			{ok,ServBC} = contracts:get_service_bc(SubContract),
			{ok,Adr} = business_cards:get_address(ServBC),
			NewSCDetails = maps:put(SubContract,{Adr,ActId},maps:get(sc_details,ServVariables)),
			ChangedServVariables = #{active_acts=>NewActiveActs,sc_details=>NewSCDetails},
			NewServVariables = maps:merge(ServVariables,ChangedServVariables),
			{ok,NewServVariables};
		_->
			{{error,"Only my spawned processes can make this call"},ServVariables}
	end.

service_package_delivery(ServVariables,From,ReceptionMap,Contract,Package,ReplyTo)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			Requester = ReplyTo,
			AdrToCheck = none;
		_->
			Requester = From,
			case security_functions:from_is_process_or_monitored_by_process(maps:get(comms,ReceptionMap), From) of
				true->
					AdrToCheck = ReplyTo;
				_->
					AdrToCheck = From
			end
	end,
	SCDetails = maps:get(sc_details,ServVariables),
	case maps:is_key(Contract, SCDetails) of
		true->
			{AdrPid,ActId} = maps:get(Contract,maps:get(sc_details,ServVariables)),
			case AdrToCheck==none orelse security_functions:from_is_process_or_monitored_by_process(AdrPid, AdrToCheck) of
				true->
					Mod = maps:get(mod,ServVariables),
					MyPid = self(),
					BusyActs = maps:get(busy_acts,ServVariables),
					{Continue,NewBusyActs} = ra_helper_funcs:busy_acts_and_contracts(ActId,BusyActs,{service_package_delivery,Contract,Package,Requester}),
					case Continue of
						true->
							{AHPid,_SubContracts} = maps:get(ActId,maps:get(active_acts,ServVariables)),
							spawn_monitor(fun()->ra_handling_funcs:handle_package(Contract,AHPid,Mod,ReceptionMap,MyPid,Package,Requester) end);
						_->
							ok
					end,
					NewServVariables = maps:update(busy_acts, NewBusyActs, ServVariables),
					case security_functions:from_is_process_or_monitored_by_process(self(), From) of
						true->
							{ok,NewServVariables};
						_->
							{noreply,NewServVariables}
					end;
				_->
					custom_erlang_functions:gen_serv_reply_if_i_called(From,{reject,"Only the service provider's comms reception can make this call"}),
					{{reject,"Only the service provider's comms reception can make this call"},ServVariables}					
			end;
		_->
			custom_erlang_functions:gen_serv_reply_if_i_called(From,{reject,"No contract with this id in my state"}),
			{{reject,"No contract with this id in my state"},ServVariables}
	end.

service_stopped(ServVariables,From,ReceptionMap,Contract,Reason,ReplyTo)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			Requester = ReplyTo,
			AdrToCheck = none;
		_->
			Requester = From,
			case security_functions:from_is_process_or_monitored_by_process(maps:get(comms,ReceptionMap), From) of
				true->
					AdrToCheck = ReplyTo;
				_->
					AdrToCheck = From
			end
	end,
	SCDetails = maps:get(sc_details,ServVariables),
	case maps:is_key(Contract, SCDetails) of
		true->
			{AdrPid,ActId} = maps:get(Contract,SCDetails),
			case AdrToCheck==none orelse security_functions:from_is_process_or_monitored_by_process(AdrPid, AdrToCheck) of
				true->
					Mod = maps:get(mod,ServVariables),
					MyPid = self(),
					BusyActs = maps:get(busy_acts,ServVariables),
					ActiveActs = maps:get(active_acts,ServVariables),
					{Continue,NewBusyActs} = ra_helper_funcs:busy_acts_and_contracts(ActId,BusyActs,{service_stopped,Contract,Reason,Requester}),
					case Continue of
						true->
							{AHPid,SubContracts} = maps:get(ActId,ActiveActs),
							NewSCDetails = maps:remove(Contract, SCDetails),
							NewSubContracts = lists:delete(Contract, SubContracts),
							case is_pid(AHPid) of
								true->
									{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
									S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
									NewS2Data = maps:put(<<"Contracts">>,NewSubContracts,S2Data),
									act_handler_api:update_progress(AHPid, NewS2Data);
								_->
									ok
							end,
							NewActiveActs = maps:update(ActId, {AHPid,NewSubContracts}, ActiveActs),
							spawn_monitor(fun()->ra_handling_funcs:handle_serv_stopped(Contract,AHPid,Mod,ReceptionMap,MyPid,Reason) end);
														
						_->
							NewActiveActs = ActiveActs,
							NewSCDetails = SCDetails
					end,
					ChangedServVariables = #{active_acts=>NewActiveActs,sc_details=>NewSCDetails,busy_acts=>NewBusyActs},
					NewServVariables = maps:merge(ServVariables,ChangedServVariables),
					{ok,NewServVariables};
				_->
					{{error,"Only the service provider's comms reception can make this call"},ServVariables}
			end;
		_->
			{{error,"No contract with this id in my state"},ServVariables}
	end.

get_act_contracts_and_give_ah(ServVariables,From,ActId,AHPid)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			{_OldAHPid,SubContracts} = maps:get(ActId,maps:get(active_acts,ServVariables)),
			NewActiveActs = maps:update(ActId, {AHPid,SubContracts}, maps:get(active_acts,ServVariables)),
			AllActRfps = maps:get(act_rfps,ServVariables),
			case maps:is_key(ActId, AllActRfps) of
				true->
					ActRfps = maps:get(ActId,AllActRfps);
				_->
					ActRfps = #{}
			end,
			{{ok,SubContracts,ActRfps},maps:update(active_acts, NewActiveActs, ServVariables)};
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.

updated_receptions(ServVariables,From,OldReceptionMap,Coordinator,NewReceptionMap)->
	case security_functions:from_is_process_or_monitored_by_process(Coordinator, From) of
		true->
			FileChecker = maps:get(file_checker,ServVariables),
			case is_pid(FileChecker) andalso is_process_alive(FileChecker) of
				true->
					exit(FileChecker,kill);%%ensuring only one process checks if this resources services has changed
				_->
					ok
			end,
			MyPid = self(),
			Mod = maps:get(mod,ServVariables),
			case self()==maps:get(comms,OldReceptionMap) of
				true->
					Services = maps:get(services,ServVariables),
					{Pid,_Ref}=spawn_monitor(fun()->spa_helper_funcs:get_file_and_register_loop(Mod,NewReceptionMap,MyPid,Services) end);
				_->
					{Pid,_Ref}=spawn_monitor(fun()->ra_helper_funcs:get_file_and_register_loop(Mod,NewReceptionMap,MyPid,#{}) end)
			end,
			{ok,maps:update(file_checker, Pid, ServVariables),NewReceptionMap};
		_->
			{{error,"Only the coordinator of this instance can make this call"},ServVariables,OldReceptionMap}
	end.
%%%done call from processes spawned by mod 
done(ServVariables,From,ActId,FinalS2Data,ClearSched)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			ActiveActs = maps:get(active_acts,ServVariables),
			case maps:is_key(ActId,ActiveActs) of
				true->
					BusyActs = maps:get(busy_acts,ServVariables),
					{Continue,NewBusyActs} = ra_helper_funcs:busy_acts_and_contracts(ActId,BusyActs,{done,ActId,FinalS2Data}),
					case Continue of
						true->
							{AHPid,_SubContracts} = maps:get(ActId,maps:get(active_acts,ServVariables)),
							MyPid = self(),
							spawn_monitor(fun()->ra_helper_funcs:handle_done(ActId,AHPid,MyPid,FinalS2Data,ClearSched) end);
						_->
							ok
					end,
					{ok,maps:update(busy_acts, NewBusyActs, ServVariables)};
				_->
					{{error,"No activity with this id"},ServVariables}
			end;
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.



%%%SPA-specific calls
handle_request(ServProvMod,ReceptionMap,Contract,Requester)->
	{ok,ServType} = contracts:get_service_type(Contract),
	{ok,ClientBC} = contracts:get_client_bc(Contract),
	try
		{AcceptOrReject,TschedList,CustomS1DataMap,_AcceptSetupInfo} = ServProvMod:handle_request(ReceptionMap,ServType,Contract),%%just return a empty TschedList to schedule no activity
		case AcceptOrReject of 
			accept->
				spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "accept")end),
				case custom_erlang_functions:isListOfIntegers(TschedList) and is_map(CustomS1DataMap) of
					true->
						
						S1Data = maps:put(<<"Contract">>, Contract, CustomS1DataMap),
						ActType = ServType++" (SPA)",
						ActIds = spa_helper_funcs:schedule_new_acts(maps:get(sched,ReceptionMap),ActType,TschedList,S1Data,[]),
						case (length(ActIds)>0) of
							true->
								{ok,ClientBC} = contracts:get_client_bc(Contract),
								{ok,ReqAdr} = business_cards:get_address(ClientBC),
								
								custom_erlang_functions:myGenServCall(maps:get(comms,ReceptionMap),{new_serv_prov_acts,Contract,ReqAdr,ActIds});
							_->
								ok
						end,
						gen_server:reply(Requester,AcceptOrReject);
					_->
						notify:new(atom_to_list(ServProvMod)++" could not handle request because it returned incorrect reply parameters TschedList: ~p and CustomS1DataMap: ~p",[TschedList,CustomS1DataMap]),
						gen_server:reply(Requester,{reject,"Could not handle request"})
				end;
			_->
				spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "reject")end),
				gen_server:reply(Requester,AcceptOrReject)
		end
	catch
		A:B:StackTrace->
			error_log:log(?MODULE,0,unknown,"\nERROR: Caught: ~p:~p:~p when trying to let ~p handle request",[A,B,StackTrace,ServProvMod]),
			notify:new(atom_to_list(ServProvMod)++" could not handle request"),
			gen_server:reply(Requester,{reject,"Could not handle request"})
	end,
	custom_erlang_functions:myGenServCall(maps:get(comms,ReceptionMap),{done_working_on_contract,Contract}).

done_working_on_contract(ServVariables,From,Contract)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			BusyContracts = maps:get(busy_contracts,ServVariables),
			case maps:is_key(Contract, BusyContracts) of
				true->
					CurrentVal = maps:get(Contract,BusyContracts),
					LastCall = lists:last(CurrentVal),
					NewVal = lists:delete(LastCall, CurrentVal),
					case NewVal of
						[]->
							NewBusyContracts = maps:remove(Contract,BusyContracts);
						_->
							PendingCall = lists:last(NewVal),
							%error_log:log(?MODULE,0,unknown,"\nPendingCall:~p",[PendingCall]),
							MyPid = self(),
							spawn_monitor(fun()->custom_erlang_functions:myGenServCall(MyPid,PendingCall)end),
							NewBusyContracts = maps:update(Contract, NewVal,BusyContracts)
					end,
					NewServVariables = maps:update(busy_contracts, NewBusyContracts, ServVariables),
					{ok,NewServVariables};	
				_->
					error_log:log(?MODULE,0,unknown,"\nERROR: Contract ~p was not in busy contracts: ~p",[Contract,BusyContracts]),
					{ok,ServVariables}
			end;
		_->
			{{error,"Only my own processes can make this call"},ServVariables}
	end.

setup_info(ServVariables,From,ReceptionMap,Contract,Info,ReplyTo)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			Requester = ReplyTo;
		_->
			Requester = From
	end,
	SchedSPActs = maps:get(sched_sp_acts,ServVariables),
	case maps:is_key(Contract, SchedSPActs) of
		true->
			{ReqAdr,ActIds} = maps:get(Contract,SchedSPActs),
			case security_functions:from_is_process_or_monitored_by_process(ReqAdr, Requester) of
				true->
					ServProvMod = maps:get(mod,ServVariables),
					BusyContracts = maps:get(busy_contracts,ServVariables),
					{Continue,NewBusyContracts} = ra_helper_funcs:busy_acts_and_contracts(Contract,BusyContracts,{setup_info,Contract,Info,Requester}),
					case Continue of
						true->
							spawn_monitor(fun()->spa_helper_funcs:handle_setup_info(ServProvMod,ReceptionMap,Contract,ActIds,Info,Requester)end);
						_->
							ok
					end,
					NewServVariables = maps:update(busy_contracts, NewBusyContracts, ServVariables),
					case security_functions:from_is_process_or_monitored_by_process(self(), From) of
						true->
							{ok,NewServVariables};
						_->
							{noreply,NewServVariables}
					end;
				_->
					custom_erlang_functions:gen_serv_reply_if_i_called(From,{reject,"Only the service provider's comms reception can make this call"}),
					{{reject,"Only the service provider's comms reception can make this call"},ServVariables}
			end;					
		_->
			custom_erlang_functions:gen_serv_reply_if_i_called(From,{reject,"No activities in schedule for this contract id"}),
			{{reject,"No activities in schedule for this contract id"},ServVariables}
	end.

execution_info(ServVariables,From,ReceptionMap,Contract,Info,ReplyTo)->
	ServProvPlugins = maps:get(serv_prov_plugins,ServVariables),
	case maps:is_key(Contract, ServProvPlugins) of
		true->
			{Adr,PluginPid} = maps:get(Contract,ServProvPlugins),
			case security_functions:from_is_process_or_monitored_by_process(Adr, From) of
				true->
					spawn_monitor(fun()->forward_to_plugin(PluginPid,{execution_info,Contract,Info,From},From)end),
					{noreply,ServVariables};
				_->
					{{reject,"Only the service provider's comms reception can make this call"},ServVariables}
			end;
		_->
			case security_functions:from_is_process_or_monitored_by_process(self(), From) of
				true->
					Requester = ReplyTo;
				_->
					Requester = From
			end,
			ExeSPActs = maps:get(exe_sp_acts,ServVariables),
			case maps:is_key(Contract, ExeSPActs) of
				true->
					{ReqAdr,ActId} = maps:get(Contract,ExeSPActs),
					case security_functions:from_is_process_or_monitored_by_process(ReqAdr, Requester) of
						true->
							ServProvMod = maps:get(mod,ServVariables),
							BusyActs = maps:get(busy_acts,ServVariables),
							{Continue,NewBusyActs} = ra_helper_funcs:busy_acts_and_contracts(ActId,BusyActs,{execution_info,Contract,Info,Requester}),
							case Continue  of
								true->	
									ActiveActs = maps:get(active_acts,ServVariables),
									{AHPid,_SubContracts} = maps:get(ActId,ActiveActs),
									spawn_monitor(fun()->spa_helper_funcs:handle_execution_info(ServProvMod,ReceptionMap,Info,AHPid,Requester)end);%%subcontracts are kept in s2data
								_->
									ok
							end,
							NewServVariables = maps:update(busy_acts, NewBusyActs, ServVariables),
							case security_functions:from_is_process_or_monitored_by_process(self(), From) of
								true->
									{ok,NewServVariables};
								_->
									{noreply,NewServVariables}
							end;
						_->
							custom_erlang_functions:gen_serv_reply_if_i_called(From,{reject,"Only the service provider's comms reception can make this call"}),
							{{reject,"Only the service provider's comms reception can make this call"},ServVariables}
					end;					
				_->
					custom_erlang_functions:gen_serv_reply_if_i_called(From,{reject,"No activities in execution for this contract id"}),
					{{reject,"No activities in execution for this contract id"},ServVariables}
			end
	end.

cancel_service(ServVariables,From,ReceptionMap,Contract,Reason,ReplyTo)->
	ServProvPlugins = maps:get(serv_prov_plugins,ServVariables),
	case maps:is_key(Contract, ServProvPlugins) of
		true->
			{Adr,PluginPid} = maps:get(Contract,ServProvPlugins),
			case security_functions:from_is_process_or_monitored_by_process(Adr, From) of
				true->
					spawn_monitor(fun()->forward_to_plugin(PluginPid,{cancel_service,Contract,Reason,From},From)end),
					NewServProvPlugins = maps:remove(Contract, ServProvPlugins),
					NewServVariables = maps:update(serv_prov_plugins, NewServProvPlugins, ServVariables),
					{noreply,NewServVariables};
				_->
					{{reject,"Only the service provider's comms reception can make this call"},ServVariables}
			end;
		_->
			case security_functions:from_is_process_or_monitored_by_process(self(), From) of
				true->
					Requester = ReplyTo;
				_->
					Requester = From
			end,
			BusyContracts = maps:get(busy_contracts,ServVariables),
			{Continue,NewBusyContracts} = ra_helper_funcs:busy_acts_and_contracts(Contract,BusyContracts,{cancel_service,Contract,Reason, Requester}),
			case Continue of
				true->	
					SchedSPActs = maps:get(sched_sp_acts,ServVariables),
					case maps:is_key(Contract, SchedSPActs) of
						true->
							{ReqAdr,ActIds} = maps:get(Contract, SchedSPActs),
							case security_functions:from_is_process_or_monitored_by_process(ReqAdr, Requester) of
								true->	
									spawn_monitor(fun()->spa_helper_funcs:cancel_sched_acts(maps:get(sched,ReceptionMap), Reason, ActIds,maps:get(comms,ReceptionMap),Contract)end),
									NewSchedSPActs = maps:remove(Contract, SchedSPActs),
									NewServVariables1 = #{busy_contracts => NewBusyContracts,sched_sp_acts => NewSchedSPActs},
									Reply1 = ok;
								_->
									NewServVariables1 = #{busy_contracts => NewBusyContracts},
									Reply1 = not_allowed
							end;
						_->
							NewServVariables1 = #{busy_contracts => NewBusyContracts},
							Reply1 = no_contract
					end;
					
				_->
					%error_log:log(?MODULE,0,unknown,"\nCant continue.NewBusyContracts:~p",[NewBusyContracts]),
					NewServVariables1 = #{busy_contracts => NewBusyContracts},
					Reply1 = ok
			end,
			ExeSPActs = maps:get(exe_sp_acts,ServVariables),
			case maps:is_key(Contract, ExeSPActs) of
				true->
					{ReqAdr2,ActId} = maps:get(Contract,ExeSPActs),
					{AHPid,SubContracts} = maps:get(ActId,maps:get(active_acts,ServVariables)),
					BusyActs = maps:get(busy_acts,ServVariables),
					case security_functions:from_is_process_or_monitored_by_process(self(), From) orelse security_functions:from_is_process_or_monitored_by_process(ReqAdr2, Requester) of
						true->
							SCDetails = maps:get(sc_details,ServVariables),
							{Continue2,NewBusyActs} = ra_helper_funcs:busy_acts_and_contracts(ActId,BusyActs,{cancel_service,Contract,Reason, Requester}),
							case Continue2  of
								true->	
									spawn_monitor(fun()->spa_helper_funcs:cancel_active_act(AHPid,Reason,ReceptionMap,maps:get(mod,ServVariables))end),
									NewSCDetails = ra_helper_funcs:remove_old_contracts(maps:get(comms,ReceptionMap), SubContracts, SCDetails, "Servie request was cancelled for reason:"++Reason),
									NewExeSPActs = maps:remove(Contract, ExeSPActs),
									NewActiveActs = maps:remove(ActId, maps:get(active_acts,ServVariables)),
									NewActContracts = maps:remove(ActId, maps:get(act_contracts,ServVariables));
								_->
									NewSCDetails = SCDetails,
									NewExeSPActs = ExeSPActs,
									NewActiveActs = maps:get(active_acts,ServVariables),
									NewActContracts = maps:get(act_contracts,ServVariables)
							end,
							NewServVariables2 = #{busy_acts => NewBusyActs,sc_details => NewSCDetails,exe_sp_acts => NewExeSPActs,active_acts => NewActiveActs,act_contracts=>NewActContracts},
							Reply2 = ok;
						_->
							NewServVariables2 = ServVariables,
							Reply2 = not_allowed
					end;
				_->
					NewServVariables2 = ServVariables,
					Reply2 = no_contract
			end,	
			Temp = maps:merge(NewServVariables2,NewServVariables1),
			NewServVariables = maps:merge(ServVariables,Temp),
			case ((Reply1==not_allowed) or (Reply2==not_allowed)) of
				true->
					{{error,"Only the service requester can cancel a service"},ServVariables};
				_->
					case ((Reply1==no_contract) and (Reply2==no_contract)) of
						true->
							{{error,"Not handling any contracts with this id"},ServVariables};
						_->
							{ok,NewServVariables}
					end
			end
	end.

forward_to_plugin(PluginPid,Call,From)->
	Reply = custom_erlang_functions:myGenServCall(PluginPid,Call),
	custom_erlang_functions:gen_serv_reply_if_from(From, Reply).

%%SLOC:867
