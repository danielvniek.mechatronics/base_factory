%%the coordinator's responsibilities are:
%1. to ensure the instance's core componenets have the latest pids of each other
%2. to backup each components state so it can be passed back to them if they fail??


-module(coordinator).
-author("Daniel van Niekerk").

-behaviour(gen_server).
%% API
-export([start_link/2]).
-include("../support/base_records.hrl").
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).
-record(coord_state, {instance_pids::term(),instance_sup::pid(),
                      ready :: true|false ,
                      plugins::term(),
					  plugin_sup_monitors::term(),
					  children_ids::list(),
					  plugin_reply_to::term()}).

%%%===================================================================
%%% API
%%%===================================================================
start_link(BC, InstanceSup) ->
  gen_server:start_link(?MODULE, [BC,InstanceSup], []).
	
%%%===================================================================
%%% Gen_server callbacks
%%%===================================================================

init([BC,InstanceSup]) ->
  Plugins = get_plugins_from_settings_file(BC),
  PluginsWithPids = plugins_with_pids_converter(Plugins,#{}),
  {ok, #coord_state{
                    instance_pids = #instance_pids{coordinator = self()},
                    ready = false,
					plugins = PluginsWithPids,
					plugin_reply_to = none,
					plugin_sup_monitors = #{},
					instance_sup = InstanceSup,
					children_ids = []
					}
                    }.

handle_call({core_component_started,Component},From,State)->
	ExistingPid = get_existing_component_pid(Component,State#coord_state.instance_pids),
	case ExistingPid of
		unrecognized->
			{reply,{error,"Unrecognized component"},State};
		_->
			{Pid,_Ref} = From,				
			case (ExistingPid==undefined) orelse (is_process_alive(ExistingPid)==false) of
				true->
					NewInstancePids = update_component_pid(Component,Pid,State#coord_state.instance_pids),
					NewState = State#coord_state{instance_pids = NewInstancePids},
					case NewState#coord_state.ready of
						true->
							{ok,MyBC} = comms_api:get_bc(State#coord_state.instance_pids#instance_pids.comms_recep),
							{ok,MyID} = business_cards:get_id(MyBC),
							error_log:log(Component, "N/A", MyID, "Core component failed and was restarted"),
							spawn_monitor(fun()->share_plugin_pids(NewState)end),
							spawn_monitor(fun()->share_instance_pids(NewState#coord_state.instance_pids)end),
							spawn_monitor(fun()->send_new_receptions_to_plugins(NewState)end),
							{reply,ok,NewState};
						_->
							case is_instance_ready(NewState) of
								ready->	
									PluginSupPid = NewState#coord_state.instance_pids#instance_pids.plugin_sup,
									Plugins = maps:keys(State#coord_state.plugins),
									CoordPid = self(),
									spawn_monitor(fun()->share_instance_pids_and_start_plugins(NewState#coord_state.instance_pids,Plugins,PluginSupPid,CoordPid)end),
									{reply,ok,NewState};
								_->
									{reply,ok,NewState}
							end
					end;
				_->
					{reply,{error,"The process registered for this componenent is still alive"},State}
			end
	end;

handle_call(all_plugins_started,From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case not(State#coord_state.ready) of
				true->
					CommsRecep = State#coord_state.instance_pids#instance_pids.comms_recep,
					ok = custom_erlang_functions:myGenServCall(CommsRecep,instance_ready),		
					NewState = State#coord_state{ready = true};
				_->
					case State#coord_state.plugin_reply_to of
						none->
							NewState = State;
						_->
							ReplyTo = State#coord_state.plugin_reply_to,
							gen_server:reply(ReplyTo, ok),
							NewPluginReplyTo = none,
							NewState = State#coord_state{plugin_reply_to = NewPluginReplyTo}
					end
			end,
			{reply,ok,NewState};
		_->
			{reply,{error,"Only a coordinator process can make this call"},State}
	end;

handle_call({register_plugin_and_get_receptions,PluginDetailsMap,PluginSup},From,State)->
	{PluginPid,_Ref} = From,
	case maps:is_key(PluginDetailsMap,State#coord_state.plugins) of
		true->
			PluginDetails =  maps:get(PluginDetailsMap,State#coord_state.plugins),
			case is_tuple(PluginDetails) of
				true->
					{_PluginSup,RegisteredPid} = PluginDetails;
				_->
					RegisteredPid = none
			end,
			case (PluginDetails==pending) orelse (is_process_alive(RegisteredPid)==false) orelse (RegisteredPid == PluginPid) of
				true->
					NewPlugins = maps:update(PluginDetailsMap, {PluginSup,PluginPid}, State#coord_state.plugins),			
					NewState = State#coord_state{plugins = NewPlugins},		
					spawn_monitor(fun()->share_plugin_pids(NewState)end),
					ReceptionMap = #{bio => State#coord_state.instance_pids#instance_pids.bio_recep,
									sched=>State#coord_state.instance_pids#instance_pids.sched_recep,
									atr=>State#coord_state.instance_pids#instance_pids.atr_recep,
									vs=>State#coord_state.instance_pids#instance_pids.atr_recep,
									exe=>State#coord_state.instance_pids#instance_pids.exe_recep,
									comms=>State#coord_state.instance_pids#instance_pids.comms_recep},
					{reply,{ok,ReceptionMap},NewState};
				_->
					{reply,{error,"Another plugin process is already registered for these plugindetails"},State}
			end;
		_->
			{reply,{error,"Coordinator not expecting plugin pid from plugin with these details"},State}
	end;

handle_call({get_plugin_pid,PluginDetailsMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#coord_state.instance_pids#instance_pids.comms_recep, From) of
		true->
			case maps:is_key(PluginDetailsMap,State#coord_state.plugins) of
				true->
					{reply,{ok,maps:get(PluginDetailsMap,State#coord_state.plugins)},State};
				_->
					{reply,{error,"No plugin with these details"},State}
			end;
		_->
			{reply,{error,"Only my comms recep can make this call"},State}
	end;
	
handle_call({failed_plugin_startup,PluginDetailsMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case maps:is_key(PluginDetailsMap,State#coord_state.plugins) of
				true->
					NewPlugins = maps:update(PluginDetailsMap, failed, State#coord_state.plugins),			
					NewState = State#coord_state{plugins = NewPlugins},
					case State#coord_state.plugin_reply_to of
						none->
							NNewState = NewState;
						_->
							ReplyTo = State#coord_state.plugin_reply_to,
							gen_server:reply(ReplyTo, {error,"Could not start plugin"}),
							NNewState = State#coord_state{plugin_reply_to = none}
					end,
					{reply,ok,NNewState};
				_->
					error_log:log(?MODULE,0,unknown,"ERROR: Coordinator got failed plugin call for plugin that it does not have in its state"),
					{reply,error,State}
			end;
		_->
			{reply,{error,"Only a coordinator process can make this call"},State}
	end;
handle_call({new_plugin_sup,PluginMap,PluginSup},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case maps:is_key(PluginMap,State#coord_state.plugins) of
				true->
					MonitorRef = erlang:monitor(process,PluginSup),
					NewMonitorRefMap = maps:put(MonitorRef, PluginMap, State#coord_state.plugin_sup_monitors),
					{reply,ok,State#coord_state{plugin_sup_monitors = NewMonitorRefMap}};
				_->
					error_log:log(?MODULE,0,unknown,"ERROR: Coordinator got new plugin sup pid for plugin that it does not have in its state"),
					{reply,ok,State}
			end;
		_->
			{reply,{error,"Only a coordinator process can make this call"},State}
	end;
handle_call({get_plugins_with_status,Requester},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#coord_state.instance_pids#instance_pids.comms_recep, From) of
		true->
			case Requester of
				direct->
					{reply,{ok,State#coord_state.plugins},State};
				_->
					gen_server:reply(Requester,{ok,State#coord_state.plugins}),
					{reply,ok,State}
			end;			
		_->
			{reply,{error,"Only comms recep can make this call"},State}
	end;
handle_call({add_plugin,PluginMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#coord_state.instance_pids#instance_pids.comms_recep, From) of
		true->
			case maps:is_key(PluginMap, State#coord_state.plugins) andalso (not(maps:get(PluginMap,State#coord_state.plugins)==failed)) of
				false->
					CoordPid = self(),
					spawn_monitor(plugin_startup_functions,start_plugins,[[PluginMap],State#coord_state.instance_pids#instance_pids.plugin_sup,CoordPid]),
					NewPluginMap = maps:put(PluginMap,pending,State#coord_state.plugins),
					{noreply,State#coord_state{plugin_reply_to = From,plugins=NewPluginMap}};
				_->
					{reply,{error,"Plugin with same plugin details already exists"},State}
			end;
		_->
			{reply,{error,"Only comms recep can make this call"},State}
	end;
handle_call({remove_plugin,PluginMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#coord_state.instance_pids#instance_pids.comms_recep, From) of
		true->
			case maps:is_key(PluginMap, State#coord_state.plugins) of
				true->
					PluginStatus = maps:get(PluginMap, State#coord_state.plugins),
					case is_tuple(PluginStatus) of
						true->
							{PluginSup,PluginPid} = PluginStatus,
							custom_erlang_functions:terminate_spawned_processes(PluginPid),
							PluginsSupPid = State#coord_state.instance_pids#instance_pids.plugin_sup,
							supervisor:terminate_child(PluginsSupPid, PluginSup);
						_->
							ok
					end,
					NewPlugins = maps:remove(PluginMap, State#coord_state.plugins),
					{reply,ok,State#coord_state{plugins = NewPlugins,plugin_reply_to = From}};
				_->
					{reply,{error,"No plugin with this plugin map exists in this instance"},State}
			end;
		_->
			{reply,{error,"Only comms recep can make this call"},State}
	end;

handle_call({instance_sup,RequesterFrom},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#coord_state.instance_pids#instance_pids.comms_recep, From) of
		true->
			gen_server:reply(RequesterFrom,State#coord_state.instance_sup);
		_->
		gen_server:reply(RequesterFrom,{error,"Only comms recep can make this call"})
	end,
	{noreply,State};
handle_call(get_all_instance_pids,From,State)->		
	case security_functions:from_is_process_or_monitored_by_process(State#coord_state.instance_pids#instance_pids.comms_recep, From) of
		true->
			IPs = State#coord_state.instance_pids,
			InstancePidsList = [IPs#instance_pids.atr_recep,IPs#instance_pids.bio_recep,IPs#instance_pids.exe_recep,IPs#instance_pids.sched_recep,IPs#instance_pids.atr_def_storage,IPs#instance_pids.bio_def_storage,IPs#instance_pids.exe_def_storage,IPs#instance_pids.sched_def_storage,self(),IPs#instance_pids.comms_recep],
			{reply,{ok,InstancePidsList},State};
		_->
			{reply,{error,"Only my comms can make this call"},State}
	end;


handle_call(Unknown,_From,State)->
  error_log:log(?MODULE,0,unknown,"!! Coordinator reception: unknown message received: ~p",[Unknown]),
  {reply,not_understood,State}.
%%--------------------------------------------------------------------


handle_cast(_Request, State) ->
  {noreply, State}.

handle_info({'DOWN', MonitorRef, process, _Object, _Info},State)->
	AllMonitorRefs = maps:keys(State#coord_state.plugin_sup_monitors),
	case lists:member(MonitorRef, AllMonitorRefs) of
		true->
			PluginDetailsMap = maps:get(MonitorRef,State#coord_state.plugin_sup_monitors),
			case maps:is_key(PluginDetailsMap, State#coord_state.plugins) of
				true->
					NewPlugins = maps:update(PluginDetailsMap, failed, State#coord_state.plugins);
				_->%%when plugin was removed
					NewPlugins = State#coord_state.plugins
			end,
			NewMonitors = maps:remove(MonitorRef, State#coord_state.plugin_sup_monitors),
			demonitor(MonitorRef),
			NewState = State#coord_state{plugins = NewPlugins,plugin_sup_monitors = NewMonitors},
			spawn_monitor(fun()->share_plugin_pids(NewState)end),
			{noreply,NewState};
		_->
			
			{noreply,State}
	end;
handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

get_existing_component_pid(Component,InstancePids)->
	case Component of
		sched_recep->
			InstancePids#instance_pids.sched_recep;
		sched_def_storage->
			InstancePids#instance_pids.sched_def_storage;
		exe_recep->
			InstancePids#instance_pids.exe_recep;
		exe_def_storage->
			InstancePids#instance_pids.exe_def_storage;
		bio_recep->
			InstancePids#instance_pids.bio_recep;
		bio_def_storage->
			InstancePids#instance_pids.bio_def_storage;
		atr_recep->
			InstancePids#instance_pids.atr_recep;
		atr_def_storage->
			InstancePids#instance_pids.atr_def_storage;
		comms_recep->
			InstancePids#instance_pids.comms_recep;
		plugin_sup->
			InstancePids#instance_pids.plugin_sup;
		%comms_tcp_serv->
			%InstancePids#instance_pids.comms_tcp_serv;
		_->
			unrecognized
	end.
update_component_pid(Component,Pid,InstancePids)->
	case Component of
		sched_recep->
			InstancePids#instance_pids{sched_recep=Pid};
		sched_def_storage->
			InstancePids#instance_pids{sched_def_storage=Pid};
		exe_recep->
			InstancePids#instance_pids{exe_recep=Pid};
		exe_def_storage->
			InstancePids#instance_pids{exe_def_storage=Pid};
		bio_recep->
			InstancePids#instance_pids{bio_recep=Pid};
		bio_def_storage->
			InstancePids#instance_pids{bio_def_storage=Pid};
		atr_recep->
			InstancePids#instance_pids{atr_recep=Pid};
		atr_def_storage->
			InstancePids#instance_pids{atr_def_storage=Pid};
		comms_recep->
			InstancePids#instance_pids{comms_recep=Pid};
		plugin_sup->
			InstancePids#instance_pids{plugin_sup=Pid}
		%comms_tcp_serv->
			%InstancePids#instance_pids.comms_tcp_serv;
	end.

plugins_with_pids_converter([],PluginsWithPidsMap)->
	PluginsWithPidsMap;
plugins_with_pids_converter([Plugin|T],PluginsWithPidsMap)->
	NewMap = maps:put(Plugin, pending, PluginsWithPidsMap),
	plugins_with_pids_converter(T,NewMap).

is_instance_ready(State)->
  AtrRecep = State#coord_state.instance_pids#instance_pids.atr_recep,
  BioRecep = State#coord_state.instance_pids#instance_pids.bio_recep,
  ExeRecep = State#coord_state.instance_pids#instance_pids.exe_recep,
  SchedRecep = State#coord_state.instance_pids#instance_pids.sched_recep,
  AtrDefSt = State#coord_state.instance_pids#instance_pids.atr_def_storage,
  BioDefSt = State#coord_state.instance_pids#instance_pids.bio_def_storage,
  ExeDefSt = State#coord_state.instance_pids#instance_pids.exe_def_storage,
  SchedDefSt = State#coord_state.instance_pids#instance_pids.sched_def_storage,
  CommsRecep = State#coord_state.instance_pids#instance_pids.comms_recep,
  PluginSup = State#coord_state.instance_pids#instance_pids.plugin_sup,
  case is_pid(PluginSup) and is_pid(AtrRecep) and is_pid(BioRecep) and is_pid(ExeRecep) and is_pid(SchedRecep) and is_pid(AtrDefSt) and is_pid(BioDefSt) and is_pid(ExeDefSt) and is_pid(SchedDefSt) and is_pid(CommsRecep) of 
    true ->
		ready;
    _->
      	not_ready
  end.
share_instance_pids_and_start_plugins(InstancePids,Plugins,PluginSupPid,CoordPid)->
	share_instance_pids(InstancePids),
	plugin_startup_functions:start_plugins(Plugins, PluginSupPid, CoordPid).
share_instance_pids(InstancePids)->
  AtrRecep = InstancePids#instance_pids.atr_recep,
  BioRecep = InstancePids#instance_pids.bio_recep,
  ExeRecep = InstancePids#instance_pids.exe_recep,
  SchedRecep = InstancePids#instance_pids.sched_recep,
  AtrDefSt = InstancePids#instance_pids.atr_def_storage,
  BioDefSt = InstancePids#instance_pids.bio_def_storage,
  ExeDefSt = InstancePids#instance_pids.exe_def_storage,
  SchedDefSt = InstancePids#instance_pids.sched_def_storage,
  CommsRecep = InstancePids#instance_pids.comms_recep,
  ReceptionMap = #{bio => BioRecep,
					sched=>SchedRecep,
					atr=>AtrRecep,
					exe=>ExeRecep,
  					comms=>CommsRecep,
				   	vs=>AtrRecep},
  custom_erlang_functions:myGenServCall(AtrDefSt,{reception_pid,AtrRecep}),
  custom_erlang_functions:myGenServCall(BioDefSt,{reception_pid,BioRecep}),
  custom_erlang_functions:myGenServCall(ExeDefSt,{reception_pid,ExeRecep}),
  custom_erlang_functions:myGenServCall(SchedDefSt,{reception_pid,SchedRecep}),
  custom_erlang_functions:myGenServCall(AtrRecep,{instance_pids,ReceptionMap,AtrDefSt}),%%TODO: possibly split up into receptionmap, def storage and pluginlist and only send the changed part
  custom_erlang_functions:myGenServCall(BioRecep,{instance_pids,ReceptionMap,BioDefSt}),
  custom_erlang_functions:myGenServCall(ExeRecep,{instance_pids,ReceptionMap,ExeDefSt}),
  custom_erlang_functions:myGenServCall(SchedRecep,{instance_pids,ReceptionMap,SchedDefSt}),
  custom_erlang_functions:myGenServCall(CommsRecep,{instance_pids,ReceptionMap}).
  
  
share_plugin_pids(State)->
	PluginList = delete_failed(maps:values(State#coord_state.plugins),[]),
	AtrRecep = State#coord_state.instance_pids#instance_pids.atr_recep,
    BioRecep = State#coord_state.instance_pids#instance_pids.bio_recep,
    ExeRecep = State#coord_state.instance_pids#instance_pids.exe_recep,
    SchedRecep = State#coord_state.instance_pids#instance_pids.sched_recep,
	CommsRecep = State#coord_state.instance_pids#instance_pids.comms_recep,
	custom_erlang_functions:myGenServCall(AtrRecep,{instance_plugins,PluginList}),%%TODO: possibly split up into receptionmap, def storage and pluginlist and only send the changed part
    custom_erlang_functions:myGenServCall(BioRecep,{instance_plugins,PluginList}),
    custom_erlang_functions:myGenServCall(ExeRecep,{instance_plugins,PluginList}),
    custom_erlang_functions:myGenServCall(SchedRecep,{instance_plugins,PluginList}),
    custom_erlang_functions:myGenServCall(CommsRecep,{instance_plugins,PluginList}).
send_new_receptions_to_plugins(State)->
	PluginList = delete_failed(maps:values(State#coord_state.plugins),[]),
	ReceptionMap = #{bio => State#coord_state.instance_pids#instance_pids.bio_recep,
					sched=>State#coord_state.instance_pids#instance_pids.sched_recep,
					atr=>State#coord_state.instance_pids#instance_pids.atr_recep,
					vs=>State#coord_state.instance_pids#instance_pids.atr_recep,
					exe=>State#coord_state.instance_pids#instance_pids.exe_recep,
					comms=>State#coord_state.instance_pids#instance_pids.comms_recep},
	send_new_receptions_to_plugins(PluginList,ReceptionMap).
send_new_receptions_to_plugins([PluginPid|T],ReceptionMap)->
	case is_pid(PluginPid) of
		true->
			spawn_monitor(fun()->custom_erlang_functions:myGenServCall(PluginPid,{updated_receptions,ReceptionMap})end);
		_->
			ok
	end,
	send_new_receptions_to_plugins(T,ReceptionMap);
send_new_receptions_to_plugins([],_ReceptionMap)->
	ok.

delete_failed([],ReturnList)->
	ReturnList;
delete_failed([H|T],ReturnList)->
	case is_tuple(H) of
		true->
			{_PluginSup,PluginPid} = H,
			delete_failed(T,[PluginPid|ReturnList]);
		_->
			delete_failed(T,ReturnList)
	end.

get_plugins_from_settings_file(BC)->
	{ok,Type} = business_cards:get_type(BC),
	PluginSettingsFile = Type++".txt",
	Path = string:concat("./Plugin Configurations/",PluginSettingsFile),
	{Res,FileContent} = file:read_file(Path),
	case Res of
		ok->
			try
				SettingsMap = jsone:decode(FileContent,[{object_format, map}]),
				SettingsKeys = maps:keys(SettingsMap),
				PluginListOfMaps = plugin_startup_functions:findPluginsFromSettings(SettingsKeys,SettingsMap,[]),
				PluginListOfMaps
			catch
				A:B:StackTrace->
					error_log:log(?MODULE,0,unknown,"Could not get required information from plugin settings file ~p. Caught ~p:~p:~p. Starting resource with standard plugins",[PluginSettingsFile,A,B,StackTrace]),
					SPath = "./Plugin Configurations/Standard.txt",
					{ok,FileContent2} = file:read_file(SPath),
					SettingsMap2 = jsone:decode(FileContent2,[{object_format, map}]),
					SettingsKeys2 = maps:keys(SettingsMap2),
					PluginListOfMaps2 = plugin_startup_functions:findPluginsFromSettings(SettingsKeys2,SettingsMap2,[]),
					PluginListOfMaps2
			end;
		_->
			%error_log:log(?MODULE,0,unknown,"Plugin settings file ~p does not exist. Starting resource with standard plugins",[PluginSettingsFile]),
			SPath = "./Plugin Configurations/Standard.txt",
			{ok,FileContent2} = file:read_file(SPath),
			SettingsMap2 = jsone:decode(FileContent2,[{object_format, map}]),
			SettingsKeys2 = maps:keys(SettingsMap2),
			PluginListOfMaps2 = plugin_startup_functions:findPluginsFromSettings(SettingsKeys2,SettingsMap2,[]),
			PluginListOfMaps2
	end.
%%SLOC:414
	
