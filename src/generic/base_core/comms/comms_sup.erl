%%%-------------------------------------------------------------------
%%% @author Dale Sparrow
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%% This module acts as the supervisor and functional access to BIOGRAPHY services
%%% @end
%%% Created : 21. Jan 2019 12:15 PM
%%%-------------------------------------------------------------------
-module(comms_sup).
-author("Dale Sparrow (Daniel van Niekerk made small changes from original)").

-behaviour(supervisor).

%% API
-export([start_link/3]).

%% Supervisor callbacks
-export([init/1]).

start_link(CoordPid,BC,FactoryReady) ->
  supervisor:start_link(?MODULE, [CoordPid,BC,FactoryReady]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([CoordPid,BC,FactoryReady]) ->
  RestartStrategy = one_for_one,
  MaxRestarts = 10,
  MaxSecondsBetweenRestarts = 3,

  SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

  Restart = permanent,
  Shutdown = 2000,
  Type = worker,
  
  CommsRecepID = comms_recep,
  CommsRecepMFA = {comms_recep, start_link, [CoordPid,BC,FactoryReady]},
  CommsRecep = {CommsRecepID, CommsRecepMFA, Restart, Shutdown, Type, [comms_recep]},

  {ok, {SupFlags, [CommsRecep]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
