%%%-------------------------------------------------------------------
%%% @author Dale Sparrow
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Feb 2020 5:59 PM
%%%-------------------------------------------------------------------
-module(comms_recep).
-author("Daniel van Niekerk (developed from the service provider concept by Dale Sparrow)").

-behaviour(gen_server).

%% API
-export([start_link/3]).
-include("../support/base_records.hrl").
%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(comms_recep_state, {factory_ready,replies_when_factory_ready,serv_variables,serv_prov_plugins,coord_pid,reception_map,instance_plugins,atom_name,bc}).

%%%===================================================================
%%TODO: handle_calls in supervisors
start_link(CoordPid,BC,FactoryReady) ->
  {ok,AtomAdr} = business_cards:get_address(BC),
  gen_server:start_link({local,AtomAdr},?MODULE,[CoordPid,BC,FactoryReady], []).

init([CoordPid,BC,FactoryReady]) ->
  {ok,AtomName} = business_cards:get_address(BC),
  ok = global:unregister_name(AtomName),%if comms recep is restarted old first needs to be unregistered
  yes = global:register_name(AtomName,self()),
  custom_erlang_functions:myGenServCall(CoordPid,{core_component_started,comms_recep}),
  {ok,MyType} = business_cards:get_type(BC),
  ModS = string:lowercase(string:replace(MyType, " ", "_",all)) ++"_serv_prov_mod",
  Mod = list_to_atom(ModS),
  {ok,Services} = business_cards:get_services(BC),
  ServVariables = #{services=>Services,serv_prov_plugins=>#{},busy_contracts=>#{},mod=>Mod,file_checker=>none,busy_acts=>#{},sc_details=>#{},active_acts=>#{},rfps=>#{},act_rfps=>#{},sched_sp_acts=>#{},exe_sp_acts=>#{},act_contracts=>#{}},
  {ok, #comms_recep_state{factory_ready = FactoryReady,replies_when_factory_ready= [],serv_variables = ServVariables,coord_pid = CoordPid,reception_map = #{},instance_plugins=[],atom_name = AtomName,bc = BC}}.

%%--------------------------------------------------------------------
handle_call(prepare_for_removal,From,State)->
	UIClients = ui_state:get_clients(),
	FullPermittedList = lists:append(UIClients,State#comms_recep_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			prepare_plugins_for_removal(State#comms_recep_state.instance_plugins,[]),%%continue: putting stuff in place to ensure service requesters and service providers know when the resource they are providing to or receiving from is removed
			ServProvPlugins = maps:get(serv_prov_plugins,State#comms_recep_state.serv_variables),
			SchedSPActs = maps:get(sched_sp_acts,State#comms_recep_state.serv_variables),
			ExeSPActs = maps:get(exe_sp_acts,State#comms_recep_state.serv_variables),
			Temp = lists:append(maps:keys(ServProvPlugins), maps:keys(SchedSPActs)),
			AllMySPContracts = lists:append(Temp, maps:keys(ExeSPActs)),
			send_stop_messages_to_all_clients(AllMySPContracts),
			SubContracts = maps:keys(maps:get(sc_details,State#comms_recep_state.serv_variables)),
			{ok,PluginsSCs} = atr_api:get_attribute_value(maps:get(atr,State#comms_recep_state.reception_map), "Plugin subcontracts (CV)"),
			AllSCS = lists:append(SubContracts, PluginsSCs),
			cancel_all_subcontracts(AllSCS),
			{ok,InstancePids} = custom_erlang_functions:myGenServCall(State#comms_recep_state.coord_pid, get_all_instance_pids),
			AllInstancePids = lists:append(InstancePids,State#comms_recep_state.instance_plugins),
			
			{reply,{ok,AllInstancePids},State};
		_->
			{reply,{error,"Only a management ui can make this call"},State}
	end;	
			
			
handle_call({update_services,Services},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case business_cards:valid_services(Services) of
				true->
					OldBc = State#comms_recep_state.bc,
					{ok,OldServ} = business_cards:get_services(OldBc),
					case (Services==OldServ) of
						true->
							{reply,ok,State};
						_->
							NewBc = maps:update(<<"services">>, Services, OldBc),
							spawn_monitor(fun() -> {ok,"success"}=doha_api:register(NewBc)end),
							spawn_monitor(fun()->update_bc_in_attributes(maps:get(atr,State#comms_recep_state.reception_map),NewBc) end),
							{reply,ok,State#comms_recep_state{bc=NewBc}}
					end;
				_->
					{reply,{error,"Invalid services"},State}
			end;
		_->
			{reply,{error,"Only my own processes can add services"},State}
	end;

handle_call({send_request_info,ResourceAddress,InfoRequested,RequestDetails},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_atom(ResourceAddress) and custom_erlang_functions:is_string(InfoRequested) of
				true->
					spawn_monitor(fun()->Res = comms_api:request_info(ResourceAddress,InfoRequested,RequestDetails),gen_server:reply(From,Res)end),
					{noreply,State};
				_->
					{reply,{error,"Invalid service bc or service type is not a string"},State}
			end;
		_->
			{reply,{error,"Only my own processes and plugins can make this call"},State}
	end;

handle_call({send_inform,ResourceAddress,InformType,InformDetails},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_atom(ResourceAddress) and custom_erlang_functions:is_string(InformType) of
				true->
					spawn_monitor(fun()->Res = comms_api:inform(ResourceAddress,InformType,InformDetails),gen_server:reply(From,Res)end),
					{noreply,State};
				_->
					{reply,{error,"Invalid service bc or service type is not a string"},State}
			end;
		_->
			{reply,{error,"Only my own processes and plugins can make this call"},State}
	end;
	
handle_call({send_rfp,Contract},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case contracts:valid_contract(Contract) of
				true->
					{ok,ServiceBC} = contracts:get_service_bc(Contract),
					{ok,Adr} = business_cards:get_address(ServiceBC),
					spawn_monitor(fun()->Res=comms_api:rfp(Adr, Contract),gen_server:reply(From,Res)end),
					{noreply,State};
				error->
					{reply,{error,Contract},State}
			end;
		_->
			{reply,{error,"Only my own processes and plugins can make this call"},State}
	end;

handle_call({send_service_request,Contract},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case contracts:valid_contract(Contract) of
				true->
					{ok,ServiceBC} = contracts:get_service_bc(Contract),
					{ok,Adr} = business_cards:get_address(ServiceBC),
					MyPid = self(),
					spawn_monitor(fun()->Res = comms_api:request_service(Adr, Contract),gen_server:reply(From,Res),
										 case Res of
											 accept->
												 custom_erlang_functions:myGenServCall(MyPid, {update_plugins_scs,add,Contract});
											 _->
												 ok
										 end 
								  end),
					{noreply,State};
				error->
					{reply,{error,Contract},State}
			end;
		_->
			{reply,{error,"Only my own processes and plugins can make this call"},State}
	end;
	
handle_call({update_plugins_scs,AddOrRemove,Contract},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case AddOrRemove of
				add->
					F = fun(Val)->[Contract|Val] end;
				_->
					F = fun(Val)->lists:delete(Contract,Val) end
			end,
			AtrRecep = maps:get(atr,State#comms_recep_state.reception_map),
			spawn_monitor(fun()->atr_api:apply_function_to_atr_val(AtrRecep, "Plugin subcontracts (CV)", F) end),
			{reply,ok,State};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;
					
					
handle_call({send_service_info,SetupOrExecution,Contract,Info},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case contracts:valid_contract(Contract) of
				true->
					{ok,ServiceBC} = contracts:get_service_bc(Contract),
					{ok,Adr} = business_cards:get_address(ServiceBC),
					spawn_monitor(fun()->Res = comms_api:service_info(Adr,SetupOrExecution,Contract,Info),gen_server:reply(From,Res)end),
					{noreply,State};
				_->
					{reply,{error,"Invalid contract"},State}
			end;
		_->
			{reply,{error,"Only my own processes and plugins can make this call"},State}
	end;
	

handle_call({send_cancel_service,Contract,Reason},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case contracts:valid_contract(Contract) of
				true->
					{ok,ServiceBC} = contracts:get_service_bc(Contract),
					{ok,Adr} = business_cards:get_address(ServiceBC),
					MyPid = self(),
					spawn_monitor(fun()->Res=comms_api:cancel_service(Adr,Contract,Reason),gen_server:reply(From,Res),custom_erlang_functions:myGenServCall(MyPid, {update_plugins_scs,remove,Contract})
										 end),
					{noreply,State};
				_->
					{reply,{error,"Invalid contract"},State}
			end;
		_->
			{reply,{error,"Only my own processes and plugins can make this call"},State}
	end;

%%for serv prov eps
handle_call({deliver_package_from_plugin,Contract,Package},From,State)->
	FullPermittedList = State#comms_recep_state.instance_plugins,
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			spawn_monitor(fun()->Res = contracts:deliver_package(Contract, Package), gen_server:reply(From,Res)end),
			{noreply,State};
		_->
			{reply,{error,"Only my own plugins can make this call"},State}
	end;

handle_call({stop_service_from_plugin,Contract,Reason},From,State)->
	FullPermittedList = State#comms_recep_state.instance_plugins,
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			spawn_monitor(fun()->Res = contracts:service_stopped(Contract, Reason), gen_server:reply(From,Res)end),
			{noreply,State};
		_->
			{reply,{error,"Only my own plugins can make this call"},State}
	end;
	
%%---------
handle_call({add_plugin,Module,Args},From,State)->%%to be used by plugins if they want to add other plugins
	UIClients = ui_state:get_clients(),
	FullPermittedList = lists:append(UIClients,State#comms_recep_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_atom(Module) of
				true->
					PluginMap = #{<<"module">> => Module,<<"args">> => Args},
					CoordPid = State#comms_recep_state.coord_pid,
					spawn_monitor(fun()->Reply = custom_erlang_functions:myGenServCall(CoordPid,{add_plugin,PluginMap}),gen_server:reply(From,Reply)end),
					{noreply,State};
				_->
					{reply,{error,"Module must be an atom"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;

handle_call({remove_plugin,Module,Args},From,State)->%%to be used by plugins if they want to remove other plugins
	UIClients = ui_state:get_clients(),
	FullPermittedList = lists:append(UIClients,State#comms_recep_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case is_atom(Module) of
				true->
					PluginMap = #{<<"module">> => Module,<<"args">> => Args},
					CoordPid = State#comms_recep_state.coord_pid,
					spawn_monitor(fun()->Reply = custom_erlang_functions:myGenServCall(CoordPid,{remove_plugin,PluginMap}),gen_server:reply(From,Reply)end),
					{noreply,State};
				_->
					{reply,{error,"Module must be an atom"},State}
			end;
		_->
			{reply,{error,"Only this instance's plugins can make this call"},State}
	end;
handle_call(get_bc,_From,State)->
	{reply,{ok,State#comms_recep_state.bc},State};

handle_call(get_plugins_with_status,From,State)->%%only management ui backend process and plugins can make this call
	UIClients = ui_state:get_clients(),
	FullPermittedList = lists:append(UIClients,State#comms_recep_state.instance_plugins),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			CoordPid = State#comms_recep_state.coord_pid,
			spawn_monitor(fun()->ok = custom_erlang_functions:myGenServCall(CoordPid,{get_plugins_with_status,From}) end),
			{noreply,State};
		_->
			{reply,{error,"Only the management ui and plugins can make this call"},State}
	end;
	
handle_call({forward_call,Component,Call},From,State)->%%can be used to edit resource as well
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
			spawn_monitor(fun()->forward_call(Component,Call,State,From) end),
			{noreply,State};
		_->
			{reply,{error,"Only the management ui can make this call"},State}
	end;

handle_call(get_all_resource_info,From,State)->%%only management ui backend process can make this call
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
			spawn_monitor(fun()->get_resource_info(State,From) end),
			{noreply,State};
		_->
			{reply,{error,"Only the management ui can make this call"},State}
	end;

handle_call({request_info,InfoRequested,RequestDetails},From,State)->	
	
	{ok,Type} = business_cards:get_type(State#comms_recep_state.bc),
	ModS = string:lowercase(string:replace(Type, " ", "_",all)) ++"_request_and_inform_mod",
	Mod = list_to_atom(ModS),
	spawn_monitor(fun()->request_and_inform(Mod,request_info,State#comms_recep_state.reception_map,InfoRequested,RequestDetails,From)end),
	{noreply,State};

handle_call({inform,InformType,InformDetails},From,State)->		
	{ok,Type} = business_cards:get_type(State#comms_recep_state.bc),
	ModS = string:lowercase(string:replace(Type, " ", "_",all)) ++"_request_and_inform_mod",
	Mod = list_to_atom(ModS),
	spawn_monitor(fun()->request_and_inform(Mod,inform,State#comms_recep_state.reception_map,InformType,InformDetails,From)end),
	{noreply,State};

%%SERV_PROV calls%%%%%%%%%%%%
handle_call({done_working_on_act,ActId},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:done_working_on_act(State#comms_recep_state.serv_variables, From, ActId),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

handle_call({clear_act,ActId,Reason,ClearSched},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:clear_act(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, ActId, Reason,ClearSched),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

handle_call({acts_ready,Acts},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:acts_ready(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, Acts),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};
			
handle_call({busy_with_unfinished_act,Act,OldSubContracts},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:busy_with_unfinished_act(State#comms_recep_state.serv_variables, From, Act, OldSubContracts),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};		

handle_call({spawn_process,Mod,Fun,Args},From,State)->
	Reply = spa_and_ra_calls:spawn_process(From, Mod, Fun, Args),
	{reply,Reply,State};

handle_call({send_rfps,ActId,ServBCs,ServType,Interval,RequestArgs,PackageBlueprint,ProposalTemplate,ProposalTimeLimit,RfpIdentifier},From,State)->%%from my processes
	case is_list(ServBCs) of
		true->
			{Reply,NewServVariables} = spa_and_ra_calls:send_rfps(maps:put(bc,State#comms_recep_state.bc,State#comms_recep_state.serv_variables), From, State#comms_recep_state.reception_map, ActId, ServBCs, self(), ServType, Interval, RequestArgs, PackageBlueprint,ProposalTemplate,ProposalTimeLimit,[],RfpIdentifier),
			{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};
		_->
			{reply,{error,"ServBCs must be a list"},State}
	end;

handle_call({rfps_timeout,ActId,RfpIdentifier},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:rfps_timeout(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, ActId,RfpIdentifier),
	{reply,Reply,State#comms_recep_state{serv_variables = NewServVariables}};
	
handle_call({proposal,Contract,Proposal,ReplyTo},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:proposal(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, Contract, Proposal,ReplyTo),
	{reply,Reply,State#comms_recep_state{serv_variables = NewServVariables}};

handle_call({cancel_rfps,ActId,RfpIdentifier},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:cancel_rfps(State#comms_recep_state.serv_variables, From, ActId,RfpIdentifier),
	{reply,Reply,State#comms_recep_state{serv_variables = NewServVariables}};

handle_call({send_service_request,ActId,ServBC,ServType,Interval,RequestArgs,PackageBlueprint,Proposal},From,State)->%%from my processes
	Reply = spa_and_ra_calls:send_service_request(maps:put(bc,State#comms_recep_state.bc,State#comms_recep_state.serv_variables), From, State#comms_recep_state.reception_map, ActId, ServBC, self(), ServType, Interval, RequestArgs, PackageBlueprint,Proposal),
	case Reply of
		noreply->
			{noreply,State};
		_->
			{reply,Reply,State}
	end;

handle_call({new_sub_contract,ActId,SubContract},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:new_sub_contract(State#comms_recep_state.serv_variables, From, ActId, SubContract),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

handle_call({send_cancel,ActId,SubContract,Reason},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:send_cancel(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, ActId, SubContract, Reason),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

handle_call({service_package_delivery,Contract,Package,ReplyTo},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "Package")end),
	{Reply,NewServVariables} = spa_and_ra_calls:service_package_delivery(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, Contract, Package, ReplyTo),
	case Reply of
		noreply->
			{noreply,State#comms_recep_state{serv_variables=NewServVariables}};
		_->
			{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}}
	end;

handle_call({service_stopped,Contract,Reason,ReplyTo},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "Stop")end),
	{Reply,NewServVariables} = spa_and_ra_calls:service_stopped(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, Contract, Reason, ReplyTo),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

handle_call({get_act_contracts_and_give_ah,ActId,AHPid},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:get_act_contracts_and_give_ah(State#comms_recep_state.serv_variables, From, ActId, AHPid),
	case State#comms_recep_state.factory_ready of
		true->
			{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};
		_->
			RepliesWhenFactoryReady = State#comms_recep_state.replies_when_factory_ready,
			NewRepliesWhenFactoryReady = [{From,Reply}|RepliesWhenFactoryReady],
			{noreply,State#comms_recep_state{serv_variables=NewServVariables,replies_when_factory_ready = NewRepliesWhenFactoryReady}}
	end;

%%calls from spawned processes 1) done 
handle_call({done,ActId,FinalS2Data,ClearSched},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:done(State#comms_recep_state.serv_variables,From,ActId,FinalS2Data,ClearSched),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

handle_call({rfp,Contract},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, server, "CFP")end),
	case contracts:valid_contract(Contract) of
		true->
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientAdr} = business_cards:get_address(ClientBC),
			case security_functions:from_is_process_or_monitored_by_process(ClientAdr, From) of
				true->
					{ok,ServiceTypes} = business_cards:get_service_types(State#comms_recep_state.bc),
					{ok,ServType} = contracts:get_service_type(Contract),
					case lists:member(ServType, ServiceTypes) of
						true->
							ServVariables = State#comms_recep_state.serv_variables,
							BusyContracts = maps:get(busy_contracts,ServVariables),
							case maps:is_key(Contract, maps:get(sched_sp_acts,ServVariables)) or maps:is_key(Contract, maps:get(exe_sp_acts,ServVariables)) or maps:is_key(Contract, BusyContracts) of
								true->
									spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "refuse")end),
									{reply,{reject,"Contract already taken"},State};
								_->
									Mod = maps:get(mod,ServVariables),
									{ok,ProposalFields} = contracts:get_proposal_fields(Contract),
									spawn_monitor(fun()->spa_helper_funcs:fill_in_proposal_template_and_reply(State#comms_recep_state.reception_map,ProposalFields,Mod,Contract,#{})end),
									{reply,accept,State}
							end;
						_->
							spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "refuse")end),
							{reply,{reject,"Cannot provide for this service type"},State}
					end;
				_->
					spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "refuse")end),
					{reply,{error,"This call must come from the client specified in the client bc"},State}
			end;
		_->
			spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "refuse")end),
			{reply,{reject,"Invalid contract"},State}
	end;
					
handle_call({request_service,Contract},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, server, "Request")end),
	case contracts:valid_contract(Contract) of
		true->
			{ok,ServiceTypes} = business_cards:get_service_types(State#comms_recep_state.bc),
			{ok,ServType} = contracts:get_service_type(Contract),
			case lists:member(ServType, ServiceTypes) of
				true->
					ServVariables = State#comms_recep_state.serv_variables,
					BusyContracts = maps:get(busy_contracts,ServVariables),
					case maps:is_key(Contract, maps:get(sched_sp_acts,ServVariables)) or maps:is_key(Contract, maps:get(exe_sp_acts,ServVariables)) or maps:is_key(Contract, BusyContracts) of
						true->
							spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "reject")end),
							{reply,{reject,"Contract id already taken"},State};
						_->
							Mod = maps:get(mod,ServVariables),
							spawn_monitor(fun()->spa_and_ra_calls:handle_request(Mod,State#comms_recep_state.reception_map,Contract,From)end),
							NewBusyContracts = maps:put(Contract, [{request_service,Contract}], BusyContracts),
							NewServVariables = maps:update(busy_contracts, NewBusyContracts, ServVariables),
							{noreply,State#comms_recep_state{serv_variables = NewServVariables}}
					end;
				_->
					spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "reject")end),
					{reply,{reject,"Cannot provide for this service type"},State}
			end;
		_->
			spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "reject")end),
			{reply,{reject,"Invalid contract"},State}
	end;

handle_call({new_serv_prov_acts,Contract,RequesterAddress,ActIds},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			ServVariables = State#comms_recep_state.serv_variables,
			NewSchedSPActs = maps:put(Contract, {RequesterAddress,ActIds}, maps:get(sched_sp_acts,ServVariables)),
			NewServVariables = maps:update(sched_sp_acts, NewSchedSPActs, ServVariables),
			
			{reply,ok,State#comms_recep_state{serv_variables=NewServVariables}};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;

handle_call({done_working_on_contract,Contract},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:done_working_on_contract(State#comms_recep_state.serv_variables, From, Contract),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

handle_call({clear_sched,Contract,Reason},From,State)->%%specifically when a contract was busy (sched_info for example) while an act was being cleared
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			SchedSPActs = maps:get(sched_sp_acts,State#comms_recep_state.serv_variables),
			case maps:is_key(Contract, SchedSPActs) of
				true->
					spawn_monitor(fun()->spa_helper_funcs:cancel_sched_acts(maps:get(sched,State#comms_recep_state.reception_map),Reason,maps:get(Contract,SchedSPActs))end);
				_->
					ok
			end,
			NewSchedSPActs = maps:remove(Contract, SchedSPActs),
			{reply,ok,State#comms_recep_state{serv_variables = maps:update(sched_sp_acts, NewSchedSPActs, State#comms_recep_state.serv_variables)}};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;
	
	
handle_call({setup_info,Contract,Info,ReplyTo},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, server, "Inform")end),
	{Reply,NewServVariables} = spa_and_ra_calls:setup_info(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, Contract, Info, ReplyTo),
	case Reply of
		noreply->
			{noreply,State#comms_recep_state{serv_variables=NewServVariables}};
		_->
			{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}}
	end;
			

handle_call({execution_info,Contract,Info,ReplyTo},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, server, "Inform")end),
	{Reply,NewServVariables} = spa_and_ra_calls:execution_info(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, Contract, Info, ReplyTo),
	case Reply of
		noreply->
			{noreply,State#comms_recep_state{serv_variables=NewServVariables}};
		_->
			{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}}
	end;
							
handle_call({cancel_service,Contract,Reason,ReplyTo},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, server, "Cancel")end),
	{Reply,NewServVariables} = spa_and_ra_calls:cancel_service(State#comms_recep_state.serv_variables, From, State#comms_recep_state.reception_map, Contract, Reason, ReplyTo),
	{reply,Reply,State#comms_recep_state{serv_variables=NewServVariables}};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
handle_call({register_plugin_for_contract,Contract,ContractAdr,PluginPid},From,State)->
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(State#comms_recep_state.instance_plugins, From) of
		true->
			case is_pid(PluginPid) and contracts:valid_contract(Contract) of
				true->
					ServProvPlugins = maps:get(serv_prov_plugins,State#comms_recep_state.serv_variables),
					NewServProvPlugins = maps:put(Contract,{ContractAdr,PluginPid},ServProvPlugins),
					{reply,ok,State#comms_recep_state{serv_variables = maps:update(serv_prov_plugins, NewServProvPlugins, State#comms_recep_state.serv_variables)}};
				_->
					{reply,{error,"Invalid ContractId or PluginPid"},State}
			end;
		_->
			{reply,{error,"Only my own plugins can make this call"},State}
	end;

handle_call({deregister_plugin_for_contract,Contract},From,State)->
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(State#comms_recep_state.instance_plugins, From) of
		true->
			case contracts:valid_contract(Contract) of
				true->
					ServProvPlugins = maps:get(serv_prov_plugins,State#comms_recep_state.serv_variables),
					case maps:is_key(Contract, ServProvPlugins) of
						true->
							{_Adr,PluginPid} = maps:get(Contract,ServProvPlugins),
							case security_functions:from_is_process_or_monitored_by_process(PluginPid, From) of
								true->
									NewServProvPlugins = maps:remove(Contract,ServProvPlugins),
									{reply,ok,State#comms_recep_state{serv_variables = maps:update(serv_prov_plugins, NewServProvPlugins, State#comms_recep_state.serv_variables)}};
								_->
									{reply,{error,"Only the plugin that registered for this contract can deregister for it"},State}
							end;
						_->
							{reply,{error,"No contract with this id"},State}
					end;
				_->
					{reply,{error,"Invalid ContractId or PluginPid"},State}
			end;
		_->
			{reply,{error,"Only my own plugins can make this call"},State}
	end;

%%from us to service requesters
handle_call({deliver_package,Contract,Package},From,State)->%%call from execution plugin to deliver a package as part of fulfilling a service contract
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			spawn_monitor(fun()->deliver_package(Contract,Package,From)end),
			{noreply,State};
		_->
			{reply,{error,"Only this instance's plugins are allowed to make this call"},State}
	end;

handle_call({service_stopped,Contract,Reason},From,State)->%%call from our execution plugin who wants to inform a service requester that the service has stopped
	FullPermittedList = [State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientAdr} = business_cards:get_address(ClientBC),
			spawn_monitor(fun()->comms_api:service_stopped(ClientAdr, Contract, Reason)end),
			ServProvPlugins = maps:get(serv_prov_plugins,State#comms_recep_state.serv_variables),
			NewServProvPlugins = maps:remove(Contract,ServProvPlugins),
			{reply,ok,State#comms_recep_state{serv_variables = maps:update(serv_prov_plugins, NewServProvPlugins, State#comms_recep_state.serv_variables)}};

		_->
			{reply,{error,"Only this instance's plugins are allowed to make this call"},State}
	end;

%%from service providers to us
handle_call({deliver_proposal_to_plugin,PluginIdentifier,Contract,Proposal},From,State)->
	spawn_monitor(fun()->proposal_delivery_to_plugin(State#comms_recep_state.coord_pid,PluginIdentifier,Contract,Proposal,From)end),
	{noreply,State};

handle_call({deliver_package_to_plugin,PluginIdentifier,Contract,Package},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "Package")end),
	spawn_monitor(fun()->service_package_delivery_to_plugin(State#comms_recep_state.coord_pid,PluginIdentifier,Contract,Package,From)end),
	{noreply,State};

handle_call({deliver_service_stopped_to_plugin,PluginIdentifier,Contract,Reason},From,State)->
	spawn(fun()->holon_interactions_ets:new_contract(Contract, client, "Stop")end),
	MyPid = self(),
	spawn_monitor(fun()->service_stopped_to_plugin(State#comms_recep_state.coord_pid,PluginIdentifier,Contract,Reason,From),custom_erlang_functions:myGenServCall(MyPid, {update_plugins_scs,remove,Contract})end),
	{noreply,State};
					

%%calls from internal plugins wanting to find services for this resource
handle_call({find_services,ServiceType},From,State)->
	FullPermittedList = [self()|State#comms_recep_state.instance_plugins],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			case custom_erlang_functions:is_string(ServiceType) of
				true->
					spawn(fun()->find_services(ServiceType,From) end),
					{noreply,State};
				_->
					{reply,{error,"ServiceType must be a string"},State}
			end;
		_->
			{reply,{error,"Only instance plugins can find services via this comms recep"},State}
	end;


handle_call(instance_ready,From,State)->
	{Pid,_Ref} = From,
	case Pid == State#comms_recep_state.coord_pid of
		true->
			MyBc = State#comms_recep_state.bc,
			{ok,ID} = business_cards:get_id(MyBc),
			case State#comms_recep_state.factory_ready of
				true->%%indication that this is a new resource or restarted resource/comms so bc needs to be registered with doha
					spawn_monitor(fun() -> {ok,"success"}=doha_api:register(MyBc)end);
				_->%%indication that whole factory is restarted and doha needs to be informed when resources are ready
					spawn_monitor(fun()->doha_api:ready(ID)end)
			end,
			
			{reply,ok,State};
		_->
			{reply,{error,"Only this instance's coordinator can state that it is ready"},State}
	end;

handle_call({instance_pids, ReceptionMap},From,State)->
	{Reply,NewServVariables,NewReceptionMap} = spa_and_ra_calls:updated_receptions(State#comms_recep_state.serv_variables, From, ReceptionMap, State#comms_recep_state.coord_pid, ReceptionMap),
	spawn_monitor(fun()->update_bc_in_attributes(maps:get(atr,ReceptionMap),State#comms_recep_state.bc) end),
	spawn_monitor(fun()->create_plugin_scs_if_not_in_atrs(maps:get(atr,ReceptionMap))end),
	{reply,Reply,State#comms_recep_state{serv_variables = NewServVariables,reception_map=NewReceptionMap}};

handle_call({instance_plugins, InstancePlugins},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#comms_recep_state.coord_pid, From) of
		true->
			case State#comms_recep_state.factory_ready of
				true->%%when this resource was just added as new resource, ie not part of restart
					inform_all_plugins_factory_is_ready(InstancePlugins);
				_->
					ok
			end,
			NewState = State#comms_recep_state{instance_plugins = InstancePlugins},
			{reply, ok, NewState};
		_->
			{reply,{error,"Only coordinator and its processes can send instance pids"},State}
	end;

handle_call(get_instance_sup,From,State)->%%only management ui backend process can make this call
	CoordPid = State#comms_recep_state.coord_pid,
	spawn_monitor(fun()->custom_erlang_functions:myGenServCall(CoordPid,{instance_sup,From})end),
    {noreply,State};
handle_call({update_bc,NewBc},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			{reply,ok,State#comms_recep_state{bc=NewBc}};
		_->
			{reply,{error,"Only my own process can make this call"},State}
	end;
handle_call(Request, From, State) ->
  error_log:log(?MODULE,0,unknown,"Comms recep received unrecognized call:~p \nfrom:~p",[Request,From]),
  {reply, {not_understood,Request}, State}.

%%--------------------------------------------------------------------
handle_cast(factory_ready,State)->
	inform_all_plugins_factory_is_ready(State#comms_recep_state.instance_plugins),
	RepliesWhenFactoryReady = State#comms_recep_state.replies_when_factory_ready,
	make_all_pending_replies(RepliesWhenFactoryReady),
	{noreply,State#comms_recep_state{factory_ready=true,replies_when_factory_ready=[]}};

handle_cast(_Request, State) ->
  {noreply, State}.
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
  {noreply, State}.
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%%%===================================================================
%%% Internal functions
%%%===================================================================
inform_all_plugins_factory_is_ready([])->
	ok;
inform_all_plugins_factory_is_ready([Pid|T])->
	gen_server:cast(Pid,factory_ready),
	inform_all_plugins_factory_is_ready(T).

make_all_pending_replies([])->
	ok;
make_all_pending_replies([{From,Reply}|T])->
	custom_erlang_functions:gen_serv_reply_if_from(From, Reply),
	make_all_pending_replies(T).

find_services(ServiceType,From)->
	Reply = doha_api:find_service_providers(ServiceType),
	gen_server:reply(From, Reply).


forward_call(Component,Call,State,From)->
	case (Component==bio) or (Component==sched) or (Component==exe) or (Component==atr) of
		true->
			ComponentPid = maps:get(Component,State#comms_recep_state.reception_map),
			Reply = custom_erlang_functions:myGenServCall(ComponentPid, Call);
		_->
			Reply = {error,"Invalid component"}
	end,
	gen_server:reply(From, Reply).

get_resource_info(State,From)->
	SchedRecep = maps:get(sched,State#comms_recep_state.reception_map),
	TschedHigh = base_time:now()+3600*24*7,
	{ok,Schedule} = sched_api:get_acts_by_times(SchedRecep, "all", TschedHigh),
	{ok,Execution} = exe_api:get_all_acts(maps:get(exe,State#comms_recep_state.reception_map)),
	BioRecep = maps:get(bio,State#comms_recep_state.reception_map),
	TendLow = base_time:now()-3600*24*7,
	{ok,Biography} = bio_api:get_acts_by_times(BioRecep, 0, "all", 0, "all", TendLow, "all"),
	{ok,Attributes} = atr_api:get_all_attributes(maps:get(atr,State#comms_recep_state.reception_map)),
	{ok,Plugins} = custom_erlang_functions:myGenServCall(State#comms_recep_state.coord_pid,{get_plugins_with_status,direct}),
	BC = State#comms_recep_state.bc,
	{ok,ID} = business_cards:get_id(BC),
	{ok,Type} = business_cards:get_type(BC),
	{ok,Services} = business_cards:get_services(BC),
	{ok,Address} = business_cards:get_address(BC),
	ReplyMap = #{sched => Schedule,exe=>Execution,bio=>Biography,atr=>Attributes,plugins=>Plugins,services=>Services,id=>ID,type=>Type,address=>Address},
	gen_server:reply(From,{ok,ReplyMap}).

deliver_package(Contract,Package,From)->
	Reply = contracts:deliver_package(Contract, Package),
	gen_server:reply(From, Reply).



request_and_inform(Mod,Function,ReceptionMap,Arg1,Arg2,From)->
	try
		FromMonitors = security_functions:get_monitors(From),
		FromCommsRecep = lists:nth(1, FromMonitors),
		case (Function==request_info) and (Arg1=="Attributes") of
			true->
				case custom_erlang_functions:isListOfStrings(Arg2) of
					true->
						{ok,Atrs} = atr_api:get_attributes_by_ids(maps:get(atr,ReceptionMap), Arg2),
						ReturnMap = get_attribute_values(Atrs,#{}),
						Reply = {ok,ReturnMap};
					_->
						case Arg2 of 
							all->
								{ok,Atrs} = atr_api:get_all_attributes(maps:get(atr,ReceptionMap)),
								ReturnMap = get_attribute_values(Atrs,#{}),
								Reply = {ok,ReturnMap};
							_->
								Reply = {error,"Request details must be a list of strings"}
						end
				end,
				custom_erlang_functions:gen_serv_reply_if_from(From, Reply);
			_->
				Reply = Mod:Function(ReceptionMap,Arg1,Arg2,FromCommsRecep),
				custom_erlang_functions:gen_serv_reply_if_from(From, Reply)
		end
	catch
		A:B:C->
			error_log:log(?MODULE,0,unknown,"\n~p could not handle ~p (~p,~p). Caught ~p:~p:~p",[Mod,Function,Arg1,Arg2,A,B,C]),
			ReplyC = {error,"Cannot handle request/inform"},
			custom_erlang_functions:gen_serv_reply_if_from(From, ReplyC)
	end.

proposal_delivery_to_plugin(CoordPid,PluginIdentifier,Contract,Proposal,From)->
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,ServAdr} = business_cards:get_address(ServBC),
	case security_functions:from_is_process_or_monitored_by_process(ServAdr, From) of
		true->
			{Res,{_PluginSup,PluginPid}} = custom_erlang_functions:myGenServCall(CoordPid,{get_plugin_pid,PluginIdentifier}),
			case Res of 
				ok->
					Reply = custom_erlang_functions:myGenServCall(PluginPid, {proposal,Contract,Proposal,From}),
					custom_erlang_functions:gen_serv_reply_if_from(From, Reply);
				_->
					custom_erlang_functions:gen_serv_reply_if_from(From, {error,"No plugin with these details exists"})
			end;
		_->
			custom_erlang_functions:gen_serv_reply_if_from(From, {error,"Only the service provider of this contract can make this call"})
	end.

service_package_delivery_to_plugin(CoordPid,PluginIdentifier,Contract,Package,From)->
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,ServAdr} = business_cards:get_address(ServBC),
	case security_functions:from_is_process_or_monitored_by_process(ServAdr, From) of
		true->
			{Res,{_PluginSup,PluginPid}} = custom_erlang_functions:myGenServCall(CoordPid,{get_plugin_pid,PluginIdentifier}),
			case Res of 
				ok->
					Reply = custom_erlang_functions:myGenServCall(PluginPid, {service_package_delivery,Contract,Package,From}),
					custom_erlang_functions:gen_serv_reply_if_from(From, Reply);
				_->
					custom_erlang_functions:gen_serv_reply_if_from(From, {error,"No plugin with these details exists"})
			end;
		_->
			custom_erlang_functions:gen_serv_reply_if_from(From, {error,"Only the service provider of this contract can make this call"})
	end.

service_stopped_to_plugin(CoordPid,PluginIdentifier,Contract,Reason,From)->
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,ServAdr} = business_cards:get_address(ServBC),
	case security_functions:from_is_process_or_monitored_by_process(ServAdr, From) of
		true->
			{Res,{_PluginSup,PluginPid}} = custom_erlang_functions:myGenServCall(CoordPid,{get_plugin_pid,PluginIdentifier}),
			case Res of 
				ok->
					Reply = custom_erlang_functions:myGenServCall(PluginPid, {service_stopped,Contract,Reason,From}),%%From must be included for resource acts to work. Other plugins can just ignore this
					custom_erlang_functions:gen_serv_reply_if_from(From, Reply);
				_->
					custom_erlang_functions:gen_serv_reply_if_from(From, {error,"No plugin with these details exists"})
			end;
		_->
			custom_erlang_functions:gen_serv_reply_if_from(From, {error,"Only the service provider of this contract can make this call"})
	end.

update_bc_in_attributes(AtrRecep,NewBC)->
	timer:sleep(500),
	{ok,Atr} = attribute_functions:create_attribute("BC", "BC", "BC", NewBC),
	{Res,_Msg} = atr_api:save(AtrRecep, [Atr]),
	case Res of
		ok->
			ok;
		_->
			timer:sleep(100),
			update_bc_in_attributes(AtrRecep,NewBC)
	end.

get_attribute_values([],ReturnMap)->
	ReturnMap;
get_attribute_values([Atr|T],ReturnMap)->
	{ok,Id} = attribute_functions:get_atr_id(Atr),
	{ok,Val} = attribute_functions:get_atr_value(Atr),
	get_attribute_values(T,maps:put(Id,Val,ReturnMap)).

prepare_plugins_for_removal([],PrepProcs)->
	case all_dead(PrepProcs) of
		true->
			ok;
		_->
			timer:sleep(500),
			prepare_plugins_for_removal([],PrepProcs)
	end;

prepare_plugins_for_removal([Plugin|T],PrepProcs)->
	{Pid,_Ref}=spawn_monitor(fun()->custom_erlang_functions:myGenServCall(Plugin, prepare_for_removal,20000)end),
	prepare_plugins_for_removal(T,[Pid|PrepProcs]).

all_dead([])->
	true;
all_dead([PrepProc|T])->
	case is_process_alive(PrepProc) of
		true->
			false;
		_->
			all_dead(T)
	end.

send_stop_messages_to_all_clients([])->
	ok;
send_stop_messages_to_all_clients([Contract|T])->
	
	spawn_monitor(fun()->contracts:service_stopped(Contract, "Service provider resource is being removed from this factory")end),
	send_stop_messages_to_all_clients(T).

cancel_all_subcontracts([])->
	ok;
cancel_all_subcontracts([Contract|T])->
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,ServAdr} = business_cards:get_address(ServBC),
	spawn_monitor(fun()->comms_api:cancel_service(ServAdr, Contract, "Client resource being removed from this factory")end),
	cancel_all_subcontracts(T).

create_plugin_scs_if_not_in_atrs(AtrRecep)->
	{ok,Atrs} = atr_api:get_attributes_by_ids(AtrRecep, "Plugin subcontracts (CV)"),
	case Atrs of 
		[]->
			{ok,Atr} = attribute_functions:create_attribute("Plugin subcontracts (CV)", "Calculation variables", "List", []),
			atr_api:save(AtrRecep, [Atr]);
		_->
			ok
	end.
%%SLOC:789
