%% @author Daniel
%% @doc @todo Add description to comms_api.


-module(comms_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([request_info/3,inform/3,rfp/2,request_service/2,service_info/4,cancel_service/3,service_stopped/3,
		find_services/2,
		add_plugin/3,remove_plugin/3,
		send_request_info/4,send_inform/4,send_service_request/2,send_service_info/4,send_cancel_service/3,send_rfp/2,
		 get_plugins_with_status/1,
		forward_call/3,
		 get_bc/1,deliver_package/3,
		register_plugin_for_contract/4,deregister_plugin_for_contract/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_bc(CommsPid)->
	custom_erlang_functions:myGenServCall(CommsPid, get_bc).

%%making service requesting calls
send_request_info(CommsPid,ResourceAddress,InfoRequested,RequestDetails)->
	custom_erlang_functions:myGenServCall(CommsPid, {send_request_info,ResourceAddress,InfoRequested,RequestDetails}).
send_inform(CommsPid,ResourceAddress,InformType,InformDetails)->
	custom_erlang_functions:myGenServCall(CommsPid, {send_inform,ResourceAddress,InformType,InformDetails}).
send_service_request(CommsPid,Contract)->
	custom_erlang_functions:myGenServCall(CommsPid, {send_service_request,Contract}).
send_service_info(CommsPid,SetupOrExecution,Contract,Info)->
	custom_erlang_functions:myGenServCall(CommsPid, {send_service_info,SetupOrExecution,Contract,Info}).
send_cancel_service(CommsPid,Contract,Reason)->
	custom_erlang_functions:myGenServCall(CommsPid, {send_cancel_service,Contract,Reason}).
send_rfp(CommsPid,Contract)->
	custom_erlang_functions:myGenServCall(CommsPid, {send_rfp,Contract}).
%%-------------------------------
%%receiving service requesting calls
request_info(CommsPid,InfoRequested,RequestDetails)->
	custom_erlang_functions:myGenServCall(CommsPid, {request_info,InfoRequested,RequestDetails}).
inform(CommsPid,InformType,InformDetails)->
	custom_erlang_functions:myGenServCall(CommsPid, {inform,InformType,InformDetails}).

rfp(CommsPid,Contract)->
	custom_erlang_functions:myGenServCall(CommsPid, {rfp,Contract}).
request_service(CommsPid,Contract)->
	custom_erlang_functions:myGenServCall(CommsPid, {request_service,Contract}).

service_info(CommsPid,SetupOrExecution,Contract,Info)->
	case SetupOrExecution of
		setup->
			custom_erlang_functions:myGenServCall(CommsPid, {setup_info,Contract,Info,from});
		execution->
			custom_erlang_functions:myGenServCall(CommsPid, {execution_info,Contract,Info,from});
		_->
			custom_erlang_functions:myGenServCall(CommsPid, {setup_info,Contract,Info,from}),
			custom_erlang_functions:myGenServCall(CommsPid, {execution_info,Contract,Info,from})
	end.

%%from service requester to service provider
cancel_service(CommsPid,Contract,Reason)->
	custom_erlang_functions:myGenServCall(CommsPid, {cancel_service,Contract,Reason,from}).

deliver_package(CommsPid,Contract,Package)->
	custom_erlang_functions:myGenServCall(CommsPid, {deliver_package,Contract,Package}).

%%from service provider to service requester
service_stopped(CommsPid,Contract,Reason)->
	custom_erlang_functions:myGenServCall(CommsPid, {service_stopped,Contract,Reason,from}).
%%-------------------------------------
%%from EP to handle service
register_plugin_for_contract(CommsPid,Contract,ContractAdr,PluginPid)->
	custom_erlang_functions:myGenServCall(CommsPid, {register_plugin_for_contract,Contract,ContractAdr,PluginPid}).

deregister_plugin_for_contract(CommsPid,Contract)->
	custom_erlang_functions:myGenServCall(CommsPid, {deregister_plugin_for_contract,Contract}).

find_services(CommsPid,ServiceType)->
	custom_erlang_functions:myGenServCall(CommsPid, {find_services,ServiceType}).

add_plugin(CommsPid,Module,Args)->
	custom_erlang_functions:myGenServCall(CommsPid, {add_plugin,Module,Args}).

remove_plugin(CommsPid,Module,Args)->
	custom_erlang_functions:myGenServCall(CommsPid, {remove_plugin,Module,Args}).

get_plugins_with_status(CommsPid)->
	custom_erlang_functions:myGenServCall(CommsPid, get_plugins_with_status).

forward_call(CommsPid,Component,Call)->
	custom_erlang_functions:myGenServCall(CommsPid, {forward_call,Component,Call}).

%%SLOC:71