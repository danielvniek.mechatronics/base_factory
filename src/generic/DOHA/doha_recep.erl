
-module(doha_recep).
-author("Daniel van Niekerk (adapated from the original by Dale Sparrow)").
-behaviour(gen_server).

-include_lib("stdlib/include/ms_transform.hrl").
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(doha_state, {registered_plugins,unready_ids,
					storage_service_address,addresses_to_tell_factory_ready}).%%note that for now the default mod is always used but in future developments this can be changed to be more like the core components' receptions where the storage method can be changed

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  gen_server:start_link({local, doha}, ?MODULE, [], []).

init([]) ->
  yes = global:register_name(doha,self()),
  UnreadyIds = get_unready_ids(),
  case UnreadyIds of 
	  []->
		  io:format("\nFACTORY EMPTY");
	  _->
		  ok
  end,
  {ok, #doha_state{registered_plugins=#{},unready_ids=UnreadyIds,addresses_to_tell_factory_ready=[]}}.%%unready_ids used for factory startups

handle_call({remove_resource,ID},From,State)->
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
		 	case custom_erlang_functions:is_string(ID) of
				true->
					Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{remove_resource,ID});
				_->
					Reply = {error, "ID must be a string"}
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only management ui can make this call"},State}
	end;

handle_call({ready,ID},From,State)->
	case lists:member(ID,State#doha_state.unready_ids) of
		true->
			Adr = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
			case security_functions:from_is_process_or_monitored_by_process(Adr, From) of
				true->
					NewUnreadyIds = lists:delete(ID, State#doha_state.unready_ids),
					NewAdrs = [Adr|State#doha_state.addresses_to_tell_factory_ready],
					case NewUnreadyIds of
						[]->
							io:format("\nFACTORY READY"),
							system_restarts:finished(erlang:system_time(microsecond),length(NewAdrs)),
							%spawn_monitor(fun()->timer:sleep(10000),share_factory_ready(NewAdrs)end),
							spawn_monitor(fun()->share_factory_ready(NewAdrs)end),%this is just for time_tests without the safety 10seconds
							TS = erlang:timestamp(),
							{reply,ok,State#doha_state{unready_ids=NewUnreadyIds,addresses_to_tell_factory_ready=[]}};
						_->
							{reply,ok,State#doha_state{unready_ids=NewUnreadyIds,addresses_to_tell_factory_ready=NewAdrs}}
					end;
				_->
					error_log:log(?MODULE,0,unknown,"\nInc adr"),
					{reply,{error,"Only the comms recep of the resource with this id can make this call"},State}
			end;
		_->
			{reply,{error,"Not expecting ready call from you"},State}
	end;
					
handle_call({register,BC}, From, State) ->%%anyone can register themselves. Only concern is if some virus continuously registers BCs
  TS = erlang:timestamp(),
  case business_cards:valid_base_bc(BC) of
	  true->
		  	{ok,ID} = business_cards:get_id(BC),
			{ok,BCList} = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_bcs_by_ids,"get",[ID],active}),
			case BCList of
				[]->
					Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{save_bc,BC});
				_->
					RegBC = lists:nth(1, BCList),
					{ok,Adr}= business_cards:get_address(RegBC),
					case security_functions:from_is_process_or_monitored_by_process(Adr, From) of
						true->
							Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{save_bc,BC});
						_->
							Reply = {error,"Only the comms recep of the resource being reregistered may make this call"}
					end
			end,
			case Reply of
				{ok,"success"}->
					{ok,Services} = business_cards:get_services(BC),
					ServTypes = maps:keys(Services),
					RegPlugins = State#doha_state.registered_plugins,
					spawn_monitor(fun()->share_with_registered_plugins(ServTypes,RegPlugins,BC) end);
				_->
					ok
			end;		
	  _->
		error_log:log(?MODULE,0,unknown,"\nERROR: Doha received invalid BC:~p",[BC]),
		Reply = {error, "DOHA: Invalid BC in register call"}
  end,
  {reply,Reply,State};

handle_call({register_for_service_providers,ServType,PluginPid},_From,State)->
	case custom_erlang_functions:is_string(ServType) of
		true->
			RegPlugins = State#doha_state.registered_plugins,
			case maps:is_key(ServType, RegPlugins) of
				true->
					OldList = maps:get(ServType,RegPlugins),
					CleanList = lists:delete(PluginPid, OldList),
					NewList = [PluginPid|CleanList];
				_->
					NewList = [PluginPid]
			end,
			NewRegPlugins = maps:put(ServType, NewList, RegPlugins),
			{ok,BCs} = custom_erlang_functions:myGenServCall(doha_default_storage,{get_bcs_by_service_type,ServType}),
			spawn_monitor(fun()->share_existing(BCs,ServType,PluginPid)end),
		    {reply,ok,State#doha_state{registered_plugins = NewRegPlugins}};
		_->
			{reply,{error,"ServType must be a string"},State}
	end;
handle_call({deregister_for_service_providers,ServType,PluginPid},From,State)->
	Me = self(),
	case security_functions:from_is_process_or_monitored_by_process(Me, From) of
		true->
			RegPlugins = State#doha_state.registered_plugins,
			case maps:is_key(ServType, RegPlugins) of
				true->
					PluginList = maps:get(ServType,RegPlugins),
					NewPluginList = lists:delete(PluginPid, PluginList),
					case NewPluginList of
						[]->
							NewRegPlugins = maps:remove(ServType, RegPlugins);
						_->
							NewRegPlugins = maps:update(ServType, NewPluginList, RegPlugins)
					end;
				_->
					NewRegPlugins = RegPlugins
			end,
			{reply,ok,State#doha_state{registered_plugins = NewRegPlugins}};
		_->
			{reply,{error,"Only doha's own processes can make this call"},State}
	end;
			

handle_call({find_service_providers,ServiceType},_From,State)->%%anyone can ask this
  Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_bcs_by_service_type,ServiceType}),
  {reply,Reply,State};

handle_call({get_all_bcs,ActiveOrRemoved}, _From,State)->%%anyone can ask this
	Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_all_bcs,"get",ActiveOrRemoved}),
	{reply,Reply,State};
handle_call({get_bcs_by_ids,IDs,ActiveOrRemoved},_From,State)->%%anyone can ask this
	case custom_erlang_functions:isListOfStrings(IDs) of
		true->
			Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_bcs_by_ids,"get",IDs,ActiveOrRemoved});
		_->
			Reply = {error, "Invalid input args"}
	end,
	{reply,Reply,State};

handle_call({get_bcs_by_types,Types,ActiveOrRemoved},_From,State)->	%%anyone can ask this									
	case custom_erlang_functions:isListOfStrings(Types) of
		true->
			Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_bcs_by_types,"get",Types,ActiveOrRemoved});
		_->
			Reply = {error, "Invalid input args"}
	end,
	{reply,Reply,State};

handle_call({get_bcs_by_ids_and_type,IDs,Type},_From,State)-> 
	case custom_erlang_functions:is_string(Type) and custom_erlang_functions:is_string(Type) of
		true->
			Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_bcs_by_ids_and_type,IDs,Type});
		_->
			Reply = {error, "Invalid input args"}
	end,
	{reply,Reply,State};
handle_call({deregister_bcs_by_ids,IDs,ActiveOrRemoved},From,State)-> %%only management backend process can do this
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
		 	case custom_erlang_functions:isListOfStrings(IDs) of
				true->
					Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_bcs_by_ids,"del",IDs,ActiveOrRemoved});
				_->
					Reply = {error, "Invalid input args"}
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only management ui can make this call"},State}
	end;
handle_call({deregister_bcs_by_types,Types,ActiveOrRemoved},From,State)->  %%only management backend process can do this
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
		 	case custom_erlang_functions:isListOfStrings(Types) of
				true->
					Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_bcs_by_types,"del",Types,ActiveOrRemoved});
				_->
					Reply = {error, "Invalid input args"}
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only management ui can make this call"},State}
	end;
	
handle_call({clear_doha,ActiveOrRemoved}, From,State)->  %%only management backend process can do this
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
			Reply = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_all_bcs,"del",ActiveOrRemoved}),
			{reply,Reply,State};
		_->
			{reply,{error,"Only management ui can make this call"},State}
	end;


handle_call(Unknown, _From, State) ->
  {reply, {bad_request,Unknown}, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
share_existing([],_ServType,_PluginPid)->
	ok;
share_existing([BC|T],ServType,PluginPid)->
	case is_process_alive(PluginPid) of
		true->
			Res = custom_erlang_functions:myGenServCall(PluginPid, {new_service_provider,ServType,BC}),
			case Res of
				timeout->
					custom_erlang_functions:myGenServCall(doha,{deregister_plugin,ServType,PluginPid});
				_->
					ok
			end;
		_->
			custom_erlang_functions:myGenServCall(doha,{deregister_plugin,ServType,PluginPid})
	end,
	share_existing(T,ServType,PluginPid).

share_with_registered_plugins([],_RegPlugins,_BC)->
	ok;
share_with_registered_plugins([ServType|T],RegPlugins,BC)->
	case maps:is_key(ServType, RegPlugins) of
		true->
			PluginList = maps:get(ServType,RegPlugins),
			share_with_all_plugins(ServType,BC,PluginList),
			share_with_registered_plugins(T,RegPlugins,BC);
		_->
			share_with_registered_plugins(T,RegPlugins,BC)
	end.
share_with_all_plugins(_ServType,_BC,[])->
	ok;
share_with_all_plugins(ServType,BC,[PluginPid|T])->
	case is_process_alive(PluginPid) of
		true->
			Res = custom_erlang_functions:myGenServCall(PluginPid, {new_service_provider,ServType,BC}),
			case Res of
				timeout->
					custom_erlang_functions:myGenServCall(doha,{deregister_plugin,ServType,PluginPid});
				_->
					ok
			end;
		_->
			custom_erlang_functions:myGenServCall(doha,{deregister_plugin,ServType,PluginPid})
	end,
	share_with_all_plugins(ServType,BC,T).
			
	
get_unready_ids()->
	case is_pid(whereis(doha_default_storage)) of
		true->
			{ok,BCs} = custom_erlang_functions:myGenServCall(doha_default_storage,{get_or_del_all_bcs,"get",active}),
			IDs = bcs_to_ids(BCs,[]),
			IDs;
		_->
			timer:sleep(50),
			get_unready_ids()
	end.

bcs_to_ids([],IDs)->
	IDs;
bcs_to_ids([BC|T],IDs)->
	ID = maps:get(<<"id">>,BC),%%just a little faster than going through business_cards:get_id because no bc validation done
	bcs_to_ids(T,[ID|IDs]).

share_factory_ready([])->
	ok;
share_factory_ready([Adr|T])->
	gen_server:cast(Adr,factory_ready),
	share_factory_ready(T).
