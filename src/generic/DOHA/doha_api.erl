%% @author Daniel
%% @doc @todo Add description to doha_api.


-module(doha_api).

%% ====================================================================
%% API functions
%% ====================================================================
-export([remove_resource/1,ready/1,register/1,register_for_service_providers/2,find_service_providers/1,
		 get_all_bcs/0,get_bcs_by_ids/1,get_bcs_by_types/1,get_all_removed_resources/0,get_removed_resources_by_ids/1,get_removed_resources_by_types/1,
		 deregister_bcs_by_ids/1,deregister_bcs_by_types/1,delete_all_removed_resources/0,delete_removed_resources_by_ids/1,delete_removed_resources_by_types/1,
		 clear_doha/0,
		 register_act_ids/3,get_act_locations/1,get_holon_act_info/1,deregister_act_id/1,
		 get_bcs_by_ids_and_type/2]).

%% ====================================================================
%% Internal functions - DOHA
%% ====================================================================
remove_resource(ID)->
	custom_erlang_functions:myGenServCall(doha, {remove_resource,ID}).
ready(ID)->
	custom_erlang_functions:myGenServCall(doha, {ready,ID}).
register(BC)->
	custom_erlang_functions:myGenServCall(doha, {register,BC}).
register_for_service_providers(ServType,PluginPid)->
	custom_erlang_functions:myGenServCall(doha,{register_for_service_providers,ServType,PluginPid}).
find_service_providers(ServiceType)->
	custom_erlang_functions:myGenServCall(doha, {find_service_providers, ServiceType}).

get_all_bcs()->
	custom_erlang_functions:myGenServCall(doha, {get_all_bcs,active}).
get_all_removed_resources()->
	custom_erlang_functions:myGenServCall(doha, {get_all_bcs,removed}).

get_bcs_by_ids(IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_ids,IDs,active});
		_->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_ids,[IDs],active})
	end.
get_removed_resources_by_ids(IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_ids,IDs,removed});
		_->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_ids,[IDs],removed})
	end.

get_bcs_by_types(Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_types,Types,active});
		_->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_types,[Types],active})
	end.
get_removed_resources_by_types(Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_types,Types,removed});
		_->
			custom_erlang_functions:myGenServCall(doha, {get_bcs_by_types,[Types],removed})
	end.

get_bcs_by_ids_and_type(IDs,Type)->
	custom_erlang_functions:myGenServCall(doha, {get_bcs_by_ids_and_type,IDs,Type}).

deregister_bcs_by_ids(IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_ids,IDs,active});
		_->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_ids,[IDs],active})
	end.

delete_removed_resources_by_ids(IDs)->
	case is_list(IDs) and not(io_lib:printable_list(IDs)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_ids,IDs,removed});
		_->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_ids,[IDs],removed})
	end.

deregister_bcs_by_types(Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_types,Types,active});
		_->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_types,[Types],active})
	end.
delete_removed_resources_by_types(Types)->
	case is_list(Types) and not(io_lib:printable_list(Types)) of
		true->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_types,Types,removed});
		_->
			custom_erlang_functions:myGenServCall(doha, {deregister_bcs_by_types,[Types],removed})
	end.

clear_doha()->
	custom_erlang_functions:myGenServCall(doha, {clear_doha,active}).

delete_all_removed_resources()->
	custom_erlang_functions:myGenServCall(doha, {clear_doha,removed}).

%% ====================================================================
%% Internal functions - Activity ID storage
%% ====================================================================
register_act_ids(ActIds,HolonId,SchedExeOrBio)->
	custom_erlang_functions:myGenServCall(act_id_storage, {register_act_ids, ActIds,HolonId,SchedExeOrBio}).

get_act_locations(ActID)->
	custom_erlang_functions:myGenServCall(act_id_storage, {get_act_location,ActID}).

get_holon_act_info(HolonID)->
	custom_erlang_functions:myGenServCall(act_id_storage, {get_holon_act_info,HolonID}).

deregister_act_id(ActID)->
	custom_erlang_functions:myGenServCall(act_id_storage, {deregister_act_id, ActID}).

%%SLOC:84