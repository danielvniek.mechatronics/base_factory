
-module(doha_default_storage).
-author("Daniel van Niekerk (adapted from original by Dale Sparrow)").

-behaviour(gen_server).
-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(doha_def_storage_state, {ets_table::term(),file_path=""}).
-record(bc_record,{table_key,id,type,address,services,service_type,architecture,removed}).
%%%===================================================================

start_link() ->
  {ok,_PID} = gen_server:start_link({local,doha_default_storage},?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks

init([]) ->
  	NewState = create_or_load_table(), 
   	yes = global:register_name(doha_default_storage,self()),
  	{ok, NewState}.
%%--------------------------------------------------------------------


handle_call({remove_resource,ID},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(erlang:whereis(doha), From) of 
		true->
			MatchSpecTuple = {doha_match_spec,get_match10IDs,del_match10IDs},
			RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"get",MatchSpecTuple,[ID],[],State#doha_def_storage_state.file_path),
			case RawReply of
				{ok,BCRecords}->
					[BC] = doha_match_spec:bc_records_to_maps(BCRecords,[]),
					BCRecord = doha_match_spec:bc_map_to_records(BC,true),
					{ok,"success"} = def_storage_functions:save(State#doha_def_storage_state.ets_table,[BCRecord],State#doha_def_storage_state.file_path),
					def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"del",MatchSpecTuple,[ID],[],State#doha_def_storage_state.file_path),
					Reply = ok;
				_->
					Reply = {error,"No resource with this id"}
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only doha can call this process"},State}
	end;

handle_call({save_bc, BC}, From, State)->%%assume doha_recep already checked that BC valid
	case security_functions:from_is_process_or_monitored_by_process(erlang:whereis(doha), From) of 
		true->
			{ok,ID} = business_cards:get_id(BC),
			%%first delete old instances - this is because of multiple bc records for one bc map: each records is for different service type
			MatchSpecTuple = {doha_match_spec,get_match10IDs,del_match10IDs},
			def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"del",MatchSpecTuple,[ID],[],State#doha_def_storage_state.file_path),
			
			BCRecords = doha_match_spec:bc_map_to_records(BC,false),
			Reply = def_storage_functions:save(State#doha_def_storage_state.ets_table,BCRecords,State#doha_def_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only doha can call this process"},State}
	end;
%%only for time testing%%
handle_call({test,NrOfBcs},_From,State)->
	BCRecs = create_test_records(NrOfBcs,[]),
	Reply = def_storage_functions:save(State#doha_def_storage_state.ets_table,BCRecs,State#doha_def_storage_state.file_path),
	{reply,Reply,State};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

handle_call({get_or_del_all_bcs,GetOrDel,ActiveOrRemoved}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(erlang:whereis(doha), From) of 
		true->
			case ActiveOrRemoved of
				active->
					MatchSpecTuple = {doha_match_spec,get_matchAll,del_matchAll},
					RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,GetOrDel,MatchSpecTuple,none,[],State#doha_def_storage_state.file_path);
				_->
					MatchSpecTuple = {removed_resource_match_spec,get_matchAll,del_matchAll},
					case GetOrDel of
						"del"->
							{ok,BCs} = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"pop",MatchSpecTuple,none,[],State#doha_def_storage_state.file_path),
							spawn_monitor(fun()->delete_bcs_files(BCs) end),
							RawReply = {ok,"success"};
						_->
							RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"get",MatchSpecTuple,none,[],State#doha_def_storage_state.file_path)
					end
			end,
			
			
			case RawReply of
				{ok,BCRecords}->
					BCMaps = doha_match_spec:bc_records_to_maps(BCRecords,[]),
					Reply = {ok,BCMaps};
				_->
					Reply = RawReply
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only doha can call this process"},State}
	end;
handle_call({get_or_del_bcs_by_ids,GetOrDel,IDs,ActiveOrRemoved}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(erlang:whereis(doha), From) of 
		true->
			case ActiveOrRemoved of
				active->
					MatchSpecTuple = {doha_match_spec,get_match10IDs,del_match10IDs},
					RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,GetOrDel,MatchSpecTuple,IDs,[],State#doha_def_storage_state.file_path);
				_->
					MatchSpecTuple = {removed_resource_match_spec,get_match10IDs,del_match10IDs},
					case GetOrDel of
						"del"->
							{ok,BCs} = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"pop",MatchSpecTuple,IDs,[],State#doha_def_storage_state.file_path),
							spawn_monitor(fun()->delete_bcs_files(BCs) end),
							RawReply = {ok,"success"};
						_->
							RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"get",MatchSpecTuple,IDs,[],State#doha_def_storage_state.file_path)
					end
			end,
			
			case GetOrDel of
				"get"->
					case RawReply of
						{ok,BCRecords}->
							BCMaps = doha_match_spec:bc_records_to_maps(BCRecords,[]),
							Reply = {ok,BCMaps};
						_->
							Reply = RawReply
					end;
				_->
					Reply = RawReply
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only doha can call this process"},State}
	end;
handle_call({get_or_del_bcs_by_types, GetOrDel,Types,ActiveOrRemoved}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(erlang:whereis(doha), From) of 
		true->
			case ActiveOrRemoved of
				active->
					MatchSpecTuple = {doha_match_spec,get_match10Types,del_match10Types},
					RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,GetOrDel,MatchSpecTuple,Types,[],State#doha_def_storage_state.file_path);
				_->
					MatchSpecTuple = {removed_resource_match_spec,get_match10Types,del_match10Types},
					case GetOrDel of
						"del"->
							{ok,BCs} = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"pop",MatchSpecTuple,Types,[],State#doha_def_storage_state.file_path),
							spawn_monitor(fun()->delete_bcs_files(BCs) end),
							RawReply = {ok,"success"};
						_->
							RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"get",MatchSpecTuple,Types,[],State#doha_def_storage_state.file_path)
					end
			end,
			
			case RawReply of
				{ok,BCRecords}->
					BCMaps = doha_match_spec:bc_records_to_maps(BCRecords,[]),
					Reply = {ok,BCMaps};
				_->
					Reply = RawReply
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only doha can call this process"},State}
	end;
handle_call({get_bcs_by_ids_and_type,IDs,Type}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(erlang:whereis(doha), From) of 
		true->
			IDsAndType = [Type|IDs],
			MatchSpecTuple = {doha_match_spec,get_match10IdsAndType,no_del_function},
			RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"get",MatchSpecTuple,IDsAndType,[],State#doha_def_storage_state.file_path),
			case RawReply of
				{ok,BCRecords}->
					BCMaps = doha_match_spec:bc_records_to_maps(BCRecords,[]),
					Reply = {ok,BCMaps};
				_->
					Reply = RawReply
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only doha can call this process"},State}
	end;
handle_call({get_bcs_by_service_type,ServiceType}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(erlang:whereis(doha), From) of 
		true->
			MatchSpecTuple = {doha_match_spec,get_matchServType,none},
			RawReply = def_storage_functions:get_del_or_pop(State#doha_def_storage_state.ets_table,"get",MatchSpecTuple,ServiceType,[],State#doha_def_storage_state.file_path),
			case RawReply of
				{ok,BCRecords}->
					BCMaps = doha_match_spec:bc_records_to_maps(BCRecords,[]),
					Reply = {ok,BCMaps};
				_->
					Reply = RawReply
			end,
			{reply,Reply,State};
		_->
			{reply,{error,"Only doha can call this process"},State}
	end;

handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------


handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Doha default storage got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
%%--------------------------------------------------------------------

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Table functions:
create_or_load_table()->
	TableName = doha_table,
	{ok, CurrentDirectory} = file:get_cwd(),
	FileDir = string:concat(CurrentDirectory,"/Storage/doha_default_storage/"),
	ok = filelib:ensure_dir(FileDir),
	FilePath = string:concat(FileDir,"doha.txt"),
	ETS_table = def_storage_functions:claim_or_create_ets(TableName,FilePath),
	State = #doha_def_storage_state{ets_table = ETS_table,file_path = FilePath},
	State.

delete_bcs_files([])->
	ok;
delete_bcs_files([BCRecord|T])->
	Type = BCRecord#bc_record.type,
	ID = BCRecord#bc_record.id,
	{ok, CurrentDirectory} = file:get_cwd(),
	Temp = string:concat(CurrentDirectory,"/Storage/default_storage/"),
	Temp1 = string:concat(Temp,Type),
	TypeFolder = string:concat(Temp1,"/"),
	Temp2 = string:concat(TypeFolder,ID),
	IDFolder = string:concat(Temp2,"/"),
	AtrFile = string:concat(IDFolder, "atr.txt"),
	file:delete(AtrFile),
	BioFile = string:concat(IDFolder, "bio.txt"),
	file:delete(BioFile),
	SchedFile = string:concat(IDFolder, "sched.txt"),
	file:delete(SchedFile),
	ExeFile = string:concat(IDFolder, "exe.txt"),
	file:delete(ExeFile),
	file:del_dir(Temp2),
	delete_bcs_files(T).

%%only for time testing
create_test_records(NrOfResources,BcRecords)->
	case NrOfResources<1 of
		true->
			BcRecords;
		_->
			ID = "t"++integer_to_list(NrOfResources),
			Adr = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
			TableKey = #{id=>ID,serv_type=>"TestServ"},
			BCRecord = #bc_record{table_key = TableKey,service_type="TestServ",id = ID,type="Test",address=Adr,services=#{"TestServ"=>test_serv},architecture=base,removed=false},
			create_test_records(NrOfResources-1,[BCRecord|BcRecords])
	end.

%%SLOC:220

