%% @author Daniel
%% @doc @todo Add description to ets_match_spec.


-module(doha_match_spec).
-include_lib("stdlib/include/ms_transform.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([bc_records_to_maps/2,bc_map_to_records/2,get_matchServType/1,get_match10IdsAndType/1,get_match10IDs/1,get_match10Types/1,get_matchAll/0,del_match10IDs/1,del_match10Types/1,del_matchAll/0]).

-record(bc_record,{table_key,id,type,address,services,service_type,architecture,removed}).

%% ====================================================================
%% Internal functions
%% ====================================================================
bc_records_to_maps([],BCMaps)->
	BCMaps;
bc_records_to_maps([BCRec|T],BCMaps)->
	ID = BCRec#bc_record.id,
	Type = BCRec#bc_record.type,
	Address = BCRec#bc_record.address,
	Services = BCRec#bc_record.services,
	Architecture = BCRec#bc_record.architecture,
	{ok,NewBCMap} = business_cards:create_new_bc(ID, Type, Address, Services, Architecture),
	case lists:member(NewBCMap, BCMaps) of
		true->
			bc_records_to_maps(T,BCMaps);
		_->
			bc_records_to_maps(T,[NewBCMap|BCMaps])
	end.
bc_map_to_records(BC,Removed)->
	{ok,ID} = business_cards:get_id(BC),
	{ok,Type} = business_cards:get_type(BC),
	{ok,Address} = business_cards:get_address(BC),
	{ok,Services} = business_cards:get_services(BC),
	{ok,Architecture} = business_cards:get_architecture(BC),
	ServiceTypes = maps:keys(Services),
	BCRecord = #bc_record{id = ID,type=Type,address=Address,services=Services,architecture=Architecture,removed=Removed},
	case Removed of
		true->
			BCRecord#bc_record{table_key=ID};
		_->
			BCRecords = bc_record_for_each_service(BCRecord,ServiceTypes,[]),
			BCRecords
	end.
bc_record_for_each_service(BCRecord,[],BCRecords)->
	case BCRecords of
		[]->
			NewBcRec = BCRecord#bc_record{table_key = BCRecord#bc_record.id,service_type = "none"},
			[NewBcRec];
		_->
			BCRecords
	end;
bc_record_for_each_service(BCRecord,[ServType|T],BCRecords)->
	TableKey = #{id=>BCRecord#bc_record.id,serv_type=>ServType},
	NewBcRec = BCRecord#bc_record{table_key=TableKey,service_type=ServType},
	bc_record_for_each_service(BCRecord,T,[NewBcRec|BCRecords]).

get_matchAll()->
	MatchSpec = ets:fun2ms(
    fun(A=#bc_record{removed=false})
      ->
      A
    end),
	MatchSpec.


get_match10IDs(IDs)->
	L = length(IDs),
	case L>=10 of
		true->
			List = lists:sublist(IDs, 1, 10),
			RemainderList = lists:sublist(IDs, 11, L);
		_->
			RemainderList = [],
			Element1 = lists:nth(1, IDs),
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(IDs, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #bc_record{id=ID,removed=false})
      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
      A
    end),
	{MatchSpec,RemainderList}.
get_match10Types(Types)->
	L = length(Types),
	case L>=10 of
		true->
			List = lists:sublist(Types, 1, 10),
			RemainderList = lists:sublist(Types, 11, L);
		_->
			RemainderList = [],
			Element1 = lists:nth(1, Types),
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #bc_record{type=Type,removed=false})
      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
      A
    end),
	{MatchSpec,RemainderList}.

get_match10IdsAndType(IDsAndType)->
	Type = lists:nth(1,IDsAndType),
	L = length(IDsAndType),
	case L>=11 of
		true->
			List = lists:sublist(IDsAndType, 2, 11),
			RList = [Type|lists:sublist(IDsAndType, 12, L)],
			case length(RList) of
				1->
					RemainderList = [];
				_->
					RemainderList =RList
			end;
		_->
			RemainderList = [],
			Element1 = lists:nth(2, IDsAndType),
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(IDsAndType, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #bc_record{id=ID,type = T,removed=false})
      when T==Type andalso (ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10)->
      A
    end),
	{MatchSpec,RemainderList}.

get_matchServType(ServType)->
	MatchSpec = ets:fun2ms(
    fun(A = #bc_record{service_type=ST,removed=false})
      when ST ==ServType ->
      A
    end),
	{MatchSpec,[]}.
del_matchAll()->
	MatchSpec = ets:fun2ms(
    fun(A=#bc_record{removed=false})
      ->
      true
    end),
	MatchSpec.

del_match10IDs(IDs)->
	L = length(IDs),
	case L>=10 of
		true->
			List = lists:sublist(IDs, 1, 10),
			RemainderList = lists:sublist(IDs, 11, L);
		_->
			RemainderList = [],
			Element1 = lists:nth(1, IDs),
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(IDs, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #bc_record{id=ID,removed=false})
      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
      true
    end),
	{MatchSpec,RemainderList}.
del_match10Types(Types)->
	L = length(Types),
	case L>=10 of
		true->
			List = lists:sublist(Types, 1, 10),
			RemainderList = lists:sublist(Types, 11, L);
		_->
			RemainderList = [],
			Element1 = lists:nth(1, Types),
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	MatchSpec = ets:fun2ms(
    fun(A = #bc_record{type=Type,removed=false})
      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
      true
    end),
	{MatchSpec,RemainderList}.

%%SLOC:216
