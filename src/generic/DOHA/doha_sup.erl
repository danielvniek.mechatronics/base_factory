%%%-------------------------------------------------------------------
%%% @author Sparrow
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(doha_sup).

-behaviour(supervisor).

-export([start_link/0, init/1]).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  DohaRecep = #{id => 'doha_recep',
    start => {doha_recep, start_link, []},
    restart => temporary,
    shutdown => 2000,
    type => worker,
    modules => [doha_recep]},
  DohaDefStorage = #{id => 'doha_default_storage',
    start => {doha_default_storage, start_link, []},
    restart => temporary,
    shutdown => 2000,
    type => worker,
    modules => [doha_default_storage]},

  {ok, {#{strategy => one_for_one,
    intensity => 5,
    period => 30},
    [DohaDefStorage,DohaRecep]}
  }.
%%SLOC:20
