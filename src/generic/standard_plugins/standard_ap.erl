
-module(standard_ap).
-author("Daniel van Niekerk").

-behaviour(gen_server).

-export([start_link/3,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).
-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

-export([new_acts/5,old_acts/5]).
-record(ap_state, {reception_map,coordinator,variables,file_checker,mod,que}).%%add variables as needed for calcs	

%%%===================================================================

start_link(PluginDetailsMap,CoordinatorPid,PluginSup) ->
  {ok,PID} = gen_server:start_link(?MODULE, [PluginDetailsMap,CoordinatorPid,PluginSup], []),
  {ok,PID}.

stop() ->
  gen_server:stop(self()).

init([PluginDetailsMap,CoordinatorPid,PluginSup]) ->
	{ok,ReceptionMap} = custom_erlang_functions:myGenServCall(CoordinatorPid,{register_plugin_and_get_receptions,PluginDetailsMap,PluginSup}),
	MyPid = self(),
	{ok,MyBc} = comms_api:get_bc(maps:get(comms, ReceptionMap)),
	{ok,MyType} = business_cards:get_type(MyBc),
	ModS = string:lowercase(string:replace(MyType, " ", "_",all)) ++"_ap",
	Mod = list_to_atom(ModS),
	{Pid,_Ref}=spawn_monitor(fun()->get_file_and_register_loop(Mod,ReceptionMap,MyPid,[]) end),
	{ok, #ap_state{reception_map=ReceptionMap,coordinator = CoordinatorPid,variables=#{},file_checker=Pid,mod=Mod,que=[]}}. 

%-----------------------------------------------------------------
handle_call({old_acts,ActType,OldActs},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			case State#ap_state.que of
				[]->
					MyPid = self(),
					spawn_monitor(fun()->old_acts(State#ap_state.mod,{ActType,OldActs},State#ap_state.reception_map,State#ap_state.variables,MyPid)end),
					{reply,ok,State#ap_state{que=[{old_acts,{ActType,OldActs}}]}};
				_->
					NewQue = [{old_acts,{ActType,OldActs}}|State#ap_state.que],
					{reply,ok,State#ap_state{que=NewQue}}
			end;
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;

handle_call({spawn_process,Mod,Fun,Args},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			try
				{Pid,_Ref} = spawn_monitor(Mod,Fun,Args),
				{reply,{ok,Pid},State}
			catch
				A:B:StackTrace->
					ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
					{reply,{error,ErrorAndStackTrace},State}
			end;
		_->
			{reply,{error,"Only my processes can spawn more processes under me"},State}
	end;
	
	
handle_call({new_bio_entries,Acts},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(maps:get(bio,State#ap_state.reception_map), From) of
		true->
			case State#ap_state.que of
				[]->
					MyPid = self(),
					spawn_monitor(fun()->new_acts(State#ap_state.mod,Acts,State#ap_state.reception_map,State#ap_state.variables,MyPid)end),
					{reply,ok,State#ap_state{que=[{new_acts,Acts}]}};
				_->
					NewQue = [{new_acts,Acts}|State#ap_state.que],
					{reply,ok,State#ap_state{que=NewQue}}
			end;
			
		_->
			{reply,{error,"Only the biography of this instance can make this call"},State}
	end;

handle_call({new_variables,Variables},From,State)->%also triggers queued calls if any
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			NewQue = lists:droplast(State#ap_state.que),
			case NewQue of
				[]->
					ok;
				_->
					{Fun,SpecArgs} = lists:last(NewQue),
					MyPid = self(),
					spawn_monitor(?MODULE,Fun,[State#ap_state.mod,SpecArgs,State#ap_state.reception_map,State#ap_state.variables,MyPid])
			end,
			{reply,ok,State#ap_state{variables = Variables,que=NewQue}};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;

handle_call({updated_receptions,ReceptionMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#ap_state.coordinator, From) of
		true->
			%error_log:log(?MODULE,0,unknown,"Analysis plugin named ~p got updated receptions.",[?MODULE]),
			exit(State#ap_state.file_checker,kill),
			MyPid = self(),
			{Pid,_Ref}=spawn_monitor(fun()->get_file_and_register_loop(State#ap_state.mod,ReceptionMap,MyPid,[]) end),
			NewState = State#ap_state{reception_map=ReceptionMap,file_checker=Pid},
			{reply,ok,NewState};
		_->
			{reply,{error,"Only the coordinator of my instance can make this call"},State}
	end;

handle_call(_Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"\nAnalysis plugin named ~p got unrecognized call",[?MODULE]),
  {reply, unknown, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
%Plugin specific functions: edit below and use the top as it is
get_file_and_register_loop(Mod,ReceptionMap,MyPid,ActTypes)->	
	try
		NewActTypes = Mod:get_act_types(),
		case NewActTypes==ActTypes of
			true->
				ok;
			_->
				deregister_old(ActTypes,NewActTypes,maps:get(bio,ReceptionMap),MyPid),
				AddedActTypes = get_added_act_types(ActTypes,NewActTypes,[]),
				register_for_acts(ReceptionMap,MyPid,AddedActTypes,Mod)
		end,
		timer:sleep(30000),
		get_file_and_register_loop(Mod,ReceptionMap,MyPid,NewActTypes)
	catch
		_A:_B:_StackTrace -> 
			%error_log:log(?MODULE,0,unknown,"\nERROR: standard_ap could not get the required information from ~p. Error and stacktrace: ~p:~p:~p",[Mod,A,B,StackTrace]),
		timer:sleep(30000),
		get_file_and_register_loop(Mod,ReceptionMap,MyPid,ActTypes)
	end.

get_added_act_types(_OldTypes,[],Added)->
	Added;
get_added_act_types(OldTypes,[Type|T],Added)->
	case lists:member(Type, OldTypes) of
		true->
			get_added_act_types(OldTypes,T,Added);
		_->
			get_added_act_types(OldTypes,T,[Type|Added])
	end.

deregister_old([],_NewActTypes,_BioRecep,_MyPid)->
	ok;
deregister_old([OldType|T],NewActTypes,BioRecep,MyPid)->
	case lists:member(OldType, NewActTypes) of
		true->
			ok;
		_->
			bio_api:deregister_analysis_plugin(BioRecep, MyPid, OldType)
	end,
	deregister_old(T,NewActTypes,BioRecep,MyPid).

register_for_acts(_ReceptionMap,_MyPid,[],_Mod)->
	ok;
register_for_acts(ReceptionMap,MyPid,[ActType|T1],Mod)->
	BioPid = maps:get(bio,ReceptionMap),
	timer:sleep(20),%%give coordinator time to pass this plugins pid around
	{Res,OldActs} = bio_api:register_analysis_plugin(BioPid, MyPid, ActType),
	case Res of 
		ok->
					try 
						custom_erlang_functions:myGenServCall(MyPid,{old_acts,ActType,OldActs})
					catch 
						_:_:_->
							ok 
					end,
			register_for_acts(ReceptionMap,MyPid,T1,Mod);
		_->%%sometimes this plugin's pid has not yet been sent by coordinator to core components and they do not permit the registration
			timer:sleep(500),
			register_for_acts(ReceptionMap,MyPid,[ActType|T1],Mod)
	end.

old_acts(Mod,{ActType,Acts},ReceptionMap,Variables,MyPid)->
	try
		NewVariables = Mod:analyse_old_acts(ActType,Acts,maps:put(plugin,MyPid,ReceptionMap),Variables),
		custom_erlang_functions:myGenServCall(MyPid,{new_variables,NewVariables})
	catch
		A:B:StackTrace->
			ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
			FailedS2Data = #{<<"Result">>=><<"Failed">>,<<"Reason">>=>atom_to_list(Mod) ++" could not analyse old acts of type "++ActType,<<"Error and stacktrace">>=>ErrorAndStackTrace},
			exe_api:start_and_finish_new_act(maps:get(exe,ReceptionMap), "Analysis error", #{}, FailedS2Data),
			custom_erlang_functions:myGenServCall(MyPid,{new_variables,Variables})
	end.
	
new_acts(_Mod,[],_ReceptionMap,Variables,MyPid)->	
	custom_erlang_functions:myGenServCall(MyPid,{new_variables,Variables});

new_acts(Mod,[H|T],ReceptionMap,Variables,MyPid)->
	try
		NewVariables = Mod:analyse_act(H,maps:put(plugin,MyPid,ReceptionMap),Variables),
		new_acts(Mod,T,ReceptionMap,NewVariables,MyPid)
	catch
		A:B:StackTrace->
			ErrorAndStackTrace = lists:flatten(io_lib:format("~p:~p:~p",[A,B,StackTrace])),
			FailedS2Data = #{<<"Result">>=><<"Failed">>,<<"Reason">>=>atom_to_list(Mod) ++" could not analyse new activity of type "++H#stage3Activity.type,<<"Error and stacktrace">>=>ErrorAndStackTrace},
			exe_api:start_and_finish_new_act(maps:get(exe,ReceptionMap), "Analysis error", #{}, FailedS2Data),
			new_acts(Mod,T,ReceptionMap,Variables,MyPid)
	end.
	
