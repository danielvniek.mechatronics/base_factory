-module(resource_data_ep).
-author("Daniel van Niekerk").

-behaviour(gen_server).

-export([start_link/3,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).



-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).
-record(ep_state, {reception_map,coordinator,plugin_identifier,rd_receivers,busy_new_serv_provs,factory_ready,identifier_types}).				

%%%===================================================================

start_link(PluginDetailsMap,CoordinatorPid,PluginSup) ->
  {ok,PID} = gen_server:start_link(?MODULE, [PluginDetailsMap,CoordinatorPid,PluginSup], []),
  {ok,PID}.

stop() ->
  gen_server:stop(self()).

init([PluginDetailsMap,CoordinatorPid,PluginSup]) ->
	{ok,ReceptionMap} = custom_erlang_functions:myGenServCall(CoordinatorPid,{register_plugin_and_get_receptions,PluginDetailsMap,PluginSup}),
	MyPid = self(),
	spawn_monitor(fun()->get_current_receiver_acts_and_service_providers(ReceptionMap,MyPid)end),
    {ok, #ep_state{reception_map = ReceptionMap,coordinator = CoordinatorPid,plugin_identifier=PluginDetailsMap,rd_receivers = #{},busy_new_serv_provs=[],factory_ready=false,identifier_types=[]}}. 

%-----------------------------------------------------------------
handle_call({new_service_provider,ServType,BC},From,State)->
	DohaPid = whereis(doha),
	FullPermittedList = [self(),DohaPid],
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(FullPermittedList, From) of
		true->
			MyPid = self(),
			CurBusyServProvs = State#ep_state.busy_new_serv_provs,
			case State#ep_state.factory_ready of
				true->
					case CurBusyServProvs of
						[]->
							spawn_monitor(fun()->new_service_provider(ServType,BC,State,MyPid)end);
						_->
							ok
					end;
				_->
					ok
			end,
			
			NewBusyServProvs = [{ServType,BC}|CurBusyServProvs],
			{reply,ok,State#ep_state{busy_new_serv_provs = NewBusyServProvs}};
		_->
			{reply,{error,"Only doha can make this call"},State}
	end;

handle_call({done_with_new_serv_prov,ServType,BC},From,State)->%%idea is that we want the new_serv_prov process to know the most up to date list of sensors before adding a new one
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			CurBusyServProvs = State#ep_state.busy_new_serv_provs,
			NewBusyServProvs = lists:delete({ServType,BC}, CurBusyServProvs),
			NewState = State#ep_state{busy_new_serv_provs = NewBusyServProvs},
			case NewBusyServProvs of
				[]->
					ok;
				_->
					{NextServType,NextBC} = lists:nth(1, NewBusyServProvs),
					MyPid = self(),
					spawn_monitor(fun()->new_service_provider(NextServType,NextBC,NewState,MyPid)end)
			end,
			{reply,ok,NewState};
		_->
			{reply,{error,"Only my own process can make this call"},State}
	end;

handle_call({new_rd_receiving_act,Contract,Details},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			NewRDReceivers = maps:put(Contract, Details, State#ep_state.rd_receivers),
			{reply,ok,State#ep_state{rd_receivers = NewRDReceivers}};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;	

handle_call({remove_rd_receiving_act,Contract},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			NewRDReceivers = maps:remove(Contract, State#ep_state.rd_receivers),
			{reply,ok,State#ep_state{rd_receivers = NewRDReceivers}};
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;	
	
handle_call({service_package_delivery,Contract,Package,_Requester},From,State)->%%no need to check requester - already done by comms
	case security_functions:from_is_process_or_monitored_by_process(maps:get(comms,State#ep_state.reception_map), From) of
		true->
			case maps:is_key(Contract, State#ep_state.rd_receivers) of
				true->
					spawn_monitor(fun()->service_package_delivery(Contract,Package,State,From) end),
					{noreply,State};
				_->
					{reply,{reject,"Not expecting resource data from you"},State}
			end;
		_->
			{reply,{error,"Only my comms recep can make this call"},State}
	end;

handle_call({service_stopped,Contract,Reason,_Requester},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(maps:get(comms,State#ep_state.reception_map), From) of
		true->
			case maps:is_key(Contract, State#ep_state.rd_receivers) of
				true->
					NewRDReceivers = maps:remove(Contract, State#ep_state.rd_receivers),
					spawn_monitor(fun()->service_stopped(Contract,Reason,State#ep_state.rd_receivers) end),
					{reply,accept,State#ep_state{rd_receivers = NewRDReceivers}};
				_->
					{reply,{reject,"Not expecting resource data from you"},State}
			end;
		_->
			{reply,{error,"Only my comms recep can make this call"},State}
	end;

handle_call({updated_receptions,ReceptionMap},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(State#ep_state.coordinator, From) of
		true->
			%error_log:log(?MODULE,0,unknown,"Execution plugin named ~p got updated receptions.",[?MODULE]),
			ExePid = maps:get(exe,ReceptionMap),
			case ExePid == maps:get(exe,State#ep_state.reception_map) of
				true->
					ok;
				_->
					MyPid = self(),
					spawn_monitor(fun()->get_current_receiver_acts_and_service_providers(ReceptionMap,MyPid)end)				
			end,
			NewState = State#ep_state{reception_map = ReceptionMap},
			{reply,ok,NewState};
		_->
			{reply,{error,"Only the coordinator of this instance can make this call"},State}
	end;

handle_call({new_identifier_type,IdentifierType},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(self(), From) of
		true->
			CurTypes = State#ep_state.identifier_types,
			case lists:member(IdentifierType,CurTypes) of
				true->
					{reply,ok,State};
				_->
					NewTypes = [IdentifierType|CurTypes],
					
					{reply,ok,State#ep_state{identifier_types=NewTypes}}
			end;
		_->
			{reply,{error,"Only my own processes can make this call"},State}
	end;

handle_call({attributes_updated,Atrs},From,State)->
	case security_functions:from_is_process_or_monitored_by_process(maps:get(atr,State#ep_state.reception_map), From) of
		true->
			ChangedIdentifiers = analyse_atrs(Atrs,State#ep_state.identifier_types,[]),
			case ChangedIdentifiers of
				[]->
					ok;
				_->
					MyPid = self(),
					spawn_monitor(fun()->cancel_contracts_with_changed_identifiers_and_reregister_with_doha(ChangedIdentifiers,State,MyPid)end)
			end,
			{reply,ok,State};
		_->
			{reply,{error,"Only the atr of this instance can make this call"},State}
	end;
	
	
handle_call(Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"\nExecution plugin named ~p got unrecognized call: ~p",[?MODULE,Request]),
  {reply, unknown, State}.

handle_cast(factory_ready,State)->%%so that the sensor only starts sending data when all clients are ready
	case State#ep_state.factory_ready of
		true->
			{noreply,State};
		_->
			CurBusyServProvs = State#ep_state.busy_new_serv_provs,
			case CurBusyServProvs of
				[]->
					ok;
				_->
					{NextServType,NextBC} = lists:nth(1, CurBusyServProvs),
					MyPid = self(),
					spawn_monitor(fun()->new_service_provider(NextServType,NextBC,State,MyPid)end)%%will start chain reaction of starting pending
			end,
			{noreply,State#ep_state{factory_ready = true}}
	end;

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%Internal functions
get_current_receiver_acts_and_service_providers(ReceptionMap,MyPid)->
	timer:sleep(20),%%give coordinator time to pass this plugins pid around
	{Res,CurrentRecActs} = exe_api:get_acts_by_types(maps:get(exe,ReceptionMap), ["Receive resource data"]),
	case Res of 
		ok->
			handle_unfinished_acts(CurrentRecActs,ReceptionMap,MyPid),
			doha_api:register_for_service_providers("Observe",MyPid),%%doha registration only after unfinished acts all analysed
			atr_api:register_scheduling_plugin(maps:get(atr,ReceptionMap), MyPid, "Personal"),%%Identifier type needed might come from  personal, management or relational attributes
			atr_api:register_scheduling_plugin(maps:get(atr,ReceptionMap), MyPid, "Management"),
			atr_api:register_scheduling_plugin(maps:get(atr,ReceptionMap), MyPid, "Relational");
		_->%%sometimes this plugin's pid has not yet been sent by coordinator to core components and they do not permit the registration
			timer:sleep(500),
			get_current_receiver_acts_and_service_providers(ReceptionMap,MyPid)
	end.


handle_unfinished_acts([],_ReceptionMap,_MyPid)->
	ok;
handle_unfinished_acts([Act|T],ReceptionMap,MyPid)->
	S1Data = Act#stage2Activity.schedule_info#schedule_info.s1data,
	Contract = maps:get(<<"Contract">>, S1Data),
	DataFields = maps:get(<<"Topics">>,S1Data),
	IdentifierType = maps:get(<<"Identifier type">>,S1Data),
	{ok,AHPid} = exe_api:start_act_handler(maps:get(exe,ReceptionMap), Act, MyPid),
	{ok,ServBC} = contracts:get_service_bc(Contract),
	{ok,Services} = business_cards:get_services(ServBC),
	ServDescr = maps:get("Observe", Services),
	SensorDescr = maps:get(<<"Interface Description">>,ServDescr),
	{ok,SensorID} = business_cards:get_id(ServBC),
	LoggedS1Data = #{<<"Observer Description">>=>SensorDescr,<<"Sensor ID">>=>SensorID},
	custom_erlang_functions:myGenServCall(MyPid,{new_rd_receiving_act,Contract,{AHPid,DataFields,LoggedS1Data}}),
	custom_erlang_functions:myGenServCall(MyPid,{new_identifier_type,IdentifierType}),
	handle_unfinished_acts(T,ReceptionMap,MyPid).



new_service_provider(ServType,BC,State,MyPid)->
	{ok,MyBC} = comms_api:get_bc(maps:get(comms,State#ep_state.reception_map)),
	{ok,MyType} = business_cards:get_type(MyBC),
	{ok,Services} = business_cards:get_services(BC),
	ServDescr = maps:get(ServType, Services),
	Resources = maps:get(<<"Resources">>,ServDescr),
	case maps:is_key(all, Resources) or maps:is_key(MyType, Resources) of
		true->
			CurContracts = maps:keys(State#ep_state.rd_receivers),
			case new_sensor_or_changed_sensor(CurContracts,BC,State,MyPid) of
				true->
					case maps:is_key(all, Resources) of
						true->
							RDDetails = maps:get(all,Resources);
						_->
							RDDetails = maps:get(MyType,Resources)
					end,
					
					ServInterval = {periodically,#{start => base_time:now(),interval=>0,stop=>never}},
					{ok,Contract} = contracts:create_contract(MyBC, BC, State#ep_state.plugin_identifier, "Observe", ServInterval, #{}, #{}),
					Reply = comms_api:send_service_request(maps:get(comms,State#ep_state.reception_map), Contract),
					DataFields = maps:get(<<"Topics">>,RDDetails),
					IdentifierType = maps:get(<<"Identifier type">>,RDDetails),
					SensorDescr = maps:get(<<"Interface Description">>,ServDescr),
					{ok,SensorID} = business_cards:get_id(BC),
					S1Data = #{<<"Observer Description">>=>SensorDescr,<<"Observer ID">>=>SensorID},%%unfinished s1 data for new data packages that will come in
					case Reply of
						accept->
							SchedPid = maps:get(sched,State#ep_state.reception_map),
							{ok,ActId} = sched_api:new_act(SchedPid, "Receive resource data", base_time:now(), #{<<"Contract">>=>Contract,<<"Topics">>=>DataFields,<<"Identifier type">>=>IdentifierType}),%%base_time:now()+5 is so this plugin does not
							{ok,AHPid} = sched_api:start_act_and_get_act_handler(SchedPid, ActId, MyPid),
							act_handler_api:update_progress(AHPid, #{<<"Nr of data entries received">>=>0,<<"Date of last data entry received">>=>"none"}),
							custom_erlang_functions:myGenServCall(MyPid,{new_rd_receiving_act,Contract,{AHPid,DataFields,S1Data}}),
							ok = custom_erlang_functions:myGenServCall(MyPid,{done_with_new_serv_prov,ServType,BC});
						{reject,Reason}->
							exe_api:start_and_finish_new_act(maps:get(exe,State#ep_state.reception_map), "Resource data request rejected", #{<<"Contract">>=>Contract,<<"Data fields">>=>DataFields}, #{<<"Reason">>=>Reason}),
							ok = custom_erlang_functions:myGenServCall(MyPid,{done_with_new_serv_prov,ServType,BC});
						{error,_E}->%%plugin not recognized yet, wait a bit and try again
							timer:sleep(500),
							new_service_provider(ServType,BC,State,MyPid)
					end,
					custom_erlang_functions:myGenServCall(MyPid,{new_identifier_type,IdentifierType});
				_->
					ok
			end;
		_->
			ok
	end.
	

service_package_delivery(Contract,Package,State,From)->
	{AHPid,DataFields,S1Data} = maps:get(Contract,State#ep_state.rd_receivers),
	%error_log:log(?MODULE,0,unknown,"\nPackage:~p",[Package]),
	case is_map(Package) andalso custom_erlang_functions:isListOfStrings(maps:keys(Package)) of
		true->
			Reply = log_each_data_field(maps:keys(Package),Package,DataFields,S1Data,State#ep_state.reception_map,AHPid,accept);
		_->
			Reply = {reject,"Package is expected to be a map with data fields as keys and the recorded resource data for each field as values"}
	end,
	gen_server:reply(From, Reply).

log_each_data_field([],Package,_DataFields,_S1Data,_ReceptionMap,AHPid,Reply)->
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	CurNr = maps:get(<<"Nr of data entries received">>,S2Data),
	NewNr = CurNr + length(maps:keys(Package)),
	NewDate = base_time:base_time_to_date_and_time_string(base_time:now()),
	NewS2Data = #{<<"Nr of data entries received">>=>NewNr,<<"Date of last data entry received">>=>NewDate},
	act_handler_api:update_progress(AHPid, NewS2Data),
	Reply;

log_each_data_field([DataField|T],Package,ExpectedDataFields,S1Data,ReceptionMap,AHPid,Reply)->
	case maps:is_key(DataField, ExpectedDataFields) of
		true->
			FieldDetails = maps:get(DataField,ExpectedDataFields),
			FinalS1Data = maps:merge(S1Data, FieldDetails),
			ActType = DataField ++ " (RD)",
			Data = maps:get(DataField,Package),
			case (is_map(Data)) andalso (maps:is_key(<<"Value">>, Data)) of
				true->
					Temp = maps:remove(<<"Date">>, Data),
					S2Data = maps:remove(<<"Time">>, Temp);%%just incase date and time included
				_->
					S2Data = #{<<"Value">>=>Data}
			end,
			
			Value = maps:get(<<"Value">>,S2Data),
			Accuracy = maps:get(<<"Accuracy">>,FinalS1Data),
			case maps:is_key(<<"Date">>, Data) and maps:is_key(<<"Time">>, Data) of
				true->
					DateB = maps:get(<<"Date">>,Data),TimeB = maps:get(<<"Time">>,Data),
					case is_binary(DateB) of
						true->
							Date = binary_to_list(DateB);
						_->
							Date = DateB
					end,
					case is_binary(TimeB) of
						true->
							Time = binary_to_list(TimeB);
						_->
							Time = TimeB
					end,
					try
						BaseT = base_time:date_and_time_strings_to_base_time(Date, Time),
						ok = exe_api:start_and_finish_new_act(maps:get(exe,ReceptionMap), ActType, FinalS1Data, S2Data, BaseT, BaseT),
						DateAndTime = base_time:base_time_to_date_and_time_string(BaseT),
						update_sbb_with_rd(ReceptionMap,DataField,Value,DateAndTime,Accuracy)
					catch
						A:B:StackTrace->
							error_log:log(?MODULE,0,unknown,"\nInvalid Date and Time from sensor. ~p:~p:~p",[A,B,StackTrace]),
							T2 = base_time:now(),
							ok = exe_api:start_and_finish_new_act(maps:get(exe,ReceptionMap), ActType, FinalS1Data, S2Data, T2, T2),
							DateAndTime2 = base_time:base_time_to_date_and_time_string(base_time:now()),
							update_sbb_with_rd(ReceptionMap,DataField,Value,DateAndTime2,Accuracy)
					end;
					
				_->
					ok = exe_api:start_and_finish_new_act(maps:get(exe,ReceptionMap), ActType, FinalS1Data, S2Data),
					DateAndTime = base_time:base_time_to_date_and_time_string(base_time:now()),
					update_sbb_with_rd(ReceptionMap,DataField,Value,DateAndTime,Accuracy)
			end,
			
			case Reply of
				{reject,_Reason}->
					NewReply = Reply;
				_->
					NewReply = accept
			end,
			NewPackage = Package;
		_->
			case Reply of
				{reject,Reason}->
					NewReply = {reject,Reason++ ", "++DataField};
				_->
					NewReply = {reject,"Not expecting these data fields: "++ DataField}
			end,
			NewPackage = maps:remove(DataField, Package)
	end,
	log_each_data_field(T,NewPackage,ExpectedDataFields,S1Data,ReceptionMap,AHPid,NewReply).

service_stopped(Contract,Reason,RDReceivers)->
	{AHPid,_DataFields,_S1Data} = maps:get(Contract,RDReceivers),
	{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	ReasonS = lists:flatten(io_lib:format("~p",[Reason])),
	NewS2Data = maps:put(<<"Cancel reason">>,"Reason provided by service provider: "++ReasonS,S2Data),
	act_handler_api:done(AHPid, NewS2Data).


new_sensor_or_changed_sensor([],_BC,_State,_MyPid)->
	true;
new_sensor_or_changed_sensor([Contract|T],BC,State,MyPid)->
	{ok,CurBC} = contracts:get_service_bc(Contract),
	{ok,CurId} = business_cards:get_id(CurBC),
	{ok,NewId} = business_cards:get_id(BC),
	case (CurId==NewId) of
		true->
			{ok,CurServ} = business_cards:get_services(CurBC),
			{ok,NewServ} = business_cards:get_services(BC),
			case (CurServ==NewServ) of
				true->
					false;
				_->
					%%gonna delete old contract now
					comms_api:send_cancel_service(maps:get(comms,State#ep_state.reception_map), Contract, "Your services have changed so i am cancelling this contract and making a new one"),
					{AHPid,_D,_S} = maps:get(Contract,State#ep_state.rd_receivers),
					{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
					S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
					NewS2Data = maps:put(<<"Cancel reason">>,"Service providers service details changed. This contract is cancelled and a new one is created",S2Data),
					act_handler_api:done(AHPid, NewS2Data),
					custom_erlang_functions:myGenServCall(MyPid,{remove_rd_receiving_act,Contract}),
					true
			end;
		_->
			new_sensor_or_changed_sensor(T,BC,State,MyPid)
	end.

analyse_atrs([],_IdentifierTypes,ChangedIdentifierTypes)->
	ChangedIdentifierTypes;
analyse_atrs([Atr|T],IdentifierTypes,ChangedIdentifierTypes)->
	{ok,AtrId} = attribute_functions:get_atr_id(Atr),
	case lists:member(AtrId,IdentifierTypes) of
		true->
			analyse_atrs(T,IdentifierTypes,[AtrId|ChangedIdentifierTypes]);
		_->
			analyse_atrs(T,IdentifierTypes,ChangedIdentifierTypes)
	end.

cancel_contracts_with_changed_identifiers_and_reregister_with_doha(ChangedIdentifierTypes,State,MyPid)->
	{ok,CurrentRecActs} = exe_api:get_acts_by_types(maps:get(exe,State#ep_state.reception_map), ["Receive resource data"]),
	cancel_contracts_with_changed_identifiers(CurrentRecActs,ChangedIdentifierTypes,State,MyPid),
	doha_api:register_for_service_providers("Observe",MyPid).

cancel_contracts_with_changed_identifiers([],_ChangedIdentifierTypes,_State,_MyPid)->
	ok;																		
																				
cancel_contracts_with_changed_identifiers([Act|T],ChangedIdentifierTypes,State,MyPid)->
	S1Data = Act#stage2Activity.schedule_info#schedule_info.s1data,
	IdentifierType = maps:get(<<"Identifier type">>,S1Data),
	Contract = maps:get(<<"Contract">>,S1Data),
	case lists:member(IdentifierType,ChangedIdentifierTypes) of
		true->
			comms_api:send_cancel_service(maps:get(comms,State#ep_state.reception_map), Contract, "My identifier that you are using has changed. Will create a new service request."),
			{AHPid,_D,_S} = maps:get(Contract,State#ep_state.rd_receivers),
			{ok,S2Act} = act_handler_api:get_s2_act(AHPid),
			S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
			NewS2Data = maps:put(<<"Cancel reason">>,"My identifier that this observer is using has changed. Will create a new service request.",S2Data),
			act_handler_api:done(AHPid, NewS2Data),
			custom_erlang_functions:myGenServCall(MyPid,{remove_rd_receiving_act,Contract});
		_->
			ok
	end,
	cancel_contracts_with_changed_identifiers(T,ChangedIdentifierTypes,State,MyPid).

update_sbb_with_rd(ReceptionMap,DataField,Value,DateAndTime,Confidence)->
	AtrRecep = maps:get(atr,ReceptionMap),
	ActType = DataField ++ " (RD)",

	AtrVal = #{<<"Date and time">>=>DateAndTime,<<"Confidence">>=>Confidence,<<"Value">>=>Value},
	{ok,Atr} = attribute_functions:create_attribute(DataField, "State variables", ActType, AtrVal),
	atr_api:save(AtrRecep,[Atr]).
		
			
