
-module(standard_ep).
-author("Daniel van Niekerk").

-behaviour(gen_server).

-export([start_link/3,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-record(ep_state, {factory_ready,replies_when_factory_ready,plugin_identifier,serv_variables,reception_map,coordinator}).				

%%%===================================================================

start_link(PluginDetailsMap,CoordinatorPid,PluginSup) ->
  {ok,PID} = gen_server:start_link(?MODULE, [PluginDetailsMap,CoordinatorPid,PluginSup], []),
  {ok,PID}.

stop() ->
  gen_server:stop(self()).

init([PluginDetailsMap,CoordinatorPid,PluginSup]) ->
	{ok,ReceptionMap} = custom_erlang_functions:myGenServCall(CoordinatorPid,{register_plugin_and_get_receptions,PluginDetailsMap,PluginSup}),
	MyPid = self(),
	{ok,MyBc} = comms_api:get_bc(maps:get(comms, ReceptionMap)),
	{ok,MyType} = business_cards:get_type(MyBc),
	ModS = string:lowercase(string:replace(MyType, " ", "_",all)) ++"_ep",
	Mod = list_to_atom(ModS),
	{Pid,_Ref}=spawn_monitor(fun()->ra_helper_funcs:get_file_and_register_loop(Mod,ReceptionMap,MyPid,#{}) end),
	ServVariables = #{mod=>Mod,file_checker=>Pid,busy_acts=>#{},sc_details=>#{},active_acts=>#{},rfps=>#{},act_rfps=>#{}},
	{ok, #ep_state{factory_ready=false,replies_when_factory_ready=[],plugin_identifier=PluginDetailsMap,serv_variables=ServVariables,reception_map=ReceptionMap,coordinator = CoordinatorPid}}. 

%-----------------------------------------------------------------
handle_call({done_working_on_act,ActId},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:done_working_on_act(State#ep_state.serv_variables, From, ActId),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};

handle_call({clear_act,ActId,Reason,_ClearSched},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:clear_act(State#ep_state.serv_variables, From, State#ep_state.reception_map, ActId, Reason,na),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};

handle_call({acts_ready,Acts},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:acts_ready(State#ep_state.serv_variables, From, State#ep_state.reception_map, Acts),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};

handle_call({busy_with_unfinished_act,Act,OldSubContracts},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:busy_with_unfinished_act(State#ep_state.serv_variables, From, Act, OldSubContracts),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};
	
handle_call({spawn_process,Mod,Fun,Args},From,State)->
	Reply = spa_and_ra_calls:spawn_process(From, Mod, Fun, Args),
	{reply,Reply,State};

handle_call({send_rfps,ActId,ServBCs,ServType,Interval,RequestArgs,PackageBlueprint,ProposalTemplate,ProposalTimeLimit,RfpIdentifier},From,State)->%%from my processes
	{ok,MyBc} = comms_api:get_bc(maps:get(comms,State#ep_state.reception_map)),
	case is_list(ServBCs) of
		true->
			{Reply,NewServVariables} = spa_and_ra_calls:send_rfps(maps:put(bc,MyBc,State#ep_state.serv_variables), From, State#ep_state.reception_map, ActId, ServBCs, State#ep_state.plugin_identifier, ServType, Interval, RequestArgs, PackageBlueprint,ProposalTemplate,ProposalTimeLimit,[],RfpIdentifier),
			{reply,Reply,State#ep_state{serv_variables=NewServVariables}};
		_->
			{reply,{error,"ServBCs must be a list"},State}
	end;

handle_call({rfps_timeout,ActId,RfpIdentifier},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:rfps_timeout(State#ep_state.serv_variables, From, State#ep_state.reception_map, ActId,RfpIdentifier),
	{reply,Reply,State#ep_state{serv_variables = NewServVariables}};
	
handle_call({proposal,Contract,Proposal,ReplyTo},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:proposal(State#ep_state.serv_variables, From, State#ep_state.reception_map, Contract, Proposal,ReplyTo),
	{reply,Reply,State#ep_state{serv_variables = NewServVariables}};

handle_call({cancel_rfps,ActId,RfpIdentifier},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:cancel_rfps(State#ep_state.serv_variables, From, ActId,RfpIdentifier),
	{reply,Reply,State#ep_state{serv_variables = NewServVariables}};

handle_call({send_service_request,ActId,ServBC,ServType,Interval,RequestArgs,PackageBlueprint,Proposal},From,State)->
	{ok,MyBc} = comms_api:get_bc(maps:get(comms,State#ep_state.reception_map)),
	Reply = spa_and_ra_calls:send_service_request(maps:put(bc,MyBc,State#ep_state.serv_variables), From, State#ep_state.reception_map, ActId, ServBC, State#ep_state.plugin_identifier, ServType, Interval, RequestArgs, PackageBlueprint,Proposal),
	case Reply of
		noreply->
			{noreply,State};
		_->
			{reply,Reply,State}
	end;

handle_call({new_sub_contract,ActId,SubContract},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:new_sub_contract(State#ep_state.serv_variables, From, ActId, SubContract),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};

handle_call({send_cancel,ActId,SubContract,Reason},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:send_cancel(State#ep_state.serv_variables, From, State#ep_state.reception_map, ActId, SubContract, Reason),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};

handle_call({service_package_delivery,ContractId,Package,ReplyTo},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:service_package_delivery(State#ep_state.serv_variables, From, State#ep_state.reception_map, ContractId, Package, ReplyTo),
	case Reply of
		noreply->
			{noreply,State#ep_state{serv_variables=NewServVariables}};
		_->
			{reply,Reply,State#ep_state{serv_variables=NewServVariables}}
	end;

handle_call({service_stopped,ContractId,Reason,ReplyTo},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:service_stopped(State#ep_state.serv_variables, From, State#ep_state.reception_map, ContractId, Reason, ReplyTo),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};

handle_call({get_act_contracts_and_give_ah,ActId,AHPid},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:get_act_contracts_and_give_ah(State#ep_state.serv_variables, From, ActId, AHPid),
	case State#ep_state.factory_ready of
		true->
			{reply,Reply,State#ep_state{serv_variables=NewServVariables}};
		_->
			RepliesWhenFactoryReady = State#ep_state.replies_when_factory_ready,
			NewRepliesWhenFactoryReady = [{From,Reply}|RepliesWhenFactoryReady],
			{noreply,State#ep_state{serv_variables=NewServVariables,replies_when_factory_ready = NewRepliesWhenFactoryReady}}
	end;

%%calls from spawned processes 1) done 
handle_call({done,ActId,FinalS2Data},From,State)->
	{Reply,NewServVariables} = spa_and_ra_calls:done(State#ep_state.serv_variables,From,ActId,FinalS2Data,false),
	{reply,Reply,State#ep_state{serv_variables=NewServVariables}};


handle_call({updated_receptions,ReceptionMap},From,State)->
	{Reply,NewServVariables,NewReceptionMap} = spa_and_ra_calls:updated_receptions(State#ep_state.serv_variables, From, State#ep_state.reception_map, State#ep_state.coordinator, ReceptionMap),
	{reply,Reply,State#ep_state{serv_variables = NewServVariables,reception_map=NewReceptionMap}};

handle_call(_Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"~p got unrecognized call",[?MODULE]),
  {reply, unknown, State}. 

handle_cast(factory_ready,State)->
	RepliesWhenFactoryReady = State#ep_state.replies_when_factory_ready,
	make_all_pending_replies(RepliesWhenFactoryReady),
	{noreply,State#ep_state{factory_ready=true,replies_when_factory_ready=[]}};

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

make_all_pending_replies([])->
	ok;
make_all_pending_replies([{From,Reply}|T])->
	custom_erlang_functions:gen_serv_reply_if_from(From, Reply),
	make_all_pending_replies(T).
