%% @author Daniel
%% @doc @todo Add description to sheep_standard_ap_mod.


-module(sheep_ap).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,analyse_old_acts/4,analyse_act/3,eating_alarm/1]).

-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).
-record(biography_info,{tend::integer(),s3data::term()}).
-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	["Relocate (RA)","Eating (RD)","Weight (RD)","New housing details"].
analyse_old_acts(_ActType,_Acts,ReceptionMap,Variables)->%assume already analysed
	
	restart_eating_alarm(ReceptionMap),
	Variables.

analyse_act(Act,ReceptionMap,Variables)->
	ok=error,
	AtrRecep = maps:get(atr,ReceptionMap),
	CommsRecep = maps:get(comms,ReceptionMap),
	S1Data = Act#stage3Activity.schedule_info#schedule_info.s1data,
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	S3Data = Act#stage3Activity.biography_info#biography_info.s3data,
	%Tend = Act#stage3Activity.biography_info#biography_info.tend,
	Tend = Act#stage3Activity.execution_info#execution_info.tstart,
	case Act#stage3Activity.type of
		"Relocate (RA)"->
			Result = maps:get(<<"Result">>,S2Data),
			case (Result=="Success") or (Result==<<"Success">>) of
				true->
 					Contracts = maps:get(<<"Contracts">>,S2Data),
 					{ok,LocContract} = contracts:find_contract_with_serv_type(Contracts, "Receive sheep"),
 					{ok,LocBC} = contracts:get_service_bc(LocContract),
 					{ok,LocId} = business_cards:get_id(LocBC),
 					{ok,LocAdr} = business_cards:get_address(LocBC),
 					{ok,LocType} = business_cards:get_type(LocBC),
					%%inform old house/group that it left
					{ok,MyGroup} = atr_api:get_attribute_value(AtrRecep, "Group"),
					{ok,MyBC} = comms_api:get_bc(CommsRecep),
					{ok,ID} = business_cards:get_id(MyBC),
					case MyGroup of
						"none"->
							{ok,HouseId} = atr_api:get_attribute_value(AtrRecep, "House"),	
							case HouseId of
								"none"->
									ok;
								_->
									
									HouseAdr = erlang:list_to_atom(string:lowercase(string:replace(HouseId, " ", "_",all))),
									comms_api:send_inform(CommsRecep, HouseAdr, "Sheep left", ID)
							end;
						_->
							GroupAdr = erlang:list_to_atom(string:lowercase(string:replace(MyGroup, " ", "_",all))),
							comms_api:send_inform(CommsRecep, GroupAdr, "Sheep left", ID)
					end,
					%%Group,House and Feeder attributes update
 					case LocType of
 						"Groups"->
 							{ok,GroupAtr} = attribute_functions:create_attribute("Group", "Relational", "Base ID", LocId),
							{ok,GAtrMap} = comms_api:send_request_info(maps:get(comms,ReceptionMap), LocAdr, "Attributes", ["House","Case studies"]),
 							NewHouseId = maps:get("House",GAtrMap),
							GroupCS = maps:get("Case studies",GAtrMap),
							{ok,CurrentCS} = atr_api:get_attribute_value(AtrRecep, "Case studies"),
							IndCS = sheep_request_and_inform_mod:get_individual_cs(maps:keys(CurrentCS),CurrentCS,#{}),
							NewCS = maps:merge(IndCS,GroupCS),
							atr_api:update_attribute_value(maps:get(atr,ReceptionMap), "Case studies", NewCS);
 						_->
 							{ok,GroupAtr} = attribute_functions:create_attribute("Group", "Relational", "Base ID", "none"),
 							NewHouseId = LocId
 					end,
 					{ok,HouseAtr} = attribute_functions:create_attribute("House", "Relational", "Base ID", NewHouseId),
 					NewHouseAdr = erlang:list_to_atom(string:lowercase(string:replace(NewHouseId, " ", "_",all))),
 					{ok,AtrMap} = comms_api:send_request_info(maps:get(comms,ReceptionMap), NewHouseAdr, "Attributes", ["Feeders"]),
 					Feeders = maps:get("Feeders",AtrMap),
 					{ok,FeedersAtr} = attribute_functions:create_attribute("Feeders", "Relational", "Base ID", Feeders),
					PersonalAttributes = [GroupAtr,HouseAtr,FeedersAtr],
					
					%%Update relocation calculation variables
					{ok,CVs} = vs_api:get_all(maps:get(vs,ReceptionMap)),
					LastWeightDets = maps:get("Last weight (CV)",CVs),
					LastW = maps:get(weight,LastWeightDets),
					LastWTime = maps:get(time,LastWeightDets),
					WEaten = maps:get(eaten,LastWeightDets),
					TotalEatC = maps:get("Total eaten (CV)",CVs),
					TotalWeight = maps:get("Total weight (CV)",CVs),
					case (LastW=="pending") or (Tend>(LastWTime+3600*48)) of %%if weighed more than 2 days ago, not a valid starting weight and eaten
						true->
							NewRelocDets = #{true_time=>Tend,true_eaten=>TotalEatC,weight_time=>"pending",weight_eaten=>"pending",weight=>"pending",cum_weight=>"pending"};
						_->
							NewRelocDets = #{true_time=>Tend,true_eaten => TotalEatC,weight_time=>LastWTime,weight_eaten=>WEaten, weight => LastW,cum_weight=>TotalWeight}
					end,
					{ok,LastRelocA} = vs_api:create("Last relocation (CV)", "Relocate (RA)", NewRelocDets),
					{ok,S12} = attribute_functions:create_attribute("Eating stats since last relocation", "Eating (g/day)", "Eating (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),	
					{ok,S6} = attribute_functions:create_attribute("Weight stats since last relocation", "Weight (kg)", "Weight (RD)", #{max=>LastW,min=>LastW,avg=>LastW}),
					{ok,S7} = attribute_functions:create_attribute("Gain stats since last relocation", "Weight gain (kg/day)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
					{ok,S8} = attribute_functions:create_attribute("FCR stats since last relocation", "FCR (kg_eaten/kg_gained)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
					StateAttributes = [LastRelocA,S12,S6,S7,S8],
					
					{ok,"success"} = atr_api:save(AtrRecep,lists:append(PersonalAttributes, StateAttributes));
				_->
					ok
			end;
		"New housing details"->
			Details = maps:get(<<"Details">>,S1Data),
			case maps:is_key("Feeders", Details) of
				true->
					Feeders = maps:get("Feeders",Details),
					{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "Feeders", Feeders);
				_->
					ok
			end,
			case maps:is_key("House", Details) of
				true->
					House = maps:get("House",Details),
					{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "House", House);
				_->
					ok
			end;
			
		"Eating (RD)"->
			case S3Data of
				"pending"->
					restart_eating_alarm(ReceptionMap),
					analyse_eating_data(S2Data,S3Data,Act#stage3Activity.execution_info#execution_info.tstart,ReceptionMap);
				_->
					ok
			end;
		"Weight (RD)"->
			case S3Data of
				"pending"->
					analyse_weight_data(maps:get(<<"Value">>,S2Data),S3Data,Act#stage3Activity.execution_info#execution_info.tstart,ReceptionMap);
				_->
					ok
			end
	end,
	Variables.

restart_eating_alarm(ReceptionMap)->
	PluginPid = maps:get(plugin,ReceptionMap),
	{ok,CVs} = vs_api:get_all(maps:get(vs,ReceptionMap)),
	EatingAlarmPid = maps:get("Eating alarm (CV)",CVs),
	case is_pid(EatingAlarmPid) andalso is_process_alive(EatingAlarmPid) of
		true->
			exit(EatingAlarmPid,kill);
		_->
			ok
	end,
	{ok,NewEatingAlarmPid} = custom_erlang_functions:myGenServCall(PluginPid, {spawn_process,?MODULE,eating_alarm,[ReceptionMap]}),
	{ok,_S} = vs_api:update_value(maps:get(vs,ReceptionMap), "Eating alarm (CV)", NewEatingAlarmPid).

eating_alarm(ReceptionMap)->
	Atr = maps:get(atr,ReceptionMap),
	{ok,MaxHoursBetweenEating} = atr_api:get_attribute_value(Atr, "Max hours between eating activities"),
	timer:sleep(MaxHoursBetweenEating*60000*60),
	{ok,Humans} = doha_api:get_bcs_by_types("Human workers"),
	MyComms = maps:get(comms,ReceptionMap),
	{ok,MyBC} = comms_api:get_bc(MyComms),
	{ok,ID} = business_cards:get_id(MyBC),
	try_to_inform_each_human(Humans,MyComms,ID,MaxHoursBetweenEating).
	
try_to_inform_each_human([],_MyComms,_ID,_MaxHours)->
	ok;
try_to_inform_each_human([Human|T],MyComms,ID,MaxHours)->
	{ok,Adr} = business_cards:get_address(Human),
	comms_api:send_inform(MyComms, Adr, "SMS", "Sheep with ID:" ++ID++ " has not eaten for "++integer_to_list(MaxHours)++" hours"),
	try_to_inform_each_human(T,MyComms,ID,MaxHours).

analyse_eating_data(S2Data,_S3Data,Time,ReceptionMap)->
	Value = maps:get(<<"Value">>,S2Data),
	AtrRecep = maps:get(atr,ReceptionMap),
	%%Eating: 1) Current daily intake (g/day) 2) Eating stats since last relocation (g/day) 3) Overall eating stats
	{ok,CVs} = vs_api:get_all(maps:get(vs,ReceptionMap)),
	FirstMeal = maps:get("First meal (CV)",CVs),
	FirstMealT = maps:get(time,FirstMeal),
	case (FirstMealT=="pending") orelse (Time<FirstMealT) of
		true->
			NewFirstMeal = #{time=>Time,eaten=>Value},
			{ok,"success"}=vs_api:update_value(maps:get(vs,ReceptionMap), "First meal (CV)", NewFirstMeal);
		_->
			NewFirstMeal = FirstMeal
	end,
	
	Last24h = maps:get("Last 24 hours eating (CV)",CVs),
	%error_log:log(?MODULE,0,unknown,"\nLast24:~p",[Last24h]),
	Times = lists:sort(maps:keys(Last24h)),
	case (Times==[]) orelse (Time>(lists:last(Times))) of
		true->	
			%error_log:log(?MODULE,0,unknown,"\nTimes==[]"),
			Clean24h = clean_24h(Time,maps:keys(Last24h),Last24h),
			New24h = maps:put(Time,Value,Clean24h);
		_->
			case Time>(lists:nth(1, Times)) of
				true->
					New24h = maps:put(Time, Value, Last24h);
				_->
					%%TODO
					New24h = recalc
			end
	end,
	Duration = maps:get(<<"Duration">>,S2Data),
	update_duration_and_frequency_atrs(Duration,Time,CVs,ReceptionMap),
	%error_log:log(?MODULE,0,unknown,"\nNew24:~p",[New24h]),
	case (New24h==recalc) of
		true->
			recalculate_all_weight_and_food_atrs_and_cvs(AtrRecep);
		_->
			LastDayIntake = lists:sum(maps:values(New24h)),
			{ok,S14} = attribute_functions:create_attribute("Current daily intake", "Eating (g/day)", "Eating (RD)", LastDayIntake),
			TotalEatC = maps:get("Total eaten (CV)",CVs),
			NewTotalEatC = TotalEatC+Value,
			{ok,C1} = vs_api:create("Total eaten (CV)", "Eating (RD)", NewTotalEatC),
			{ok,C4} = vs_api:create("Last 24 hours eating (CV)", "Eating (RD)", New24h),
			%%stats
			{ok,Overall} = atr_api:get_attribute_value(AtrRecep, "Overall eating stats"),
			case (FirstMealT=="pending") orelse (Time<FirstMealT) of
				true->
					atr_api:save(AtrRecep,[C1,C4,S14]);
				_->
					%%overall
					case (Time>=(24*3600+FirstMealT)) of
						true->
							AvgOverall = (NewTotalEatC-maps:get(eaten,NewFirstMeal))/((Time-FirstMealT)/(3600*24)),
							{MinOverall,MaxOverall} = new_max_and_min(LastDayIntake,maps:get(max,Overall),maps:get(min,Overall)),
							{ok,S13} = attribute_functions:create_attribute("Overall eating stats", "Eating (g/day)", "Eating (RD)", #{max=>MaxOverall,min=>MinOverall,avg=>AvgOverall}),
							
							%%rel
							LastRelDets = maps:get("Last relocation (CV)",CVs),
							RelT = maps:get(true_time,LastRelDets),
							case RelT of
								0->%%no relocation yet
									{ok,S12} = attribute_functions:create_attribute("Eating stats since last relocation", "Eating (g/day)", "Eating (RD)", #{max=>MaxOverall,min=>MinOverall,avg=>AvgOverall});
								_->
									RelEaten = maps:get(true_eaten,LastRelDets),
									case (Time-RelT)>0 of
										true->
											AvgRel = (NewTotalEatC-RelEaten)/((Time-RelT)/(3600*24));
										_->
											AvgRel = "inf"
									end,
									{ok,RelStats} = atr_api:get_attribute_value(AtrRecep, "Eating stats since last relocation"),
									{MinRel,MaxRel} = new_max_and_min(LastDayIntake,maps:get(max,RelStats),maps:get(min,RelStats)),
									{ok,S12} = attribute_functions:create_attribute("Eating stats since last relocation", "Eating (g/day)", "Eating (RD)", #{max=>MaxRel,min=>MinRel,avg=>AvgRel})
							end,
							%%Case studies
							CS = maps:get("FCR Case studies (CV)",CVs),
							CSAtrs = cs_eating_stats(AtrRecep,maps:keys(CS),CS,Time,NewTotalEatC,LastDayIntake,[]),
							Atrs = lists:append(CSAtrs, [S12,S13,S14,C1,C4]),
							atr_api:save(AtrRecep,Atrs),
							{ok,MyGroup} = atr_api:get_attribute_value(AtrRecep, "Group"),
							case MyGroup of
								"none"->
									ok;
								_->
									GroupAdr = erlang:list_to_atom(string:lowercase(string:replace(MyGroup, " ", "_",all))),
									ok = comms_api:send_inform(maps:get(comms,ReceptionMap), GroupAdr, "Sheep stats", lists:append([S14,S12],CSAtrs))
							end;
						_->
							atr_api:save(AtrRecep,[C1,C4,S14])
					end
			end
	end.

cs_eating_stats(_AtrRecep,[],_CS,_Time,_NewTotalEatC,_LastDayIntake,Atrs)->
	Atrs;
cs_eating_stats(AtrRecep,[Key|T],CS,Time,NewTotalEatC,LastDayIntake,Atrs)->
	CSDets = maps:get(Key,CS),
	CST = maps:get(true_time,CSDets),
	Eaten = maps:get(true_eaten,CSDets),
	case (Time-CST)>=(24*3600) of
		true->
			%error_log:log(?MODULE,0,unknown,"\n1"),
			EndT = maps:get(end_time,CSDets),
			case (Time=<EndT) of
				true->
					%error_log:log(?MODULE,0,unknown,"\n2"),
					Avg = (NewTotalEatC-Eaten)/((Time-CST)/(3600*24)),
					{ok,Stats} = atr_api:get_attribute_value(AtrRecep, Key++" Eating (g/day)"),
					{Min,Max} = new_max_and_min(LastDayIntake,maps:get(max,Stats),maps:get(min,Stats)),
					{ok,NewStats} = attribute_functions:create_attribute(Key++" Eating (g/day)", Key, "Eating (RD)", #{max=>Max,min=>Min,avg=>Avg}),
					cs_eating_stats(AtrRecep,T,CS,Time,NewTotalEatC,LastDayIntake,[NewStats|Atrs]);
				_->
					%error_log:log(?MODULE,0,unknown,"\n3"),
					cs_eating_stats(AtrRecep,T,CS,Time,NewTotalEatC,LastDayIntake,Atrs)
			end;
		_->
			%error_log:log(?MODULE,0,unknown,"\n4"),
			case (Time>=CST) and (Eaten=="pending") of
				true->
					%error_log:log(?MODULE,0,unknown,"\n5"),
					{ok,OldTotalEatC} = vs_api:get_value(AtrRecep, "Total eaten (CV)"),
					NewCSDets = maps:update(true_eaten, OldTotalEatC, CSDets),
					NewCS = maps:update(Key, NewCSDets, CS),%%TODO: add (CV) to FCR Case STudies
				
					{ok,CSAtr} = vs_api:create("FCR Case studies (CV)", "Case studies", NewCS),
					{ok,NewStats} = attribute_functions:create_attribute(Key++" Eating (g/day)", Key, "Eating (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
					cs_eating_stats(AtrRecep,T,CS,Time,NewTotalEatC,LastDayIntake,lists:append(Atrs,[CSAtr,NewStats]));
				_->
					%error_log:log(?MODULE,0,unknown,"\n6"),
					cs_eating_stats(AtrRecep,T,CS,Time,NewTotalEatC,LastDayIntake,Atrs)
			end
	end.

update_duration_and_frequency_atrs(Duration,Time,CVs,ReceptionMap)->
	TotalDuration = maps:get("Total duration (CV)",CVs),
	NewTotalDuration = TotalDuration+Duration,
	vs_api:update_value(maps:get(vs,ReceptionMap), "Total duration (CV)", NewTotalDuration),
	NrEatingActs = maps:get("Number of eating acts (CV)",CVs),
	NewNrEatingActs = NrEatingActs+1,
	vs_api:update_value(maps:get(vs,ReceptionMap), "Number of eating acts (CV)", NewNrEatingActs),
	
	{ok,CurDurAtr} = attribute_functions:create_attribute("Current Duration", "Eating Duration (s)", "Eating (RD)", Duration),
	Last24h = maps:get("Last 24 hours eating (CV)",CVs),
	Times = lists:sort(maps:keys(Last24h)),
	case Times==[] of
		true->
			Frequency = "pending";
		_->
			LastTime = lists:last(Times),
			X = Time-LastTime,
			Frequency = (1/X)*3600*24
	end,
	{ok,CurFreqAtr} = attribute_functions:create_attribute("Current Frequency", "Eating Frequency (meals per day)", "Eating (RD)", Frequency),
	
	%%overall stats
	{ok,DurStats} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Overall Eating Duration stats"),
	DurAvg = NewTotalDuration/NewNrEatingActs,
	{DurMin,DurMax} = new_max_and_min(Duration,maps:get(max,DurStats),maps:get(min,DurStats)),
	{ok,FrStats} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Overall Eating Frequency stats"),
	FirstMeal = maps:get("First meal (CV)",CVs),
	FirstTime = maps:get(time,FirstMeal),
	case (Time>FirstTime) of
		true->
			FrAvg = ((NewNrEatingActs-1)/(Time-FirstTime))*3600*24;
		_->
			FrAvg = Frequency
	end,
	{FrMin,FrMax} = new_max_and_min(Frequency,maps:get(max,FrStats),maps:get(min,FrStats)),
	{ok,OverallDurAtr} = attribute_functions:create_attribute("Overall Eating Duration stats", "Eating Duration (s)", "Eating (RD)", #{max=>DurMax,min=>DurMin,avg=>DurAvg}),
	{ok,OverallFreqAtr} = attribute_functions:create_attribute("Overall Eating Frequency stats", "Eating Frequency (meals per day)", "Eating (RD)", #{max=>FrMax,min=>FrMin,avg=>FrAvg}),
	Atrs = [CurDurAtr,CurFreqAtr,OverallDurAtr,OverallFreqAtr],
	UpdateRes=atr_api:save(maps:get(atr,ReceptionMap),Atrs).
	
	
clean_24h(_Time,[],Last24h)->
	Last24h;
clean_24h(Time,[Key|T],Last24h)->
	case ((Key+24*3600)=<Time) of
		true->
			clean_24h(Time,T,maps:remove(Key, Last24h));
		_->
			clean_24h(Time,T,Last24h)
	end.

sum_eating_acts([],TotalEaten)->
	TotalEaten;
sum_eating_acts([Act|T],TotalEaten)->
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	Val = maps:get(<<"Value">>,S2Data),
	sum_eating_acts(T,TotalEaten+Val).

analyse_weight_data(Value,_S3Data,Time,ReceptionMap)->
	AtrRecep = maps:get(atr,ReceptionMap),
	{ok,CVs} = vs_api:get_all(maps:get(vs,ReceptionMap)),
	%%Weight gain (kg/day): 1) Current daily gain	2) 	Gain stats since last relocation	3) Overall gain stats							   	
	%%Weight (kg): 1) Current weight 2) Weight stats since last relocation 3) Overall weight stats 
	%%FCR (g_gained/kg_eaten): 1) Current FCR   2) FCR stats since last relocation 3) Overall FCR stats
	CurrentEatC = maps:get("Total eaten (CV)",CVs),
	{ok,EatingActs} = bio_api:get_acts_by_types_and_times(maps:get(bio,ReceptionMap), ["Eating (RD)"], Time, all, all, all, all, all),
	TotalEatenSinceW = sum_eating_acts(EatingActs,0),
	TotalEatC = CurrentEatC-TotalEatenSinceW,
	FirstWeightDets = maps:get("First weight (CV)",CVs),
	FirstWeightTime = maps:get(time,FirstWeightDets),
	case (FirstWeightTime=="pending") orelse (FirstWeightTime>Time) of
		true->
 			NewFirstWeightDets = #{time=>Time,eaten=>TotalEatC,weight=>Value},
			{ok,"success"}=vs_api:update_value(maps:get(vs,ReceptionMap), "First weight (CV)", NewFirstWeightDets);
		_->
			ok
	end,	
	LastWeightDets = maps:get("Last weight (CV)",CVs),
	LastW = maps:get(weight,LastWeightDets),
	LastWTime = maps:get(time,LastWeightDets),
	TimeDif = Time-LastWTime,
	
	case LastW of 
		"pending"->
			NewTotalWeight = 0,
			NewLastWeightDets = #{time=>Time,weight=>Value,eaten=>TotalEatC},
			{ok,WeightA} = attribute_functions:create_attribute("Current weight", "Weight (kg)", "Weight (RD)", Value),
			NewOverallW = #{avg=>Value,min=>Value,max=>Value},
			{ok,S9} = attribute_functions:create_attribute("Overall weight stats", "Weight (kg)", "Weight (RD)", NewOverallW),
			{ok,S6} = attribute_functions:create_attribute("Weight stats since last relocation", "Weight (kg)", "Weight (RD)", NewOverallW),
			atr_api:save(AtrRecep, [WeightA,S9,S6]);	
		_->
			case Time>LastWTime of
				true->
					TotalWeight = maps:get("Total weight (CV)",CVs),
					AvgW = (Value+LastW)/2,
					NewTotalWeight = AvgW*TimeDif + TotalWeight,
					NewLastWeightDets = #{time=>Time,weight=>Value,eaten=>TotalEatC},	
					GainSinceLast = Value - LastW,
					
					DaysSinceLastW = TimeDif/(3600*24),
					DailyGain = GainSinceLast/DaysSinceLastW,
					{ok,DailyGainA} = attribute_functions:create_attribute("Current daily gain", "Weight gain (kg/day)", "Weight (RD)", DailyGain),
					EatCountAtLastW = maps:get(eaten,LastWeightDets),
					EatDif = TotalEatC - EatCountAtLastW,
					case not(GainSinceLast==0) of
						true->
							FCR = (EatDif/1000)/GainSinceLast;
						_->
							FCR = (EatDif/1000)/0.001
					end,
					{ok,FCR_A} = attribute_functions:create_attribute("Current FCR", "FCR (kg_eaten/kg_gained)", "Weight (RD)", FCR),
					{ok,WeightA} = attribute_functions:create_attribute("Current weight", "Weight (kg)", "Weight (RD)", Value),
					StatsAtrs = calculate_weight_gain_and_fcr_stats(Time,Value,DailyGain,FCR,maps:update("Total weight (CV)", NewTotalWeight, CVs),ReceptionMap,TotalEatC),
					Atrs = lists:append([WeightA,DailyGainA,FCR_A], StatsAtrs),
					atr_api:save(AtrRecep, Atrs),
					{ok,MyGroup} = atr_api:get_attribute_value(AtrRecep, "Group"),
					case MyGroup of
						"none"->
							ok;
						_->
							GroupAdr = erlang:list_to_atom(string:lowercase(string:replace(MyGroup, " ", "_",all))),
							ok = comms_api:send_inform(maps:get(comms,ReceptionMap), GroupAdr, "Sheep stats", lists:append([DailyGainA,FCR_A,WeightA],StatsAtrs))
					end;
				_->
					recalculate_all_weight_and_food_atrs_and_cvs(AtrRecep),
					NewTotalWeight = recalc,
					NewLastWeightDets = LastWeightDets
			end
	end,
	case NewTotalWeight of
		recalc->
			ok;
		_->
			{ok,C3} = vs_api:create("Last weight (CV)", "Weight (RD)", NewLastWeightDets),
			{ok,C6} = vs_api:create("Total weight (CV)", "Weight (RD)", NewTotalWeight),
			atr_api:save(AtrRecep,[C3,C6])
	end.
					
calculate_weight_gain_and_fcr_stats(Time,Value,DailyGain,FCR,CVs,ReceptionMap,TotalEatC)->%%this function wont be called on first weight
	%%overall 
	AtrRecep = maps:get(atr,ReceptionMap),
	FirstWeightDets = maps:get("First weight (CV)",CVs),
	FirstT = maps:get(time,FirstWeightDets),
	FirstW = maps:get(weight,FirstWeightDets),
	FirstE = maps:get(eaten,FirstWeightDets),
	%%change: there is one attribute per weight, gain and fcr
	{ok,OverallW} = atr_api:get_attribute_value(AtrRecep, "Overall weight stats"),
	{ok,OverallG} = atr_api:get_attribute_value(AtrRecep, "Overall gain stats"),
	{ok,OverallF} = atr_api:get_attribute_value(AtrRecep, "Overall FCR stats"),

	NewTotalWeight = maps:get("Total weight (CV)",CVs),
	
	NewOverallWAvg = NewTotalWeight/(Time-FirstT),
	NewOverallGainAvg = (Value-FirstW)/((Time-FirstT)/(3600*24)),
	OGain = Value-FirstW,
	case not(OGain==0) of
		true->
			NewOverallFCRAvg = ((TotalEatC-FirstE)/1000)/OGain;
		_->
			NewOverallFCRAvg = ((TotalEatC-FirstE)/1000)/0.001
	end,
	
	{NewOverallMinW,NewOverallMaxW} = new_max_and_min(Value,maps:get(max,OverallW),maps:get(min,OverallW)),
	{NewOverallMinGain,NewOverallMaxGain} = new_max_and_min(DailyGain,maps:get(max,OverallG),maps:get(min,OverallG)),
	{NewOverallMinFCR,NewOverallMaxFCR} = new_max_and_min(FCR,maps:get(max,OverallF),maps:get(min,OverallF)),
	
	NewOverallW = #{avg=>NewOverallWAvg,min=>NewOverallMinW,max=>NewOverallMaxW},
	NewOverallG = #{avg=>NewOverallGainAvg,min=>NewOverallMinGain,max=>NewOverallMaxGain},
	NewOverallF = #{avg=>NewOverallFCRAvg,min=>NewOverallMinFCR,max=>NewOverallMaxFCR},
	{ok,S9} = attribute_functions:create_attribute("Overall weight stats", "Weight (kg)", "Weight (RD)", NewOverallW),
	{ok,S10} = attribute_functions:create_attribute("Overall gain stats", "Weight gain (kg/day)", "Weight (RD)", NewOverallG),
	{ok,S11} = attribute_functions:create_attribute("Overall FCR stats", "FCR (kg_eaten/kg_gained)", "Weight (RD)", NewOverallF),
	%%since relocation
	LastRelDets = maps:get("Last relocation (CV)",CVs),
	LastRelW = maps:get(weight,LastRelDets),
	LastRelWT = maps:get(weight_time,LastRelDets),
	LastRelCumW = maps:get(cum_weight,LastRelDets),
	LastRelWEaten = maps:get(weight_eaten,LastRelDets),
	LastRelTrueT = maps:get(true_time,LastRelDets),
	case LastRelTrueT of
		0->
			{ok,S6} = attribute_functions:create_attribute("Weight stats since last relocation", "Weight (kg)", "Weight (RD)", NewOverallW),
			{ok,S7} = attribute_functions:create_attribute("Gain stats since last relocation", "Weight gain (kg/day)", "Weight (RD)", NewOverallG),
			{ok,S8} = attribute_functions:create_attribute("FCR stats since last relocation", "FCR (kg_eaten/kg_gained)", "Weight (RD)", NewOverallF);
		_->
			case LastRelW of
				"pending"->
					NewLastRelDets = maps:merge(LastRelDets, #{weight_time=>Time,weight=>Value,weight_eaten=>TotalEatC,cum_weight=>NewTotalWeight}),
					{ok,"success"}=vs_api:update_value(maps:get(vs,ReceptionMap), "Last relocation (CV)", NewLastRelDets),
					{ok,S6} = attribute_functions:create_attribute("Weight stats since last relocation", "Weight (kg)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
					{ok,S7} = attribute_functions:create_attribute("Gain stats since last relocation", "Weight gain (kg/day)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
					{ok,S8} = attribute_functions:create_attribute("FCR stats since last relocation", "FCR (kg_eaten/kg_gained)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"});
				_->
					{ok,RelW} = atr_api:get_attribute_value(AtrRecep, "Weight stats since last relocation"),
					{ok,RelG} = atr_api:get_attribute_value(AtrRecep, "Gain stats since last relocation"),
					{ok,RelF} = atr_api:get_attribute_value(AtrRecep, "FCR stats since last relocation"),
					
					NewRelWAvg = (NewTotalWeight-LastRelCumW)/(Time-LastRelWT),
					NewRelGainAvg = (Value-LastRelW)/((Time-LastRelWT)/(3600*24)),
					
					RGain = Value-LastRelW,
					case not(RGain==0) of
						true->
							NewRelFCRAvg = ((TotalEatC-LastRelWEaten)/1000)/RGain;
						_->
							NewRelFCRAvg = ((TotalEatC-LastRelWEaten)/1000)/0.001
					end,
					
					{NewRelMinW,NewRelMaxW} = new_max_and_min(Value,maps:get(max,RelW),maps:get(min,RelW)),
					{NewRelMinGain,NewRelMaxGain} = new_max_and_min(DailyGain,maps:get(max,RelG),maps:get(min,RelG)),
					{NewRelMinFCR,NewRelMaxFCR} = new_max_and_min(FCR,maps:get(max,RelF),maps:get(min,RelF)),
					
					NewRelW = #{avg=>NewRelWAvg,min=>NewRelMinW,max=>NewRelMaxW},
					NewRelG = #{avg=>NewRelGainAvg,min=>NewRelMinGain,max=>NewRelMaxGain},
					NewRelF = #{avg=>NewRelFCRAvg,min=>NewRelMinFCR,max=>NewRelMaxFCR},
					{ok,S6} = attribute_functions:create_attribute("Weight stats since last relocation", "Weight (kg)", "Weight (RD)", NewRelW),
					{ok,S7} = attribute_functions:create_attribute("Gain stats since last relocation", "Weight gain (kg/day)", "Weight (RD)", NewRelG),
					{ok,S8} = attribute_functions:create_attribute("FCR stats since last relocation", "FCR (kg_eaten/kg_gained)", "Weight (RD)", NewRelF)
			end
	end,
	Atrs = [S9,S10,S11,S6,S7,S8],
	CS = maps:get("FCR Case studies (CV)",CVs),
	CSAtrs = cs_weight_stats(DailyGain,FCR,ReceptionMap,maps:keys(CS),CS,Time,NewTotalWeight,Value,TotalEatC,[]),
	%error_log:log(?MODULE,0,unknown,"\nCSAtrs:~p",[CSAtrs]),
	lists:append(Atrs, CSAtrs).



get_first_weight(ReceptionMap,[],Time,WVal)->
	
	BioRecep = maps:get(bio,ReceptionMap),
	{ok,CurrentEatC} = vs_api:get_value(maps:get(vs,ReceptionMap), "Total eaten (CV)"),
	{ok,EatingActs} = bio_api:get_acts_by_types_and_times(BioRecep, ["Eating (RD)"], Time, all, all, all, all, all),
	TotalEatenSinceW = sum_eating_acts(EatingActs,0),
	LastWEaten = CurrentEatC-TotalEatenSinceW,
	{WVal,Time,LastWEaten};
get_first_weight(ReceptionMap,[WAct|T],Time,WVal)->
	StartTime = WAct#stage3Activity.execution_info#execution_info.tstart,
	case (Time==time) orelse (Time>StartTime) of
		true->
			S2Data = WAct#stage3Activity.execution_info#execution_info.s2data,
			NewVal = maps:get(<<"Value">>,S2Data),
			get_first_weight(ReceptionMap,T,StartTime,NewVal);
		_->
			get_first_weight(ReceptionMap,T,Time,WVal)
	end.
	
cs_weight_stats(_DailyGain,_FCR,_ReceptionMap,[],_CS,_Time,_NewTotalWeight,_Weight,_TotalEatC,Atrs)->
	Atrs;
cs_weight_stats(DailyGain,FCR,ReceptionMap,[Key|T],CS,Time,NewTotalWeight,Weight,TotalEatC,Atrs)->
	AtrRecep = maps:get(atr,ReceptionMap),
	CSDets = maps:get(Key,CS),
	CST = maps:get(true_time,CSDets),
	case (Time-CST)>=0 of
		true->
			EndT = maps:get(end_time,CSDets),
			case (Time=<EndT) of
				true->
					CSWOld = maps:get(weight,CSDets),
					case CSWOld of
						"pending"->
							{ok,WActs} = bio_api:get_acts_by_types_and_times(maps:get(bio,ReceptionMap), ["Weight (RD)"], CST, all, all, all, all, all),
							case WActs of
								[]->
									{ok,LastWeightDets} = vs_api:get_value(maps:get(vs,ReceptionMap), "Last weight (CV)"),
									LastW = maps:get(weight,LastWeightDets),
									LastWTime = maps:get(time,LastWeightDets),
									LastWEaten = maps:get(eaten,LastWeightDets);
								_->
									{LastW,LastWTime,LastWEaten} = get_first_weight(ReceptionMap,WActs,time,value)
							end,
									
							{ok,OldTotalWeight} = vs_api:get_value(maps:get(vs,ReceptionMap),"Total weight (CV)"),
							ChangedDets = #{weight=>LastW,weight_eaten=>LastWEaten,weight_time=>LastWTime,cum_weight=>OldTotalWeight},
							NewCSDets = maps:merge(CSDets, ChangedDets),
							NewCS = maps:update(Key, NewCSDets, CS),
							{ok,CSAtr} = vs_api:create("FCR Case studies (CV)" , "Case studies", NewCS),
							W = #{max=>LastW,min=>LastW,avg=>LastW},
							G= #{max=>"pending",min=>"pending",avg=>"pending"},
							F = #{max=>"pending",min=>"pending",avg=>"pending"};
						_->
							{ok,W} = atr_api:get_attribute_value(AtrRecep, Key++" Weight (kg)"),
							{ok,G} = atr_api:get_attribute_value(AtrRecep, Key++" Weight gain (kg/day)"),
							{ok,F} = atr_api:get_attribute_value(AtrRecep, Key++" FCR (kg_eaten/kg_gained)"),
							NewCSDets = CSDets,
							CSAtr = none
					end,
					CSW = maps:get(weight,NewCSDets),
					CSWT = maps:get(weight_time,NewCSDets),
					CSWE = maps:get(weight_eaten,NewCSDets),
					CSWCum = maps:get(cum_weight,NewCSDets),
					Gain = Weight - CSW,
					WAvg = (NewTotalWeight-CSWCum)/(Time-CSWT),
					GainAvg = Gain/((Time-CSWT)/(3600*24)),
					case not(Gain==0) of
						true->
							FCRAvg = ((TotalEatC-CSWE)/1000)/Gain;
						_->
							FCRAvg = ((TotalEatC-CSWE)/1000)/0.001
					end,
					
					{MinW,MaxW} = new_max_and_min(Weight,maps:get(max,W),maps:get(min,W)),
					{MinGain,MaxGain} = new_max_and_min(DailyGain,maps:get(max,G),maps:get(min,G)),
					{MinFCR,MaxFCR} = new_max_and_min(FCR,maps:get(max,F),maps:get(min,F)),
					{ok,NewW} = attribute_functions:create_attribute(Key++" Weight (kg)", Key, "Weight (RD)", #{max=>MaxW,min=>MinW,avg=>WAvg}),
					{ok,NewG} = attribute_functions:create_attribute(Key++" Weight gain (kg/day)", Key, "Weight (RD)", #{max=>MaxGain,min=>MinGain,avg=>GainAvg}),
					{ok,NewF} = attribute_functions:create_attribute(Key++" FCR (kg_eaten/kg_gained)", Key, "Weight (RD)", #{max=>MaxFCR,min=>MinFCR,avg=>FCRAvg}),
					case CSWOld of
						"pending"->
							cs_weight_stats(DailyGain,FCR,AtrRecep,T,CS,Time,NewTotalWeight,Weight,TotalEatC,lists:append(Atrs, [NewW,NewG,NewF,CSAtr]));
						_->
							cs_weight_stats(DailyGain,FCR,AtrRecep,T,CS,Time,NewTotalWeight,Weight,TotalEatC,lists:append(Atrs, [NewW,NewG,NewF]))
					end;
				_->
					cs_weight_stats(DailyGain,FCR,AtrRecep,T,CS,Time,NewTotalWeight,Weight,TotalEatC,Atrs)
			end;
		_->
			cs_weight_stats(DailyGain,FCR,AtrRecep,T,CS,Time,NewTotalWeight,Weight,TotalEatC,Atrs)
	end.
	
	
new_max_and_min(Val,Max,Min)->
	case Max=="pending" orelse Val>Max of
		true->
			NewMax = Val;
		_->
			NewMax = Max
	end,
	case Min=="pending" orelse Val<Min of
		true->
			NewMin = Val;
		_->
			NewMin = Min
	end,
	{NewMin,NewMax}.

recalculate_all_weight_and_food_atrs_and_cvs(_AtrRecep)->
	error_log:log(?MODULE,0,unknown,"\nERROR: Recalculate all weight and food atrs not coded yet for when old data received").

%%SLOC 505
