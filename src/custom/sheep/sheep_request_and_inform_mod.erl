%% @author Daniel
%% @doc @todo Add description to sheep_request_and_inform_mod.


-module(sheep_request_and_inform_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([inform/4,request_info/4,get_individual_cs/3]).

-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).



%% ====================================================================
%% Internal functions
%% ====================================================================
request_info(ReceptionMap,Type,Details,_Requester)->
	case Type of
		"Case study data"->
			%error_log:log(?MODULE,0,unknown,"\nSheep requested for case study data"),
			CaseStudyName = maps:get(<<"cs_name">>,Details),
			{ok,CaseStudies} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Case studies"),
			case maps:is_key(CaseStudyName, CaseStudies) of
				true->
					CSDets = maps:get(CaseStudyName,CaseStudies),
					CaseStudyType = maps:get(<<"Case study type">>,CSDets),
					case CaseStudyType of
						<<"FCR">>->
							{ok,CS_CVS} = vs_api:get_value(maps:get(vs,ReceptionMap), "FCR Case studies (CV)"),
							ThisCS = maps:get(binary_to_list(CaseStudyName),CS_CVS),
							StartT = maps:get(true_time,ThisCS),EndT = maps:get(end_time,ThisCS),
							{ok,Acts} = bio_api:get_acts_by_types_and_times(maps:get(bio,ReceptionMap), ["Eating (RD)","Weight (RD)"], StartT,EndT, all,all, all,all),
							DataMap = create_fcr_data_map(Acts,#{}),
							StartW = maps:get(weight,ThisCS),
							WTime = maps:get(weight_time,ThisCS),
							NewDataMap = #{<<"Starting weight">>=>StartW,<<"Starting weight time">>=>WTime,<<"CS start time">>=>StartT,<<"CS end time">>=>EndT,<<"Data">>=>DataMap},
							{ok,NewDataMap};
						_->
							{error,"Cannot provide data for case study of this type"}
					end;
				_->
					{error,"No case study with this name"}
			end;
		_->
			{error,"Cannot handle request of this type"}
	end.

create_fcr_data_map([],DataMap)->
	DataMap;
create_fcr_data_map([Act|T],DataMap)->
	SchedTime = Act#stage3Activity.schedule_info#schedule_info.tsched,
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	Value = maps:get(<<"Value">>,S2Data),
	case Act#stage3Activity.type=="Eating (RD)" of
		true->
			Duration = maps:get(<<"Duration">>,S2Data),
			Map = #{<<"eating (g)">>=>Value,<<"duration">>=>Duration},
			case maps:is_key(integer_to_binary(SchedTime), DataMap) of%%if weight happened exactly at same time
				true->
					Cur = maps:get(integer_to_binary(SchedTime),DataMap),
					NewCur = maps:put(<<"eating (g)">>,Map,Cur),
					NewDataMap = maps:update(integer_to_binary(SchedTime),NewCur,DataMap);
				_->
					NewDataMap = maps:put(integer_to_binary(SchedTime),#{<<"eating (g)">>=>Map},DataMap)
			end;
		_->
			case maps:is_key(integer_to_binary(SchedTime), DataMap) of%%if eating happened exactly at same time
				true->
					Cur = maps:get(integer_to_binary(SchedTime),DataMap),
					NewCur = maps:put(<<"weight (kg)">>,Value,Cur),
					NewDataMap = maps:update(integer_to_binary(SchedTime),NewCur,DataMap);
				_->
					NewDataMap = maps:put(integer_to_binary(SchedTime),#{<<"weight (kg)">>=>Value},DataMap)
			end
			
	end,
	create_fcr_data_map(T,NewDataMap).

inform(ReceptionMap,InformType,InformDetails,RequesterCommsRecep)->
	AtrRecep = maps:get(atr,ReceptionMap),
	ExeRecep = maps:get(exe,ReceptionMap),
	case InformType of
		"New housing details"->
			{ok,House} = atr_api:get_attribute_value(AtrRecep, "House"),
			{ok,Group} = atr_api:get_attribute_value(AtrRecep, "Group"),
			{ok,BC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(BC),
			case (ReqId==House) or (ReqId==Group) of
				true->
					exe_api:start_and_finish_new_act(ExeRecep, "New housing details", #{<<"Details">>=>InformDetails}, #{<<"Result">>=>"Success"}),
					ok;
				_->
					{error,"Only my house can update my feeders"}
			end;
		"Updated case studies"->
			{ok,Group} = atr_api:get_attribute_value(AtrRecep, "Group"),
			{ok,BC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(BC),
			case (ReqId==Group) of
				true->
					{ok,CurrentCS} = atr_api:get_attribute_value(AtrRecep, "Case studies"),
					IndCS = get_individual_cs(maps:keys(CurrentCS),CurrentCS,#{}),
					NewCS = maps:merge(IndCS,InformDetails),
					
					atr_api:update_attribute_value(maps:get(atr,ReceptionMap), "Case studies", NewCS),
					ok;
				_->
					{error,"Only my group can update my case studies"}
			end
	end.


get_individual_cs([],_CurCs,NewCS)->
	NewCS;
get_individual_cs([Key|T],CurCs,NewCS)->
	Dets = maps:get(Key,CurCs),
	IndOrG = maps:get(<<"Group or individual">>,Dets),
	case IndOrG of
		<<"Individual">>->
			NewNewCS = maps:put(Key,Dets,NewCS);
		_->
			NewNewCS = NewCS
	end,
	get_individual_cs(T,CurCs,NewNewCS).

%%SLOC: 102