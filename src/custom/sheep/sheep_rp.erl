%% @author Daniel
%% @doc @todo Add description to sheep_standard_rp_mod.


-module(sheep_rp).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,reflect_on_pending_acts/4,reflect_on_act/3]).

-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	["Eating (RD)", "Weight (RD)"].

reflect_on_pending_acts(_ActType,[],_ReceptionMap,Variables)->
	Variables;
reflect_on_pending_acts(_ActType,[Act|T],ReceptionMap,Variables)->
	NewVariables = reflect_on_act(Act,ReceptionMap,Variables),
	reflect_on_pending_acts(_ActType,T,ReceptionMap,NewVariables).

reflect_on_act(Act,ReceptionMap,Variables)->
	AtrRecep = maps:get(atr,ReceptionMap),
	BioRecep = maps:get(bio,ReceptionMap),
	CommsRecep = maps:get(comms,ReceptionMap),
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	case Act#stage3Activity.type of
		"Eating (RD)"->
			%%Eating duration
			case maps:is_key(<<"Duration">>, S2Data) of
				true->
					Duration = maps:get(<<"Duration">>,S2Data),
					DurS3Data = #{<<"Duration">>=>#{<<"Value">>=>Duration,<<"Unit">>=>"s"}};
				_->
					DurS3Data = #{<<"Duration">>=>#{<<"Value">>=>"unknown",<<"Unit">>=>"s"}}
			end,
			%%Product id
			{ok,Feeders} = atr_api:get_attribute_value(AtrRecep, "Feeders"),
			case Feeders of
				"none"->
					update_bio(DurS3Data,BioRecep,Act,<<"unknown">>,<<"unknown">>,<<"unknown">>,<<"unknown">>,<<"unknown">>),
					NewVariables = Variables;
				[Feeder]->%normally this would be the case
					FeederAdr = erlang:list_to_atom(string:lowercase(string:replace(Feeder, " ", "_",all))),
					{ok,FeederReply}=comms_api:send_request_info(CommsRecep, FeederAdr, "Attributes", ["Current food product id"]),
					case maps:is_key("Current food product id", FeederReply) of
						true->
							ProdId = maps:get("Current food product id",FeederReply),
							NewVariables = get_product_details_from_wh_and_update_bio(DurS3Data,ProdId,Act,Feeder,CommsRecep,Variables,BioRecep);
						_->
							update_bio(DurS3Data,BioRecep,Act,<<"unknown">>,Feeder,<<"unknown">>,<<"unknown">>,<<"unknown">>),
							%error_log:log(?MODULE,0,unknown,"\nERROR: Feeder did not include the product id in its reply when requested for current food content"),
							NewVariables = Variables
					end;
				_->%multiple feeders
					
					case maps:is_key(<<"Feeder ID">>, S2Data) of
						true->
							FeederID = maps:get(<<"Feeder ID">>,S2Data),
							FeederAdr = erlang:list_to_atom(string:lowercase(string:replace(FeederID, " ", "_",all))),
							{ok,FeederReply}=comms_api:send_request_info(CommsRecep, FeederAdr, "Attributes", ["Current food product id"]),
							case maps:is_key("Current food product id", FeederReply) of
								true->
									ProdId = maps:get("Current food product id",FeederReply),
									NewVariables = get_product_details_from_wh_and_update_bio(DurS3Data,ProdId,Act,FeederID,CommsRecep,Variables,BioRecep);
								_->
									update_bio(DurS3Data,BioRecep,Act,<<"unknown">>,FeederID,<<"unknown">>,<<"unknown">>,<<"unknown">>),
									%error_log:log(?MODULE,0,unknown,"\nERROR: Feeder did not include the product id in its reply when requested for current food content"),
									NewVariables = Variables
							end;
						_->
							case maps:is_key(<<"Product ID">>, S2Data) of
								true->
									ProdId = maps:get(<<"Product id">>,S2Data),
									NewVariables = get_product_details_from_wh_and_update_bio(DurS3Data,ProdId,Act,<<"unknown">>,CommsRecep,Variables,BioRecep);
								_->
									update_bio(DurS3Data,BioRecep,Act,<<"unknown">>,<<"unknown">>,<<"unknown">>,<<"unknown">>,<<"unknown">>),
									NewVariables = Variables
							end
					end
			end;
		"Weight (RD)"->
			NewVariables = Variables,
			{ok,LastWeightDets} = atr_api:get_attribute_value(AtrRecep, "Last weight (CV)"),
			LastW = maps:get(weight,LastWeightDets),
			case LastW of 
				"pending"->
					S3Data = #{<<"FCR">>=>"Starting weight: cannot calculate FCR"};
				_->
					{ok,FirstWeightDets} = atr_api:get_attribute_value(AtrRecep, "First weight (CV)"),
					FirstW = maps:get(weight,FirstWeightDets),
					FirstE = maps:get(eaten,FirstWeightDets),
					NewW = maps:get(<<"Value">>,S2Data),
					Time = Act#stage3Activity.execution_info#execution_info.tstart,
					{ok,EatingActs} = bio_api:get_acts_by_types_and_times(maps:get(bio,ReceptionMap), ["Eating (RD)"], Time, all, all, all, all, all),
					{ok,CurrentEaten} = atr_api:get_attribute_value(AtrRecep, "Total eaten (CV)"),
					TotalEatenSinceW = sum_eating_acts(EatingActs,0),
					TotalEaten = CurrentEaten-TotalEatenSinceW,
					Eaten = TotalEaten-FirstE,
					Gain = NewW-FirstW,
					case not(Gain==0) of
						true->
							FCR = (Eaten/1000)/Gain;
						_->
							FCR = (Eaten/1000)/0.001
					end,
					S3Data = #{<<"Overall FCR">>=>#{<<"Value">>=>FCR,<<"Unit">>=>"kg_eaten/kg_gained"}}
			end,
			
			bio_api:update_s3_data(BioRecep, Act#stage3Activity.id, S3Data)
	end,
	NewVariables.

sum_eating_acts([],TotalEaten)->
	TotalEaten;
sum_eating_acts([Act|T],TotalEaten)->
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	Val = maps:get(<<"Value">>,S2Data),
	sum_eating_acts(T,TotalEaten+Val).

get_product_details_from_wh_and_update_bio(DurS3Data,ProdId,Act,FeederId,CommsRecep,Variables,BioRecep)->
	case maps:is_key(food_wh_adr, Variables) of
		true->
			FoodWHAdr = maps:get(food_wh_adr,Variables);
		_->
			{ok,BCs}=comms_api:find_services(CommsRecep, "Store products"),
			FoodWHAdr = find_food_wh(BCs)
	end,
	case FoodWHAdr of
		"none"->
			update_bio(DurS3Data,BioRecep,Act,ProdId,FeederId,<<"unknown">>,<<"unknown">>,<<"unknown">>),
			NewVariables = maps:remove(food_wh_adr, Variables);
		_->
			Res = comms_api:send_request_info(CommsRecep, FoodWHAdr, "Product details", #{<<"Product id">>=>ProdId}),
			case Res of
				{ok,ProdDetails}->
					PromisedContent = maps:get(<<"promised_content">>,ProdDetails),
					Suppliers = maps:get(<<"suppliers">>,ProdDetails),
					ExpirationDate = maps:get(<<"expiration_date">>,ProdDetails),
					ExpDateInt = base_time:date_and_time_strings_to_base_time(binary_to_list(ExpirationDate), "00:00:00"),
					Dif = ExpDateInt - base_time:now(),
					DaysTillExp = Dif/(3600*24),
					update_bio(DurS3Data,BioRecep,Act,ProdId,FeederId,PromisedContent,Suppliers,DaysTillExp),
					NewVariables = maps:put(food_wh_adr, FoodWHAdr, Variables);
				_->
					update_bio(DurS3Data,BioRecep,Act,ProdId,FeederId,<<"unknown">>,<<"unknown">>,<<"unknown">>),
					NewVariables = maps:remove(food_wh_adr, Variables)
			end
	end,
	NewVariables.

update_bio(DurS3Data,BioRecep,Act,ProdId,FeederId,PromisedContent,Suppliers,DaysTillExp)->
	bio_api:update_s3_data(BioRecep, Act#stage3Activity.id, maps:merge(DurS3Data,#{<<"Product id">>=>ProdId,<<"Feeder id">>=>FeederId,<<"Promised content">>=>PromisedContent,<<"Suppliers">>=>Suppliers,<<"Days till expiration date">>=>DaysTillExp})).

find_food_wh([])->
	"none";
find_food_wh([BC|T])->
	{ok,Services} = business_cards:get_services(BC),
	{ok,SD} = business_cards:get_service_description(Services, "Store products"),
	ProductType = maps:get(<<"Product type">>,SD),
	case ProductType of
		<<"Sheep food">>->
			{ok,Adr} = business_cards:get_address(BC),
			Adr;
		_->
			find_food_wh(T)
	end.
%%SLOC: 144