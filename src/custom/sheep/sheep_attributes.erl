%% @author Daniel
%% @doc @todo Add description to sheep_attributes.


-module(sheep_attributes).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get()->
	{ok,P1} = attribute_functions:create_attribute("Date of birth", "Personal", "Date", "pending"),
	{ok,P2} = attribute_functions:create_attribute("EID", "Personal", "Electronic ID of the tag attached to this resource", "none"),
	{ok,P3} = attribute_functions:create_attribute("Gender", "Personal", "Male/Female/pending", "pending"),
	{ok,P4} = attribute_functions:create_attribute("Dam", "Personal", "unknown database", "pending"),
	{ok,P5} = attribute_functions:create_attribute("Sire", "Personal", "unknown database", "pending"),
	{ok,P6} = attribute_functions:create_attribute("Offspring", "Personal", "Map with keys as database names and values as ids in respective databases", #{}),
	{ok,P7} = attribute_functions:create_attribute("Breed", "Personal", "Dorper,Merino etc", "pending"),
	{ok,Photos} = attribute_functions:create_attribute("Photos", "Personal", "GD links or actual photos?", []),
	
	{ok,R1} = attribute_functions:create_attribute("House", "Relational", "Base ID", "none"),
	{ok,R2} = attribute_functions:create_attribute("Group", "Relational", "Base ID", "none"),
	{ok,R3} = attribute_functions:create_attribute("Feeders", "Relational", "Base ID", []),
	{ok,R4} = attribute_functions:create_attribute("Sensors", "Relational", "Base ID", []),
	
	{ok,S1} = attribute_functions:create_attribute("Healthy", "Health", "Start time", true),%%TODO - contexts must be lists of act types
	{ok,S2} = attribute_functions:create_attribute("On heat", "Reproduction", "Start time", false),
	{ok,S3} = attribute_functions:create_attribute("Pregnant", "Reproduction", "Start time", false),
	{ok,S4} = attribute_functions:create_attribute("In labor", "Reproduction", "Base ID", false),
	{ok,S5} = attribute_functions:create_attribute("Desired food content", "Eating", "Percentages of nutrients like protein and energy", #{}),
	
	{ok,C0} = attribute_functions:create_attribute("FCR Case studies (CV)", "Calculation variables", "Case studies", #{}),
	{ok,C1} = attribute_functions:create_attribute("Total eaten (CV)", "Calculation variables", "Eating (RD)", 0),
	{ok,C2} = attribute_functions:create_attribute("Last relocation (CV)", "Calculation variables", "Relocate (RA)", #{true_time=>0,true_eaten=>0,weight_time=>0,weight_eaten=>0,weight=>"pending",cum_weight=>"pending"}),
	{ok,C3} = attribute_functions:create_attribute("Last weight (CV)", "Calculation variables", "Weight (RD)", #{time=>0,eaten=>0,weight=>"pending"}),
	{ok,C4} = attribute_functions:create_attribute("Last 24 hours eating (CV)", "Calculation variables", "Eating (RD)", #{}),
	{ok,C5} = attribute_functions:create_attribute("First weight (CV)", "Calculation variables", "Weight (RD)", #{time=>"pending",eaten=>0,weight=>"pending"}),
	{ok,C6} = attribute_functions:create_attribute("Total weight (CV)", "Calculation variables", "Weight (RD)", 0),
	{ok,C7} = attribute_functions:create_attribute("First meal (CV)", "Calculation variables", "Eating (RD)", #{time=>"pending",eaten=>0}),
	{ok,C8} = attribute_functions:create_attribute("Total duration (CV)", "Calculation variables", "Eating (RD)", 0),
	{ok,C9} = attribute_functions:create_attribute("Number of eating acts (CV)", "Calculation variables", "Eating (RD)", 0),
	{ok,C10} = attribute_functions:create_attribute("Eating alarm (CV)", "Calculation variables", "Eating (RD)", 0),
	
	{ok,S6} = attribute_functions:create_attribute("Weight stats since last relocation", "Weight (kg)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	{ok,S7} = attribute_functions:create_attribute("Gain stats since last relocation", "Weight gain (kg/day)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	{ok,S8} = attribute_functions:create_attribute("FCR stats since last relocation", "FCR (kg_eaten/kg_gained)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	
	{ok,S9} = attribute_functions:create_attribute("Overall weight stats", "Weight (kg)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	{ok,S10} = attribute_functions:create_attribute("Overall gain stats", "Weight gain (kg/day)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	{ok,S11} = attribute_functions:create_attribute("Overall FCR stats", "FCR (kg_eaten/kg_gained)", "Weight (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	
	{ok,S12} = attribute_functions:create_attribute("Eating stats since last relocation", "Eating (g/day)", "Eating (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	{ok,S13} = attribute_functions:create_attribute("Overall eating stats", "Eating (g/day)", "Eating (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	
	{ok,S14} = attribute_functions:create_attribute("Overall Eating Duration stats", "Eating Duration (s)", "Eating (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	{ok,S15} = attribute_functions:create_attribute("Overall Eating Frequency stats", "Eating Frequency (meals per day)", "Eating (RD)", #{max=>"pending",min=>"pending",avg=>"pending"}),
	
	{ok,M1} = attribute_functions:create_attribute("Set food content", "Management", "Percentages of nutrients like protein and energy", #{}),
	{ok,M2} = attribute_functions:create_attribute("Case studies", "Management", "Case studies", #{}),
	{ok,M3} = attribute_functions:create_attribute("Max hours between eating activities", "Management", "SMS", 24),
	
	[P1,P2,P3,P4,P5,P6,P7,R1,R2,R3,R4,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,M3,M1,M2,Photos].

