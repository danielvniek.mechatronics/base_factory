%% @author Daniel
%% @doc @todo Add description to sheep_standard_sp_mod.


-module(sheep_sp).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_atr_types/0,analyse_current_atrs/4,analyse_changed_atr/3]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_atr_types()->
	
	["Management"].

analyse_current_atrs(_AtrType,_Atrs,ReceptionMap,_Variables)->
	%{ok,_P}=sched_api:new_act(maps:get(sched,ReceptionMap), "CR Test", 63797097811, #{}),
	ok.

analyse_changed_atr(Atr,ReceptionMap,_Variables)->
	{ok,AtrId} = attribute_functions:get_atr_id(Atr),
	{ok,AtrVal} = attribute_functions:get_atr_value(Atr),
	case AtrId of
		"Case studies"->
			update_cs_calc_vars(maps:keys(AtrVal),AtrVal,maps:get(atr,ReceptionMap),#{});
		_->
			ok
	end.

update_cs_calc_vars([],_CaseStudies,AtrRecep,CS_CV)->
	{ok,OldCSCV} = atr_api:get_attribute_value(AtrRecep, "FCR Case studies (CV)"),
	RemovedCSs = find_removed_cs(maps:keys(CS_CV),maps:keys(OldCSCV),[]),
	atr_api:del_attributes_by_types(AtrRecep, RemovedCSs),
	FinalCSCV = keep_unchanged_cs(maps:keys(CS_CV),CS_CV,OldCSCV,#{}),
	atr_api:update_attribute_value(AtrRecep, "FCR Case studies (CV)", FinalCSCV);
update_cs_calc_vars([CSName|T],CaseStudies,AtrRecep,CS_CV)->
	Dets = maps:get(CSName,CaseStudies),
	Type = maps:get(<<"Case study type">>,Dets),
	case Type of
		<<"FCR">>->
			StartDate = binary_to_list(maps:get(<<"Case study start date">>,Dets)),
			StartTime = binary_to_list(maps:get(<<"Case study start time">>,Dets)),
			StartT = base_time:date_and_time_strings_to_base_time(StartDate, StartTime),
			EndDate = binary_to_list(maps:get(<<"Case study end date">>,Dets)),
			EndTime = binary_to_list(maps:get(<<"Case study end time">>,Dets)),
			EndT = base_time:date_and_time_strings_to_base_time(EndDate, EndTime),
			CVDets = #{true_time=>StartT,end_time=>EndT,true_eaten=>"pending",weight_time=>"pending",weight_eaten=>"pending",weight=>"pending",cum_weight=>"pending"},
			CSNameS = binary_to_list(CSName),
			update_cs_calc_vars(T,CaseStudies,AtrRecep,maps:put(CSNameS, CVDets, CS_CV));
		_->
			update_cs_calc_vars(T,CaseStudies,AtrRecep,CS_CV)
	end.

keep_unchanged_cs([],_NewCS,_OldCS,FinalCS)->
	FinalCS;
keep_unchanged_cs([CSName|T],NewCS,OldCS,FinalCS)->
	New =  maps:get(CSName,NewCS),
	case maps:is_key(CSName, OldCS) of
		true->
			Old = maps:get(CSName,OldCS),
			OStartT = maps:get(true_time,Old),
			OEndT = maps:get(end_time,Old),
			NStartT = maps:get(true_time,New),
			NEndT = maps:get(end_time,New),
			case (OStartT==NStartT) andalso (OEndT==NEndT) of
				true->
					keep_unchanged_cs(T,NewCS,OldCS,maps:put(CSName,Old,FinalCS));
				_->
					keep_unchanged_cs(T,NewCS,OldCS,maps:put(CSName,New,FinalCS))
			end;
		_->
			keep_unchanged_cs(T,NewCS,OldCS,maps:put(CSName,New,FinalCS))
	end.

find_removed_cs(_NewK,[],RemovedList)->
	RemovedList;
find_removed_cs(NewKeys,[OldKey|T],RemovedList)->
	case lists:member(OldKey, NewKeys) of
		true->
			find_removed_cs(NewKeys,T,RemovedList);
		_->
			find_removed_cs(NewKeys,T,[OldKey|RemovedList])
	end.
%%SLOC:66
