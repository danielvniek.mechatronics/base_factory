%% @author Daniel
%% @doc @todo Add description to sheep_standard_ap_mod.


-module(groups_ap).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,analyse_old_acts/4,analyse_act/3]).

-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).
%-record(biography_info,{tend::integer(),s3data::term()}).
-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	["Relocate (RA)","Receive sheep (SPA)", "Remove sheep", "New housing details"].
analyse_old_acts(_ActType,_Acts,_ReceptionMap,Variables)->%assume already analysed
	Variables.

analyse_act(Act,ReceptionMap,Variables)->
	AtrRecep = maps:get(atr,ReceptionMap),
	CommsRecep = maps:get(comms,ReceptionMap),	
	S1Data = Act#stage3Activity.schedule_info#schedule_info.s1data,
	S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
	case maps:is_key(<<"Contracts">>, S2Data) of
		true->
			Contracts = maps:get(<<"Contracts">>,S2Data);
		_->
			Contracts = []
	end,
	case Act#stage3Activity.type of
		"Relocate (RA)"->
			Result = maps:get(<<"Result">>,S2Data),
			case (Result=="Success") or (Result==<<"Success">>) of
				true->
					%%inform old house that group left
					{ok,HouseId} = atr_api:get_attribute_value(AtrRecep, "House"),	
					case HouseId of
						"none"->
							ok;
						_->
							{ok,MyBC} = comms_api:get_bc(CommsRecep),
							{ok,ID} = business_cards:get_id(MyBC),
							HouseAdr = erlang:list_to_atom(string:lowercase(string:replace(HouseId, " ", "_",all))),
							comms_api:send_inform(CommsRecep, HouseAdr, "Group left", ID)
					end,
					%%update house in attributes
 					{ok,LocContract} = contracts:find_contract_with_serv_type(Contracts, "Receive group"),
					{ok,LocBC} = contracts:get_service_bc(LocContract),
					{ok,LocID} = business_cards:get_id(LocBC),
					{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "House", LocID),
					%%get current feeder and update in own attributes
					{ok,LocAdr} = business_cards:get_address(LocBC),
					{ok,HouseAtrMap} = comms_api:send_request_info(maps:get(comms,ReceptionMap), LocAdr, "Attributes", ["Feeders"]),
					Feeders = maps:get("Feeders",HouseAtrMap),
					{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "Feeders", Feeders),
					%%inform sheep of new house and feeder
					{ok,Sheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
					inform_sheep_of_new_house(Sheep,LocID,Feeders,CommsRecep);
				_->
					ok
			end;
		"Receive sheep (SPA)"->
			Result = maps:get(<<"Result">>,S2Data),		
			case (Result=="Success") or (Result ==<<"Success">>) of
				true->
					Contract = maps:get(<<"Contract">>,S1Data),
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,SheepId} = business_cards:get_id(ClientBC),
					%%update sheep and number of sheep atrs
					F = fun(Val)->[SheepId|Val] end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Sheep", F),
					F2 = fun(Val)->Val+1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of sheep", F2),
					%%inform house of new sheep
					{ok,MyHouseId} = atr_api:get_attribute_value(AtrRecep, "House"),
					HouseAdr = erlang:list_to_atom(string:lowercase(string:replace(MyHouseId, " ", "_",all))),
					comms_api:send_inform(maps:get(comms,ReceptionMap), HouseAdr, "Sheep received", SheepId);
				_->
					ok
			end;
		"Remove sheep"->
			SheepId = maps:get(<<"Sheep id">>,S1Data),
			%%update sheep and number of sheep atrs
			F = fun(Val)->lists:delete(SheepId,Val) end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Sheep", F),
			F2 = fun(Val)->Val-1 end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of sheep", F2),
			%%update stat vars (cv)
			{ok,StatVars} = vs_api:get_value(maps:get(vs,ReceptionMap), "Stat vars (CV)"),
			NewStatVars = clean_stat_vars(maps:keys(StatVars),StatVars,SheepId),
			vs_api:update_value(maps:get(vs,ReceptionMap), "Stat vars (CV)", NewStatVars),
			%%inform house of sheep that left
			{ok,MyHouseId} = atr_api:get_attribute_value(AtrRecep, "House"),
			HouseAdr = erlang:list_to_atom(string:lowercase(string:replace(MyHouseId, " ", "_",all))),
			comms_api:send_inform(maps:get(comms,ReceptionMap), HouseAdr, "Sheep left", SheepId);
		"New housing details"->
			Details = maps:get(<<"Details">>,S1Data),
			case maps:is_key("Feeders", Details) of
				true->
					Feeders = maps:get("Feeders",Details),
					{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "Feeders", Feeders);
				_->
					ok
			end;
		_->
			ok
	end,
	Variables.

clean_stat_vars([],StatVars,_S)->
	StatVars;
clean_stat_vars([Key|T],StatVars,SheepId)->
	Dets = maps:get(Key,StatVars),
	NewDets = maps:remove(SheepId, Dets),
	clean_stat_vars(T,maps:put(Key,NewDets,StatVars),SheepId).

inform_sheep_of_new_house([],_House,_Feeders,_CommsRecep)->
	ok;
inform_sheep_of_new_house([Sheep|T],House,Feeders,CommsRecep)->
	SheepAdr = erlang:list_to_atom(string:lowercase(string:replace(Sheep, " ", "_",all))),
	Res = comms_api:send_inform(CommsRecep, SheepAdr, "New housing details", #{"House"=>House,"Feeders"=>Feeders}),
	case Res of
		ok->
			ok;
		_->
			error_log:log(?MODULE,0,unknown,"ERROR:Group got ~p when informing sheep about new housing details",[Res])
	end,
	inform_sheep_of_new_house(T,House,Feeders,CommsRecep).

%SLOC: 110
