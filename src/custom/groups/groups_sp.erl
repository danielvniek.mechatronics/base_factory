%% @author Daniel
%% @doc @todo Add description to groups_standard_sp_mod.


-module(groups_sp).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_atr_types/0,analyse_current_atrs/4,analyse_changed_atr/3]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_atr_types()->
	["Management"].

analyse_current_atrs(_AtrType,_Atrs,_ReceptionMap,_Variables)->
	ok.

analyse_changed_atr(Atr,ReceptionMap,_Variables)->
	{ok,AtrId} = attribute_functions:get_atr_id(Atr),
	{ok,AtrVal} = attribute_functions:get_atr_value(Atr),
	case AtrId of
		"Case studies"->
			{ok,MySheep} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Sheep"),
			%error_log:log(?MODULE,0,unknown,"\nGroup sharing cs with sheep"),
			inform_all_sheep_of_updated_case_studies(MySheep,maps:get(comms,ReceptionMap),AtrVal),
			{ok,CSNames} = vs_api:get_value(maps:get(vs,ReceptionMap), "Case study names (CV)"),
			{ok,StatVars} = vs_api:get_value(maps:get(vs,ReceptionMap), "Stat vars (CV)"),
			NewStatVars = clean_removed_cs(maps:keys(AtrVal),CSNames,StatVars,maps:get(atr,ReceptionMap)),
			NewCSNames = custom_erlang_functions:binary_list_to_str_list(maps:keys(AtrVal), []),
			vs_api:update_value(maps:get(vs,ReceptionMap), "Case study names (CV)", NewCSNames),
			vs_api:update_value(maps:get(vs,ReceptionMap), "Stat vars (CV)", NewStatVars);
		_->
			ok
	end.

inform_all_sheep_of_updated_case_studies([],_CommsRecep,_CS)->
	ok;
inform_all_sheep_of_updated_case_studies([Sheep|T],CommsRecep,CS)->
	SheepAdr = erlang:list_to_atom(string:lowercase(string:replace(Sheep, " ", "_",all))),
	Res = comms_api:send_inform(CommsRecep, SheepAdr, "Updated case studies", CS),
	case Res of
		ok->
			ok;
		_->
			error_log:log(?MODULE,0,unknown,"ERROR:Group got ~p when informing sheep about new case studies",[Res])
	end,
	inform_all_sheep_of_updated_case_studies(T,CommsRecep,CS).

clean_removed_cs(_NewK,[],StatVars,_AtrRecep)->
	StatVars;
clean_removed_cs(NewKeys,[OldKey|T],StatVars,AtrRecep)->
	case lists:member(list_to_binary(OldKey), NewKeys) of
		true->
			clean_removed_cs(NewKeys,T,StatVars,AtrRecep);
		_->
			atr_api:del_attributes_by_types(AtrRecep, ["Average sheep's stats in case study: "++OldKey]),
			SV1 = maps:remove(OldKey++" Eating (g/day)",StatVars),
			SV2 = maps:remove(OldKey++" FCR (kg_eaten/kg_gained)",SV1),
			SV3 = maps:remove(OldKey++" Weight (kg)",SV2),
			SV4 = maps:remove(OldKey++" Weight gain (kg/day)",SV3),
			clean_removed_cs(NewKeys,T,SV4,AtrRecep)
	end.
%%SLOC:47
