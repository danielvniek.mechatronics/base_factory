%% @author Daniel
%% @doc @todo Add description to house_serv_prov_mod.


-module(groups_serv_prov_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_services/0,handle_request/3,handle_execution_info/4,start/4,continue/4,handle_proposal/7,handle_package/6,service_cancelled/2]).



-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).
-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_services()->
	#{"Sheep Activity"=>{"Orchestrate activities of sheep in group",0},"Receive sheep"=>{"Allow sheep to join this group if there is space and the house is not occupied by another group",0}}.

handle_request(ReceptionMap,ServType,Contract)->
	AtrRecep = maps:get(atr,ReceptionMap),
	{ok,MySheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
	case ServType of
		"Sheep Activity"->
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientType} = business_cards:get_type(ClientBC),
			{ok,ClientId} = business_cards:get_id(ClientBC),
			case ClientType of
				"Sheep"->
					case lists:member(ClientId,MySheep) of
						true->
							{accept,[base_time:now()-1],#{<<"Sheep id">>=>ClientId},false};
						_->
							{{reject,"You are not one of my sheep"},0,0,0}
					end;
				_->
					{{reject,"You are of the wrong resource type"},0,0,0}
			end;
		"Receive sheep"->
			{ok,MyHouseId} = atr_api:get_attribute_value(AtrRecep, "House"),
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientType} = business_cards:get_type(ClientBC),
			{ok,ClientId} = business_cards:get_id(ClientBC),
			case MyHouseId of
				"none"->
					{{reject,"This group is not part of any house and can thus not accept any sheep"},0,0,0};
				_->
					HouseAdr = erlang:list_to_atom(string:lowercase(string:replace(MyHouseId, " ", "_",all))),
					{ok,HouseAtrMap} = comms_api:send_request_info(maps:get(comms,ReceptionMap), HouseAdr, "Attributes", ["Number of sheep","Number of incoming sheep","Sheep limit"]),
					case ClientType of
						"Sheep"->
							case lists:member(ClientId,MySheep) of
								true->
									{{reject,"You are already in this group"},0,0,0};
								_->
									NrSheep = maps:get("Number of sheep",HouseAtrMap),
									NrIncoming = maps:get("Number of incoming sheep",HouseAtrMap),
									SheepLimit = maps:get("Sheep limit",HouseAtrMap),
									case ((NrSheep+NrIncoming)<SheepLimit) of
										true->
											comms_api:send_inform(maps:get(comms,ReceptionMap), HouseAdr, "Incoming sheep", ClientId),
											{accept,[base_time:now()-1],#{<<"Sheep id">>=>ClientId},false};
										_->
											{{reject,"No space left"},0,0,0}
									end
							end;
						_->
							{{reject,"You are of the wrong resource type"},0,0,0}
					end
				end;
		_->					
			{{reject,"Unrecognized service type"},0,0,0}
	end.

start(S2Act,_AHPid,ReceptionMap,MyRecep)->
	CommsRecep = maps:get(comms,ReceptionMap),
	ActType = S2Act#stage2Activity.type,
	S1Data = S2Act#stage2Activity.schedule_info#schedule_info.s1data,
	case ActType of
		"Sheep Activity (SPA)"->%%SPA stands for service provision activity
			Contract = maps:get(<<"Contract">>,S1Data),
			{ok,ClientBC} = contracts:get_client_bc(Contract),
			{ok,ClientId} = business_cards:get_id(ClientBC),
			{ok,GroupRequestArgs} = contracts:get_request_arguments(Contract),
			RequestType = maps:get(<<"Serv type">>,GroupRequestArgs),
			RequestArgs = maps:get(<<"Args">>,GroupRequestArgs),
			{ok,ServProvs} = comms_api:find_services(CommsRecep, RequestType),
			case ServProvs of
				[]->
					%error_log:log(?MODULE,0,unknown,"\nFound no service providers"),
					notify:new("Group could not orchestrate sheep activity: "++RequestType++" for sheep with id:  "++io_lib:print(ClientId) ++" because the farm has no resources that can do this requested activity"),
					{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers available for this activity"}};
				_->
					{ok,ProposalTemplate} = contracts:add_proposal_field(#{}, "Available", boolean, "unitless"),
					ok = ra_api:send_rfps(MyRecep, S2Act#stage2Activity.id, ServProvs, RequestType, once_off, RequestArgs, #{}, ProposalTemplate, 10000,RequestType),
					{false,#{<<"Result">>=>"Waiting for a service provider to accept"}}
			end;
		"Receive sheep (SPA)"->%%SPA stands for service provision activity
			{false,#{<<"Result">>=>"waiting"}};
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized activity type to start"}}
	end.

handle_execution_info(S2Act,_AHPid,_ReceptionMap,Info)->
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Receive sheep (SPA)"->
			case Info of
				done->	
					{accept,true,#{<<"Result">>=>"Success"}};
				_->
					{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized info"}}
			end;
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized service type"}}
	end.
continue(S2Act,_AHPid,_ReceptionMap,_MyPid)->%%TODO: ensure sheep is still coming to this house after restart
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Receive sheep (SPA)"->%%SPA stands for service provision activity
			{false,#{<<"Result">>=>"waiting"}};
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized activity type to continue"}}
	end.

handle_proposal(S2Act,_AHPid,_ReceptionMap,MyRecep,_RfpIdentifier,ProposalContracts,PendingProposalContracts)->%%only expecting proposals from "Move sheep" service providers

	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	NewProposalContract = lists:nth(1, ProposalContracts),
	Proposal = contracts:get_proposal(NewProposalContract),
	case not(Proposal==invalid) of
		true->
			{ok,Available} = contracts:get_proposal_field_value("Available", NewProposalContract),
			case Available of
				true->
					{_Contract,WorkerReply} = ra_api:send_service_request(MyRecep, S2Act#stage2Activity.id, NewProposalContract),
					case WorkerReply of
						accept->
							%error_log:log(?MODULE,0,unknown,"6"),
							NewS2Data = maps:update(<<"Result">>, "Waiting for service provider to complete service", S2Data),
							{true,false,NewS2Data};
						_->
							case PendingProposalContracts==[] of
								true->
									%error_log:log(?MODULE,0,unknown,"4"),
									NewS2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed that they were available"},
									{true,true,NewS2Data};
								_->
									%error_log:log(?MODULE,0,unknown,"5"),
									{false,false,S2Data}
							end
					end;
				_->
					case PendingProposalContracts==[] of
						true->
							
							NewS2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed that they were available"},
							{true,true,NewS2Data};
						_->
							{false,false,S2Data}
					end
			end;
		_->
			%error_log:log(?MODULE,0,unknown,"3"),
			{false,false,S2Data}
	end.

handle_package(S2Act,_AHPid,ReceptionMap,_MyRecep,_Contract,Package)->
	%%no need to check contract id cause we only gave one contract that we want to receive
	ActType = S2Act#stage2Activity.type,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	case ActType of
		"Sheep Activity (SPA)"->
			case Package of
				done->
					NewS2Data = maps:update(<<"Result">>, <<"Success">>, S2Data),
					spa_api:deliver_package(S2Act, ReceptionMap, done),
					{accept,true,NewS2Data};
				_->%%no other packages expected from location but activity continues
					{{reject,"Only the atom done is a recognized package"},false,S2Data}
				end;
		_->%%will never happen
			error_log:log(?MODULE,0,unknown,"\nERROR: ~p requested to handle package for an act of unrecognized act type",[?MODULE]),
			{{reject,"Unrecognized act type"},true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Activiy of unrecognized type:"}}
	end.

service_cancelled(S2Act,ReceptionMap)->
	%error_log:log(?MODULE,0,unknown,"\nGroup received service cancelled"),
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Sheep Activity (SPA)"->%%SPA stands for service provision activity
			S2Act#stage2Activity.execution_info#execution_info.s2data;
		"Receive sheep (SPA)"->%%SPA stands for service provision activity
			{ok,MyHouseId} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "House"),
			HouseAdr = erlang:list_to_atom(string:lowercase(string:replace(MyHouseId, " ", "_",all))),
			comms_api:send_inform(maps:get(comms,ReceptionMap), HouseAdr, "Sheep cancelled", "_"),
			S2Act#stage2Activity.execution_info#execution_info.s2data;
		_->
			#{}
	end.

%%SLOC: 171
	
