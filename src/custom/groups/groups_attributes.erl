%% @author Daniel
%% @doc @todo Add description to groups_attributes.


-module(groups_attributes).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get()->
	{ok,M1} = attribute_functions:create_attribute("Age boundaries (months)", "Management", "[min,max] in months with min and max being numbers or none", ["none","none"]),
	{ok,M2} = attribute_functions:create_attribute("Weight boundaries (kg)", "Management", "[min,max] in kg with min and max being numbers or none", ["none","none"]),
	{ok,M3} = attribute_functions:create_attribute("Genders allowed", "Management", "Male/Female/all", "all"),
	{ok,M4} = attribute_functions:create_attribute("States allowed", "Management", "Map with state attribute id as key and specific value as value", "all"),
	{ok,M5} = attribute_functions:create_attribute("Avg food intake boundaries (grams)", "Management", "[min,max] in grams with min and max being numbers or none", ["none","none"]),
	{ok,M6} = attribute_functions:create_attribute("Set food content", "Management", "Percentages of nutrients like protein and energy", #{}),
	{ok,M7} = attribute_functions:create_attribute("Case studies", "Management", "Case studies", #{}),
	
	{ok,R1} = attribute_functions:create_attribute("House", "Relational", "Base ID", "none"),
	{ok,R2} = attribute_functions:create_attribute("Feeders", "Relational", "Base ID", []),
	{ok,R3} = attribute_functions:create_attribute("Sheep", "Relational", "Base ID", []),
	{ok,R4} = attribute_functions:create_attribute("Number of sheep", "Relational", "integer", 0),
	
	{ok,C1} = attribute_functions:create_attribute("Stat vars (CV)", "Calculation variables", "Sheep stats", #{}),
	{ok,C2} = attribute_functions:create_attribute("Case study names (CV)", "Calculation variables", "Sheep stats", []),
	[M1,M2,M3,M4,M5,M6,M7,R1,R2,R3,R4,C1,C2].

