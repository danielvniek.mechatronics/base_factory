%% @author Daniel
%% @doc @todo Add description to sheep_ia_requester_mod.


-module(groups_ep).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,start/4,handle_package/6,handle_serv_stopped/6,handle_proposal/7,handle_rfp_timeout/7]).

-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	#{"Relocate (RA)"=>0}.
	
start(Act,_AHPid,ReceptionMap,MyRecep)->
	case Act#stage2Activity.type of
		"Relocate (RA)"->
			CommsRecep = maps:get(comms,ReceptionMap),
			
			S1Data = Act#stage2Activity.schedule_info#schedule_info.s1data,
			LocID = binary_to_list(maps:get(<<"Location ID">>,S1Data)),
			AtrRecep = maps:get(atr,ReceptionMap),
			{ok,MyHouseId} = atr_api:get_attribute_value(AtrRecep, "House"),
			case (LocID==MyHouseId) of
				true->
					{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Group already part of this house"}};
				_->
					{ok,MyBC} = comms_api:get_bc(CommsRecep),
					{ok,ID} = business_cards:get_id(MyBC),
					{ok,[ServiceBC]} = doha_api:get_bcs_by_ids([LocID]),
					{LocationContract,LocReply} = ra_api:send_service_request(MyRecep,Act#stage2Activity.id, ServiceBC, "Receive group", once_off, #{}, #{}),
					case LocReply of
						accept->
							{ok,Sheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
							
							case Sheep of
								[]->
									NewS2Data = #{<<"Result">>=>"Success"},									
									%%inform new house that group in it
									comms_api:send_service_info(CommsRecep, both,LocationContract, done),
									{true,NewS2Data};
								_->
									relocate_loc_accepted(Act,ServiceBC,MyHouseId,LocationContract,CommsRecep,AtrRecep,MyRecep,MyBC)
							end;
							
						_->
							{reject,Reason} = LocReply,
							notify:new("Group with id: "++io_lib:print(ID)++" could not be relocated to house with id "++LocID ++" because of reason supplied by that house:" ++Reason),
							{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"House refused because of reason:"++Reason}}
					end
			end;
		_->
			error_log:log(?MODULE,0,unknown,"\nERROR: ~p requested to start an act of unrecognized act type",[?MODULE]),
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Activiy of unrecognized type:"}}
	end.

relocate_loc_accepted(Act,DestinationHouseBC,MyHouseId,_LocationContract,CommsRecep,_AtrRecep,MyRecep,MyBC)->
	{ok,MyId} = business_cards:get_id(MyBC),
	{ok,DestinationAdr} = business_cards:get_address(DestinationHouseBC),
	{ok,DestinationID} = business_cards:get_id(DestinationHouseBC),
	{ok,IdentificationMap} = comms_api:send_request_info(CommsRecep, DestinationAdr, "Attributes", ["EID","Photos"]),
	DestinationEID = maps:get("EID",IdentificationMap),DestinationPhotos = maps:get("Photos",IdentificationMap),
	case MyHouseId of
		"none"->
			{CurrentEID,CurrentPhotos} = {"none",[]};
		_->
			MyHouseAdr = erlang:list_to_atom(string:lowercase(string:replace(MyHouseId, " ", "_",all))),
			{ok,IdentificationMap2} = comms_api:send_request_info(CommsRecep, MyHouseAdr, "Attributes", ["EID","Photos"]),
			CurrentEID = maps:get("EID",IdentificationMap2),CurrentPhotos = maps:get("Photos",IdentificationMap2)
	end,
	RequestArgs = #{<<"Group ID">>=>MyId,
			   <<"Current house ID">>=>MyHouseId,<<"Current house EID">>=>CurrentEID,<<"Current house photos">>=>CurrentPhotos,
			   <<"New house ID">>=>DestinationID,<<"New house EID">>=>DestinationEID,<<"New house photos">>=>DestinationPhotos},

	{ok,ServProvs} = comms_api:find_services(CommsRecep, "Move sheep"),
	case ServProvs of
		[]->
			%error_log:log(?MODULE,0,unknown,"\nFound no service providers"),
			notify:new("Group with id: "++io_lib:print(MyId)++" could not be relocated to house with id "++DestinationID ++" because the farm has no resources that can do this requested activity"),
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers available for this activity"}};
		_->
			{ok,ProposalTemplate} = contracts:add_proposal_field(#{}, "Available", boolean, "unitless"),
			ok = ra_api:send_rfps(MyRecep, Act#stage2Activity.id, ServProvs, "Move sheep", once_off, RequestArgs, #{}, ProposalTemplate, 10000,"Move sheep"),
			{false,#{<<"Result">>=>"Waiting for a sheep mover to accept"}}
	end.

handle_proposal(S2Act,_AHPid,_ReceptionMap,MyRecep,_RfpIdentifier,ProposalContracts,PendingProposalContracts)->%%only expecting proposals from "Move sheep" service providers

	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	NewProposalContract = lists:nth(1, ProposalContracts),
	Proposal = contracts:get_proposal(NewProposalContract),
	case not(Proposal==invalid) of
		true->
			{ok,Available} = contracts:get_proposal_field_value("Available", NewProposalContract),
			case Available of
				true->
					{_Contract,WorkerReply} = ra_api:send_service_request(MyRecep, S2Act#stage2Activity.id, NewProposalContract),
					case WorkerReply of
						accept->
							NewS2Data = maps:update(<<"Result">>, "Waiting for sheep mover to complete service", S2Data),
							{true,false,NewS2Data};
						_->
							case PendingProposalContracts==[] of
								true->
									NewS2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed that they were available"},
									{true,true,NewS2Data};
								_->
									{false,false,S2Data}
							end
					end;
				_->
					case PendingProposalContracts==[] of
						true->
							
							NewS2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed that they were available"},
							{true,true,NewS2Data};
						_->
							{false,false,S2Data}
					end
			end;
		_->
			{false,false,S2Data}
	end.

handle_package(S2Act,_AHPid,ReceptionMap,_MyRecep,Contract,Package)->
	%%no need to check contract id cause we only gave one contract that we want to receive
	ActType = S2Act#stage2Activity.type,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	Contracts = maps:get(<<"Contracts">>,S2Data),
	case ActType of
		"Relocate (RA)"->
			{ok,ServType} = contracts:get_service_type(Contract),
			case (ServType=="Move sheep") of%%sheep mover or sheep group
				true->
					CommsRecep = maps:get(comms,ReceptionMap),
					case Package of
						done->
							NewS2Data = maps:update(<<"Result">>, <<"Success">>, S2Data),
							{ok,LocContract} = contracts:find_contract_with_serv_type(Contracts, "Receive group"),
							%%inform new house that group in it
							comms_api:send_service_info(CommsRecep, execution,LocContract, done),
							{accept,true,NewS2Data};
						_->%%no other packages expected from location but activity continues
							{{reject,"Only the atom done is a recognized package"},false,S2Data}
						end;
				_->%%only alternative is package from location which is not expected but activity continues
					{{reject,"Not expecting package from location"},false,S2Data}
			end;
		_->%%will never happen
			error_log:log(?MODULE,0,unknown,"\nERROR: ~p requested to handle package for an act of unrecognized act type",[?MODULE]),
			{{reject,"Unrecognized act type"},true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Activiy of unrecognized type:"}}
	end.


	
handle_serv_stopped(S2Act,_AHPid,ReceptionMap,MyRecep,Contract,Reason)->
	%error_log:log(?MODULE,0,unknown,"\nGroup got service stopped"),
	ActType = S2Act#stage2Activity.type,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	Contracts = maps:get(<<"Contracts">>,S2Data),
	CommsRecep = maps:get(comms,ReceptionMap),
	AtrRecep = maps:get(atr,ReceptionMap),
	{ok,MyGroup} = atr_api:get_attribute_value(AtrRecep, "Group"),
	case ActType of
		"Relocate (RA)"->
			%error_log:log(?MODULE,0,unknown,"\nGroup got service stopped for Relocate (RA)"),
			{ok,ServType} = contracts:get_service_type(Contract),
			case (ServType=="Move sheep") of%%sheep mover or sheep group
				true->%%relocater stopped service
					case MyGroup of
						"none"->
							{ok,ServProvs} = comms_api:find_services(CommsRecep, "Move sheep"),
							{ok,RequestArgs} = contracts:get_request_arguments(Contract),
							{ok,MyBC} = comms_api:get_bc(CommsRecep),
							{ok,MyId} = business_cards:get_id(MyBC),
							case ServProvs of
								[]->
									{ok,DestinationContract} = contracts:find_contract_with_serv_type(Contracts, "Receive group"),
									{ok,DestinationBC} = contracts:get_service_bc(DestinationContract),
									{ok,DestinationID} = business_cards:get_id(DestinationBC),
									notify:new("Grouop with id: "++io_lib:print(MyId)++" could not be relocated to house/group with id "++DestinationID ++" because the farm has no resources that can do this requested activity"),
									{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers available for this activity"}};
								_->
									{ok,ProposalTemplate} = contracts:add_proposal_field(#{}, "Available", boolean, unitless),
									ra_api:send_rfps(MyRecep, S2Act#stage2Activity.id, ServProvs, "Move sheep", once_off, RequestArgs, #{}, ProposalTemplate, 10000,"Move sheep"),
									{false,#{<<"Result">>=>"Waiting for a sheep mover to accept"}}
							end;
						_->
							{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Reason provided by group: "++Reason}}	
					end;
				_->%%only alternative is that location provider stopped service
					%ra_api:send_cancel(MyRecep, S2Act#stage2Activity.id, RelocaterContract, "Reason provided by location:"++Reason),
					{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"New location stopped service of receiving this group"}}
			end;
		_->
			{true,[],#{<<"Result">>=>"Failed",<<"Reason">>=>"Activiy of unrecognized type:"}}
	end.

handle_rfp_timeout(_S2Act,_AHPid,_ReceptionMap,_MyPid,_RfpIdentifier,_ProposalContracts,_PendingProposalContracts)->
	{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed within the required time"}}.

%%SLOC:176



