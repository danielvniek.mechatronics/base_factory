%% @author Daniel
%% @doc @todo Add description to groups_request_and_inform_mod.


-module(groups_request_and_inform_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([inform/4,request_info/4]).



%% ====================================================================
%% Internal functions
%% ====================================================================
request_info(ReceptionMap,Type,Details,_Requester)->
	case Type of
		"Case study data"->
			%error_log:log(?MODULE,0,unknown,"\nGroup requested for case study data"),
			CaseStudyName = maps:get(<<"cs_name">>,Details),
			{ok,CaseStudies} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Case studies"),
			case maps:is_key(CaseStudyName, CaseStudies) of
				true->
					CSDets = maps:get(CaseStudyName,CaseStudies),
					CaseStudyType = maps:get(<<"Case study type">>,CSDets),
					case CaseStudyType of
						<<"FCR">>->
							%error_log:log(?MODULE,0,unknown,"\nCSName:~p",[CaseStudyName]),
							{ok,MySheep} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Sheep"),
							FCRDataMap = build_fcr_data_map(MySheep,CaseStudyName,maps:get(comms,ReceptionMap),#{}),
							{ok,maps:put(<<"Case study name">>,CaseStudyName,FCRDataMap)};
						_->
							{error,"Cannot provide data for case study of this type"}
					end;
				_->
					{error,"No case study with this name"}
			end;
		_->
			{error,"Cannot handle request of this type"}
	end.

build_fcr_data_map([],_CaseStudyName,_Comms,FCRDataMap)->
	FCRDataMap;
build_fcr_data_map([SheepId|T],CaseStudyName,Comms,FCRDataMap)->
	SheepAdr = erlang:list_to_atom(string:lowercase(string:replace(SheepId, " ", "_",all))),
	{ok,SheepFCRData} = comms_api:send_request_info(Comms, SheepAdr, "Case study data", #{<<"cs_name">>=>CaseStudyName}),
	build_fcr_data_map(T,CaseStudyName,Comms,maps:put(list_to_binary(SheepId),SheepFCRData,FCRDataMap)).

inform(ReceptionMap,InformType,InformDetails,RequesterCommsRecep)->
	AtrRecep = maps:get(atr,ReceptionMap),
	ExeRecep = maps:get(exe,ReceptionMap),
	case InformType of
		"Sheep left"->
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			{ok,MySheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
			case ReqId==InformDetails andalso lists:member(InformDetails,MySheep) of
				true->
					exe_api:start_and_finish_new_act(ExeRecep, "Remove sheep", #{<<"Sheep id">>=>InformDetails}, #{<<"Result">>=>"Success"}),
					ok;
				_->
					{error,"No sheep with this id in this group"}
			end;
		"New housing details"->
			{ok,House} = atr_api:get_attribute_value(AtrRecep, "House"),
			{ok,BC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(BC),
			case (ReqId==House) of
				true->
					exe_api:start_and_finish_new_act(ExeRecep, "New housing details", #{<<"Details">>=>InformDetails}, #{<<"Result">>=>"Success"}),
					ok;
				_->
					{error,"Only my house can update my feeders"}
			end;
		"Sheep stats"->
			%error_log:log(?MODULE,0,unknown,"\nGroup informed about sheep stats"),
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			{ok,MySheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
			case lists:member(ReqId,MySheep) of
				true->
					{ok,StatVars} = vs_api:get_value(maps:get(vs,ReceptionMap), "Stat vars (CV)"),
					evaluate_sheep_stats(AtrRecep,StatVars,ReqId,InformDetails,[]),
					ok;
				_->
					{error,"No sheep with this id in this group"}
			end
		
	end.

evaluate_sheep_stats(AtrRecep,StatVars,_SheepId,[],MyAtrs)->
	{ok,StatVars} = vs_api:create("Stat vars (CV)", "Sheep stats", StatVars),
	{ok,"success"}=atr_api:save(AtrRecep, lists:append(MyAtrs, [StatVars]));
evaluate_sheep_stats(AtrRecep,StatVars,SheepId,[Atr|T],MyAtrs)->
	{ok,AtrId} = attribute_functions:get_atr_id(Atr),
	{ok,AtrVal} = attribute_functions:get_atr_value(Atr),
	case AtrId of
		"Current daily intake"->
			{NewAtr,NewStatVars} = update_current(AtrRecep,StatVars,SheepId,"Current daily intake","Average sheep's eating (g/day)",AtrVal);
		"Eating stats since last relocation"->
			{NewAtr,NewStatVars} = update_stats(AtrRecep,StatVars,SheepId,"Eating stats since part of this group","Average sheep's eating (g/day)",AtrVal);
		"Current weight"->
			%error_log:log(?MODULE,0,unknown,"\nGroup got current weight"),
			{NewAtr,NewStatVars} = update_current(AtrRecep,StatVars,SheepId,"Current weight","Average sheep's weight (kg)",AtrVal);
		"Weight stats since last relocation"->
			{NewAtr,NewStatVars} = update_stats(AtrRecep,StatVars,SheepId,"Weight stats since part of this group","Average sheep's weight (kg)",AtrVal);
		"Current daily gain"->
			{NewAtr,NewStatVars} = update_current(AtrRecep,StatVars,SheepId,"Current daily gain","Average sheep's weight gain (kg/day)",AtrVal);
		"Gain stats since last relocation"->
			{NewAtr,NewStatVars} = update_stats(AtrRecep,StatVars,SheepId,"Gain stats since part of this group","Average sheep's weight gain (kg/day)",AtrVal);
		"Current FCR"->
			{NewAtr,NewStatVars} = update_current(AtrRecep,StatVars,SheepId,"Current FCR","Average sheep's FCR (kg_eaten/kg_gained)",AtrVal);
		"FCR stats since last relocation"->
			{NewAtr,NewStatVars} = update_stats(AtrRecep,StatVars,SheepId,"FCR stats since part of this group","Average sheep's FCR (kg_eaten/kg_gained)",AtrVal);
		_->
			{ok,AtrType} = attribute_functions:get_atr_type(Atr),
			{ok,CaseStudies} = atr_api:get_attribute_value(AtrRecep, "Case studies"),
			case maps:is_key(list_to_binary(AtrType), CaseStudies) of
				true->
					{NewAtr,NewStatVars} = update_stats(AtrRecep,StatVars,SheepId,AtrId,"Average sheep's stats in case study: "++AtrType,AtrVal);
				_->
					{NewAtr,NewStatVars} = {none,StatVars}
			end
	end,
	case NewAtr of
		none->
			evaluate_sheep_stats(AtrRecep,NewStatVars,SheepId,T,MyAtrs);
		_->
			evaluate_sheep_stats(AtrRecep,NewStatVars,SheepId,T,[NewAtr|MyAtrs])
	end.

update_current(_AtrRecep,StatVars,SheepId,AtrId,AtrType,AtrVal)->
	case maps:is_key(AtrId, StatVars) of
		true->
			Vars = maps:get(AtrId,StatVars),
			NewVars = maps:put(SheepId, AtrVal, Vars),
			Vals = maps:values(NewVars),
			Avg = lists:sum(Vals)/length(Vals);
		_->
			NewVars = #{SheepId=>AtrVal},
			Avg = AtrVal
	end,
	NewStatVars = maps:put(AtrId, NewVars, StatVars),
	{ok,Atr} = attribute_functions:create_attribute(AtrId, AtrType, "Sheep stats", Avg),
	{Atr,NewStatVars}.
			
update_stats(AtrRecep,StatVars,SheepId,AtrId,AtrType,AtrVal)->
	SAvg = maps:get(avg,AtrVal),
	SMax = maps:get(max,AtrVal),
	SMin = maps:get(min,AtrVal),
	case maps:is_key(AtrId, StatVars) of
		true->
			Vars = maps:get(AtrId,StatVars),
			case is_number(SAvg) of
				true->
					
					NewVars = maps:put(SheepId, SAvg, Vars),
					Vals = maps:values(NewVars),
					Avg = lists:sum(Vals)/length(Vals);
				_->
					NewVars = Vars,
					Avg = current
			end;
		_->
			case is_number(SAvg) of
				true->
					NewVars = #{SheepId=>SAvg},
					Avg = SAvg;
				_->
					NewVars = #{},
					Avg = current
			end
	end,
	NewStatVars = maps:put(AtrId, NewVars, StatVars),
	{Res,Current} = atr_api:get_attribute_value(AtrRecep, AtrId),
	case Res of
		ok->
			Max = maps:get(max,Current),
			Min = maps:get(min,Current),
			case Avg of
				current->
					FinalAvg = maps:get(avg,Current);
				_->
					FinalAvg = Avg
			end;
		_->
			Max = "pending",Min="pending",
			case Avg of
				current->
					FinalAvg = "pending";
				_->
					FinalAvg = Avg
			end
	end,
	case Max=="pending" orelse SMax=="pending" orelse SMax>Max of
		true->
			NewMax = SMax;
		_->
			NewMax = Max
	end,
	case Min=="pending" orelse SMin =="pending" orelse SMin<Min of
		true->
			NewMin = SMin;
		_->
			NewMin = Min
	end,
	
	{ok,Atr} = attribute_functions:create_attribute(AtrId, AtrType, "Sheep stats", #{max=>NewMax,min=>NewMin,avg=>FinalAvg}),
	{Atr,NewStatVars}.
			
%%SLOC: 187	




