%% @author Daniel
%% @doc @todo Add description to human_workers_serv_prov_mod.


-module(human_workers_serv_prov_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_services/0,propose/6,handle_request/3,start/4,continue/4,service_cancelled/2]).

-export([interface_with_worker/4]).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).


%% ====================================================================
%% Internal functions
%% ====================================================================
get_services()->
	#{"Move sheep"=>{"Moving sheep around",0},"Move feeder"=>{"Moving feeders between houses",0}}.

propose(_ReceptionMap,ServType,FieldDescription,_FieldEntryType,_Unit,_Contract)->
	case ServType of
		"Move sheep"->
			case FieldDescription of
				"Available"->
					true;
				_->
					none
			end;
		"Move feeder"->
			case FieldDescription of
				"Available"->
					true;
				_->
					none
			end;
		_->
			none
	end.

handle_request(_ReceptionMap,ServType,_Contract)->
	case ServType of
		"Move sheep"->
			%TODO:ask person to accept
			{accept,[base_time:now()-1],#{},false};
		"Move feeder"->
			%TODO:ask person to accept
			{accept,[base_time:now()-1],#{},false};
		_->
			{{reject,"Unrecognized service type"},0,0,0}
	end.

start(S2Act,AHPid,ReceptionMap,MyRecep)->
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Move sheep (SPA)"->
			{ok,Pid} = spa_api:spawn_process(MyRecep, ?MODULE, interface_with_worker, [S2Act,AHPid,ReceptionMap,"TODO"]),
			{false,#{<<"Result">>=>"Pending",<<"Interface process">>=>Pid}};
		"Move feeder (SPA)"->
			{ok,Pid} = spa_api:spawn_process(MyRecep, ?MODULE, interface_with_worker, [S2Act,AHPid,ReceptionMap,"TODO"]),
			{false,#{<<"Result">>=>"Pending",<<"Interface process">>=>Pid}};
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized activity type to start"}}
	end.

continue(S2Act,AHPid,ReceptionMap,MyRecep)->
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Move sheep (SPA)"->
			{ok,Pid} = spa_api:spawn_process(MyRecep, ?MODULE, interface_with_worker, [S2Act,AHPid,ReceptionMap,"TODO"]),
			{false,#{<<"Result">>=>"Pending",<<"Interface process">>=>Pid}};
		"Move feeder (SPA)"->
			{ok,Pid} = spa_api:spawn_process(MyRecep, ?MODULE, interface_with_worker, [S2Act,AHPid,ReceptionMap,"TODO"]),
			{false,#{<<"Result">>=>"Pending",<<"Interface process">>=>Pid}};
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized activity type to start"}}
	end.

service_cancelled(S2Act,_ReceptionMap)->
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Move sheep (SPA)"->
			S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
			Pid = maps:get(<<"Interface process">>,S2Data),
			exit(Pid,kill),
			maps:remove(<<"Interface process">>,S2Data);
		"Move feeder (SPA)"->
			S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
			Pid = maps:get(<<"Interface process">>,S2Data),
			exit(Pid,kill),
			maps:remove(<<"Interface process">>,S2Data);
		_->
			#{}
	end.

interface_with_worker(S2Act,_AHPid,ReceptionMap,_Info)->
	timer:sleep(2000),%%simulating sheep or feeder being moved, TODO: whatsapp interface
	Res = spa_api:deliver_package(S2Act, ReceptionMap, done),
	FinalS2Data = #{<<"Result">>=>"Success",<<"Package delivery">>=>Res},
	spa_api:done(maps:get(comms,ReceptionMap), S2Act#stage2Activity.id, FinalS2Data, false).
			