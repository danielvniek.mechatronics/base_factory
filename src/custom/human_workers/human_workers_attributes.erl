%% @author Daniel
%% @doc @todo Add description to human_workers_attributes.


-module(human_workers_attributes).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get()->
	{ok,P1} = attribute_functions:create_attribute("Date of birth", "Personal", "Date", "pending"),
	{ok,P2} = attribute_functions:create_attribute("Gender", "Personal", "Male/Female/pending", "none"),
	{ok,P3} = attribute_functions:create_attribute("Weight (kg)", "Personal", "kg", "pending"),
	{ok,P4} = attribute_functions:create_attribute("Height (cm)", "Personal", "cm", "pending"),
	
	{ok,M1} = attribute_functions:create_attribute("Cell phone number", "Management", "27836566942", <<"27836566942">>),
	{ok,Photos} = attribute_functions:create_attribute("Photos", "Personal", "GD links or actual photos?", []),
	[P1,P2,P3,P4,M1,Photos].

