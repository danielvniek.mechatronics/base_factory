%% @author Daniel
%% @doc @todo Add description to human_workers_request_and_inform_mod.


-module(human_workers_request_and_inform_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([request_info/4,inform/4]).



%% ====================================================================
%% Internal functions
%% ====================================================================
request_info(_ReceptionMap,InfoRequested,RequestDetails,_From)->
	%io:format("\nInfo requested from human worker"),
	case InfoRequested of
		"available"->
			{ServType,_RequestArgs}=RequestDetails,
			case ServType of
				"Move sheep"->
					available;%%TODO: change this to ask worker via whatsapp
				_->
					{error,"Unrecognized service type"}
			end;
		_->
			{error,"Unrecognized info requested"}
	end.

inform(ReceptionMap,InformTopic,InformDetails,_From)->
	case InformTopic of
		"SMS"->
			io:format("\nSMS received by human"),
			Atr = maps:get(atr,ReceptionMap),
			{ok,PhoneNumberB} = atr_api:get_attribute_value(Atr, "Cell phone number"),
			PhoneNumber = binary_to_list(PhoneNumberB),
			deliver_sms(PhoneNumber,InformDetails),
			ok;
		_->
			{error,"No informs allowed for this resource"}
	end.

deliver_sms(PhoneNumberString,MessageString)->
	case string:to_integer(PhoneNumberString) of
		{error,_E}->
			io:format("Could not deliver sms because ~p is not a valid phone number",[PhoneNumberString]);
		_->
			PhoneNumber = ensure27PhoneNumber(PhoneNumberString),
			
			Cmd = "curl -X \"POST\" " ++ "\"https://rest.nexmo.com/sms/json\" \\ -d \"from=Vonage APIs\" \\ -d \"text="++MessageString++"\" \\ -d \"to="++PhoneNumber++"\" \\ -d \"api_key=dd78198d\" \\ -d \"api_secret=jdfHJStwPs8DCKLV\"",
		    io:format("\nCommand:~p",[Cmd]),
			Output = os:cmd(Cmd),
			io:format("SMS output for sms(~p) to ~p:~p",[PhoneNumberString,MessageString,Output])
	end.

ensure27PhoneNumber(PhoneNumberString)->
	case string:length(PhoneNumberString)<11 of
		true->
			string:replace(PhoneNumberString, "0", "27",leading);
		_->
			PhoneNumberString
	end.



