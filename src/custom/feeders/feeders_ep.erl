%% @author Daniel
%% @doc @todo Add description to sheep_ia_requester_mod.


-module(feeders_ep).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,start/4,handle_package/6,handle_proposal/7,handle_rfp_timeout/7]).

-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	#{"Relocate (RA)"=>0}.
	
start(Act,_AHPid,ReceptionMap,MyRecep)->
	case Act#stage2Activity.type of
		"Relocate (RA)"->
			CommsRecep = maps:get(comms,ReceptionMap),
			{ok,MyBC} = comms_api:get_bc(CommsRecep),
			S1Data = Act#stage2Activity.schedule_info#schedule_info.s1data,
			LocID = binary_to_list(maps:get(<<"Location ID">>,S1Data)),
			AtrRecep = maps:get(atr,ReceptionMap),
			{ok,MyHouseId} = atr_api:get_attribute_value(AtrRecep, "House"),
			case (LocID==MyHouseId) of
				true->
					{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Feeder already part of this house"}};
				_->
					{ok,MyId} = business_cards:get_id(MyBC),
					{ok,MyPhotos} = atr_api:get_attribute_value(AtrRecep, "Photos"),
					{ok,MyEID} = atr_api:get_attribute_value(AtrRecep,"EID"),
					LocAdr = erlang:list_to_atom(string:lowercase(string:replace(LocID, " ", "_",all))),
					{ok,IdentificationMap} = comms_api:send_request_info(CommsRecep, LocAdr, "Attributes", ["EID","Photos"]),
					DestinationEID = maps:get("EID",IdentificationMap),DestinationPhotos = maps:get("Photos",IdentificationMap),
					case MyHouseId of
						"none"->
							{CurrentEID,CurrentPhotos} = {"none",[]};
						_->
							MyHouseAdr = erlang:list_to_atom(string:lowercase(string:replace(MyHouseId, " ", "_",all))),
							{ok,IdentificationMap2} = comms_api:send_request_info(CommsRecep, MyHouseAdr, "Attributes", ["EID","Photos"]),
							CurrentEID = maps:get("EID",IdentificationMap2),CurrentPhotos = maps:get("Photos",IdentificationMap2)
					end,
					RequestArgs = #{<<"Feeder ID">>=>MyId,<<"Feeder EID">>=>MyEID,<<"Feeder photos">>=>MyPhotos,
				   <<"Current house ID">>=>MyHouseId,<<"Current house EID">>=>CurrentEID,<<"Current house photos">>=>CurrentPhotos,
				   <<"New house ID">>=>LocID,<<"New house EID">>=>DestinationEID,<<"New house photos">>=>DestinationPhotos},
					{ok,ServProvs} = comms_api:find_services(CommsRecep, "Move feeder"),
					case ServProvs of
						[]->
							%error_log:log(?MODULE,0,unknown,"\nFound no service providers"),
							notify:new("Feeder with id: "++io_lib:print(MyId)++" could not be relocated to house with id "++LocID ++" because the farm has no resources that can do this requested activity"),
							{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers available for this activity"}};
						_->
							{ok,ProposalTemplate} = contracts:add_proposal_field(#{}, "Available", boolean, "unitless"),
							ok = ra_api:send_rfps(MyRecep, Act#stage2Activity.id, ServProvs, "Move feeder", once_off, RequestArgs, #{}, ProposalTemplate, 10000,"Move feeder"),
							{false,#{<<"Result">>=>"Waiting for a sheep mover to accept"}}
					end
				
			end;
		_->
			error_log:log(?MODULE,0,unknown,"\nERROR: ~p requested to start an act of unrecognized act type",[?MODULE]),
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Activiy of unrecognized type:"}}
	end.

				
handle_proposal(S2Act,_AHPid,_ReceptionMap,MyRecep,_RfpIdentifier,ProposalContracts,PendingProposalContracts)->%%only expecting proposals from "Move feeder" service providers

	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	NewProposalContract = lists:nth(1, ProposalContracts),
	Proposal = contracts:get_proposal(NewProposalContract),
	case not(Proposal==invalid) of
		true->
			{ok,Available} = contracts:get_proposal_field_value("Available", NewProposalContract),
			case Available of
				true->
					{_Contract,WorkerReply} = ra_api:send_service_request(MyRecep, S2Act#stage2Activity.id, NewProposalContract),
					case WorkerReply of
						accept->
							NewS2Data = maps:update(<<"Result">>, "Waiting for feeder mover to complete service", S2Data),
							{true,false,NewS2Data};
						_->
							case PendingProposalContracts==[] of
								true->
									NewS2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed that they were available"},
									{true,true,NewS2Data};
								_->
									{false,false,S2Data}
							end
					end;
				_->
					case PendingProposalContracts==[] of
						true->
							
							NewS2Data = #{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed that they were available"},
							{true,true,NewS2Data};
						_->
							{false,false,S2Data}
					end
			end;
		_->
			{false,false,S2Data}
	end.
	

handle_package(S2Act,_AHPid,ReceptionMap,_MyRecep,Contract,Package)->
	%%no need to check contract id cause we only gave one contract that we want to receive
	ActType = S2Act#stage2Activity.type,
	S2Data = S2Act#stage2Activity.execution_info#execution_info.s2data,
	AtrRecep = maps:get(atr,ReceptionMap),
	case ActType of
		"Relocate (RA)"->
			{ok,ServType} = contracts:get_service_type(Contract),
			case (ServType=="Move feeder") of%%sheep mover or sheep group
				true->
					CommsRecep = maps:get(comms,ReceptionMap),
					{ok,MyBC} = comms_api:get_bc(CommsRecep),
					{ok,MyId} = business_cards:get_id(MyBC),
					case Package of
						done->
							NewS2Data = maps:update(<<"Result">>, <<"Success">>, S2Data),
							{ok,HouseId} = atr_api:get_attribute_value(AtrRecep, "House"),	
							case HouseId of
								"none"->
									ok;
								_->
									HouseAdr = erlang:list_to_atom(string:lowercase(string:replace(HouseId, " ", "_",all))),
									comms_api:send_inform(CommsRecep, HouseAdr, "Feeder left", MyId)
							end,
							S1Data = S2Act#stage2Activity.schedule_info#schedule_info.s1data,
							LocID = binary_to_list(maps:get(<<"Location ID">>,S1Data)),
							LocAdr = erlang:list_to_atom(string:lowercase(string:replace(LocID, " ", "_",all))),
							comms_api:send_inform(CommsRecep, LocAdr, "New feeder", MyId),
							{ok,"success"}=atr_api:update_attribute_value(AtrRecep, "House", LocID),
							{accept,true,NewS2Data};
						_->%%no other packages expected from location but activity continues
							{{reject,"Only the atom done is a recognized package"},false,S2Data}
						end;
				_->%%only alternative is package from location which is not expected but activity continues
					{{reject,"Not expecting package from location"},false,S2Data}
			end;
		_->%%will never happen
			error_log:log(?MODULE,0,unknown,"\nERROR: ~p requested to handle package for an act of unrecognized act type",[?MODULE]),
			{{reject,"Unrecognized act type"},true,#{<<"Result">>=>"Failed",<<"Reason">>=>"Activiy of unrecognized type:"}}
	end.
		


handle_rfp_timeout(_S2Act,_AHPid,_ReceptionMap,_MyPid,_RfpIdentifier,_ProposalContracts,_PendingProposalContracts)->
	{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"No service providers proposed within the required time"}}.



