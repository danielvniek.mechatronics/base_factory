%% @author Daniel
%% @doc @todo Add description to feeders_attributes.


-module(feeders_attributes).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get()->
	{ok,P1} = attribute_functions:create_attribute("EID", "Personal", "Electronic ID of the tag attached to this resource", "none"),
	{ok,R1} = attribute_functions:create_attribute("House", "Relational", "Base ID", "none"),
	{ok,M1} = attribute_functions:create_attribute("Current food product id","Management","Product in sheep_food_warehouse","none"),
	{ok,Photos} = attribute_functions:create_attribute("Photos", "Personal", "GD links or actual photos?", []),
	[P1,R1,M1,Photos].

