%% @author Daniel
%% @doc @todo Add description to test_interface.


-module(rfid_sheep_scale_interface_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_gateway_details_and_min_time_between_failures/1,start_interface/3,valid_identifier/2,get_interface_info/1]).


%% ====================================================================
%% Internal functions
%% ====================================================================
get_gateway_details_and_min_time_between_failures(_Id)->%%used by sensor_ep
	{ok,#{<<"Interface type">>=>"Management UI",<<"Input type">>=>"Excel file"},10000}.
get_interface_info(_Id)->%%used by sensors_serv_prov_mod
	{ok,Topics} = cp_interface_creation:add_observer_topic(#{},"Weight",99,"kg"),
	{ok,Resources} = cp_interface_creation:add_to_resources(observer, #{}, "Sheep", all, "EID", Topics),
	{"Measures weight of sheep and stores on an excel file using EIDs of sheep",Resources}.

valid_identifier("EID",Identifier)->
	case custom_erlang_functions:is_binary_string(Identifier) of
		true->
			true;
		_->
			false
	end.

start_interface(MyPlugin,_MyId,ReceptionMap)->
	%{Pid,_Ref} = spawn_monitor(fun()->simulate(MyPlugin,50)end),
	{Pid,_Ref} = spawn_monitor(fun()->do_nothing(MyPlugin,100)end),
	{ok,Pid}.

do_nothing(MyPlugin,Food)->
	receive
		_->
			do_nothing(MyPlugin,Food)
	end.

simulate(MyPlugin,Weight)->
	timer:sleep(60000),
	DataMap = #{"Weight"=>#{<<"Value">>=>Weight}},
	ok = custom_erlang_functions:myGenServCall(MyPlugin,{new_data,<<"964001031059312">>,DataMap}),
	ok = custom_erlang_functions:myGenServCall(MyPlugin,{new_data,<<"964001031058998">>,DataMap}),
	ok = custom_erlang_functions:myGenServCall(MyPlugin,{new_data,<<"964001031059972">>,DataMap}),
	ok = custom_erlang_functions:myGenServCall(MyPlugin,{new_data,<<"964001031061601">>,DataMap}),
	NewWeight = Weight*1.01,
	simulate(MyPlugin,NewWeight).

