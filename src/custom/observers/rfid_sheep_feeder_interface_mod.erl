%% @author Daniel
%% @doc @todo Add description to smart_feeder_interface.


-module(rfid_sheep_feeder_interface_mod).
-define(TOPIC,<<"djvniekerk@sun.ac.za/pi_temp">>).%change to <<"djvniekerk@sun.ac.za/rfid_sheep_feeder">> for real thing. pi_temp just for testing with own raspberry pi
-define(REPLY_TOPIC,<<"djvniekerk@sun.ac.za/bf_ack">>).
-define(BROKER,"broker.hivemq.com").
-define(PORT,1883).
%% ====================================================================
%% API functions
%% ====================================================================
-export([get_gateway_details_and_min_time_between_failures/1,start_interface/3,valid_identifier/2,get_interface_info/1]).


%% ====================================================================
%% Internal functions
%% ====================================================================
get_gateway_details_and_min_time_between_failures(_Id)->%%used by sensor_ep
	{ok,#{<<"Interface type">>=>"MQTT",<<"Broker">>=>?BROKER,<<"TCP Port">>=>?PORT},10000}.
get_interface_info(_Id)->%%used by sensors_serv_prov_mod
	{ok,Topics} = cp_interface_creation:add_observer_topic(#{}, "Eating", 95, "g"),
	{ok,Resources} = cp_interface_creation:add_to_resources(observer, #{}, "Sheep", all, "EID", Topics),
	{ok,Topics2} = cp_interface_creation:add_observer_topic(Topics, "Ambient temperature", 99, "C"),
	{ok,Resources2} = cp_interface_creation:add_to_resources(observer,Resources, "Houses", [<<"RFID Feeder Building">>], "Location", Topics2),
	{"Primary:Measures how much individual sheep eat. Secondary:Measures ambient temperature of building containing rfid feeding sensor",Resources2}.

valid_identifier("EID",Identifier)->
	case custom_erlang_functions:is_binary_string(Identifier) of
		true->
			true;
		_->
			false
	end;
valid_identifier("Location",Identifier)->
	case custom_erlang_functions:is_binary_string(Identifier) of
		true->
			true;
		_->
			false
	end.

start_interface(MyPlugin,_MyId,ReceptionMap)->
	%{Pid,_Ref} = spawn_monitor(fun()->start_emqtt(MyPlugin,ReceptionMap,false)end),
	{Pid,_Ref} = spawn_monitor(fun()->simulate(MyPlugin,100)end),
	{ok,Pid}.
%sloc:30
simulate(MyPlugin,Food)->
	timer:sleep(10000),
	DataMap = #{"Eating"=>#{<<"Date">>=>"2021-08-02",<<"Time">>=>"14:00:05",<<"Value">>=>425,<<"Duration">>=>32}},
	ok = custom_erlang_functions:myGenServCall(MyPlugin,{new_data,<<"9142345389245">>,DataMap}),
	simulate(MyPlugin,Food).
%%Internal
start_emqtt(MyPlugin,ReceptionMap,MessageSent)->
	io:format("\nStarting emqtt"),
	{ok, ConnPid} = emqtt:start_link([{host,?BROKER},{username,"djvniekerk@sun.ac.za"},{password,"BASE_meets_daisy"}]),
	unlink(ConnPid),%do not want this process to fail with ConnPid
	ok = try_to_connect(ConnPid,MyPlugin,ReceptionMap,MessageSent),
	
	SubOpts = [{qos, 1}],
	{ok, _Props, _ReasonCodes} = emqtt:subscribe(ConnPid, #{}, [{<<"djvniekerk@sun.ac.za/pi_temp">>, SubOpts}]),
	receive_data(ConnPid,MyPlugin,base_time:now(),ReceptionMap,MessageSent).
	

try_to_connect(ConnPid,MyPlugin,ReceptionMap,MessageSent)->
	io:format("\nTrying to connect to ~p",[?BROKER]),
	%io:format("\nConnPid alive ~p",[is_process_alive(ConnPid)]),
	case is_process_alive(ConnPid) of
		true->
			Res = emqtt:connect(ConnPid),
			case Res of
				{ok,_Props}->
					io:format("\nConnected"),
					custom_erlang_functions:myGenServCall(MyPlugin, sensor_running),
					ok;
				_->
					io:format("\nRes when trying to connect to ~p: ~p",[?BROKER,Res]),
					timer:sleep(5000),
					io:format("\nGonna restart"),
					try_to_connect(ConnPid,MyPlugin,ReceptionMap,MessageSent)
			end;
		_->
			start_emqtt(MyPlugin,ReceptionMap,MessageSent)
	end.
%60
remove_all_spaces(RawEID)->
	NewS = re:replace(RawEID, "(\\s)", "", [global,{return,list}]),
	list_to_binary(NewS).

receive_data(ConnPid,MyPlugin,LastT,ReceptionMap,MessageSent)->
	%io:format("\nWaiting for packages from maqiatto"),
	receive
	    {disconnect, ReasonCode, Properties} ->
	        io:format("\nNETW ERROR: Recv a DISCONNECT packet from ~p for rfid_feeder - ReasonCode: ~p, Properties: ~p~n", [?BROKER,ReasonCode, Properties]),
			custom_erlang_functions:myGenServCall(MyPlugin, {sensor_down,lists:flatten(io_lib:format("~p", [ReasonCode]))}),
			try_to_connect(ConnPid,MyPlugin,ReceptionMap,MessageSent),
			SubOpts = [{qos, 1}],
			{ok, _Props, _ReasonCodes} = emqtt:subscribe(ConnPid, #{}, [{<<"djvniekerk@sun.ac.za/pi_temp">>, SubOpts}]),
			receive_data(ConnPid,MyPlugin,LastT,ReceptionMap,MessageSent);
					
	    {publish, PUBLISH} ->
			PayLoad = maps:get(payload,PUBLISH),
	        %io:format("\nRecv a PUBLISH payload: ~p~n", [PayLoad]),
			PayLoadMap = jsone:decode(PayLoad,[{object_format, map}]),
			RawEID = maps:get(<<"eid">>,PayLoadMap),
			EID = remove_all_spaces(binary_to_list(RawEID)),
			StartDateAndTime = binary_to_list(maps:get(<<"start_date">>,PayLoadMap)),
			[StartDate|[StartTime]] = string:split(StartDateAndTime, " "),
			StartInt = base_time:date_and_time_strings_to_base_time(StartDate, StartTime),
			EndDateAndTime = binary_to_list(maps:get(<<"end_date">>,PayLoadMap)),
			[EndDate|[EndTime]] = string:split(EndDateAndTime, " "),
			EndInt = base_time:date_and_time_strings_to_base_time(EndDate, EndTime),
			case EndInt of
				LastT->
					FinalT = EndInt+1;	
				_->
					FinalT = EndInt
			end,
			FinalDateAndT = base_time:base_time_to_date_and_time_string(FinalT),
			[FinalDate|[FinalTime]] = string:split(FinalDateAndT, " "),
			Duration = EndInt-StartInt,
			AmountEaten = maps:get(<<"eaten">>,PayLoadMap),
			DataMap = #{"Eating"=>#{<<"Date">>=>FinalDate,<<"Time">>=>FinalTime,<<"Value">>=>AmountEaten,<<"Duration">>=>Duration}},
			Temperature = maps:get(<<"temp">>,PayLoadMap),
			TempDataMap = #{"Ambient temperature"=>#{<<"Date">>=>FinalDate,<<"Time">>=>FinalTime,<<"Value">>=>Temperature}},
			case is_process_alive(MyPlugin) of
				true->
					ok = custom_erlang_functions:myGenServCall(MyPlugin,{new_data,EID,DataMap}),
					ok = custom_erlang_functions:myGenServCall(MyPlugin,{new_data,<<"RFID Feeder Building">>,TempDataMap}),
					ok = emqtt:publish(ConnPid, ?REPLY_TOPIC, #{}, PayLoad, [{qos, 0}]),%to inform sensor that data has been received
					receive_data(ConnPid,MyPlugin,FinalT,ReceptionMap,false);
				_->
					exit(ConnPid,kill)
			end;
	    _->
			io:format("\nEMQTT ERROR:Received unknown from emqtt"),
			receive_data(ConnPid,MyPlugin,LastT,ReceptionMap,MessageSent)
	after 60000->
		case is_process_alive(MyPlugin) of
			true->
				case is_process_alive(ConnPid) of
					true->
						%%check if human holon needs to be informed
						{ok,MaxMinutesWithNoActivity} = atr_api:get_attribute_value(maps:get(atr,ReceptionMap), "Max minutes with no activity"),
						case (base_time:now()>=(LastT+MaxMinutesWithNoActivity*60)) and not(MessageSent) of
							true->
								{ok,Humans} = doha_api:get_bcs_by_types(["Human workers"]),
								try_to_inform_each_human(Humans,maps:get(comms,ReceptionMap),MaxMinutesWithNoActivity),
								NewMessageSent=true;
							_->
								NewMessageSent = MessageSent
						end,
						receive_data(ConnPid,MyPlugin,LastT,ReceptionMap,NewMessageSent);
					_->
						start_emqtt(MyPlugin,ReceptionMap,MessageSent)
				end;
			_->%%if plugin dead kill this process and ConnPid
				case is_process_alive(ConnPid) of
					true->
						exit(ConnPid,kill);
					_->
						ok
				end
		end
	end.

try_to_inform_each_human([],_MyComms,_MaxMinutes)->
	ok;
try_to_inform_each_human([Human|T],MyComms,MaxMinutes)->
	{ok,Adr} = business_cards:get_address(Human),
	comms_api:send_inform(MyComms, Adr, "SMS", "There has not been any activity for "++integer_to_list(MaxMinutes)++" minutes in the RFID Feeders"),
	try_to_inform_each_human(T,MyComms,MaxMinutes).
%%SLOC: 125

