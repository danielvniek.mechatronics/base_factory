
-module(warehouse_def_storage).
-author("Daniel van Niekerk (adapted from original by Dale Sparrow)").

-behaviour(gen_server).

-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([start_link/2]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(FOLDERPATH,"basic_resources/warehouses/").
-record(warehouse_storage_state, {file_path = "",ets_table::term(),my_recep::pid()}).

%%%===================================================================

start_link(RecepPid,ProdType) ->
  {ok,_PID} = gen_server:start_link(?MODULE, [RecepPid,ProdType], []).

%%%===================================================================
%%% gen_server callbacks

init([RecepPid,ProdType]) ->
  {ETS_Table,FilePath} = create_or_load_table(ProdType),
  {ok, #warehouse_storage_state{ets_table = ETS_Table,file_path = FilePath,my_recep = RecepPid}}.
%%--------------------------------------------------------------------

handle_call({save_products, _ProdType, Products}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#warehouse_storage_state.my_recep, From) of
		true->
			Reply = def_storage_functions:save(State#warehouse_storage_state.ets_table,Products,State#warehouse_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"}}
	end;
handle_call({get_del_or_pop_all_products, _ProdType,GetDelOrPop,IncludeEmpty}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#warehouse_storage_state.my_recep, From) of
		true->
			MatchSpecTuple = {warehouse_match_spec,get_matchAll,del_matchAll},
			Reply = def_storage_functions:get_del_or_pop(State#warehouse_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,IncludeEmpty,[],State#warehouse_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"}}
	end;
handle_call({get_del_or_pop_prods_by_ids, _ProdType, GetDelOrPop,IncludeEmpty,IDs}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#warehouse_storage_state.my_recep, From) of
		true->
			MatchSpecTuple = {warehouse_match_spec,get_match10IDs,del_match10IDs},
			Reply = def_storage_functions:get_del_or_pop(State#warehouse_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,{IncludeEmpty,IDs},[],State#warehouse_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"}}
	end;
handle_call({get_del_or_pop_prods_by_groups, _ProdType, GetDelOrPop,IncludeEmpty,Groups}, From, State)->
	case security_functions:from_is_process_or_monitored_by_process(State#warehouse_storage_state.my_recep, From) of
		true->
			MatchSpecTuple = {warehouse_match_spec,get_match10Groups,del_match10Groups},
			Reply = def_storage_functions:get_del_or_pop(State#warehouse_storage_state.ets_table,GetDelOrPop,MatchSpecTuple,{IncludeEmpty,Groups},[],State#warehouse_storage_state.file_path),
			{reply,Reply,State};
		_->
			{reply,{error,"Only my reception can make this call"}}
	end;



handle_call(_What,_From,State)->
  {reply,not_understood,State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(WHAT,State) ->
  error_log:log(?MODULE,0,unknown,"~n # Warehouse def storage got unknown: ~p ~n",[WHAT]),
  {noreply, State}.
terminate(_Reason, _State) ->
  ok.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

create_or_load_table(ProdType)->
	FileName = string:concat(ProdType,".txt"),
	FilePath = string:concat(?FOLDERPATH,FileName),
	TableNameS = string:concat(string:lowercase(ProdType),"_warehouse_table"),
	TableName = erlang:list_to_atom(TableNameS),
	ETS_table = def_storage_functions:claim_or_create_ets(TableName,FilePath),
	{ETS_table,FilePath}.