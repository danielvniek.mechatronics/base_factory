%% @author Daniel
%% @doc @todo Add description to ets_match_spec.


-module(warehouse_match_spec).
-include_lib("stdlib/include/ms_transform.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_match10IDs/1,get_match10Groups/1,get_matchAll/1,del_match10IDs/1,del_match10Groups/1,del_matchAll/1]).
-record(product,{id,type,group,suppliers,production_date,delivery_date,expiration_date,promised_content,analysed_content,usage,amount_used,amount_left,unit,location_log,temperature_log,empty}).


%% ====================================================================
%% Internal functions
%% ====================================================================
get_matchAll(IncludeEmpty)->
	case IncludeEmpty of
		true->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{})
		      ->
		      A
		    end);
		_->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{empty = false})
		      ->
		      A
		    end)
	end,
	{MatchSpec,[]}.
get_match10IDs({IncludeEmpty,IDs})->
	L = length(IDs),
	case L>=10 of
		true->
			error_log:log(?MODULE,0,unknown,"\nERROR: The product warehouse cannot search for more than 10 product ids at a time"),
			List = lists:sublist(IDs, 1, 10);
		_->
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, IDs)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(IDs, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	case IncludeEmpty of
		true->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{id = ID})
		      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
		      A
		    end);
		_->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{id = ID,empty=false})
		      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
		      A
		    end)
	end,
	
	{MatchSpec,[]}.
get_match10Groups({IncludeEmpty,Types})->
	L = length(Types),
	case L>=10 of
		true->
			error_log:log(?MODULE,0,unknown,"\nERROR: The product warehouse cannot search for more than 10 product groups at a time"),
			List = lists:sublist(Types, 1, 10);
		_->
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, Types)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	case IncludeEmpty of
		true->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{group = Type})
		      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
		      A
		    end),
			{MatchSpec,[]};
		_->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{group = Type,empty=false})
		      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
		      A
		    end),
			{MatchSpec,[]}
	end.


del_matchAll(IncludeEmpty)->
	case IncludeEmpty of
		true->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{})
		      ->
		      true
		    end);
		_->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{empty = false})
		      ->
		      true
		    end)
	end,
	{MatchSpec,[]}.
del_match10IDs({IncludeEmpty,IDs})->
	L = length(IDs),
	case L>=10 of
		true->
			error_log:log(?MODULE,0,unknown,"\nERROR: The product warehouse cannot search for more than 10 product ids at a time"),
			List = lists:sublist(IDs, 1, 10);
		_->
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, IDs)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(IDs, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	case IncludeEmpty of
		true->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{id = ID})
		      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
		      true
		    end);
		_->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{id = ID,empty=false})
		      when ID ==E1 orelse ID==E2 orelse ID==E3 orelse ID==E4 orelse ID==E5 orelse ID==E6 orelse ID==E7 orelse ID==E8 orelse ID==E9 orelse ID==E10->
		      true
		    end)
	end,
	
	{MatchSpec,[]}.
del_match10Groups({IncludeEmpty,Types})->
	L = length(Types),
	case L>=10 of
		true->
			error_log:log(?MODULE,0,unknown,"\nERROR: The product warehouse cannot search for more than 10 product groups at a time"),
			List = lists:sublist(Types, 1, 10);
		_->
			case L==0 of
				true->
					Element1 = none;
				_->
					Element1 = lists:nth(1, Types)
			end,
			AddOnList = [Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1,Element1],
			OverExageratedList = lists:append(Types, AddOnList),
			List = lists:sublist(OverExageratedList, 1, 10)
	end,
	E1 = lists:nth(1,List),
	E2 = lists:nth(2,List),
	E3 = lists:nth(3,List),
	E4 = lists:nth(4,List),
	E5 = lists:nth(5,List),
	E6 = lists:nth(6,List),
	E7 = lists:nth(7,List),
	E8 = lists:nth(8,List),
	E9 = lists:nth(9,List),
	E10 = lists:nth(10,List),
	case IncludeEmpty of
		true->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{group = Type})
		      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
		      true
		    end),
			{MatchSpec,[]};
		_->
			MatchSpec = ets:fun2ms(
		    fun(A = #product{group = Type,empty=false})
		      when Type ==E1 orelse Type==E2 orelse Type==E3 orelse Type==E4 orelse Type==E5 orelse Type==E6 orelse Type==E7 orelse Type==E8 orelse Type==E9 orelse Type==E10->
		      true
		    end),
			{MatchSpec,[]}
	end.

