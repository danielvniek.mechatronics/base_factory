-module(warehouses).
-author("Daniel van Niekerk (adapted from the original by Dale Sparrow)").

-behaviour(gen_server).

%% ATTRIBUTES API

-export([start_link/4,
  stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).
-define(SERVTYPE,"Store products").


-record(warehouse_state, {
					default_storage_pid::pid(),storage_service_address::default|pid(),instance_sup::pid(),bc,prod_type,unit,
					atr,basic_bio,registered_adrs
					}).				
-record(product,{id,type,group,suppliers,production_date,delivery_date,expiration_date,promised_content,analysed_content,usage,amount_used,amount_left,unit,location_log,temperature_log,empty}).
%%%===================================================================

start_link(BC,InstanceSup,CustomArgs,FactoryReady) ->
	{ok,ID} = business_cards:get_id(BC),
	AtomName = erlang:list_to_atom(string:lowercase(string:replace(ID, " ", "_",all))),
	
  	{ok,PID} = gen_server:start_link({local,AtomName},?MODULE, [BC,InstanceSup,CustomArgs,FactoryReady], []),
  	{ok,PID}.

stop() ->
  gen_server:stop(self()).

init([BC,InstanceSup,CustomArgs,FactoryReady]) ->
	case FactoryReady of
		true->
			CustomArgsMap = jsone:decode(CustomArgs,[{object_format, map}]),
			ProdType = maps:get(<<"Product type">>,CustomArgsMap),
			Unit = maps:get(<<"Unit">>,CustomArgsMap),
			ServiceDetails = CustomArgsMap,
			Services = #{?SERVTYPE=>ServiceDetails},
			NewBC = maps:update(<<"services">>, Services, BC);
		_->
			NewBC = BC,
			{ok,Services} = business_cards:get_services(BC),
			ServiceDetails = maps:get(?SERVTYPE,Services),
			ProdType = maps:get(<<"Product type">>,ServiceDetails),
			Unit = maps:get(<<"Unit">>,ServiceDetails)			
	end, 	
	{ok,Pid} = warehouse_def_storage:start_link(self(),binary_to_list(ProdType)),
	spawn_monitor(fun()->doha_api:register(NewBC)end),
	{ok,Atr1} = attribute_functions:create_attribute("Status", "Warehouse attributes", "Context", "Active"),
	{ok,Atr2} = attribute_functions:create_attribute("Product type", "Warehouse attributes", "Context", ProdType),
	{ok,Atr3} = attribute_functions:create_attribute("Unit", "Warehouse attributes", "Context", Unit),
	AtrMap = #{"Status"=>Atr1,"Product type"=>Atr2,"Unit"=>Atr3},
	{ok,ID} = business_cards:get_id(BC),	
	spawn_monitor(fun()->doha_api:ready(ID)end),
    {ok, #warehouse_state{
					storage_service_address = default,
						  default_storage_pid = Pid,instance_sup=InstanceSup,bc=NewBC,prod_type=ProdType,unit=Unit,
					atr=AtrMap,basic_bio=#{},
					registered_adrs = []}}. 

%-----------------------------------------------------------------

%This call is used to change storage method of an instance. For example using some cloud storage service
handle_call({change_storage, NewStoragePid},From,State)->%%TODO: Give resource id of storage service and let this resource figure out via doha where the service is. Do this with core receptions as well
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
			case erlang:is_pid(NewStoragePid) of
				true->
					NewState = State#warehouse_state{storage_service_address = NewStoragePid},
					Reply = ok;
				_->
					NewState = State,
					Reply = {error, "Invalid NewStoragePid"}
			end,
			{reply, Reply, NewState};
		_->
			{reply,{error,"Only this instance's processes can make this call"},State}
	end;

handle_call({request_info,"Product details",RequestDetails},_From,State)->	
	ProdId = maps:get(<<"Product id">>,RequestDetails),
	{ok,Prods} = storage_request({get_del_or_pop_prods_by_ids,State#warehouse_state.prod_type,"get",true,[ProdId]},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address),
	case Prods of
		[Prod]->
			BinProd = build_product_map(Prod),
			{reply,{ok,BinProd},State};
		_->
			{reply,{error,"No product with this id"},State}
	end;
handle_call({request_service,Contract},From,State)->
	{Pid,_Ref} = From,
	case contracts:valid_contract(Contract) of
		true->
			{ok,ServiceType} = contracts:get_service_type(Contract),	
			{ok,RequestArgs} = contracts:get_request_arguments(Contract),		
			case ServiceType == ?SERVTYPE of
				true->
					Request = maps:get(<<"request">>,RequestArgs),
					case Request of
						<<"new_product">>->
							case ui_state:allow(2,Pid) of
								allow->
									ProdId = maps:get(<<"id">>,RequestArgs),
									Type = State#warehouse_state.prod_type,
									Unit = State#warehouse_state.unit,
									Group = maps:get(<<"group">>,RequestArgs),
									Suppliers = maps:get(<<"suppliers">>,RequestArgs),
									ProductionDate = maps:get(<<"production_date">>,RequestArgs),
									DeliveryDate = maps:get(<<"delivery_date">>,RequestArgs),
									ExpirationDate = maps:get(<<"expiration_date">>,RequestArgs),
									PromisedContentRaw = maps:get(<<"promised_content">>,RequestArgs),
									case is_map(PromisedContentRaw) of
										true->
											PromisedContent = PromisedContentRaw;
										_->
											PromisedContent = jsone:decode(PromisedContentRaw,[{object_format, map}])
									end,
									AnalysedContentRaw = maps:get(<<"analysed_content">>,RequestArgs),
									case is_map(AnalysedContentRaw) of
										true->
											AnalysedContent = AnalysedContentRaw;
										_->
											AnalysedContent = jsone:decode(AnalysedContentRaw,[{object_format, map}])
									end,
									AmountUsed = maps:get(<<"amount_used">>,RequestArgs),%%should be 0
									AmountLeft = maps:get(<<"amount_left">>,RequestArgs),
									LocationLogRaw = maps:get(<<"location_log">>,RequestArgs),
									case is_map(LocationLogRaw) of
										true->
											LocationLog = LocationLogRaw;
										_->
											LocationLog = jsone:decode(LocationLogRaw,[{object_format, map}])
									end,
									TemperatureLogRaw = maps:get(<<"temperature_log">>,RequestArgs),
									case is_map(TemperatureLogRaw) of
										true->
											TemperatureLog = TemperatureLogRaw;
										_->
											TemperatureLog = jsone:decode(TemperatureLogRaw,[{object_format, map}])
									end,
									Unit = State#warehouse_state.unit,
									Empty = maps:get(<<"empty">>,RequestArgs),
									Product = #product{id = ProdId,type=Type,unit=Unit,group=Group,suppliers=Suppliers,production_date=ProductionDate,delivery_date=DeliveryDate,expiration_date=ExpirationDate,promised_content=PromisedContent,analysed_content=AnalysedContent,usage=#{},amount_used=AmountUsed,amount_left=AmountLeft,location_log=LocationLog,temperature_log=TemperatureLog,empty=Empty},
									{ok,Existing} = storage_request({get_del_or_pop_prods_by_ids,State#warehouse_state.prod_type,"get",true,[ProdId]},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address),
									NewState = State,
									case (length(Existing)>0) of
										true->
											Res = {error,"A product with this id already exists"};
										_->
											spawn_monitor(fun()->share_with_all_clients(State#warehouse_state.registered_adrs,Product)end),
											Res=storage_request({save_products,Type,[Product]},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address)
									end;
								ReplyMap->
									NewState = State,
									Res = {not_allowed,ReplyMap}
							end;
							
						<<"get_del_or_pop_prods_by_ids">>->
							IDs = maps:get(<<"ids">>,RequestArgs),
							GetDelOrPop = binary_to_list(maps:get(<<"get_del_or_pop">>,RequestArgs)),
							IncludeEmpty = maps:get(<<"include_empty">>,RequestArgs),
							NewState = State,
							case GetDelOrPop of
								"get"->
									case ui_state:allow(2,Pid) of
										allow->
											Res = storage_request({get_del_or_pop_prods_by_ids,State#warehouse_state.prod_type,GetDelOrPop,IncludeEmpty,IDs},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address);
										ReplyMap->
											Res = {not_allowed,ReplyMap}
									end;
								_->
									case ui_state:allow(2,Pid) of
										allow->
											Res = storage_request({get_del_or_pop_prods_by_ids,State#warehouse_state.prod_type,GetDelOrPop,IncludeEmpty,IDs},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address);
										ReplyMap->
											Res = {not_allowed,ReplyMap}
									end
							end;
										
										
						<<"get_del_or_pop_prods_by_groups">>->
							Groups = maps:get(<<"groups">>,RequestArgs),
							GetDelOrPop = binary_to_list(maps:get(<<"get_del_or_pop">>,RequestArgs)),
							IncludeEmpty = maps:get(<<"include_empty">>,RequestArgs),
							NewState = State,
							case GetDelOrPop of
								"get"->
									case ui_state:allow(3,Pid) of
										allow->
											Res = storage_request({get_del_or_pop_prods_by_groups,State#warehouse_state.prod_type,GetDelOrPop,IncludeEmpty,Groups},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address);
										ReplyMap->
											Res = {not_allowed,ReplyMap}
									end;
								_->
									case ui_state:allow(2,Pid) of
										allow->
											Res = storage_request({get_del_or_pop_prods_by_groups,State#warehouse_state.prod_type,GetDelOrPop,IncludeEmpty,Groups},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address);
										ReplyMap->
											Res = {not_allowed,ReplyMap}
									end
							end;
							
						<<"get_del_or_pop_all_products">>->
							GetDelOrPop = binary_to_list(maps:get(<<"get_del_or_pop">>,RequestArgs)),
							IncludeEmpty = maps:get(<<"include_empty">>,RequestArgs),
							NewState = State,
							case GetDelOrPop of
								"get"->
									case ui_state:allow(3,Pid) of
										allow->
											Res = storage_request({get_del_or_pop_all_products,State#warehouse_state.prod_type,GetDelOrPop,IncludeEmpty},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address);
										ReplyMap->
											Res = {not_allowed,ReplyMap}
									end;
								_->
									case ui_state:allow(2,Pid) of
										allow->
											Res = storage_request({get_del_or_pop_all_products,State#warehouse_state.prod_type,GetDelOrPop,IncludeEmpty},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address);
										ReplyMap->
											Res = {not_allowed,ReplyMap}
									end
							end;
							
						<<"use">>->
							case ui_state:allow(2,Pid) of
								allow->
									Id = maps:get(<<"id">>,RequestArgs),
									{ok,Prods} = storage_request({get_del_or_pop_prods_by_ids,State#warehouse_state.prod_type,"get",true,[Id]},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address),
									case Prods of
										[]->
											NewState = State,
											Res = {error,"No non-empty product with this id"};
										_->
											Prod = lists:nth(1, Prods),
											case (Prod#product.empty) of
												true->
													NewState = State,
													Res = {error,"This product is empty. Please recheck product id. If this product is not empty the previous used made a mistake and the entire warehouse must be rechecked and its digital twin must be updated"};
												_->
													Amount = maps:get(<<"amount">>,RequestArgs),
													case maps:is_key(<<"date_and_time">>, RequestArgs) of
														true->
															DateAndTime = maps:get(<<"date_and_time">>,RequestArgs);
														_->
															DateAndTime = list_to_binary(base_time:base_time_to_date_and_time_string(base_time:now()))
													end,
													case Amount of
														<<"all">>->
															UsedAmount = Prod#product.amount_left,
															AmountLeft = 0,
															
															Empty = true;
														_->
															UsedAmount = Amount,
															AmountLeft = Prod#product.amount_left-UsedAmount,
															Empty = maps:get(<<"empty">>,RequestArgs)
													end,
													AmountUsed = Prod#product.amount_used+UsedAmount,
													Handlers = maps:get(<<"handlers">>,RequestArgs),
													Receivers = maps:get(<<"receivers">>,RequestArgs),
													
													case maps:is_key(DateAndTime, Prod#product.usage) of
														true->
															UsageEntry = maps:get(DateAndTime,Prod#product.usage),
															NewAmount = maps:get(<<"amount">>,UsageEntry)+UsedAmount,
															NewHandlers = lists:append(Handlers, maps:get(<<"handlers">>,UsageEntry)),
															NewReceivers = lists:append(Receivers, maps:get(<<"receivers">>,UsageEntry)),
															NewUsageEntry =#{<<"amount">>=>NewAmount,<<"handlers">>=>NewHandlers,<<"receivers">>=>NewReceivers};
														_->
															NewUsageEntry = #{<<"amount">>=>UsedAmount,<<"handlers">>=>Handlers,<<"receivers">>=>Receivers}%%handlers and receivers will be binary
													end,
													NewUsage = maps:put(DateAndTime, NewUsageEntry, Prod#product.usage),
													NewProd = Prod#product{empty=Empty,amount_left=AmountLeft,amount_used=AmountUsed,usage=NewUsage},
													NewState = State,
													Res=storage_request({save_products,State#warehouse_state.prod_type,[NewProd]},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address)
											end
									end;
								ReplyMap->
									NewState = State,
									Res = {not_allowed,ReplyMap}
							end;
						<<"edit">>->
							case ui_state:allow(2, Pid) of
								true->
									Id = maps:get(<<"id">>,RequestArgs),
									{ok,Prods} = storage_request({get_del_or_pop_prods_by_ids,State#warehouse_state.prod_type,"get",true,[Id]},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address),
									case Prods of
										[]->
											NewState = State,
											Res = {error,"No product with this id"};
										_->
											Prod = lists:nth(1, Prods),
											Type = State#warehouse_state.prod_type,
											Unit = State#warehouse_state.unit,
											Group = maps:get(<<"group">>,RequestArgs),
											Suppliers = maps:get(<<"suppliers">>,RequestArgs),
											ProductionDate = maps:get(<<"production_date">>,RequestArgs),
											DeliveryDate = maps:get(<<"delivery_date">>,RequestArgs),
											ExpirationDate = maps:get(<<"expiration_date">>,RequestArgs),
											PromisedContentRaw = maps:get(<<"promised_content">>,RequestArgs),
											case is_map(PromisedContentRaw) of
												true->
													PromisedContent = PromisedContentRaw;
												_->
													PromisedContent = jsone:decode(PromisedContentRaw,[{object_format, map}])
											end,
											AnalysedContentRaw = maps:get(<<"analysed_content">>,RequestArgs),
											case is_map(AnalysedContentRaw) of
												true->
													AnalysedContent = AnalysedContentRaw;
												_->
													AnalysedContent = jsone:decode(AnalysedContentRaw,[{object_format, map}])
											end,
											AmountUsed = maps:get(<<"amount_used">>,RequestArgs),%%should be 0
											AmountLeft = maps:get(<<"amount_left">>,RequestArgs),
											LocationLog = Prod#product.location_log,%%keep old location and temp logs
											TemperatureLog = Prod#product.temperature_log,
											Unit = State#warehouse_state.unit,
											Empty = maps:get(<<"empty">>,RequestArgs),
											Product = #product{id = Id,type=Type,unit=Unit,group=Group,suppliers=Suppliers,production_date=ProductionDate,delivery_date=DeliveryDate,expiration_date=ExpirationDate,promised_content=PromisedContent,analysed_content=AnalysedContent,usage=#{},amount_used=AmountUsed,amount_left=AmountLeft,location_log=LocationLog,temperature_log=TemperatureLog,empty=Empty},
											spawn_monitor(fun()->share_with_all_clients(State#warehouse_state.registered_adrs,Product)end),
											NewState = State,
											Res=storage_request({save_products,Type,[Product]},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address)
											
									
									end;
								ReplyMap->
									NewState = State,
									Res = {not_allowed,ReplyMap}
							end;
						<<"register_for_product_info">>->%%not used by front end
							%error_log:log(?MODULE,0,unknown,"\nWH received register call"),
							ProdType = maps:get(<<"type">>,RequestArgs),
							case is_binary(ProdType) of
								true->
									case ProdType==State#warehouse_state.prod_type of
										true->
											CurrentClientList = State#warehouse_state.registered_adrs,
											case lists:member(Contract, CurrentClientList) of
												true->
													NewRegisteredAdrs = CurrentClientList;
												_->
													NewRegisteredAdrs = [Contract|CurrentClientList]
											end,
											Res = storage_request({get_del_or_pop_all_products,State#warehouse_state.prod_type,"get",true},State#warehouse_state.default_storage_pid,State#warehouse_state.storage_service_address),
											NewState = State#warehouse_state{registered_adrs = NewRegisteredAdrs};
										_->
											NewState = State,
											Res = {error,"This warehouse does not store products of this type"}
									end;
								_->
									NewState = State,
									Res = {error,"ProdType must be a binary string"}
							end;
						_->
							NewState = State,
							Res = {error,"Invalid request"}
					end,
					{reply,Res,NewState};
				_->
					{reply,{error,"Unrecognized service type"},State}
			end;
		_->
			{reply,{error,"Invalid contract"},State}
	end;
handle_call({cancel_service,Contract,ReplyTo},_From,State)->%%TODO: add security check
	%error_log:log(?MODULE,0,unknown,"\nSensor's service being stopped"),
	case contracts:valid_contract(Contract) of
		true->
			{ok,ServiceType} = contracts:get_service_type(Contract),
			case ServiceType == ?SERVTYPE of
				true->
					{ok,RequestArgs} = contracts:get_request_arguments(Contract),
					case is_map(RequestArgs) andalso maps:is_key(<<"request">>, RequestArgs) of
						true->
							Request = maps:get(<<"request">>,RequestArgs),
							case Request of
								<<"register_for_product_info">>->
									CurrentClientList = State#warehouse_state.registered_adrs,
									NewClientList = lists:delete(Contract, CurrentClientList),
									Res = ok,
									custom_erlang_functions:gen_serv_reply_if_from(ReplyTo,Res),
									{reply,ok,State#warehouse_state{registered_adrs=NewClientList}};
								_->
									Res = {error,"Invalid request in contract"},
									custom_erlang_functions:gen_serv_reply_if_from(ReplyTo,Res),
									{reply,Res,State}
							end;
						_->
							Res = {error,"Request not map or does not have expected keys"},
							custom_erlang_functions:gen_serv_reply_if_from(ReplyTo,Res),
							{reply,Res,State}
					end;
				_->
					Res = {error,"Unrecognized service type"},
					custom_erlang_functions:gen_serv_reply_if_from(ReplyTo,Res),
					{reply,Res,State}
			end;
		_->
			Res = {error,"Invalid contract"},
			custom_erlang_functions:gen_serv_reply_if_from(ReplyTo,Res),
			{reply,Res,State}
	end;
handle_call(get_bc,From,State)->
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
			{reply,{ok,State#warehouse_state.bc},State};
		_->
			{reply,{error,"Only the management ui can make this call"},State}
	end;
handle_call(get_resource_info,From,State)->%%only management ui backend process can make this call
	UIClients = ui_state:get_clients(),
	case security_functions:from_is_in_process_list_or_monitored_by_a_process_in_process_list(UIClients, From) of
		true->
			spawn_monitor(fun()->get_resource_info(State,From) end),
			{noreply,State};
		_->
			{reply,{error,"Only the management ui can make this call"},State}
	end;

handle_call(get_instance_sup,_From,State)->
	{reply,State#warehouse_state.instance_sup,State};
handle_call(_Request, _From, State) ->
	error_log:log(?MODULE,0,unknown,"Warehouse_recep:unknown request"),
  {reply, unknown, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

storage_request(Request,DefaultPid,StorageServAdr)->
	case StorageServAdr of
		default->
			custom_erlang_functions:myGenServCall(DefaultPid, Request);
		_->
			custom_erlang_functions:myGenServCall(StorageServAdr, Request)
	end.
get_resource_info(State,From)->%%CONTINUE:include time in bio and send only last weeks bio data
	BC = State#warehouse_state.bc,
	{ok,ID} = business_cards:get_id(BC),
	{ok,Services} = business_cards:get_services(BC),
	{ok,Type} = business_cards:get_type(BC),
	{ok,Address} = business_cards:get_address(BC),
	ReplyMap = #{basic_bio=>State#warehouse_state.basic_bio,atr=>maps:values(State#warehouse_state.atr),services=>Services,id=>ID,type=>Type,address=>Address},
	gen_server:reply(From,{ok,ReplyMap}).

share_with_all_clients([],_Product)->
	ok;
share_with_all_clients([Contract|T],Product)->
	contracts:deliver_package(Contract, Product),
	share_with_all_clients(T,Product).

build_product_map(Prod)->
	ProdBinMap = #{<<"type">>=>Prod#product.type,<<"group">>=>Prod#product.group,<<"suppliers">>=>Prod#product.suppliers,<<"production_date">>=>Prod#product.production_date,<<"delivery_date">>=>Prod#product.delivery_date,<<"expiration_date">>=>Prod#product.expiration_date,<<"promised_content">>=>Prod#product.promised_content,<<"analysed_content">>=>Prod#product.analysed_content,<<"usage">>=>Prod#product.usage,<<"amount_used">>=>Prod#product.amount_used,<<"amount_left">>=>Prod#product.amount_left,<<"unit">>=>Prod#product.unit,<<"location_log">>=>Prod#product.location_log,<<"temperature_log">>=>Prod#product.temperature_log,<<"empty">>=>Prod#product.empty},
	ProdBinMap.