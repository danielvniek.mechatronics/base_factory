%% @author Daniel
%% @doc @todo Add description to house_serv_prov_mod.


-module(houses_serv_prov_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_services/0,handle_request/3,handle_execution_info/4,start/4,continue/4,service_cancelled/2]).



-record(stage2Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term()}).

-record(execution_info,{tstart::integer(),s2data::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_services()->
	#{"Receive sheep"=>{"Allow sheep to join this house if there is space and the house is not occupied by another group",0},"Receive group"=>{"Allow group to join this house if the house is not occupied by another group and the number of sheep in the new group is not more than the house's limit",0}}.

handle_request(ReceptionMap,ServType,Contract)->
	AtrRecep = maps:get(atr,ReceptionMap),
	{ok,GroupId} = atr_api:get_attribute_value(AtrRecep, "Group"),
	{ok,IncomingGroup} = atr_api:get_attribute_value(AtrRecep, "Incoming group"),
	{ok,MySheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
	case ServType of
		"Receive sheep"->
			case (GroupId=="none") and (IncomingGroup=="none") of
				true->
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,ClientType} = business_cards:get_type(ClientBC),
					{ok,ClientId} = business_cards:get_id(ClientBC),
					case ClientType of
						"Sheep"->
							case lists:member(ClientId,MySheep) of
								true->
									{{reject,"You are already in this house"},0,0,0};
								_->
									{ok,NrSheep} = atr_api:get_attribute_value(AtrRecep, "Number of sheep"),
									{ok,NrIncoming} = atr_api:get_attribute_value(AtrRecep, "Number of incoming sheep"),
									{ok,SheepLimit} = atr_api:get_attribute_value(AtrRecep, "Sheep limit"),
									case ((NrSheep+NrIncoming)<SheepLimit) of
										true->
											F = fun(Val)->Val+1 end,
											ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of incoming sheep", F),
											{accept,[base_time:now()-1],#{<<"Sheep id">>=>ClientId},false};
										_->
											{{reject,"No space left"},0,0,0}
									end
							end;
						_->
							{{reject,"You are of the wrong resource type"},0,0,0}
					end;
				_->
					{{reject,"Occupied by group"},0,0,0}
			end;
		"Receive group"->
			case (GroupId=="none") and (IncomingGroup=="none") of
				true->
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,ClientType} = business_cards:get_type(ClientBC),
					{ok,ClientId} = business_cards:get_id(ClientBC),
					{ok,ClientAdr} = business_cards:get_address(ClientBC),
					case ClientType of
						"Groups"->
							{ok,NrIncoming} = atr_api:get_attribute_value(AtrRecep, "Number of incoming sheep"),
							case (MySheep==[]) and (NrIncoming==0) of
								true->
									
									{ok,GroupAtrMap} = comms_api:send_request_info(maps:get(comms,ReceptionMap), ClientAdr, "Attributes", ["Number of sheep"]),
									GroupNrSheep = maps:get("Number of sheep",GroupAtrMap),
									{ok,SheepLimit} = atr_api:get_attribute_value(AtrRecep, "Sheep limit"),
									case GroupNrSheep>SheepLimit of
										true->
											{{reject,"You have more sheep than I can handle"},0,0,0};
										_->
											F = fun(_Val)->ClientId end,
											ok = atr_api:apply_function_to_atr_val(AtrRecep, "Incoming group", F),
											{accept,[base_time:now()-1],#{<<"Sheep id">>=>ClientId},false}
									end;
								_->
									{{reject,"House already occupied by sheep without a group"},0,0,0}
							end;
						_->
							{{reject,"You are of the wrong resource type. Expecting groups"},0,0,0}
					end;	
				_->
					{{reject,"Occupied by group"},0,0,0}
			end;							
		_->					
			{{reject,"Unrecognized service type"},0,0,0}
	end.

start(S2Act,_AHPid,_ReceptionMap,_MyPid)->
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Receive sheep (SPA)"->%%SPA stands for service provision activity
			{false,#{<<"Result">>=>"waiting"}};
		"Receive group (SPA)"->%%SPA stands for service provision activity
			{false,#{<<"Result">>=>"waiting"}};
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized activity type to start"}}
	end.

handle_execution_info(S2Act,_AHPid,_ReceptionMap,Info)->
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Receive sheep (SPA)"->
			case Info of
				done->		
					{accept,true,#{<<"Result">>=>"Success"}};
				_->
					{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized info"}}
			end;
		"Receive group (SPA)"->
			case Info of
				done->	
					{accept,true,#{<<"Result">>=>"Success"}};
				_->
					{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized info"}}
			end;
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized service type"}}
	end.
							
continue(S2Act,_AHPid,_ReceptionMap,_MyPid)->%%TODO: ensure sheep is still coming to this house after restart
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Receive sheep (SPA)"->%%SPA stands for service provision activity
			{false,#{<<"Result">>=>"waiting"}};
		"Receive group (SPA)"->%%SPA stands for service provision activity
			{false,#{<<"Result">>=>"waiting"}};
		_->
			{true,#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized activity type to start"}}
	end.

service_cancelled(S2Act,_ReceptionMap)->
	%error_log:log(?MODULE,0,unknown,"\nHouse received service cancelled"),
	ActType = S2Act#stage2Activity.type,
	case ActType of
		"Receive sheep (SPA)"->%%SPA stands for service provision activity
			S2Act#stage2Activity.execution_info#execution_info.s2data;
		"Receive group (SPA)"->%%SPA stands for service provision activity
			S2Act#stage2Activity.execution_info#execution_info.s2data;
		_->
			#{<<"Result">>=>"Failed",<<"Reason">>=>"unrecognized activity type to cancel"}
	end.
%%SLOC:124
	
