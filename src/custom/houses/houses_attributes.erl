%% @author Daniel
%% @doc @todo Add description to houses_attributes.


-module(houses_attributes).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get/0]).

%% ====================================================================
%% Internal functions
%% ====================================================================
get()->
	{ok,P1} = attribute_functions:create_attribute("Size", "Personal", "LxW (m)", "0x0m"),
	{ok,P2} = attribute_functions:create_attribute("Floor material", "Personal", "Description", "pending"),
	{ok,P3} = attribute_functions:create_attribute("Roof", "Personal", "true/false", "pending"),
	{ok,P4} = attribute_functions:create_attribute("Solid walls", "Personal", "true/false", "pending"),
	{ok,P5} = attribute_functions:create_attribute("Location", "Personal", "A GPS coordinate or description", <<"RFID Feeder Building">>),
	{ok,P6} = attribute_functions:create_attribute("EID", "Personal", "Electronic ID of the tag attached to this resource", "none"),
	{ok,Photos} = attribute_functions:create_attribute("Photos", "Personal", "GD links or actual photos?", []),
	
	{ok,M1} = attribute_functions:create_attribute("Sheep limit", "Management", "integer", 50),
	
	{ok,R1} = attribute_functions:create_attribute("Sensors", "Relational", "Base ID", []),
	{ok,R2} = attribute_functions:create_attribute("Feeders", "Relational", "Base ID", []),
	{ok,R3} = attribute_functions:create_attribute("Sheep", "Relational", "Base ID", []),
	{ok,R4} = attribute_functions:create_attribute("Group", "Relational", "Base ID", "none"),
	{ok,R5} = attribute_functions:create_attribute("Number of sheep", "Relational", "integer", 0),
	
	{ok,S1} = attribute_functions:create_attribute("Number of incoming sheep", "State variables", "execution", 0),
	{ok,S2} = attribute_functions:create_attribute("Incoming group", "State variables", "execution", "none"),
	[P1,P2,P3,P4,P5,P6,M1,R1,R2,R3,R4,R5,S1,S2,Photos].

