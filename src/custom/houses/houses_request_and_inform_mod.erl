%% @author Daniel
%% @doc @todo Add description to houses_request_and_inform_mod.


-module(houses_request_and_inform_mod).

%% ====================================================================
%% API functions
%% ====================================================================
-export([inform/4]).



%% ====================================================================
%% Internal functions
%% ====================================================================


inform(ReceptionMap,InformType,InformDetails,RequesterCommsRecep)->
	AtrRecep = maps:get(atr,ReceptionMap),
	ExeRecep = maps:get(exe,ReceptionMap),
	case InformType of
		"Sheep left"->
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			{ok,GroupVal} = atr_api:get_attribute_value(AtrRecep, "Group"),
			case ((GroupVal=="none") and (InformDetails == ReqId)) or (GroupVal==ReqId) of
				true->
					{ok,MySheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
					case lists:member(InformDetails,MySheep) of
						true->
							exe_api:start_and_finish_new_act(ExeRecep, "Remove sheep", #{<<"Sheep id">>=>InformDetails}, #{<<"Result">>=>"Success"}),
							ok;
						_->
							{error,"No sheep with this id in this house"}
					end;
				_->
					{error,"You are not allowed to inform me about this"}
			end;
		"Group left"->
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			{ok,GroupVal} = atr_api:get_attribute_value(AtrRecep, "Group"),
			case (GroupVal==ReqId) of
				true->
					exe_api:start_and_finish_new_act(ExeRecep, "Remove group", #{<<"Group id">>=>InformDetails}, #{<<"Result">>=>"Success"}),
					ok;
				_->
					{error,"You are not allowed to inform me about this"}
			end;
		"Feeder left"->
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			case (InformDetails == ReqId) of
				true->
					{ok,MyFeeders} = atr_api:get_attribute_value(AtrRecep, "Feeders"),
					case lists:member(InformDetails,MyFeeders) of
						true->
							exe_api:start_and_finish_new_act(ExeRecep, "Remove feeder", #{<<"Feeder id">>=>InformDetails}, #{<<"Result">>=>"Success"}),
							ok;
						_->
							{error,"No feeder with this id in this house"}
					end;
				_->
					{error,"You are not allowed to inform me about this"}
			end;
		"New feeder"->
			%error_log:log(?MODULE,0,unknown,"\new feeder"),
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			case (InformDetails == ReqId) of
				true->
					exe_api:start_and_finish_new_act(ExeRecep, "Add feeder", #{<<"Feeder id">>=>InformDetails}, #{<<"Result">>=>"Success"}),
					ok;
				_->
					{error,"You are not allowed to inform me about this"}
			end;
		"Incoming sheep"->
			{ok,Group} = atr_api:get_attribute_value(AtrRecep, "Group"),
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			case (Group==ReqId) of
				true->
					F = fun(Val)->Val+1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of incoming sheep", F),
					ok;
				_->
					{error,"You are not allowed to inform me about this"}
			end;
		"Sheep received"->
			{ok,Group} = atr_api:get_attribute_value(AtrRecep, "Group"),
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			case (Group==ReqId) of
				true->
					exe_api:start_and_finish_new_act(ExeRecep, "Add sheep", #{<<"Sheep id">>=>InformDetails,<<"Group id">>=>Group}, #{<<"Result">>=>"Success"}),
					ok;
				_->
					{error,"You are not allowed to inform me about this"}
			end;
		"Sheep cancelled"->
			{ok,Group} = atr_api:get_attribute_value(AtrRecep, "Group"),
			{ok,ReqBC} = comms_api:get_bc(RequesterCommsRecep),
			{ok,ReqId} = business_cards:get_id(ReqBC),
			case (Group==ReqId) of
				true->
					F = fun(Val)->Val-1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of incoming sheep", F),
					ok;
				_->
					{error,"You are not allowed to inform me about this"}
			end;
		_->
			{error,"Unrecognized inform"}
	end.
%%SLOC:97

