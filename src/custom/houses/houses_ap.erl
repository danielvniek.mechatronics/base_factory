%% @author Daniel
%% @doc @todo Add description to houses_standard_ap_mod.


-module(houses_ap).

%% ====================================================================
%% API functions
%% ====================================================================
-export([get_act_types/0,analyse_old_acts/4,analyse_act/3]).

-record(schedule_info,{tsched::integer(),s1data::term()}).
-record(execution_info,{tstart::integer(),s2data::term()}).

-record(stage3Activity,
          {id::term(),type::term(),schedule_info::term(),execution_info::term(),biography_info::term()}).

%% ====================================================================
%% Internal functions
%% ====================================================================
get_act_types()->
	["Remove sheep","Add sheep", "Remove group","Receive sheep (SPA)","Receive group (SPA)","Add feeder","Remove feeder"].
analyse_old_acts(_ActType,_Acts,_ReceptionMap,Variables)->%assume already analysed
	Variables.
analyse_act(Act,ReceptionMap,Variables)->
	AtrRecep = maps:get(atr,ReceptionMap),
	S1Data = Act#stage3Activity.schedule_info#schedule_info.s1data,
	case Act#stage3Activity.type of
		"Add sheep"->%%from group
			F0 = fun(Val)->Val-1 end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of incoming sheep", F0),		
			SheepId = maps:get(<<"Sheep id">>,S1Data),
			F = fun(Val)->[SheepId|Val] end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Sheep", F),
			F2 = fun(Val)->Val+1 end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of sheep", F2);
		"Remove sheep"->%%from group or sheep
			SheepId = maps:get(<<"Sheep id">>,S1Data),
			F = fun(Val)->lists:delete(SheepId,Val) end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Sheep", F),
			F2 = fun(Val)->Val-1 end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of sheep", F2);
		"Remove group"->
			{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Group", "none"),
			{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Number of sheep", 0),
			{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Number of incoming sheep", 0),
			{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Sheep", []);
		"Receive sheep (SPA)"->
			S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
			Result = maps:get(<<"Result">>,S2Data),
			F0 = fun(Val)->Val-1 end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of incoming sheep", F0),		
			case (Result=="Success") or (Result ==<<"Success">>) of
				true->
					Contract = maps:get(<<"Contract">>,S1Data),
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,SheepId} = business_cards:get_id(ClientBC),
					F = fun(Val)->[SheepId|Val] end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Sheep", F),
					F2 = fun(Val)->Val+1 end,
					ok = atr_api:apply_function_to_atr_val(AtrRecep, "Number of sheep", F2);
				_->
					ok
			end;
		"Receive group (SPA)"->
			S2Data = Act#stage3Activity.execution_info#execution_info.s2data,
			Result = maps:get(<<"Result">>,S2Data),
			F = fun(_Val)->"none" end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Incoming group", F),
			case (Result=="Success") or (Result ==<<"Success">>) of
				true->	
					%%update group
					S1Data = Act#stage3Activity.schedule_info#schedule_info.s1data,
					Contract = maps:get(<<"Contract">>,S1Data),
					{ok,ClientBC} = contracts:get_client_bc(Contract),
					{ok,GroupId} = business_cards:get_id(ClientBC),
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Group", GroupId),
					%%update incoming group
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Incoming group", "none"),
					%%update sheep and nr sheep
					{ok,GroupAdr} = business_cards:get_address(ClientBC),
					{ok,GroupAtr}=comms_api:send_request_info(maps:get(comms,ReceptionMap), GroupAdr, "Attributes", ["Sheep"]),
					
					Sheep = maps:get("Sheep",GroupAtr),
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Sheep",Sheep),
					{ok,"success"} = atr_api:update_attribute_value(AtrRecep, "Number of sheep",length(Sheep));
				_->
					ok
			end;
		"Add feeder"->
			FeederId = maps:get(<<"Feeder id">>,S1Data),
			F = fun(Val)->[FeederId|Val] end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Feeders", F),
			{ok,Sheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
			{ok,Group} = atr_api:get_attribute_value(AtrRecep, "Group"),
			{ok,Feeders} = atr_api:get_attribute_value(AtrRecep, "Feeders"),
			inform_all_sheep_and_group_of_updated_feeders(Sheep,Group,maps:get(comms,ReceptionMap),Feeders);
		"Remove feeder"->
			FeederId = maps:get(<<"Feeder id">>,S1Data),
			F = fun(Val)->lists:delete(FeederId,Val) end,
			ok = atr_api:apply_function_to_atr_val(AtrRecep, "Feeders", F),
			{ok,Sheep} = atr_api:get_attribute_value(AtrRecep, "Sheep"),
			{ok,Group} = atr_api:get_attribute_value(AtrRecep, "Group"),
			{ok,Feeders} = atr_api:get_attribute_value(AtrRecep, "Feeders"),
			inform_all_sheep_and_group_of_updated_feeders(Sheep,Group,maps:get(comms,ReceptionMap),Feeders)
	end,
	Variables.

inform_all_sheep_and_group_of_updated_feeders("none",Group,Comms,Feeders)->
	case Group of
		"none"->
			ok;
		_->
			GroupAdr=erlang:list_to_atom(string:lowercase(string:replace(Group, " ", "_",all))),
			comms_api:send_inform(Comms, GroupAdr, "New housing details", #{"Feeders"=>Feeders})
	end;
inform_all_sheep_and_group_of_updated_feeders([],Group,Comms,Feeders)->
	case Group of
		"none"->
			ok;
		_->
			GroupAdr=erlang:list_to_atom(string:lowercase(string:replace(Group, " ", "_",all))),
			comms_api:send_inform(Comms, GroupAdr, "New housing details", #{"Feeders"=>Feeders})
	end;
inform_all_sheep_and_group_of_updated_feeders([Sheep|T],Group,Comms,Feeders)->
	SheepAdr = erlang:list_to_atom(string:lowercase(string:replace(Sheep, " ", "_",all))),
	comms_api:send_inform(Comms, SheepAdr, "New housing details", #{"Feeders"=>Feeders}),
	inform_all_sheep_and_group_of_updated_feeders(T,Group,Comms,Feeders).

%%SLOC:108
