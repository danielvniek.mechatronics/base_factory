# Description #
The BASE factory represents an an implementation of the extended BASE architecture (thesis in doc/vanNiekerk_Thesis_19879822). This implementation is done in Erlang, because of Erlang's great concurrency, and error handling capabilities. The BASE factory can be used to create the Cyber part of a Cyber-Physical System (CPS). In order to use it, an in-depth knowledge of CPSs, holonic systems, and service-oriented architectures is required. The benefits of CPSs is discussed in doc/vanNiekerk_Thesis_19879822.

# Requirements #
Erlang 22.3 or higher

# How to use #
Pull the sheep_base_factory branch (contains the most up to date code) onto your computer. The master branch contains older code currently being used by other students and merging the updates from sheep_base_factory might cause problems for them.
### 1. Setup ###
Replace Erlang shortcut in the ebin folder with an Erlang shortcut of the Erlang version on your computer. After this, click on the new shortcut's properties and clear the "start in" property as shown in the figure below.\
<img src="/doc/RM_1.JPG" height="400">

After this ensure that the ebin/storage folder has no contents in it

### 2. Start Back-end###
Double click on the shortcut and type _bf:start()._\
You should see the following:\
<img src="/doc/RM_2.JPG" height="200">

### 3. User Interface ###
In order to add and remove holons, view data about these holons and reconfigure them the UI need a view small reconfigurations to fit your specific CPS.
##### 3.1 Source code reconfigurations #####
The UI's source code needs to be edited according to your specific CPS. There are two locations in the _MUI Frontend/index.html_ file that needs to be edited. It is important that you delete any include statements about files that you deleted.
The first location is shown in the figure below. These lines of code are used to include the custom front-end files required by your CPS. \
<img src="/doc/RM_3.jpg" height="400">\
The second location is shown in the figure below. This includes your CPS's custom non-Interface holons. You can copy the existing code and merely change the holon types, for example if your CPS has a bunch of "Drill robots" then the following line of code is required:
```
<a onclick = "change_page('existing_resources.html','Drill robots')">Drill robots</a>
```
<img src="/doc/RM_4.jpg" height="400">\
The code in these two locations together with the custom JavaScript, HTML or CSS files you develop will continuously change as the development of your CPS continues.\
\
After these changes have been made you can go to localhost:9000 in your browser. Login with "123" as both the username and password. If the dropdown menu under _Custom Holons_ does not show your updates, then you must clear your browsers' cached data, which will ensure the newest code is used. 
##### 3.2 Edit users of the system #####
To edit which users are allowed to use the system go to _Users_. On this page you users can change their password, and users with a permission level of 1 can view, add and remove other users as shown in the figure below. \
<img src="/doc/RM_5.JPG" height="400">\

### 4. New non-Interface holon type ###
There are 4 steps to adding a new non-Interface holon type: 
- Edit the holon's attributes
- Edit the holon's custom modules
- Add front-end code required to edit attributes dynamically
- Add front-end code required to schedule new activities\
The following video shows how new non-Interface holon types can be added to your BASE factory:\
https://youtu.be/NkhyQ2hoZTI
### 5. New Interface Holon ###
There are 2 steps to adding a new Interface Holons: 
- Create an interface module
- Add holon via UI\
The following video explains how new Interface Holons can be added to your BASE factory:\
https://youtu.be/mSvnIKWO4JQ\

### 6. View holon data ###
The UI allows you to do the following in each holon:
- View and edit attributes
- View and edit schedule
- View execution
- View biography
- See numeric holon data on a plot (shown in figure below)\
<img src="/doc/RM_6.JPG" height="400">\
